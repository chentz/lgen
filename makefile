#Build Paths
BASEB=build
BGE=$(BASEB)/GenEngine
BTrans=$(BASEB)/Trans

#Source
SGE=GenerationEngine
STrans=Translator

build: clean-struct bStruct mkTrans mkKernel TExamples GExamples buildInfo
	echo "The Generation Engine is generated!"
	@echo
	cat $(BASEB)/buildInfo.txt

bStruct:
	mkdir -p $(BGE)/bin
	mkdir -p $(BTrans)/bin

buildInfo:
	echo "Build Date: `date`" > bInfo.txt 
	lua -v >> bInfo.txt
	echo "Git version:" >> bInfo.txt
	git log --oneline --since=1.day | head -1 >> bInfo.txt 
	cp bInfo.txt $(BASEB)/buildInfo.txt
	cp changelog.txt $(BASEB)/
	
list:
	@grep -o -P '^[a-zA-Z]+' makefile | sort -u
	
clean: clean-output clean-bin clean-aux clean-testlog clean-struct
	
clean-struct: 
	rm -rf $(BASEB)
	
clean-output:
	find . -name '*.out' -type f -delete
	find . -name '*.prf' -type f -delete
	find . -name '*.time' -type f -delete
	find . -name \.AppleDouble -exec rm -rf {} \;

clean-bin:
	rm -rf $(BGE)/bin/*
	mkdir -p $(BGE)/bin
	
clean-aux:
	find . -name '*.marks' -type f -delete
	
clean-testlog:
	rm -rf tests/*.log

#GenEng
mkKernel: covCrit auxLib
	#cp profileLua.sh bin/profileLua.sh
	sed 's/RunGrammar.lua/RunGrammar.luac/g;s/profile.lua/profile.luac/g' $(SGE)/profileLua.sh > $(BGE)/bin/profileLua.sh
	chmod a+x $(BGE)/bin/profileLua.sh
	cp $(SGE)/init.lua $(BGE)/bin/init.lua
	luac -s -o $(BGE)/bin/grammar_engine.luac $(SGE)/grammar_engine.lua
	luac -s -o $(BGE)/bin/grammar_engine_EBNF.luac $(SGE)/grammar_engine_EBNF.lua
	luac -s -o $(BGE)/bin/coverageCriteria.luac $(SGE)/coverageCriteria.lua
	luac -s -o $(BGE)/bin/RunGrammar.luac $(SGE)/RunGrammar.lua
	luac -s -o $(BGE)/bin/grammar_transformation.luac $(SGE)/grammar_transformation.lua
	luac -s -o $(BGE)/bin/MetaInfo.luac $(SGE)/MetaInfo.lua
	
covCrit:
#	luac -s -o bin/terminalCoverage.luac terminalCoverage.lua
	cp $(SGE)/terminalCoverage.lua $(BGE)/bin/terminalCoverage.lua
#	luac -s -o bin/productionCoverage.luac productionCoverage.lua
	cp $(SGE)/productionCoverage.lua $(BGE)/bin/productionCoverage.lua

GExamples:
	cp -R $(SGE)/Examples $(BGE)/

auxLib:
	mkdir $(BGE)/bin/Aux $(BGE)/bin/modules
	luac -s -o $(BGE)/bin/Aux/statsGrammar.luac $(SGE)/Aux/statsGrammar.lua
	
	luac -s -o $(BGE)/bin/modules/Set.luac LuaTools/Set.lua
	luac -s -o $(BGE)/bin/modules/Relation.luac LuaTools/Relation.lua
	luac -s -o $(BGE)/bin/modules/List.luac LuaTools/List.lua
	luac -s -o $(BGE)/bin/modules/Stack.luac LuaTools/Stack.lua
	luac -s -o $(BGE)/bin/modules/BenchTimer.luac LuaTools/BenchTimer.lua	
	luac -s -o $(BGE)/bin/modules/dbg.luac LuaTools/dbg.lua
	luac -s -o $(BGE)/bin/modules/Array.luac LuaTools/Array.lua
	luac -s -o $(BGE)/bin/modules/Queue.luac LuaTools/Queue.lua
	luac -s -o $(BGE)/bin/modules/sha2.luac LuaTools/sha2.lua
	
# These Libs used to coverage criteria
	luac -s -o $(BGE)/bin/modules/TimeMachine.luac LuaTools/TimeMachine.lua
	luac -s -o $(BGE)/bin/modules/Util.luac LuaTools/Util.lua
	
# This library is used to show coverage percent.
	luac -s -o $(BGE)/bin/profile.luac $(SGE)/profile.lua
	
#Translator
mkTrans:
	sed 's/parser.lua/parser.luac/g' $(STrans)/parseTrans.sh > $(BTrans)/bin/parseTrans.sh
	chmod a+x $(BTrans)/bin/parseTrans.sh
	
	cp $(STrans)/init.lua $(BTrans)/bin/init.lua
	luac -s -o $(BTrans)/bin/EBNF_G.luac $(STrans)/EBNF_G.lua
	luac -s -o $(BTrans)/bin/ANTLR_G.luac $(STrans)/ANTLR_G.lua
	luac -s -o $(BTrans)/bin/parser.luac $(STrans)/parser.lua
	luac -s -o $(BTrans)/bin/pegdebug.luac $(STrans)/pegdebug.lua
	luac -s -o $(BTrans)/bin/LGenSpecTrans.luac $(STrans)/LGenSpecTrans.lua
	
	#Modules
	cd $(BTrans)/bin; ln -s ../../GenEngine/bin/modules

TExamples:
	mkdir -p $(BTrans)/Examples
	cp -R $(STrans)/Examples/*.ebnf $(BTrans)/Examples/
	cp -R $(STrans)/Examples/*.g $(BTrans)/Examples/
	
dist: build
	cd $(BASEB); tar cf - . | pbzip2 > ../LGen.$(shell date +%Y%m%d_%H%M).tar.bz	
	openssl md5 LGen.$(shell date +%Y%m%d_%H%M).tar.bz

latestTStatus:
	grep --color -P '[0-9]+ tests' tests/test*.log
	
test: clean tGenEng tGenEngEBNF tGrammars tIssues
	ps aux | grep '[0-9] lua'
	@echo "Test set is Done!"
	
tGrammars:
	tsc -f tests/testGrammars.lua &> tests/testGrammars.log &

tGenEng:
	tsc -f tests/testGrammar_engine.lua &> tests/testGrammar_engine.log &
	
tGenEngEBNF:
	tsc -f tests/testGrammar_engine_EBNF.lua &> tests/testGrammar_engine_EBNF.log &

tIssues:
	tsc -f tests/testIssues.lua &> tests/testIssues.log &