require("init")
local SHA = require("sha2")

if arg[1] then
  local file = assert(io.open(arg[1], 'rb'))
  local x = SHA.new256()
  for b in file:lines(2^12) do
    x:add(b)
  end
  file:close()
  print(x:close())
end