G.startSymbol = "Component"

G.Component = alt(V.Machine_abstract , V.Refinement, V.Implementation)
G.Machine_abstract = seq(terminal("MACHINE"), seq(V.Header, seq(V.Machine_abstractVRZ, terminal("END"))))
G.Machine_abstractVRZ = alt(empty(), seq(V.Clause_machine_abstract, V.Machine_abstractVRZ))
G.Clause_machine_abstract = alt(V.Clause_constraints , V.Clause_sees, V.Clause_includes, V.Clause_promotes, V.Clause_EXTENDS, V.Clause_uses, V.Clause_sets, V.Clause_concrete_constants, V.Clause_abstract_constants, V.Clause_properties, V.Clause_concrete_variables, V.Clause_abstract_variables, V.Clause_invariant, V.Clause_assertions, V.Clause_initialization, V.Clause_operations)
G.Header = seq(V.Ident, optional(seq(terminal("("), seq(oneOrMoreSep(V.Ident, ","), terminal(")")))))
G.Refinement = seq(terminal("REFINEMENT"), seq(V.Header, seq(V.Clause_refines, seq(V.RefinementVRZ, terminal("END")))))
G.RefinementVRZ = alt(empty(), seq(V.Clause_refinement, V.RefinementVRZ))
G.Clause_refinement = alt(V.Clause_sees , V.Clause_includes, V.Clause_promotes, V.Clause_EXTENDS, V.Clause_sets, V.Clause_concrete_constants, V.Clause_abstract_constants, V.Clause_properties, V.Clause_concrete_variables, V.Clause_abstract_variables, V.Clause_invariant, V.Clause_assertions, V.Clause_initialization, V.Clause_operations)
G.Implementation = seq(terminal("IMPLEMENTATION"), seq(V.Header, seq(V.Clause_refines, seq(V.ImplementationVRZ, terminal("END")))))
G.ImplementationVRZ = alt(empty(), seq(V.Clause_implementation, V.ImplementationVRZ))
G.Clause_implementation = alt(V.Clause_sees , V.Clause_IMPORTS, V.Clause_promotes, V.Clause_EXTENDS_B0, V.Clause_sets, V.Clause_concrete_constants, V.Clause_properties, V.Clause_values, V.Clause_concrete_variables, V.Clause_invariant, V.Clause_assertions, V.Clause_initialization_B0, V.Clause_operations_B0)
G.Clause_constraints = seq(terminal("CONSTRAINTS"), oneOrMoreSep(V.Predicate, "&"))
G.Clause_refines = seq(terminal("REFINES"), V.Ident)
G.Clause_IMPORTS = seq(terminal("IMPORTS"), oneOrMoreSep(seq(optional(oneOrMoreSep(V.Ident, ".")), seq(V.Ident, optional(seq(terminal("("), seq(oneOrMoreSep(V.Instanciation_B0, ","), terminal(")")))))), ","))
G.Instanciation_B0 = alt(V.Term , V.Integer_set_B0, terminal("BOOL"), V.Interval)
G.Clause_sees = seq(terminal("SEES"), oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","))
G.Clause_includes = seq(terminal("INCLUDES"), oneOrMoreSep(seq(optional(seq(V.Ident, terminal("."))), seq(V.Ident, optional(seq(terminal("("), seq(oneOrMoreSep(V.Instanciation, ","), terminal(")")))))), ","))
G.Instanciation = alt(V.Term , V.Integer_set, terminal("BOOL"), V.Interval)
G.Clause_promotes = seq(terminal("PROMOTES"), oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","))
G.Clause_EXTENDS = seq(terminal("EXTENDS"), oneOrMoreSep(seq(optional(seq(V.Ident, terminal("."))), seq(V.Ident, optional(seq(terminal("("), seq(oneOrMoreSep(V.Instanciation, ","), terminal(")")))))), ","))
G.Clause_EXTENDS_B0 = seq(terminal("EXTENDS"), oneOrMoreSep(seq(optional(seq(V.Ident, terminal("."))), seq(V.Ident, optional(seq(terminal("("), seq(oneOrMoreSep(V.Instanciation_B0, ","), terminal(")")))))), ","))
G.Clause_uses = seq(terminal("USES"), oneOrMoreSep(seq(optional(seq(V.Ident, terminal("."))), V.Ident), ","))
G.Clause_sets = seq(terminal("SETS"), oneOrMoreSep(V.Set, ";"))
G.Set = alt(V.Ident, seq(V.Ident, seq(terminal("="), seq(terminal("{"), seq(oneOrMoreSep(V.Ident, ","), terminal("}"))))))
G.Clause_concrete_constants = alt(seq(terminal("CONCRETE_CONSTANTS"), oneOrMoreSep(V.Ident, ",")), seq(terminal("CONSTANTS"), oneOrMoreSep(V.Ident, ",")))
G.Clause_abstract_constants = seq(terminal("ABSTRACT_CONSTANTS"), oneOrMoreSep(V.Ident, ","))
G.Clause_properties = seq(terminal("PROPERTIES"), oneOrMoreSep(V.Predicate, "&"))
G.Clause_values = seq(terminal("VALUES"), oneOrMoreSep(V.Valuation, ";"))
G.Valuation = alt(seq(V.Ident, seq(terminal("="), V.Term)) , seq(V.Ident, seq(terminal("="), seq(terminal("Bool"), seq(terminal("("), seq(V.Condition, terminal(")")))))), seq(V.Ident, seq(terminal("="), V.Expr_array)), seq(V.Ident, seq(terminal("="), V.Interval_B0)))
G.Clause_concrete_variables = seq(terminal("CONCRETE_VARIABLES"), oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","))
G.Clause_abstract_variables = alt(seq(terminal("ABSTRACT_VARIABLES"), oneOrMoreSep(V.Ident, ",")), seq(terminal("VARIABLES"), oneOrMoreSep(V.Ident, ",")))
G.Clause_invariant = seq(terminal("INVARIANT"), oneOrMoreSep(V.Predicate, "&"))
G.Clause_assertions = seq(terminal("ASSERTIONS"), oneOrMoreSep(V.Predicate, ";"))
G.Clause_initialization = seq(terminal("INITIALISATION"), V.Substitution)
G.Clause_initialization_B0 = seq(terminal("INITIALISATION"), V.Instruction)
G.Clause_operations = seq(terminal("OPERATIONS"), oneOrMoreSep(V.Operation, ";"))
G.Operation = seq(V.Header_operation, seq(terminal("="), V.Level1_substitution))
G.Header_operation = seq(optional(seq(oneOrMoreSep(V.Ident, ","), terminal("<--"))), seq(oneOrMoreSep(V.Ident, "."), optional(seq(terminal("("), seq(oneOrMoreSep(V.Ident, ","), terminal(")"))))))
G.Clause_operations_B0 = seq(terminal("OPERATIONS"), oneOrMoreSep(V.Operation_B0, ";"))
G.Operation_B0 = seq(V.Header_operation, seq(terminal("="), V.Level1_substitution))
G.Term = alt(V.Simple_term , seq(oneOrMoreSep(V.Ident, "."), seq(terminal("("), seq(oneOrMoreSep(V.Term, ","), terminal(")")))), V.Arithmetical_expression)
G.Simple_term = alt(oneOrMoreSep(V.Ident, ".") , V.Integer_lit, V.Boolean_lit)
G.Integer_lit = alt(V.Integer_literal , terminal("MAXINT"), terminal("MININT"))
G.Boolean_lit = alt(terminal("FALSE"), terminal("TRUE"))
G.Arithmetical_expression = alt(V.Integer_lit , oneOrMoreSep(V.Ident, "."), seq(V.Arithmetical_expression, seq(terminal("+"), V.Arithmetical_expression)), seq(V.Arithmetical_expression, seq(terminal("-"), V.Arithmetical_expression)), seq(terminal("-"), V.Arithmetical_expression), seq(V.Arithmetical_expression, seq(terminal("*"), V.Arithmetical_expression)), seq(V.Arithmetical_expression, seq(terminal("/"), V.Arithmetical_expression)), seq(V.Arithmetical_expression, seq(terminal("mod"), V.Arithmetical_expression)), seq(V.Arithmetical_expression, seq(terminal("**"), V.Expressions_arithmetical)), seq(terminal("succ"), seq(terminal("("), seq(V.Arithmetical_expression, terminal(")")))), seq(terminal("pred"), seq(terminal("("), seq(V.Arithmetical_expression, terminal(")")))))
G.Expr_array = alt(V.Ident , seq(terminal("{"), seq(oneOrMoreSep(seq(oneOrMoreSep(V.Simple_term, "|->"), seq(terminal("|->"), V.Term)), ","), terminal("}"))), oneOrMoreSep(seq(oneOrMoreSep(V.Range2, "*"), seq(terminal("*"), seq(terminal("{"), seq(V.Term, terminal("}"))))), "or"))
G.Range2 = alt(V.Ident , V.Interval_B0, seq(terminal("{"), seq(oneOrMoreSep(V.Simple_term, ","), terminal("}"))))
G.Interval_B0 = alt(seq(V.Arithmetical_expression, seq(terminal(".."), V.Arithmetical_expression)), V.Integer_set_B0)
G.Integer_set_B0 = alt(terminal("NAT") , terminal("NAT1"), terminal("INT"))
G.Condition = alt(seq(V.Simple_term, seq(terminal("="), V.Simple_term)) , seq(V.Simple_term, seq(terminal("/="), V.Simple_term)), seq(V.Simple_term, seq(terminal("<"), V.Simple_term)), seq(V.Simple_term, seq(terminal(">"), V.Simple_term)), seq(V.Simple_term, seq(terminal("<="), V.Simple_term)), seq(V.Simple_term, seq(terminal(">="), V.Simple_term)), seq(V.Condition, seq(terminal("&"), V.Condition)), seq(V.Condition, seq(terminal("or"), V.Condition)), seq(terminal("not"), seq(terminal("("), seq(V.Condition, terminal(")")))))
G.Instruction = alt(V.Level1_instruction, V.Sequence_instruction)
G.Level1_instruction = alt(V.Block_instruction , V.Var_instruction, V.Identity_substitution, V.Become_equals_instruction, V.Callup_instruction, V.If_instruction, V.Case_instruction, V.Assert_instruction, V.While_substitution)
G.Block_instruction = seq(terminal("BEGIN"), seq(V.Instruction, terminal("END")))
G.Var_instruction = seq(terminal("VAR"), seq(oneOrMoreSep(V.Ident, ","), seq(terminal("IN"), seq(V.Instruction, terminal("END")))))
G.Become_equals_instruction = alt(seq(oneOrMoreSep(V.Ident, "."), seq(optional(seq(terminal("("), seq(oneOrMoreSep(V.Term, ","), terminal(")")))), seq(terminal(":="), V.Term))) , seq(oneOrMoreSep(V.Ident, "."), seq(terminal(":="), V.Expr_array)), seq(oneOrMoreSep(V.Ident, "."), seq(terminal(":="), seq(terminal("bool"), seq(terminal("("), seq(V.Condition, terminal(")")))))))
G.Callup_instruction = seq(optional(seq(oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","), terminal("<-"))), seq(oneOrMoreSep(V.Ident, "."), optional(seq(terminal("("), seq(oneOrMoreSep(alt(V.Term, V.String_lit), ","), terminal(")"))))))
G.Sequence_instruction = seq(V.Instruction, seq(terminal(";"), V.Instruction))
G.If_instruction = seq(terminal("IF"), seq(V.Condition, seq(terminal("THEN"), seq(V.Instruction, seq(V.If_instructionVRZ, seq(optional(seq(terminal("ELSE"), V.Instruction)), terminal("END")))))))
G.If_instructionVRZ = alt(empty(), seq(seq(terminal("ELSIF"), seq(V.Condition, seq(terminal("THEN"), V.Instruction))), V.If_instructionVRZ))
G.Case_instruction = seq(terminal("CASE"), seq(V.Simple_term, seq(terminal("OF"), seq(terminal("EITHER"), seq(oneOrMoreSep(V.Simple_term, ","), seq(terminal("THEN"), seq(V.Instruction, seq(V.Case_instructionVRZ, seq(optional(seq(terminal("ELSE"), V.Instruction)), seq(terminal("END"), terminal("END")))))))))))
G.Case_instructionVRZ = alt(empty(), seq(seq(terminal("OR"), seq(oneOrMoreSep(V.Simple_term, ","), seq(terminal("THEN"), V.Instruction))), V.Case_instructionVRZ))
G.Assert_instruction = seq(terminal("ASSERT"), seq(V.Predicate, seq(terminal("THEN"), seq(V.Instruction, terminal("END")))))
G.Predicate = alt(V.Bracketed_predicate , V.Conjunction_predicate, V.Negation_predicate, V.Disjunction_predicate, V.Implication_predicate, V.Equivalence_predicate, V.Universal_predicate, V.Existential_predicate, V.Equals_predicate, V.Unequals_predicate, V.Belongs_predicate, V.Non_belongs_predicate, V.Inclusion_predicate, V.Strict_inclusion_predicate, V.Non_inclusion_predicate, V.Non_strict_inclusion_predicate, V.Less_than_or_equal_predicate, V.Strictly_less_than_predicate, V.Greater_or_equal_predicate, V.Strictly_greater_predicate)
G.Bracketed_predicate = seq(terminal("("), seq(V.Predicate, terminal(")")))
G.Conjunction_predicate = seq(V.Predicate, seq(terminal("&"), V.Predicate))
G.Negation_predicate = seq(terminal("not"), seq(terminal("("), seq(V.Predicate, terminal(")"))))
G.Disjunction_predicate = seq(V.Predicate, seq(terminal("or"), V.Predicate))
G.Implication_predicate = seq(V.Predicate, seq(terminal("=>"), V.Predicate))
G.Equivalence_predicate = seq(V.Predicate, seq(terminal("<=>"), V.Predicate))
G.Universal_predicate = seq(terminal("!"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, seq(terminal("=>"), seq(V.Predicate, terminal(")"))))))))
G.Existential_predicate = seq(terminal("#"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, terminal(")"))))))
G.Equals_predicate = seq(V.Expression, seq(terminal("="), V.Expression))
G.Unequals_predicate = seq(V.Expression, seq(terminal("/="), V.Expression))
G.Belongs_predicate = seq(V.Expression, seq(terminal(":"), V.Expression))
G.Non_belongs_predicate = seq(V.Expression, seq(terminal("/:"), V.Expression))
G.Inclusion_predicate = seq(V.Expression, seq(terminal("<:"), V.Expression))
G.Strict_inclusion_predicate = seq(V.Expression, seq(terminal("<<:"), V.Expression))
G.Non_inclusion_predicate = seq(V.Expression, seq(terminal("/<:"), V.Expression))
G.Non_strict_inclusion_predicate = seq(V.Expression, seq(terminal("/<<:"), V.Expression))
G.Less_than_or_equal_predicate = seq(V.Expression, seq(terminal("<="), V.Expression))
G.Strictly_less_than_predicate = seq(V.Expression, seq(terminal("<"), V.Expression))
G.Greater_or_equal_predicate = seq(V.Expression, seq(terminal(">="), V.Expression))
G.Strictly_greater_predicate = seq(V.Expression, seq(terminal(">"), V.Expression))
G.Expression = alt(V.Expressions_primary , V.Expressions_boolean, V.Expressions_arithmetical, V.Expressions_of_couple, V.Expressions_of_sets, V.Construction_of_sets, V.Expressions_of_relations, V.Expressions_of_functionss, V.Construction_of_functionss, V.Expressions_of_sequences, V.Construction_of_sequences)
G.Expressions_primary = alt(V.Data, V.Expr_bracketed)
G.Expressions_boolean = alt(V.Boolean_lit, V.Conversion_Bool)
G.Expressions_arithmetical = alt(V.Integer_lit , V.Addition, V.Difference, V.Unary_minus, V.Product, V.Division, V.Modulo, V.Power_of, V.Sucessor, V.Predecessor, V.Maximum, V.Minimum, V.Cardinal, V.Generalized_sum, V.Generalized_product)
G.Expressions_of_couple = V.Couple
G.Expressions_of_sets = alt(V.Empty_set , V.Integer_set, V.Boolean_set, V.String_set)
G.Construction_of_sets = alt(V.Product , V.Comprehension_set, V.Subsets, V.Finite_subsets, V.Set_extension, V.Interval, V.Difference, V.Union, V.Intersection, V.Generalized_union, V.Generalized_intersection, V.Quantified_union, V.Quantified_intersection)
G.Expressions_of_relations = alt(V.Relations , V.Identify, V.Reverse, V.First_projection, V.Second_projection, V.Composition, V.Direct_product, V.Parallel_product, V.Iteration, V.Reflexive_closure, V.Closure, V.Domain, V.Range, V.Image, V.Domain_restriction, V.Domain_subtraction, V.Range_restriction, V.Range_subtraction, V.Overwrite)
G.Expressions_of_functionss = alt(V.Partial_functions , V.Total_functions, V.Partial_injections, V.Total_injections, V.Partial_surjections, V.Total_surjections, V.Total_bijections)
G.Construction_of_functionss = alt(V.Lambada_expression , V.Evaluation_functions, V.Transformed_function, V.Transformed_relation)
G.Expressions_of_sequences = alt(V.Sequences , V.Non_empty_sequences, V.Injective_sequences, V.Non_empty_inj_sequences, V.Permutations)
G.Construction_of_sequences = alt(V.Empty_sequence , V.Sequence_extension, V.Sequence_size, V.Sequence_first_element, V.Sequence_last_element, V.Sequence_front, V.Sequence_tail, V.Reverse_sequence, V.Concatenation, V.Insert_front, V.Insert_tail, V.Restrict_front, V.Restrict_tail, V.Generalized_concat)
G.Conversion_Bool = seq(terminal("bool"), seq(terminal("("), seq(V.Predicate, terminal(")"))))
G.Addition = seq(V.Expression, seq(terminal("+"), V.Expression))
G.Difference = seq(V.Expression, seq(terminal("-"), V.Expression))
G.Unary_minus = seq(terminal("-"), V.Expression)
G.Product = seq(V.Expression, seq(terminal("*"), V.Expression))
G.Division = seq(V.Expression, seq(terminal("/"), V.Expression))
G.Modulo = seq(V.Expression, seq(terminal("mod"), V.Expression))
G.Power_of = seq(V.Expression, seq(terminal("**"), V.Expression))
G.Sucessor = seq(terminal("succ"), optional(seq(terminal("("), seq(V.Expression, terminal(")")))))
G.Predecessor = seq(terminal("pred"), optional(seq(terminal("("), seq(V.Expression, terminal(")")))))
G.Maximum = seq(terminal("max"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Minimum = seq(terminal("max"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Cardinal = seq(terminal("card"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Generalized_sum = seq(terminal("SIGMA"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, seq(terminal("|"), seq(V.Expression, terminal(")"))))))))
G.Generalized_product = seq(terminal("PI"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, seq(terminal("|"), seq(V.Expression, terminal(")"))))))))
G.Couple = alt(seq(V.Expression, seq(terminal("|->"), V.Expression)), seq(V.Expression, V.Expression))
G.Empty_set = terminal("{}")
G.Integer_set = alt(terminal("INTEGER") , terminal("NATURAL"), terminal("NATURAL1"), terminal("NAT"), terminal("NAT1"), terminal("INT"))
G.Boolean_set = terminal("BOOL")
G.String_set = terminal("STRING")
G.Comprehension_set = seq(terminal("{"), seq(oneOrMoreSep(V.Ident, ","), seq(terminal("|"), seq(V.Predicate, terminal("}")))))
G.Subsets = alt(seq(terminal("POW"), seq(terminal("("), seq(V.Expression, terminal(")")))), seq(terminal("POW1"), seq(terminal("("), seq(V.Expression, terminal(")")))))
G.Finite_subsets = alt(seq(terminal("FIN"), seq(terminal("("), seq(V.Expression, terminal(")")))), seq(terminal("FIN1"), seq(terminal("("), seq(V.Expression, terminal(")")))))
G.Set_extension = seq(terminal("{"), seq(oneOrMoreSep(V.Expression, ","), terminal("}")))
G.Interval = seq(V.Expression, seq(terminal(".."), V.Expression))
G.Union = seq(V.Expression, seq(terminal("\\/"), V.Expression))
G.Intersection = seq(V.Expression, seq(terminal("/\\"), V.Expression))
G.Generalized_union = seq(terminal("union"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Generalized_intersection = seq(terminal("inter"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Quantified_union = seq(terminal("\\/"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, seq(terminal("|"), seq(V.Expression, terminal(")"))))))))
G.Quantified_intersection = seq(terminal("/\\"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, seq(terminal("|"), seq(V.Expression, terminal(")"))))))))
G.Relations = seq(V.Expression, seq(terminal("<->"), V.Expression))
G.Identify = seq(terminal("id"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Reverse = seq(V.Expression, terminal("~"))
G.First_projection = seq(terminal("prj1"), seq(terminal("("), seq(V.Expression, seq(terminal(","), seq(V.Expression, terminal(")"))))))
G.Second_projection = seq(terminal("prj2"), seq(terminal("("), seq(V.Expression, seq(terminal(","), seq(V.Expression, terminal(")"))))))
G.Composition = seq(V.Expression, seq(terminal(";"), V.Expression))
G.Direct_product = seq(V.Expression, seq(terminal("><"), V.Expression))
G.Parallel_product = seq(V.Expression, seq(terminal("||"), V.Expression))
G.Iteration = seq(terminal("iterate"), seq(terminal("("), seq(V.Expression, seq(terminal(","), seq(V.Expression, terminal(")"))))))
G.Reflexive_closure = seq(terminal("closure"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Closure = seq(terminal("closure1"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Domain = seq(terminal("dom"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Range = seq(terminal("ran"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Image = seq(V.Expression, seq(terminal("["), seq(V.Expression, terminal("]"))))
G.Domain_restriction = seq(V.Expression, seq(terminal("<|"), V.Expression))
G.Domain_subtraction = seq(V.Expression, seq(terminal("<<|"), V.Expression))
G.Range_restriction = seq(V.Expression, seq(terminal("|>"), V.Expression))
G.Range_subtraction = seq(V.Expression, seq(terminal("|>>"), V.Expression))
G.Overwrite = seq(V.Expression, seq(terminal("<+"), V.Expression))
G.Partial_functions = seq(V.Expression, seq(terminal("+->"), V.Expression))
G.Total_functions = seq(V.Expression, seq(terminal("-->"), V.Expression))
G.Partial_injections = seq(V.Expression, seq(terminal(">+>"), V.Expression))
G.Total_injections = seq(V.Expression, seq(terminal(">->"), V.Expression))
G.Partial_surjections = seq(V.Expression, seq(terminal("+->>"), V.Expression))
G.Total_surjections = seq(V.Expression, seq(terminal("-->>"), V.Expression))
G.Total_bijections = seq(V.Expression, seq(terminal(">->>"), V.Expression))
G.Lambada_expression = seq(terminal("%"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, seq(terminal("|"), seq(V.Expression, terminal(")"))))))))
G.Evaluation_functions = seq(V.Expression, seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Transformed_function = seq(terminal("fnc"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Transformed_relation = seq(terminal("rel"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Sequences = seq(terminal("seq"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Non_empty_sequences = seq(terminal("seq1"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Injective_sequences = seq(terminal("iseq"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Non_empty_inj_sequences = seq(terminal("iseq1"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Permutations = seq(terminal("perm"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Empty_sequence = seq(terminal("["), terminal("]"))
G.Sequence_extension = seq(terminal("["), seq(oneOrMoreSep(V.Expression, ","), terminal("]")))
G.Sequence_size = seq(terminal("size"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Sequence_first_element = seq(terminal("first"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Sequence_last_element = seq(terminal("last"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Sequence_front = seq(terminal("front"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Sequence_tail = seq(terminal("tail"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Reverse_sequence = seq(terminal("rev"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Concatenation = seq(V.Expression, seq(terminal("^"), V.Expression))
G.Insert_front = seq(V.Expression, seq(terminal("->"), V.Expression))
G.Insert_tail = seq(V.Expression, seq(terminal("<-"), V.Expression))
G.Restrict_front = seq(V.Expression, seq(terminal("/|\\"), V.Expression))
G.Restrict_tail = seq(V.Expression, seq(terminal("\\|/"), V.Expression))
G.Generalized_concat = seq(terminal("conc"), seq(terminal("("), seq(V.Expression, terminal(")"))))
G.Substitution = alt(V.Level1_substitution , V.Simultaneous_substitution, V.Sequence_substitution)
G.Level1_substitution = alt(V.Block_substitution , V.Identity_substitution, V.Becomes_equal_substitution, V.Precondition_substitution, V.Assertion_substitution, V.Choice_limited_substitution, V.If_substitution, V.Select_substitution, V.Case_substitution, V.Any_substitution, V.Let_substitution, V.Becomes_elt_substitution, V.Become_such_that_substitution, V.Var_substitution, V.Call_up_substitution, V.While_substitution)
G.Block_substitution = seq(terminal("BEGIN"), seq(V.Substitution, terminal("END")))
G.Identity_substitution = terminal("skip")
G.Becomes_equal_substitution = alt(seq(oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","), seq(terminal(":="), oneOrMoreSep(V.Expression, ","))), seq(oneOrMoreSep(V.Ident, "."), seq(terminal("("), seq(oneOrMoreSep(V.Expression, ","), seq(terminal(")"), seq(terminal(":="), V.Expression))))))
G.Precondition_substitution = seq(terminal("PRE"), seq(V.Predicate, seq(terminal("THEN"), seq(V.Substitution, terminal("END")))))
G.Assertion_substitution = seq(terminal("ASSERT"), seq(V.Predicate, seq(terminal("THEN"), seq(V.Substitution, terminal("END")))))
G.Choice_limited_substitution = seq(terminal("CHOICE"), seq(V.Substitution, seq(V.Choice_limited_substitutionVRZ, terminal("END"))))
G.Choice_limited_substitutionVRZ = alt(empty(), seq(seq(terminal("OR"), V.Substitution), V.Choice_limited_substitutionVRZ))
G.If_substitution = seq(terminal("IF"), seq(V.Predicate, seq(terminal("THEN"), seq(V.Substitution, seq(V.If_substitutionVRZ, seq(optional(seq(terminal("ELSE"), V.Substitution)), terminal("END")))))))
G.If_substitutionVRZ = alt(empty(), seq(seq(terminal("ELSIF"), seq(V.Predicate, seq(terminal("THEN"), V.Substitution))), V.If_substitutionVRZ))
G.Select_substitution = seq(terminal("SELECT"), seq(V.Predicate, seq(terminal("THEN"), seq(V.Substitution, seq(V.Select_substitutionVRZ, seq(optional(seq(terminal("ELSE"), V.Substitution)), terminal("END")))))))
G.Select_substitutionVRZ = alt(empty(), seq(seq(terminal("WHEN"), seq(V.Predicate, seq(terminal("THEN"), V.Substitution))), V.Select_substitutionVRZ))
G.Case_substitution = seq(terminal("CASE"), seq(V.Expression, seq(terminal("OF"), seq(terminal("EITHER"), seq(oneOrMoreSep(V.Simple_term, ","), seq(terminal("THEN"), seq(V.Substitution, seq(V.Case_substitutionVRZ, seq(optional(seq(terminal("ELSE"), V.Substitution)), seq(terminal("END"), terminal("END")))))))))))
G.Case_substitutionVRZ = alt(empty(), seq(seq(terminal("OR"), seq(oneOrMoreSep(V.Simple_term, ","), seq(terminal("THEN"), V.Substitution))), V.Case_substitutionVRZ))
G.Any_substitution = seq(terminal("ANY"), seq(oneOrMoreSep(V.Ident, ","), seq(terminal("WHERE"), seq(V.Predicate, seq(terminal("THEN"), seq(V.Substitution, terminal("END")))))))
G.Let_substitution = seq(terminal("LET"), seq(oneOrMoreSep(V.Ident, ","), seq(terminal("BE"), seq(oneOrMoreSep(seq(V.Ident, seq(terminal("="), V.Expression)), "&"), seq(terminal("IN"), seq(V.Substitution, terminal("END")))))))
G.Becomes_elt_substitution = seq(oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","), seq(terminal("::"), V.Expression))
G.Become_such_that_substitution = seq(oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","), seq(terminal(":"), seq(terminal("("), seq(V.Predicate, terminal(")")))))
G.Var_substitution = seq(terminal("VAR"), seq(oneOrMoreSep(V.Ident, ","), seq(terminal("IN"), seq(V.Substitution, terminal("END")))))
G.Call_up_substitution = seq(optional(seq(oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","), terminal("<-"))), seq(oneOrMoreSep(V.Ident, "."), optional(seq(terminal("("), seq(oneOrMoreSep(V.Expression, ","), terminal(")"))))))
G.While_substitution = seq(terminal("WHILE"), seq(V.Condition, seq(terminal("DO"), seq(V.Instruction, seq(terminal("INVARIANT"), seq(V.Predicate, seq(terminal("VARIANT"), seq(V.Expression, terminal("END")))))))))
G.Simultaneous_substitution = seq(V.Substitution, seq(terminal("||"), V.Substitution))
G.Sequence_substitution = seq(V.Substitution, seq(terminal(";"), V.Substitution))
G.List_ident = alt(V.Ident, seq(terminal("("), seq(oneOrMoreSep(V.Ident, ","), terminal(")"))))
G.Data = alt(oneOrMoreSep(V.Ident, "."), seq(oneOrMoreSep(V.Ident, "."), terminal("$0")))
G.Expr_bracketed = seq(terminal("("), seq(V.Expression, terminal(")")))
G.String_lit = V.Character_string
G.Ident = terminal("ID")
G.Integer_literal = terminal("INTLit")
G.Character_string = terminal("CString")