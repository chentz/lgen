G.startSymbol = "probes"

G.probes = seq(V.titleprobe,seq(V.rightsprobe,seq(V.linkprobe,V.linkprobe)))
G.titleprobe = seq(V.type,seq(V.html,V.html))
G.rightsprobe = seq(V.type,seq(V.html,V.html))
G.linkprobe = alt(seq(V.type,seq(V.html,V.html)),seq(V.type,seq(V.html,V.html)))
G.type = terminal('List(">","type="html">")')
G.lt = terminal('&lt')
G.gt = terminal('&gt')
G.html = alt(seq(V.lt,seq(terminal('script'),seq(V.attr,seq(V.gt,seq(V.lt,seq(terminal('/script'),V.gt)))))),seq(V.lt,seq(terminal('applet'),seq(V.attr,seq(V.gt,seq(V.lt,seq(terminal('/applet'),V.gt)))))),seq(V.lt,seq(terminal('embed'),seq(V.attr,seq(V.gt,seq(V.lt,seq(terminal('/embed'),V.gt)))))),seq(V.lt,seq(terminal('object'),seq(V.attr,seq(V.gt,seq(V.lt,seq(terminal('/object'),V.gt)))))),seq(V.lt,seq(terminal('meta'),seq(V.attr,seq(V.gt,seq(V.lt,seq(terminal('/meta'),V.gt)))))),seq(V.lt,seq(terminal('a'),seq(V.attr,seq(V.gt,seq(V.lt,seq(terminal('/a'),V.gt)))))))
G.attr = alt(terminal('""'),seq(terminal('style'),V.valueempty),seq(terminal('abbr'),V.valueempty))
G.valueempty = seq(terminal('='),terminal('""'))

