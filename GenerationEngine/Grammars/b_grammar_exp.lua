G.startSymbol = "Component"

G.Component = V.Machine_abstract
G.Machine_abstract = seq(terminal("MACHINE"), seq(V.Header, seq(V.Machine_abstractVRZ, terminal("END"))))
G.Machine_abstractVRZ = alt(empty(), seq(V.Clause_machine_abstract, V.Machine_abstractVRZ))
G.Header = seq(V.Ident, optional(seq(terminal("("), seq(oneOrMoreSep(V.Ident, ","), terminal(")")))))
G.Clause_machine_abstract = alt(V.Clause_constraints, V.Clause_abstract_variables,
                                V.Clause_invariant, V.Clause_initialization,
																V.Clause_operations)
G.Clause_constraints = seq(terminal("CONSTRAINTS"), oneOrMoreSep(V.Predicate, "&"))
G.Clause_abstract_variables = alt(seq(terminal("ABSTRACT_VARIABLES"), oneOrMoreSep(V.Ident, ",")), seq(terminal("VARIABLES"), oneOrMoreSep(V.Ident, ",")))
G.Clause_invariant = seq(terminal("INVARIANT"), oneOrMoreSep(V.Predicate, "&"))
G.Clause_initialization = seq(terminal("INITIALISATION"), V.Substitution)
G.Clause_operations = seq(terminal("OPERATIONS"), oneOrMoreSep(V.Operation, ";"))
G.Operation = seq(V.Header_operation, seq(terminal("="), V.Level1_substitution))
G.Header_operation = seq(optional(seq(oneOrMoreSep(V.Ident, ","), terminal("<--"))), seq(oneOrMoreSep(V.Ident, "."), optional(seq(terminal("("), seq(oneOrMoreSep(V.Ident, ","), terminal(")"))))))
G.Substitution = alt(V.Level1_substitution, V.Simultaneous_substitution,
                     V.Sequence_substitution)
G.Simultaneous_substitution = seq(V.Substitution, seq(terminal("||"), V.Substitution))
G.Sequence_substitution = seq(V.Substitution, seq(terminal(";"), V.Substitution))
G.Level1_substitution = alt(V.Identity_substitution, V.Becomes_equal_substitution,
                            V.Becomes_elt_substitution)
G.Identity_substitution = terminal("skip")
G.Becomes_equal_substitution = alt(seq(oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","), seq(terminal(":="), oneOrMoreSep(V.Expression, ","))), seq(oneOrMoreSep(V.Ident, "."), seq(terminal("("), seq(oneOrMoreSep(V.Expression, ","), seq(terminal(")"), seq(terminal(":="), V.Expression))))))
G.Becomes_elt_substitution = seq(oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","), seq(terminal("::"), V.Expression))
G.Predicate = alt(V.Bracketed_predicate, V.Conjunction_predicate,
                  V.Negation_predicate, V.Implication_predicate,
									V.Universal_predicate, V.Existential_predicate,
									V.Equals_predicate, V.Belongs_predicate)
G.Bracketed_predicate = seq(terminal("("), seq(V.Predicate, terminal(")")))
G.Conjunction_predicate = seq(V.Predicate, seq(terminal("&"), V.Predicate))
G.Negation_predicate = seq(terminal("not"), seq(terminal("("), seq(V.Predicate, terminal(")"))))
G.Implication_predicate = seq(V.Predicate, seq(terminal("=>"), V.Predicate))
G.Universal_predicate = seq(terminal("!"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, seq(terminal("=>"), seq(V.Predicate, terminal(")"))))))))
G.Existential_predicate = seq(terminal("#"), seq(V.List_ident, seq(terminal("."), seq(terminal("("), seq(V.Predicate, terminal(")"))))))
G.Equals_predicate = seq(V.Expression, seq(terminal("="), V.Expression))
G.Belongs_predicate = seq(V.Expression, seq(terminal(":"), V.Expression))
G.Expression = alt(V.Expressions_primary, V.Expressions_boolean,
                   V.Expressions_of_functionss)
G.Expressions_primary = alt(V.Data, V.Expr_bracketed)
G.Expressions_boolean = alt(V.Boolean_lit, V.Conversion_Bool)
G.Boolean_lit = alt(terminal("FALSE"), terminal("TRUE"))
G.Conversion_Bool = seq(terminal("bool"), seq(terminal("("), seq(V.Predicate, terminal(")"))))
G.Expressions_of_functionss = alt(V.Partial_functions, V.Total_functions)
G.Partial_functions = seq(V.Expression, seq(terminal("+->"), V.Expression))
G.Total_functions = seq(V.Expression, seq(terminal("-->"), V.Expression))
G.Data = alt(oneOrMoreSep(V.Ident, "."), seq(oneOrMoreSep(V.Ident, "."), terminal("$0")))
G.Expr_bracketed = seq(terminal("("), seq(V.Expression, terminal(")")))
G.Ident = terminal("ID")
G.List_ident = alt(V.Ident, seq(terminal("("), seq(oneOrMoreSep(V.Ident, ","), terminal(")"))))