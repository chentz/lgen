G.startSymbol = "GPL"

G.GPL = alt(seq(V.Graph_Type,V.Algorithms),seq(V.Graph_Type,seq(V.Algorithms,V.search)))
G.Graph_Type = alt(V.r_1_2,V.r_1_5)
G.LHS_directed = seq(terminal('t_directed'),terminal('t_Strongly_Connected'))
G.r_1_2 = alt(V.LHS_directed,terminal('t_undirected'))
G.r_1_5 = alt(terminal('t_weighted'),terminal('t_unweighted'))
G.search = V.r_8_9
G.LHS_DFS = seq(terminal('t_DFS'),terminal('t_Cycle_Detection'))
G.r_8_9 = alt(V.LHS_DFS,terminal('t_BFS'))
G.Algorithms = V.r_12_13
G.r_12_13_sec = alt(empty(),V.r_12_13)
G.LHS_Cycle_Detection = seq(terminal('t_Cycle_Detection'),terminal('t_DFS'))
G.LHS_Strongly_Connected = seq(terminal('t_Strongly_Connected'),terminal('t_directed'))
G.r_12_13 = alt(seq(terminal('t_Shortest_Path'),V.r_12_13_sec),seq(V.Coloring,V.r_12_13_sec),seq(V.LHS_Cycle_Detection,V.r_12_13_sec),seq(terminal('t_MST'),V.r_12_13_sec),seq(V.LHS_Strongly_Connected,V.r_12_13_sec))
G.Coloring = V.r_12_13_16_20
G.r_12_13_16_20_sec = alt(empty(),V.r_12_13_16_20)
G.r_12_13_16_20 = alt(seq(terminal('t_Approximation'),V.r_12_13_16_20_sec),seq(terminal('t_Brute_Force'),V.r_12_13_16_20_sec))