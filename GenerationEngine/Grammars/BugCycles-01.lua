G.startSymbol = "A"
G.A = alt(
  terminal('A#1'),
  seq(terminal('A#2'), V.B),
  seq(terminal('A#3'), V.C)
)
G.B = terminal('B#1')
G.C = alt( -- 0
  seq(terminal('C#1'), V.C),
  terminal('C#2')
)