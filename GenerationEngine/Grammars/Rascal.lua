G.startSymbol = "Module"
G.BooleanLiteral = alt( terminal('true'),
  terminal('false') )
G.Literal = alt( V.IntegerLiteral,
  V.RegExpLiteral,
  V.RealLiteral,
  V.BooleanLiteral,
  V.StringLiteral,
  V.DateTimeLiteral,
  V.LocationLiteral,
  V.RationalLiteral )
G.Expression = V.Concrete
G.Pattern = V.Concrete
G.Concrete = seq(terminal('('),seq(V.LAYOUTLIST,seq(V.Sym,seq(V.LAYOUTLIST,seq(terminal(')'),seq(V.LAYOUTLIST,seq(terminal('`'),seq(zeroOrMore(V.ConcretePart),terminal('`')))))))))
G.ConcretePart = alt( oneOrMore(negation(alt(terminal('`'),terminal('<'),terminal('>'),terminal('\\\\'),terminal('\\n')))),
  seq(terminal('\\n'),seq(zeroOrMore(alt(terminal('\\ '),terminal('\\t'),terminal('\\u00A0'),terminal('\\u1680'),range('\\u2000','\\u200A'),terminal('\\u202F'),terminal('\\u205F'),terminal('\\u3000'))),terminal("\\'"))),
  V.ConcreteHole,
  terminal('\\<'),
  terminal('\\>'),
  terminal('\\\\`'),
  terminal('\\\\\\\\') )
G.ConcreteHole = seq(terminal('<'),seq(V.Sym,seq(V.Name,terminal('>'))))
G.Module = seq(V.Header,V.Body)
G.ModuleParameters = seq(terminal('['),seq(V.TypeVar,seq(zeroOrMore(seq(terminal(','),V.TypeVar)),terminal(']'))))
G.DateAndTime = alt( seq(terminal('$'),seq(V.DatePart,seq(terminal('T'),seq(V.TimePartNoTZ,terminal('$'))))),
  seq(terminal('$'),seq(V.DatePart,seq(terminal('T'),seq(V.TimePartNoTZ,seq(V.TimeZonePart,terminal('$')))))) )
G.Strategy = alt( terminal('top-down-break'),
  terminal('top-down'),
  terminal('bottom-up'),
  terminal('bottom-up-break'),
  terminal('outermost'),
  terminal('innermost') )
G.UnicodeEscape = terminal('<UnicodeEscape>')
G.Variable = alt( seq(V.Name,seq(terminal('='),V.Expression)),
  V.Name )
G.OctalIntegerLiteral = seq(terminal('0'),oneOrMore(range('0','7')))
G.TypeArg = alt( V.Type,
  seq(V.Type,V.Name) )
G.Renaming = seq(V.Name,seq(terminal('=>'),V.Name))
G.Catch = alt( seq(terminal('catch'),seq(terminal(':'),V.Statement)),
  seq(terminal('catch'),seq(V.Pattern,seq(terminal(':'),V.Statement))) )
G.PathChars = seq(V.URLChars,terminal('|'))
G.Signature = alt( seq(V.FunctionModifiers,seq(V.Type,seq(V.Name,seq(V.Parameters,seq(terminal('throws'),seq(V.Type,zeroOrMore(seq(terminal(','),V.Type)))))))),
  seq(V.FunctionModifiers,seq(V.Type,seq(V.Name,V.Parameters))) )
G.Sym = alt( V.Nonterminal,
  seq(terminal('&'),V.Nonterminal),
  seq(terminal('start'),seq(terminal('['),seq(V.Nonterminal,terminal(']')))),
  seq(V.Sym,V.NonterminalLabel),
  V.Class,
  V.StringConstant,
  V.CaseInsensitiveStringConstant,
  seq(V.Sym,terminal('+')),
  seq(V.Sym,terminal('*')),
  seq(terminal('{'),seq(V.Sym,seq(V.Sym,seq(terminal('}'),terminal('+'))))),
  seq(terminal('{'),seq(V.Sym,seq(V.Sym,seq(terminal('}'),terminal('*'))))),
  seq(V.Sym,terminal('?')),
  seq(terminal('('),seq(V.Sym,seq(terminal('|'),seq(V.Sym,seq(zeroOrMore(seq(terminal('|'),V.Sym)),terminal(')')))))),
  seq(terminal('('),seq(V.Sym,seq(oneOrMore(V.Sym),terminal(')')))),
  seq(terminal('('),terminal(')')),
  seq(V.Sym,seq(terminal('@'),V.IntegerLiteral)),
  seq(V.Sym,terminal('$')),
  seq(terminal('^'),V.Sym),
  seq(V.Sym,seq(terminal('!'),V.NonterminalLabel)),
  seq(V.Sym,seq(terminal('>>'),V.Sym)),
  seq(V.Sym,seq(terminal('!>>'),V.Sym)),
  seq(V.Sym,seq(terminal('<<'),V.Sym)),
  seq(V.Sym,seq(terminal('!<<'),V.Sym)),
  seq(V.Sym,seq(terminal('\\\\'),V.Sym)) )
G.TimePartNoTZ = alt( seq(range('0','2'),seq(range('0','9'),seq(range('0','5'),seq(range('0','9'),seq(range('0','5'),seq(range('0','9'),optional(seq(alt(terminal(','),terminal('.')),seq(range('0','9'),optional(seq(range('0','9'),optional(range('0','9'))))))))))))),
  seq(range('0','2'),seq(range('0','9'),seq(terminal(':'),seq(range('0','5'),seq(range('0','9'),seq(terminal(':'),seq(range('0','5'),seq(range('0','9'),optional(seq(alt(terminal(','),terminal('.')),seq(range('0','9'),optional(seq(range('0','9'),optional(range('0','9'))))))))))))))) )
G.Header = alt( seq(V.Tags,seq(terminal('module'),seq(V.QualifiedName,seq(V.ModuleParameters,zeroOrMore(V.Import))))),
  seq(V.Tags,seq(terminal('module'),seq(V.QualifiedName,zeroOrMore(V.Import)))) )
G.Name = terminal('<NAME>')
G.SyntaxDefinition = alt( seq(V.Visibility,seq(terminal('layout'),seq(V.Sym,seq(terminal('='),seq(V.Prod,terminal(';')))))),
  seq(terminal('lexical'),seq(V.Sym,seq(terminal('='),seq(V.Prod,terminal(';'))))),
  seq(terminal('keyword'),seq(V.Sym,seq(terminal('='),seq(V.Prod,terminal(';'))))),
  seq(V.Start,seq(terminal('syntax'),seq(V.Sym,seq(terminal('='),seq(V.Prod,terminal(';')))))) )
G.Kind = alt( terminal('function'),
  terminal('variable'),
  terminal('all'),
  terminal('anno'),
  terminal('data'),
  terminal('view'),
  terminal('alias'),
  terminal('module'),
  terminal('tag') )
G.ImportedModule = alt( V.QualifiedName,
  seq(V.QualifiedName,seq(V.ModuleActuals,V.Renamings)),
  seq(V.QualifiedName,V.Renamings),
  seq(V.QualifiedName,V.ModuleActuals) )
G.Target = alt( empty(),
  V.Name )
G.IntegerLiteral = alt( V.DecimalIntegerLiteral,
  V.HexIntegerLiteral,
  V.OctalIntegerLiteral )
G.FunctionBody = seq(terminal('{'),seq(zeroOrMore(V.Statement),terminal('}')))
G.Expression = alt( V.Concrete,
  seq(terminal('{'),seq(oneOrMore(V.Statement),terminal('}'))),
  seq(terminal('('),seq(V.Expression,terminal(')'))),
  seq(V.Type,seq(V.Parameters,seq(terminal('{'),seq(oneOrMore(V.Statement),terminal('}'))))),
  seq(terminal('['),seq(V.Expression,seq(terminal(','),seq(V.Expression,seq(terminal('..'),seq(V.Expression,terminal(']'))))))),
  seq(V.Parameters,seq(terminal('{'),seq(zeroOrMore(V.Statement),terminal('}')))),
  seq(V.Label,V.Visit),
  seq(terminal('('),seq(V.Expression,seq(terminal('|'),seq(V.Expression,seq(terminal('|'),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal(')')))))))),
  seq(terminal('type'),seq(terminal('('),seq(V.Expression,seq(terminal(','),seq(V.Expression,terminal(')')))))),
  seq(V.Expression,seq(terminal('('),seq(optional(seq(V.Expression,zeroOrMore(seq(terminal(','),V.Expression)))),seq(V.KeywordArguments,terminal(')'))))),
  V.Literal,
  seq(terminal('any'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal(')'))))),
  seq(terminal('all'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal(')'))))),
  V.Comprehension,
  seq(terminal('{'),seq(optional(seq(V.Expression,zeroOrMore(seq(terminal(','),V.Expression)))),terminal('}'))),
  seq(terminal('['),seq(optional(seq(V.Expression,zeroOrMore(seq(terminal(','),V.Expression)))),terminal(']'))),
  seq(terminal('#'),V.Type),
  seq(terminal('['),seq(V.Expression,seq(terminal('..'),seq(V.Expression,terminal(']'))))),
  seq(terminal('<'),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal('>')))),
  seq(terminal('('),seq(optional(seq(V.Mapping,zeroOrMore(seq(terminal(','),V.Mapping)))),terminal(')'))),
  terminal('<it>'),
  V.QualifiedName,
  seq(V.Expression,seq(terminal('['),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal(']'))))),
  seq(V.Expression,seq(terminal('['),seq(V.OptionalExpression,seq(terminal('..'),seq(V.OptionalExpression,terminal(']')))))),
  seq(V.Expression,seq(terminal('['),seq(V.OptionalExpression,seq(terminal(','),seq(V.Expression,seq(terminal('..'),seq(V.OptionalExpression,terminal(']')))))))),
  seq(V.Expression,seq(terminal('.'),V.Name)),
  seq(V.Expression,seq(terminal('['),seq(V.Name,seq(terminal('='),seq(V.Expression,terminal(']')))))),
  seq(V.Expression,seq(terminal('<'),seq(V.Field,seq(zeroOrMore(seq(V.Field,terminal(','))),terminal('>'))))),
  seq(V.Expression,seq(terminal('['),seq(terminal('@'),seq(V.Name,seq(terminal('='),seq(V.Expression,terminal(']'))))))),
  seq(V.Expression,seq(terminal('@'),V.Name)),
  seq(V.Expression,seq(terminal('is'),V.Name)),
  seq(V.Expression,seq(terminal('has'),V.Name)),
  seq(V.Expression,terminal('+')),
  seq(V.Expression,terminal('*')),
  seq(V.Expression,terminal('?')),
  seq(terminal('!'),V.Expression),
  seq(terminal('-'),V.Expression),
  seq(terminal('*'),V.Expression),
  seq(terminal('['),seq(V.Type,seq(terminal(']'),V.Expression))),
  seq(V.Expression,seq(terminal('o'),V.Expression)),
  seq(V.Expression,seq(terminal('*'),V.Expression)),
  seq(V.Expression,seq(terminal('join'),V.Expression)),
  seq(V.Expression,seq(terminal('%'),V.Expression)),
  seq(V.Expression,seq(terminal('/'),V.Expression)),
  seq(V.Expression,seq(terminal('&'),V.Expression)),
  seq(V.Expression,seq(terminal('+'),V.Expression)),
  seq(V.Expression,seq(terminal('-'),V.Expression)),
  seq(V.Expression,seq(terminal('<<'),V.Expression)),
  seq(V.Expression,seq(terminal('>>'),V.Expression)),
  seq(V.Expression,seq(terminal('mod'),V.Expression)),
  seq(V.Expression,seq(terminal('notin'),V.Expression)),
  seq(V.Expression,seq(terminal('in'),V.Expression)),
  seq(V.Expression,seq(terminal('>='),V.Expression)),
  seq(V.Expression,seq(terminal('<='),V.Expression)),
  seq(V.Expression,seq(terminal('<'),V.Expression)),
  seq(V.Expression,seq(terminal('>'),V.Expression)),
  seq(V.Expression,seq(terminal('=='),V.Expression)),
  seq(V.Expression,seq(terminal('!='),V.Expression)),
  seq(V.Expression,seq(terminal('?'),V.Expression)),
  seq(V.Pattern,seq(terminal('!:='),V.Expression)),
  seq(V.Pattern,seq(terminal(':='),V.Expression)),
  seq(V.Pattern,seq(terminal('<-'),V.Expression)),
  seq(V.Expression,seq(terminal('==>'),V.Expression)),
  seq(V.Expression,seq(terminal('<==>'),V.Expression)),
  seq(V.Expression,seq(terminal('&&'),V.Expression)),
  seq(V.Expression,seq(terminal('||'),V.Expression)),
  seq(V.Expression,seq(terminal('?'),seq(V.Expression,seq(terminal(':'),V.Expression)))) )
G.OptionalExpression = alt( V.Expression,
  empty() )
G.UserType = alt( V.QualifiedName,
  seq(V.QualifiedName,seq(terminal('['),seq(V.Type,seq(zeroOrMore(seq(terminal(','),V.Type)),terminal(']'))))) )
G.Import = alt( seq(terminal('extend'),seq(V.ImportedModule,terminal(';'))),
  seq(terminal('import'),seq(V.ImportedModule,terminal(';'))),
  seq(terminal('import'),seq(V.QualifiedName,seq(terminal('='),seq(V.LocationLiteral,terminal(';'))))),
  V.SyntaxDefinition )
G.Body = zeroOrMore(V.Toplevel)
G.URLChars = zeroOrMore(negation(alt(range('\\t','\\n'),terminal('\\r'),terminal('\\ '),terminal('<'),terminal('|'))))
G.TimeZonePart = alt( seq(alt(terminal('+'),terminal('\\-')),seq(range('0','1'),seq(range('0','9'),seq(terminal(':'),seq(range('0','5'),range('0','9')))))),
  terminal('Z'),
  seq(alt(terminal('+'),terminal('\\-')),seq(range('0','1'),range('0','9'))),
  seq(alt(terminal('+'),terminal('\\-')),seq(range('0','1'),seq(range('0','9'),seq(terminal(':'),seq(range('0','5'),range('0','9')))))) )
G.ProtocolPart = alt( V.ProtocolChars,
  seq(V.PreProtocolChars,seq(V.Expression,V.ProtocolTail)) )
G.StringTemplate = alt( seq(terminal('if'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal(')'),seq(terminal('{'),seq(zeroOrMore(V.Statement),seq(V.StringMiddle,seq(zeroOrMore(V.Statement),terminal('}')))))))))),
  seq(terminal('if'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal(')'),seq(terminal('{'),seq(zeroOrMore(V.Statement),seq(V.StringMiddle,seq(zeroOrMore(V.Statement),seq(terminal('}'),seq(terminal('else'),seq(terminal('{'),seq(zeroOrMore(V.Statement),seq(V.StringMiddle,seq(zeroOrMore(V.Statement),terminal('}')))))))))))))))),
  seq(terminal('for'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal(')'),seq(terminal('{'),seq(zeroOrMore(V.Statement),seq(V.StringMiddle,seq(zeroOrMore(V.Statement),terminal('}')))))))))),
  seq(terminal('do'),seq(terminal('{'),seq(zeroOrMore(V.Statement),seq(V.StringMiddle,seq(zeroOrMore(V.Statement),seq(terminal('}'),seq(terminal('while'),seq(terminal('('),seq(V.Expression,terminal(')')))))))))),
  seq(terminal('while'),seq(terminal('('),seq(V.Expression,seq(terminal(')'),seq(terminal('{'),seq(zeroOrMore(V.Statement),seq(V.StringMiddle,seq(zeroOrMore(V.Statement),terminal('}'))))))))) )
G.PreStringChars = seq(terminal('"'),seq(zeroOrMore(V.StringCharacter),terminal('<')))
G.CaseInsensitiveStringConstant = seq(terminal("\\'"),seq(zeroOrMore(V.StringCharacter),terminal("\\'")))
G.Backslash = terminal('\\\\')
G.Label = alt( seq(V.Name,terminal(':')),
  empty() )
G.MidProtocolChars = seq(terminal('>'),seq(V.URLChars,terminal('<')))
G.NamedBackslash = terminal('\\\\')
G.Field = alt( V.IntegerLiteral,
  V.Name )
G.JustDate = seq(terminal('$'),seq(V.DatePart,terminal('$')))
G.PostPathChars = seq(terminal('>'),seq(V.URLChars,terminal('|')))
G.PathPart = alt( V.PathChars,
  seq(V.PrePathChars,seq(V.Expression,V.PathTail)) )
G.DatePart = alt( seq(range('0','9'),seq(range('0','9'),seq(range('0','9'),seq(range('0','9'),seq(terminal('-'),seq(range('0','1'),seq(range('0','9'),seq(terminal('-'),seq(range('0','3'),range('0','9')))))))))),
  seq(range('0','9'),seq(range('0','9'),seq(range('0','9'),seq(range('0','9'),seq(range('0','1'),seq(range('0','9'),seq(range('0','3'),range('0','9')))))))) )
G.FunctionModifier = alt( terminal('java'),
  terminal('test'),
  terminal('default') )
G.Assignment = alt( terminal('?='),
  terminal('/='),
  terminal('*='),
  terminal('&='),
  terminal('-='),
  terminal('='),
  terminal('+='),
  terminal('<<=') )
G.Assignable = alt( seq(terminal('('),seq(V.Assignable,terminal(')'))),
  V.QualifiedName,
  seq(V.Assignable,seq(terminal('['),seq(V.Expression,terminal(']')))),
  seq(V.Assignable,seq(terminal('['),seq(V.OptionalExpression,seq(terminal('..'),seq(V.OptionalExpression,terminal(']')))))),
  seq(V.Assignable,seq(terminal('['),seq(V.OptionalExpression,seq(terminal(','),seq(V.Expression,seq(terminal('..'),seq(V.OptionalExpression,terminal(']')))))))),
  seq(V.Assignable,seq(terminal('.'),V.Name)),
  seq(V.Assignable,seq(terminal('?'),V.Expression)),
  seq(V.Name,seq(terminal('('),seq(V.Assignable,seq(zeroOrMore(seq(terminal(','),V.Assignable)),terminal(')'))))),
  seq(terminal('<'),seq(V.Assignable,seq(zeroOrMore(seq(terminal(','),V.Assignable)),terminal('>')))),
  seq(V.Assignable,seq(terminal('@'),V.Name)) )
G.StringConstant = seq(terminal('\\"'),seq(zeroOrMore(V.StringCharacter),terminal('\\"')))
G.Assoc = alt( terminal('assoc'),
  terminal('left'),
  terminal('non-assoc'),
  terminal('right') )
G.Replacement = alt( V.Expression,
  seq(V.Expression,seq(terminal('when'),seq(V.Expression,zeroOrMore(seq(terminal(','),V.Expression))))) )
G.DataTarget = alt( empty(),
  seq(V.Name,terminal(':')) )
G.StringCharacter = alt( seq(terminal('\\\\'),alt(terminal('"'),terminal("\\'"),terminal('<'),terminal('>'),terminal('\\\\'),terminal('b'),terminal('f'),terminal('n'),terminal('r'),terminal('t'))),
  V.UnicodeEscape,
  negation(alt(terminal('"'),terminal("\\'"),terminal('<'),terminal('>'),terminal('\\\\'))),
  seq(terminal('\\n'),seq(zeroOrMore(alt(terminal('\\ '),terminal('\\t'),terminal('\\u00A0'),terminal('\\u1680'),range('\\u2000','\\u200A'),terminal('\\u202F'),terminal('\\u205F'),terminal('\\u3000'))),terminal("\\'"))) )
G.JustTime = alt( seq(terminal('$T'),seq(V.TimePartNoTZ,terminal('$'))),
  seq(terminal('$T'),seq(V.TimePartNoTZ,seq(V.TimeZonePart,terminal('$')))) )
G.MidStringChars = seq(terminal('>'),seq(zeroOrMore(V.StringCharacter),terminal('<')))
G.ProtocolChars = seq(terminal('|'),seq(V.URLChars,terminal('://')))
G.RegExpModifier = zeroOrMore(alt(terminal('d'),terminal('i'),terminal('m'),terminal('s')))
G.CommonKeywordParameters = alt( empty(),
  seq(terminal('('),seq(V.KeywordFormal,seq(zeroOrMore(seq(terminal(','),V.KeywordFormal)),terminal(')')))) )
G.Parameters = alt( seq(terminal('('),seq(V.Formals,seq(V.KeywordFormals,terminal(')')))),
  seq(terminal('('),seq(V.Formals,seq(terminal('...'),seq(V.KeywordFormals,terminal(')'))))) )
G.OptionalComma = optional(terminal(','))
G.KeywordFormals = alt( seq(V.OptionalComma,alt(terminal(','),terminal('\\ '),terminal('('),terminal('\\t'),terminal('\\n'))),
  empty() )
G.KeywordFormal = seq(V.Type,seq(V.Name,seq(terminal('='),V.Expression)))
G.KeywordArguments = alt( seq(V.OptionalComma,alt(terminal(','),terminal('\\ '),terminal('('),terminal('\\t'),terminal('\\n'))),
  empty() )
G.KeywordArgument = seq(V.Name,seq(terminal('='),terminal('<&T>')))
G.RegExp = alt( negation(alt(terminal('/ '),terminal('<'),terminal('>'),terminal('\\\\'))),
  seq(terminal('<'),seq(V.Name,terminal('>'))),
  seq(terminal('\\\\'),alt(terminal('/ '),terminal('<'),terminal('>'),terminal('\\\\'))),
  seq(terminal('<'),seq(V.Name,seq(terminal(':'),seq(zeroOrMore(V.NamedRegExp),terminal('>'))))),
  V.Backslash )
G.LAYOUTLIST = zeroOrMore(V.LAYOUT)
G.LocalVariableDeclaration = alt( V.Declarator,
  seq(terminal('dynamic'),V.Declarator) )
G.RealLiteral = alt( seq(oneOrMore(range('0','9')),alt(terminal('D'),terminal('F'),terminal('d'),terminal('f'))),
  seq(oneOrMore(range('0','9')),seq(alt(terminal('E'),terminal('e')),seq(optional(alt(terminal('+'),terminal('-'))),seq(oneOrMore(range('0','9')),optional(alt(terminal('D'),terminal('F'),terminal('d'),terminal('f'))))))),
  seq(oneOrMore(range('0','9')),seq(terminal('.'),seq(zeroOrMore(range('0','9')),optional(alt(terminal('D'),terminal('F'),terminal('d'),terminal('f')))))),
  seq(oneOrMore(range('0','9')),seq(terminal('.'),seq(zeroOrMore(range('0','9')),seq(alt(terminal('E'),terminal('e')),seq(optional(alt(terminal('+'),terminal('-'))),seq(oneOrMore(range('0','9')),optional(alt(terminal('D'),terminal('F'),terminal('d'),terminal('f'))))))))),
  seq(terminal('.'),seq(oneOrMore(range('0','9')),optional(alt(terminal('D'),terminal('F'),terminal('d'),terminal('f'))))),
  seq(terminal('.'),seq(oneOrMore(range('0','9')),seq(alt(terminal('E'),terminal('e')),seq(optional(alt(terminal('+'),terminal('-'))),seq(oneOrMore(range('0','9')),optional(alt(terminal('D'),terminal('F'),terminal('d'),terminal('f')))))))) )
G.Range = alt( seq(V.Char,seq(terminal('-'),V.Char)),
  V.Char )
G.LocationLiteral = seq(V.ProtocolPart,V.PathPart)
G.ShellCommand = alt( seq(terminal('set'),seq(V.QualifiedName,V.Expression)),
  seq(terminal('undeclare'),V.QualifiedName),
  terminal('help'),
  seq(terminal('edit'),V.QualifiedName),
  seq(terminal('unimport'),V.QualifiedName),
  terminal('declarations'),
  terminal('quit'),
  terminal('history'),
  terminal('test'),
  terminal('modules'),
  terminal('clear') )
G.StringMiddle = alt( V.MidStringChars,
  seq(V.MidStringChars,seq(V.StringTemplate,V.StringMiddle)),
  seq(V.MidStringChars,seq(V.Expression,V.StringMiddle)) )
G.QualifiedName = seq(V.Name,zeroOrMore(seq(terminal('::'),V.Name)))
G.RationalLiteral = alt( seq(range('0','9'),seq(zeroOrMore(range('0','9')),terminal('r'))),
  seq(range('1','9'),seq(zeroOrMore(range('0','9')),seq(terminal('r'),seq(range('0','9'),zeroOrMore(range('0','9')))))) )
G.DecimalIntegerLiteral = alt( terminal('0'),
  seq(range('1','9'),zeroOrMore(range('0','9'))) )
G.DataTypeSelector = seq(V.QualifiedName,seq(terminal('.'),V.Name))
G.StringTail = alt( seq(V.MidStringChars,seq(V.Expression,V.StringTail)),
  V.PostStringChars,
  seq(V.MidStringChars,seq(V.StringTemplate,V.StringTail)) )
G.PatternWithAction = alt( seq(V.Pattern,seq(terminal('=>'),V.Replacement)),
  seq(V.Pattern,seq(terminal(':'),V.Statement)) )
G.LAYOUT = terminal('<LAYOUT>')
G.Visit = alt( seq(V.Strategy,seq(terminal('visit'),seq(terminal('('),seq(V.Expression,seq(terminal(')'),seq(terminal('{'),seq(oneOrMore(V.Case),terminal('}')))))))),
  seq(terminal('visit'),seq(terminal('('),seq(V.Expression,seq(terminal(')'),seq(terminal('{'),seq(oneOrMore(V.Case),terminal('}'))))))) )
G.Commands = oneOrMore(V.EvalCommand)
G.EvalCommand = alt( V.Declaration,
  V.Statement,
  V.Import,
  V.Output )
G.Output = terminal('<Output>')
G.Command = alt( V.Expression,
  V.Declaration,
  seq(terminal(':'),V.ShellCommand),
  V.Statement,
  V.Import )
G.TagString = terminal('<TagString>')
G.ProtocolTail = alt( seq(V.MidProtocolChars,seq(V.Expression,V.ProtocolTail)),
  V.PostProtocolChars )
G.Nonterminal = terminal('<Nonterminal>')
G.PathTail = alt( seq(V.MidPathChars,seq(V.Expression,V.PathTail)),
  V.PostPathChars )
G.Visibility = alt( terminal('private'),
  empty(),
  terminal('public') )
G.StringLiteral = alt( seq(V.PreStringChars,seq(V.StringTemplate,V.StringTail)),
  seq(V.PreStringChars,seq(V.Expression,V.StringTail)),
  V.StringConstant )
G.Comment = terminal('<Comment>')
G.Renamings = seq(terminal('renaming'),seq(V.Renaming,zeroOrMore(seq(terminal(','),V.Renaming))))
G.Tags = zeroOrMore(V.Tag)
G.Formals = optional(seq(V.Pattern,zeroOrMore(seq(terminal(','),V.Pattern))))
G.PostProtocolChars = seq(terminal('>'),seq(V.URLChars,terminal('://')))
G.Start = alt( empty(),
  terminal('start') )
G.Statement = alt( seq(terminal('assert'),seq(V.Expression,terminal(';'))),
  seq(terminal('assert'),seq(V.Expression,seq(terminal(':'),seq(V.Expression,terminal(';'))))),
  seq(V.Expression,terminal(';')),
  seq(V.Label,V.Visit),
  seq(V.Label,seq(terminal('while'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal(')'),V.Statement)))))),
  seq(V.Label,seq(terminal('do'),seq(V.Statement,seq(terminal('while'),seq(terminal('('),seq(V.Expression,seq(terminal(')'),terminal(';')))))))),
  seq(V.Label,seq(terminal('for'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal(')'),V.Statement)))))),
  seq(V.Label,seq(terminal('if'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal(')'),V.Statement)))))),
  seq(V.Label,seq(terminal('if'),seq(terminal('('),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal(')'),seq(V.Statement,seq(terminal('else'),V.Statement)))))))),
  seq(V.Label,seq(terminal('switch'),seq(terminal('('),seq(V.Expression,seq(terminal(')'),seq(terminal('{'),seq(oneOrMore(V.Case),terminal('}')))))))),
  seq(terminal('fail'),seq(V.Target,terminal(';'))),
  seq(terminal('break'),seq(V.Target,terminal(';'))),
  seq(terminal('continue'),seq(V.Target,terminal(';'))),
  seq(terminal('filter'),terminal(';')),
  seq(terminal('solve'),seq(terminal('('),seq(V.QualifiedName,seq(zeroOrMore(seq(terminal(','),V.QualifiedName)),seq(V.Bound,seq(terminal(')'),V.Statement)))))),
  seq(terminal('try'),seq(V.Statement,oneOrMore(V.Catch))),
  seq(terminal('try'),seq(V.Statement,seq(oneOrMore(V.Catch),seq(terminal('finally'),V.Statement)))),
  seq(V.Label,seq(terminal('{'),seq(oneOrMore(V.Statement),terminal('}')))),
  terminal(';'),
  seq(terminal('global'),seq(V.Type,seq(V.QualifiedName,seq(zeroOrMore(seq(terminal(','),V.QualifiedName)),terminal(';'))))),
  seq(V.Assignable,seq(V.Assignment,V.Statement)),
  seq(terminal('return'),V.Statement),
  seq(terminal('throw'),V.Statement),
  seq(terminal('insert'),seq(V.DataTarget,V.Statement)),
  seq(terminal('append'),seq(V.DataTarget,V.Statement)),
  V.FunctionDeclaration,
  seq(V.LocalVariableDeclaration,terminal(';')) )
G.StructuredType = seq(V.BasicType,seq(terminal('['),seq(V.TypeArg,seq(zeroOrMore(seq(terminal(','),V.TypeArg)),terminal(']')))))
G.NonterminalLabel = seq(range('a','z'),zeroOrMore(alt(range('0','9'),range('A','Z'),terminal('_'),range('a','z'))))
G.FunctionType = seq(V.Type,seq(terminal('('),seq(V.TypeArg,seq(zeroOrMore(seq(terminal(','),V.TypeArg)),terminal(')')))))
G.Case = alt( seq(terminal('case'),V.PatternWithAction),
  seq(terminal('default'),seq(terminal(':'),V.Statement)) )
G.Declarator = seq(V.Type,seq(V.Variable,zeroOrMore(seq(terminal(','),V.Variable))))
G.Bound = alt( seq(terminal(';'),V.Expression),
  empty() )
G.RascalKeywords = alt( terminal('o'),
  terminal('syntax'),
  terminal('keyword'),
  terminal('lexical'),
  terminal('int'),
  terminal('break'),
  terminal('continue'),
  terminal('rat'),
  terminal('true'),
  terminal('bag'),
  terminal('num'),
  terminal('node'),
  terminal('finally'),
  terminal('private'),
  terminal('real'),
  terminal('list'),
  terminal('fail'),
  terminal('filter'),
  terminal('if'),
  terminal('tag'),
  V.BasicType,
  terminal('extend'),
  terminal('append'),
  terminal('rel'),
  terminal('lrel'),
  terminal('void'),
  terminal('non-assoc'),
  terminal('assoc'),
  terminal('test'),
  terminal('anno'),
  terminal('layout'),
  terminal('data'),
  terminal('join'),
  terminal('it'),
  terminal('bracket'),
  terminal('in'),
  terminal('import'),
  terminal('false'),
  terminal('all'),
  terminal('dynamic'),
  terminal('solve'),
  terminal('type'),
  terminal('try'),
  terminal('catch'),
  terminal('notin'),
  terminal('else'),
  terminal('insert'),
  terminal('switch'),
  terminal('return'),
  terminal('case'),
  terminal('while'),
  terminal('str'),
  terminal('throws'),
  terminal('visit'),
  terminal('tuple'),
  terminal('for'),
  terminal('assert'),
  terminal('loc'),
  terminal('default'),
  terminal('map'),
  terminal('alias'),
  terminal('any'),
  terminal('module'),
  terminal('mod'),
  terminal('bool'),
  terminal('public'),
  terminal('one'),
  terminal('throw'),
  terminal('set'),
  terminal('start'),
  terminal('datetime'),
  terminal('value') )
G.Type = alt( seq(terminal('('),seq(V.Type,terminal(')'))),
  V.UserType,
  V.FunctionType,
  V.StructuredType,
  V.BasicType,
  V.DataTypeSelector,
  V.TypeVar,
  V.Sym )
G.Declaration = alt( seq(V.Tags,seq(V.Visibility,seq(V.Type,seq(V.Variable,seq(zeroOrMore(seq(terminal(','),V.Variable)),terminal(';')))))),
  seq(V.Tags,seq(V.Visibility,seq(terminal('anno'),seq(V.Type,seq(V.Type,seq(terminal('@'),seq(V.Name,terminal(';')))))))),
  seq(V.Tags,seq(V.Visibility,seq(terminal('alias'),seq(V.UserType,seq(terminal('='),seq(V.Type,terminal(';'))))))),
  seq(V.Tags,seq(V.Visibility,seq(terminal('tag'),seq(V.Kind,seq(V.Name,seq(terminal('on'),seq(V.Type,seq(zeroOrMore(seq(terminal(','),V.Type)),terminal(';'))))))))),
  seq(V.Tags,seq(V.Visibility,seq(terminal('data'),seq(V.UserType,seq(V.CommonKeywordParameters,terminal(';')))))),
  seq(V.Tags,seq(V.Visibility,seq(terminal('data'),seq(V.UserType,seq(V.CommonKeywordParameters,seq(terminal('='),seq(V.Variant,seq(zeroOrMore(seq(terminal('|'),V.Variant)),terminal(';'))))))))),
  V.FunctionDeclaration )
G.Class = alt( seq(terminal('['),seq(zeroOrMore(V.Range),terminal(']'))),
  seq(terminal('!'),V.Class),
  seq(V.Class,seq(terminal('-'),V.Class)),
  seq(V.Class,seq(terminal('&&'),V.Class)),
  seq(V.Class,seq(terminal('||'),V.Class)),
  seq(terminal('('),seq(V.Class,terminal(')'))) )
G.RegExpLiteral = seq(terminal('/'),seq(zeroOrMore(V.RegExp),seq(terminal('/'),V.RegExpModifier)))
G.FunctionModifiers = zeroOrMore(V.FunctionModifier)
G.Comprehension = alt( seq(terminal('{'),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal('|'),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal('}'))))))),
  seq(terminal('('),seq(V.Expression,seq(terminal(':'),seq(V.Expression,seq(terminal('|'),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal(')')))))))),
  seq(terminal('['),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),seq(terminal('|'),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal(']'))))))) )
G.Variant = seq(V.Name,seq(terminal('('),seq(V.TypeArg,seq(zeroOrMore(seq(terminal(','),V.TypeArg)),seq(V.KeywordFormals,terminal(')'))))))
G.FunctionDeclaration = alt( seq(V.Tags,seq(V.Visibility,seq(V.Signature,terminal(';')))),
  seq(V.Tags,seq(V.Visibility,seq(V.Signature,seq(terminal('='),seq(V.Expression,terminal(';')))))),
  seq(V.Tags,seq(V.Visibility,seq(V.Signature,seq(terminal('='),seq(V.Expression,seq(terminal('when'),seq(V.Expression,seq(zeroOrMore(seq(terminal(','),V.Expression)),terminal(';'))))))))),
  seq(V.Tags,seq(V.Visibility,seq(V.Signature,V.FunctionBody))) )
G.PreProtocolChars = seq(terminal('|'),seq(V.URLChars,terminal('<')))
G.NamedRegExp = alt( seq(terminal('<'),seq(V.Name,terminal('>'))),
  seq(terminal('\\\\'),alt(terminal('/'),terminal('<'),terminal('>'),terminal('\\\\'))),
  V.NamedBackslash,
  negation(alt(terminal('/'),terminal('<'),terminal('>'),terminal('\\\\'))) )
G.ProdModifier = alt( V.Assoc,
  terminal('bracket'),
  V.Tag )
G.Toplevel = V.Declaration
G.PostStringChars = seq(terminal('>'),seq(zeroOrMore(V.StringCharacter),terminal('\\"')))
G.HexIntegerLiteral = seq(terminal('0'),seq(alt(terminal('X'),terminal('x')),oneOrMore(alt(range('0','9'),range('A','F'),range('a','f')))))
G.TypeVar = alt( seq(terminal('&'),V.Name),
  seq(terminal('&'),seq(V.Name,seq(terminal('<:'),V.Type))) )
G.BasicType = alt( terminal('value'),
  terminal('loc'),
  terminal('node'),
  terminal('num'),
  terminal('type'),
  terminal('bag'),
  terminal('int'),
  terminal('rat'),
  terminal('rel'),
  terminal('lrel'),
  terminal('real'),
  terminal('tuple'),
  terminal('str'),
  terminal('bool'),
  terminal('void'),
  terminal('datetime'),
  terminal('set'),
  terminal('map'),
  terminal('list') )
G.Char = alt( seq(terminal('\\\\'),alt(terminal('\\ '),terminal('\\"'),terminal("\\'"),terminal('\\-'),terminal('<'),terminal('>'),terminal('\\['),terminal('\\\\'),terminal('\\]'),terminal('b'),terminal('f'),terminal('n'),terminal('r'),terminal('t'))),
  negation(alt(terminal('\\ '),terminal('"'),terminal("\\'"),terminal('\\-'),terminal('<'),terminal('>'),terminal('\\['),terminal('\\\\'),terminal('\\]'))),
  V.UnicodeEscape )
G.Prod = alt( seq(terminal(':'),V.Name),
  seq(zeroOrMore(V.ProdModifier),seq(V.Name,seq(terminal(':'),zeroOrMore(V.Sym)))),
  terminal('...'),
  seq(zeroOrMore(V.ProdModifier),zeroOrMore(V.Sym)),
  seq(V.Assoc,seq(terminal('('),seq(V.Prod,terminal(')')))),
  seq(V.Prod,seq(terminal('|'),V.Prod)),
  seq(V.Prod,seq(terminal('>'),V.Prod)) )
G.DateTimeLiteral = alt( V.JustDate,
  V.JustTime,
  V.DateAndTime )
G.PrePathChars = seq(V.URLChars,terminal('<'))
G.Mapping = terminal('<Mapping>')
G.MidPathChars = seq(terminal('>'),seq(V.URLChars,terminal('<')))
G.Pattern = alt( V.Concrete,
  seq(terminal('{'),seq(optional(seq(V.Pattern,zeroOrMore(seq(terminal(','),V.Pattern)))),terminal('}'))),
  seq(terminal('['),seq(optional(seq(V.Pattern,zeroOrMore(seq(terminal(','),V.Pattern)))),terminal(']'))),
  V.QualifiedName,
  seq(V.QualifiedName,terminal('*')),
  seq(terminal('*'),V.Pattern),
  seq(terminal('+'),V.Pattern),
  seq(terminal('-'),V.Pattern),
  V.Literal,
  seq(terminal('<'),seq(V.Pattern,seq(zeroOrMore(seq(terminal(','),V.Pattern)),terminal('>')))),
  seq(V.Type,V.Name),
  seq(terminal('('),seq(optional(seq(V.Mapping,zeroOrMore(seq(terminal(','),V.Mapping)))),terminal(')'))),
  seq(terminal('type'),seq(terminal('('),seq(V.Pattern,seq(terminal(','),seq(V.Pattern,terminal(')')))))),
  seq(V.Pattern,seq(terminal('('),seq(optional(seq(V.Pattern,zeroOrMore(seq(terminal(','),V.Pattern)))),seq(V.KeywordArguments,terminal(')'))))),
  seq(V.Name,seq(terminal(':'),V.Pattern)),
  seq(terminal('['),seq(V.Type,seq(terminal(']'),V.Pattern))),
  seq(terminal('/'),V.Pattern),
  seq(terminal('!'),V.Pattern),
  seq(V.Type,seq(V.Name,seq(terminal(':'),V.Pattern))) )
G.Tag = alt( seq(terminal('@'),seq(V.Name,V.TagString)),
  seq(terminal('@'),V.Name),
  seq(terminal('@'),seq(V.Name,seq(terminal('='),V.Expression))) )
G.ModuleActuals = seq(terminal('['),seq(V.Type,seq(zeroOrMore(seq(terminal(','),V.Type)),terminal(']'))))

