G.startSymbol = "Component"

G.Component = V.Machine_abstract
G.Machine_abstract = seq(terminal("MACHINE"),seq(V.Header,seq(zeroOrMore(V.Clause_machine_abstract),terminal("END"))))
G.Header = V.Ident
G.Clause_machine_abstract = alt(V.Clause_abstract_variables,V.Clause_operations)
G.Clause_abstract_variables = seq(terminal("VARIABLES"),oneOrMoreSep(V.Ident, ","))

G.Clause_operations = seq(terminal("OPERATIONS"),oneOrMoreSep(V.Operation, ";"))
G.Operation = seq(V.Header_operation,seq(terminal("="),V.Level1_substitution))
G.Header_operation = seq(optional(seq(oneOrMoreSep(V.Ident, ","),terminal("<--"))),seq(oneOrMoreSep(V.Ident, "."),optional(seq(terminal("("),seq(oneOrMoreSep(V.Ident, ","),terminal(")"))))))
G.Level1_substitution = alt(V.Identity_substitution,V.Becomes_equal_substitution)
G.Identity_substitution = terminal("skip")
G.Becomes_equal_substitution = alt(seq(oneOrMoreSep(oneOrMoreSep(V.Ident, "."), ","),seq(terminal(":="),oneOrMoreSep(V.Expression, ","))),seq(oneOrMoreSep(V.Ident, "."),seq(terminal("("),seq(oneOrMoreSep(V.Expression, ","),seq(terminal(")"),seq(terminal(":="),V.Expression))))))
G.Expression = alt(V.Expressions_primary,V.Expressions_boolean)
G.Expressions_primary = V.Data
G.Expressions_boolean = V.Boolean_lit
G.Boolean_lit = alt(terminal("FALSE"),terminal("TRUE"))
G.Data = alt(oneOrMoreSep(V.Ident, "."),seq(oneOrMoreSep(V.Ident, "."),terminal("$0")))

--G.Ident = terminal("ID")
G.Ident = idInc("ID","%CONTEXT%_%VALUE%")
--G.Ident = terminal("ID", false)