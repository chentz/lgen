require ("init")

--local SHA = require("sha2")
local Debug = require("dbg")
local TM = require("TimeMachine")

dbg = Debug:new()
--dbg.Enabled = true

local BenchTimer = require("BenchTimer")

local Set = require ("Set")
Set.configToString("{","}",", ","[")

local Rel = require("Relation")

local TerminalCoverage = {}

local mGrammar = nil
local currTerms = nil
local NTerms = nil
local IDX_NTerms = nil
local tm = TM:new{}

local currDerivNT, penTerms, newTerminal

local quietExec = false

local function qPrint(...)
  if not quietExec then
    print(...)
  end
end

local function ntSymbols(NT)
  local preds = mGrammar.Productions
  
  if not NT then
    local r1, r2 = {}, Set.new()
    for k, nt in pairs(preds) do
      r2 = nt[1]
      for _i=2,#nt do
        r2 = r2 + nt[_i]
      end
      r1[k] = r2
    end
    return r1
  elseif not preds[NT] then
    return nil
  else
    local rsl = preds[NT][1]
    for _i=2,#preds[NT] do
      rsl = rsl + preds[NT][_i]
    end
    return rsl
  end
end

local iID = 0
local function getIDIdent()
  iID = iID + 1
  return iID
end

local function checkNTIndex(NT, currHeight)
  if IDX_NTerms and currHeight then
    --print("checkNTIndex",NT, currHeight)
    if currHeight <= #IDX_NTerms then
      return IDX_NTerms[currHeight]%NT
    else
      return IDX_NTerms[#IDX_NTerms]%NT
    end
  end
  
  return nil
end

local function checkNewTerminal(NT)
  --print("checkNewTerminal", NT, currHeight)
  --[[
  local s = ntSymbols(NT)
  if not s then return false end
  
  s = s - currTerms
  return Set.card(s) > 0
  ]]
  local r = Set.card(NTerms[NT] - currTerms) > 0
  --print('CNT:NTerms',NTerms[NT])
  --print('CNT:currTerms',currTerms)
  --print('checkNewTerminal',NT,r)
  return r
end

local function calcNTerms(productions, gNT, gTerms, maxCycles, maxTreeHeight)
  --print("calcNTerms_New")
  
  local rDep = Rel.new()
  for k,vs in pairs(productions) do
    for v in pairs(vs) do
      rDep(k, v)
    end
  end
  --print("calcNTerms_New, Step 1",rDep)
  
  local r,i = rDep:transitiveClosure(maxTreeHeight)
  r = r:filterOverRelation(pairs, function(k,v) return gTerms:inSet(v) end, false)
  
  --print("calcNTerms_New, Step 2",i,r%"Automatic")
  
  local rel
  IDX_NTerms = {}
  for j=1,i-1 do
    --print(j)
    rel = rDep:transitiveClosure(j)
    IDX_NTerms[j] = rel:filterOverRelation(pairs, function(k,v) return gTerms:inSet(v) end, false)
  end
  IDX_NTerms[i] = r
  --print("calcNTerms_New",r)
  --print("calcNTerms_New",rDep:card(), i, r:card())
  return r:intRepresentation()
end

local function checkGrammar(metaGrammar)
  if (metaGrammar.NTerminals * metaGrammar.Terminals):card() > 0 then
    return false, "The grammar is not context-free. (T must be disjoint of V)"
  end
  
  --print("CG:StartSymbol",metaGrammar.startSymbol)
  local defSet, useSet = Set.new(), Set.new();
  for i, v in pairs(metaGrammar.NTerminals:internalSet()) do
    --print("CG1",i,v,metaGrammar.startSymbol)
    defSet = defSet:include(i)
  end
  
  for i,v in pairs(metaGrammar.Productions) do
    --print("CG2",i,v,#v)
    for i2,v2 in pairs(v) do
      --print("CG2.1",i2,v2)
      for i3,v3 in pairs(v2) do
        --print("CG2.2",i,i3)
        if not metaGrammar.Terminals:inSet(i3) then
          --print("CG2.3",i,i3)
          useSet = useSet:include(i3)
        end
      end
    end
  end
  
  --print("checkGrammar: useSet", useSet)
  --print("checkGrammar: defSet", defSet)
  local rS1, rS2 = useSet - defSet, defSet - useSet 
  if rS1:card() > 0 then
    print("checkGrammar:useSet - defSet", rS1)
    return false, "Some non-terminals aren't defined."
  end
  
  local tmp = rS2:card()
  if tmp > 0 and
     not(tmp == 1 and rS2:inSet(metaGrammar.startSymbol)) then
    print("checkGrammar:defSet - useSet", rS2)
    return false, "Some non-terminals are defined but not used."
  end
  
  return true
end

--Convert internal representation of production from Array to Set.
local function convertProdSet(prod)
  local rsl = {}
  for i,v in pairs(prod) do
    --print("convertProdSet", i,v)
    rsl[i] = prod[i]
    for i2,v2 in pairs(v) do
      --print("convertProdSet:2", i2,v2)
      rsl[i][i2] = Set.new(v2)
    end
  end
  return rsl
end

--Controle da Geracao

function TerminalCoverage.finalizeGrammar(MG)
  mGrammar = MG
  mGrammar.Productions = convertProdSet(MG.Productions)
  
  NTerms = ntSymbols()
  
  --check Grammar for errors.
  local err, errM = checkGrammar(mGrammar)
  if not err then
    error(errM)
  end
end

function TerminalCoverage.initGen(maxCycles, maxTreeHeight, runConfig)
  quietExec = runConfig.quietExec
  currTerms = Set.new()
  penTerms = Set.new()
  newTerminal = true
  
  qPrint("Terminal Coverage informations:")
  qPrint("Terminal", mGrammar.Terminals:card())
  qPrint("Terminal:", mGrammar.Terminals)
  qPrint("NTerminal", mGrammar.NTerminals:card())
  qPrint("NTerminal:", mGrammar.NTerminals)
  
  qPrint("TerminalCoverage.initGen", maxCycles, maxTreeHeight)
  
  --[[
  local tmp = dbg:tostring(NTerms)
  print("NTerms Before", SHA.hash224(tmp), tmp)
  ]]
  local btCalcNT = BenchTimer.new()
  btCalcNT:start()
  NTerms = calcNTerms(NTerms, mGrammar.NTerminals, mGrammar.Terminals, maxCycles, maxTreeHeight)
  btCalcNT:stop()
  --[[
  tmp = dbg:tostring(NTerms)
  print("NTerms", NTerms["Automatic"])
  print("NTerms After", SHA.hash224(tmp), tmp)
  ]]
  local r, v = btCalcNT:timeSum()
  qPrint("TerminalCoverage.initGen", "Time to calcNTerms:",v)
end

function TerminalCoverage.finalizeGen(rslFunc, rslDebug)
  local ret = {}
  local cTerm, cCurrTerm = Set.card(mGrammar.Terminals), Set.card(currTerms)
  ret.txCoverage = (cCurrTerm/cTerm)*100
  rslDebug(string.format("TerminalCoverage{%s}", ret.txCoverage))
  ret.NotCover = (mGrammar.Terminals - currTerms):table()
  local dStr = string.gsub(tostring(mGrammar.Terminals - currTerms), "\\","\\\\")
  dStr = string.gsub(dStr, "'''", "'\\''") --Replace '
  rslDebug(string.format("TerminalNotCovered%s", dStr))
  qPrint("N. Terminal:", cTerm)
  return ret
end

function TerminalCoverage.checkNonTerminal(nonTerminal, firstInNT, prodNT, maxCycles, maxTreeHeight)
  --print('TerminalCoverage.checkNonTerminal', nonTerminal, firstInNT, prodNT, maxCycles, maxTreeHeight)
  currDerivNT = nonTerminal
  local lvSet = checkNTIndex(nonTerminal, maxTreeHeight) 
  if lvSet and lvSet:card() == 0 then return false end
  
  if newTerminal then return true end
  
  --print('TerminalCoverage.checkNonTerminal', 'go to chkNewTerminal')
  return checkNewTerminal(nonTerminal) 
end

function TerminalCoverage.checkTerminal(terminal, firstInNT, prodNT)
  --print('TerminalCoverage.checkTerminal', terminal, prodNT)
  penTerms = penTerms:include(terminal)
  return true
end

local iCnt = 0
function TerminalCoverage.checkSentence(str)
  local cReq, cSat = mGrammar.Terminals:card(), currTerms:card()
  if (iCnt % 100) == 0 then
    qPrint("Sent. Count:", iCnt)
    qPrint(string.format("TerminalCoverage{%s}", cSat/cReq*100))
  end
  iCnt = iCnt + 1
  
  if cReq == cSat then return false, true end
  
  local s = penTerms - currTerms
  --print("TerminalCoverage.checkSentence1", penTerms, currTerms)
  
  if Set.card(s) > 0 then -- Existe algum novo terminal em penTerms
    --print("TerminalCoverage.checkSentence2", '"'..str..'"')
    currTerms = currTerms + s
    penTerms = Set.new()
    --print("TerminalCoverage.checkSentence3", penTerms)
    newTerminal = false -- Reinicia a sinalizacao de novos terminais na derivacao.
    --print(string.format("TerminalCoverage{%s}",(currTerms:card()/mGrammar.Terminals:card())*100))
    return true
  else
    return false
  end
end

function TerminalCoverage.checkAlt(firstInNT, prodNT)
  local cNT
  --print("TerminalCoverage.checkAlt", currDerivNT, prodNT)  
  if prodNT then
    cNT = prodNT
  else
    cNT = currDerivNT
  end
  
  local nTerm = newTerminal
  local label = string.format("%s%s-%s", cNT, os.clock(), getIDIdent())
  local cnt = tm:current()
  --print("checkAlt:Bind penSymbols", penTerms, label)
  cnt.penTerms = penTerms
  tm:savePoint(label)
  -- Funcao anonima para verificacao de opcoes.
  return function(lastResult, index, count)
    --print('cAlt:Inicio', lastResult, index, count, newTerminal,cNT)
    if index > 1 then
      --Restaura ambiente
      tm:rollBack(label, index ~= count)
      penTerms = cnt.penTerms
      
      --print('cAlt:lastResult', index, lastResult)
      --[[Uma vez gerada uma sentenca que tem terminais novos, entao a variavel de controle
          newTerminal deve voltar a ser false.]]
      if lastResult then
        newTerminal = false
      else
        newTerminal = nTerm
      end
    end
    
    if newTerminal or checkNewTerminal(cNT) then
      --print('cAlt:true', index, newTerminal)
      newTerminal = true
      return true
    else
      --print('cAlt:false', index, newTerminal)
      return false
    end
  end
end

function TerminalCoverage.checkOneOrMoreSep(firstInNT, terminalSep, recNumLim, prodNT)
  --print("TerminalCoverage.checkOneOrMoreSep", firstInNT, terminalSep, recNumLim, prodNT)
  --print("TerminalCoverage.checkOneOrMoreSep2",penTerms, currTerms)
  
  if currTerms:inSet(terminalSep) then return nil end
  if not currTerms:inSet(terminalSep) and penTerms:inSet(terminalSep) then return nil end
  
  local lbl = string.format("%s_%s_%s-%s", prodNT, recNum, os.clock(), getIDIdent())
  local cnt = tm:current()
  
  cnt.penTerms = penTerms
  tm:savePoint(lbl)
  
  return function(idx)
    --print("TerminalCoverage.checkOneOrMoreSep:Anon", firstInNT, terminalSep, recNumLim, prodNT, idx)
    if idx > 1 then
      --Restaura ambiente
      tm:rollBack(lbl, idx ~= recNumLim)
      penTerms = cnt.penTerms
      --print("TerminalCoverage.checkOneOrMoreSep:Anon2", idx, penTerms)
      --Include terminalSep in set
      penTerms = penTerms:include(terminalSep)
      --print("TerminalCoverage.checkOneOrMoreSep:Anon3", idx, penTerms)
    end
  
    return idx <= 2 --After this the terminalSep don't add tr to test set
  end
end

return TerminalCoverage
