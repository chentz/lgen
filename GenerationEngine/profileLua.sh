#!/bin/bash
# $1 => lua source
# $2 => maxCycles
# $3 => maxDerivLen
# $4 => CoverCrit [Opt]
# $5 => NiceLevel [Opt]

lua=
if [ -z "$lua"]; then
  if [ -f "./lua" ]; then
    lua=./lua
  else
    if [ -f "/usr/local/bin/lua" ]; then
      lua=/usr/local/bin/lua
    else
      lua=lua
    fi
  fi
fi

echo "Lua Path: $lua"
echo "Lua Version: `$lua -v`"

#LuaSource
if [ -z "$1" ]; then
  echo "The parameter LuaSource is necessary."
  exit 100
fi
lSource=$1

#maxCyclesOp
mCyclesOp=1

#maxCycles
if [ -z "$2" ]; then
  echo "Using the config file!"
  cFile=on
else
  mCycles=$2
  
  #maxDerivLen
  if [ -z "$3" ]; then
    echo "The parameter maxDerivLen is necessary."
    exit 100
  fi
  mDerivLen=$3
  
  #coverCrit
  if [ -z "$4" ]; then
    echo "Generation without coverage criteria."
    coverCrit=none
  else
    case "$4" in
      term )
        coverCrit="terminalCoverage"
        ;;
      prod )
        coverCrit="productionCoverage"
        ;;
      CDRC )
        coverCrit="CDRCoverage"
        ;;
    esac
  fi
fi

#NiceLevel
nonice=0
if [ -z "$5" ]; then
  echo "Default nice level (-20) is used."
  nice_level=-20
else
  nice_level=$5
  case "$5" in
    -on )
	    printOut=1
		  nice_level=-20
		  ;;
	  -Noff )
	    nonice=1
	    ;;
  esac
fi

UNAME_SYSTEM=`(uname -s) 2>/dev/null`  || UNAME_SYSTEM=unknown

case "${UNAME_SYSTEM}" in
  Darwin )
	  cmd_time=gtime
	  md5=md5
		;;
	Linux )
	  cmd_time=/usr/bin/time
	  md5=md5sum
		;;
esac

#Config filename output
fName="${lSource%.*}"
file_name_base=$fName.$coverCrit.$mCycles.$mDerivLen
file_name_prof=$file_name_base.prf
file_name_time=$file_name_base.time

rg=RunGrammar.lua

if [ $nonice ]; then
  if [ $cFile ]; then
    echo "lua $rg $lSource"
    $lua $rg $lSource
  else
    echo "$cmd_time -v -a -o $file_name_time lua $rg $lSource $mCyclesOp $mCycles $mDerivLen $coverCrit $file_name_base"
    $cmd_time -v -a -o $file_name_time $lua $rg $lSource $mCyclesOp $mCycles $mDerivLen $coverCrit $file_name_base
  fi
else
  echo "sudo nice -n$nice_level $cmd_time -v -a -o $file_name_time lua $rg $lSource $mCyclesOp $mCycles $mDerivLen $coverCrit $file_name_base"
  sudo nice -n$nice_level $cmd_time -v -a -o $file_name_time $lua $rg $lSource $mCyclesOp $mCycles $mDerivLen $coverCrit $file_name_base
fi

if [[ $? -gt 0 ]]; then
  echo "Error during the generation process!"
  exit 1
fi

if [ -z $cFile ]; then
  echo "Generated file: ($file_name_base.out,$file_name_prof, $file_name_time)"
  echo "Nice: $nice_level"
  echo "---------------------------------"

  $lua profile.lua $file_name_prof $file_name_base.out

  cs=`$md5 $file_name_base.out | grep -P -o "[a-z0-9]{32,}"`
  echo "Output checksum is: $cs"

  tail -n 20 $file_name_time
  if [ $printOut ]; then
    tail -n 30 $file_name_base.out
  fi
fi