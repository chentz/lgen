--- Transformation module.
require("init")
local DbgMod = require("dbg")
dbg = DbgMod:new()
dbg.Enabled = true

local Set = require ("Set")
local Rel = require("Relation")
local Util = require("Util")
local MI = require("MetaInfo")
local BenchTimer = require("BenchTimer")

local quietExec = false

local function qPrint(...)
  if not quietExec then
    print(...)
  end
end

--[[ == Transformation algorithms ==]]

--- Transformation algorithm constants.
local algConst = {}

--- Just print the grammar rules.
local function printOnly()
  local t = function(G_Orig, G_Curr, runConf, cntx)
    local outF = (cntx.printOnly and cntx.printOnly.OutFunction) or print
    outF(string.format("G.startSymbol = %s", G_Curr.startSymbol))
    
    local eachProd = function(ntProd, patt)
      if ntProd == "startSymbol" then
        return patt
      end
      
      local t
      if patt.opIdent == MI.cOper.alt then
        if not patt.params[1].index then
          outF(string.format("G.%s = %s", ntProd, patt:tostring()))
          return patt
        end
        
        local tPrefix = "--[[%s]]"
        local t1 = tPrefix.."G.%s = alt(%s,"
        local t2 = tPrefix.." %s,"
        local t3 = tPrefix.." %s )"
        for i,v in ipairs(patt.params) do
          t = v.index
          if v.MinTreeHeight then t = v.index..","..v.MinTreeHeight end
          --print("printOnly", ntProd, t)
          
          if i == 1 then outF(string.format(t1, t, ntProd, v:tostring()))
          elseif i < #patt.params then outF(string.format(t2, t, v:tostring()))
          else outF(string.format(t3, t, v:tostring())) end
        end
      else
        t = patt.index
        if not t then
          outF(string.format("G.%s = %s", ntProd, patt:tostring()))
          return patt
        end
        
        if patt.MinTreeHeight then t = patt.index..","..patt.MinTreeHeight end
        outF(string.format("--[[%s]]G.%s = %s", t, ntProd, patt:tostring()))
      end
      
      return patt
    end
    
    local lG = Util.map(G_Curr, eachProd)
    local f = outF("")
    if f then f:close() end
    return false, G_Curr
  end
  
  return "printOnly", t, false
end
algConst.printOnly = printOnly

--- Remove the OOMS construction from the grammar
local function simplifyOOMS()
  local t = function(G_Orig, G_Curr)
    local f = function (ntProd, patt)
      print("simplifyOOMS:", patt.opIdent, "On "..ntProd)
      local p = terminal("simplifyOOMS", true)
      p.index = patt.index
      return true, p
      --return patt
    end

    local change = false
    local eachProd = function(ntProd, patt)
      if ntProd == "startSymbol" then return patt end
      vRet, vPatt = MI.visitor("oneOrMoreSep.*", f, ntProd, patt)
      
      if vRet then
        change = true
        return vPatt
      else return patt end
    end
    
    return change, Util.map(G_Curr, eachProd)
  end
  return "simplifyOOMS", t, false
end
algConst.simplifyOOMS = simplifyOOMS

--- Verify the Index info on the grammar nodes.
local function checkIndex()
  local t = function(G_Orig, G_Curr)
    local okGrammar = true
    
    local eachProd = function(ntProd, patt)
      if ntProd == "startSymbol" then return patt end
      
      if not patt.index then 
        print("checkIndex:Invalid index info on", ntProd)
        okGrammar = false
        return patt
      end
      return patt
    end
    
    Util.map(G_Curr, eachProd)
    return okGrammar, G_Curr 
  end
  
  return "CheckGrammarIndex", t, true
end
algConst.checkIndex = checkIndex

--- Simplify the nonterminal set by replace each of them by terminal constants.
local function simplifyNT(lstNT)
  return function()
    local t = function(G_Orig, G_Curr)
      local change = false
      local eachProd = function(ntProd, patt)
        if ntProd == "startSymbol" then return patt end
        
        if lstNT[ntProd] then
          print("simplifyNT:Apply on", ntProd)
          change = true
          local p = terminal(string.format("<%s>",ntProd), false)
          p.index = patt.index
          return p
        end
        return patt
      end
      local tmp = Util.map(G_Curr, eachProd)
      return change, tmp 
    end
    
    return "SimplifyNonTerminals", t, false
  end
end
algConst.simplifyNT = simplifyNT

--- Calculate the minimal tree height for each production rule in the grammar
local function calcMinTreeHeight()
  local t = function(G_Orig, G_Curr, runConf)
    local igOps = {MI.cOper.zm,MI.cOper.opt}
    local rPId, rSym, sExcNT
    
    --print("Grammar hashcode:", Util.hashCode(G_Curr))
    G_Curr, rPId, rSym, sExcNT = MI.extractStructures(G_Curr,"r", Util.getOrderIndex(G_Curr), true, igOps)
    
    local inf = -1
    --print("PID:", rPId)
    --print("rPId hashCode:", rPId:hashCode())
    --print("Rules:", rSym)
    --print("altRel:",altRel, altRel:card())
    --print("rSym hashCode:", rSym:hashCode())
    
    --Detect Direct Cycles
    local sDCycles = Set.new()
    local rRules = (rSym..rPId):filterOverRelation(pairs, function(k,v)
      if k == v then
        sDCycles = sDCycles:include(k)
        --rRuleMinHeight(v,inf)
        return true -- Include this cycle on relation
      else return true end
    end)
    
    local sICycles = Set.new();
    rTmp = (rRules+0):filterOverRelation(pairs, function(k,v)
    --rTmp = (rRules+runConf.maxDerivLen):filterOverRelation(pairs, function(k,v)
      if k == v then sICycles = sICycles:include(k) end
      return false
    end)
    --print(rRules)
    --qPrint("Direct Cycles:", sDCycles)
    --qPrint("Indirect Cycles:", sICycles)
    
    local rRuleMinHeight,b = MI.calcMinTreeHeightRules(rPId, rSym)
    
    qPrint("Is over?", rRuleMinHeight ~= nil)
    if not rRuleMinHeight then
      qPrint(b)
      qPrint("Direct cycles:", b:domain() * sDCycles)
      return false
    else
      --print(rTmp)
      --print(rRuleMinHeight)
      --print(rRuleMinHeight:first())
      rTmp = rPId%G_Curr.startSymbol
      if not rTmp then 
        print("Invalid start symbol!", G_Curr.startSymbol)
        return false
      end
      qPrint("startSymbol: ", G_Curr.startSymbol)
      qPrint("startSymbol prods. ids:", rTmp:card(), rTmp)
      rTmp = rRuleMinHeight:filterOverRelation(pairs, function(k,v) return rTmp:inSet(k) end)
      qPrint("startSymbol Min:", rTmp:card(), rTmp)
      local a,b = (rTmp:invert()):min()
      qPrint(string.format("The smallest tree has the height of: %s (%s)", a, b))
      a,b = (rTmp:invert()):max()
      qPrint(string.format("The max tree has the height of: %s (%s)", a, b))
      --print(rRuleMinHeight%"r71")
      --Setting MinTreeHeight over rules
      local eachProd = function(ntProd, patt)
          --print("calcMinTreeHeight:", ntProd, ntProd == "ShellCommand")
          local s = rRuleMinHeight%("r"..patt.index)
          --assert(s,"calcMinTreeHeight:Invalid result set on single rule.")
          patt.MinTreeHeight = s:first()
          --print(patt, patt.index, patt.MinTreeHeight)
          --assert(patt.MinTreeHeight)
          return patt
      end
      
      return true, MI.mapEachRule(G_Curr, eachProd)
    end
  end
  
  return "calcMinTreeHeight", t, false
end
algConst.calcMinTreeHeight = calcMinTreeHeight

--- Simplify the grammar eliminating the unreachable symbols from startSymbol.
local function simplifyGrammar()
  local t = function(G_Orig, G_Curr, runConf)
    
    local rPId, rSym, sExcNT
    G_Curr, rPId, rSym, sExcNT = MI.extractStructures(G_Curr,"r", Util.getOrderIndex(G_Curr), true)
    --local sReachableNT = ((rPId..rSym)+runConf.maxDerivLen)%G_Curr.startSymbol
    local sReachableNT = ((rPId..rSym)+0)%G_Curr.startSymbol
    --print(sReachableNT, sReachableNT:card())
    --print("Excluded NT: ", rPId:domain() - sReachableNT)
--    error("break")
    
    local eachProd = function(ntProd, patt)
      if (ntProd == "startSymbol") or 
         (ntProd == G_Curr.startSymbol) or
         sReachableNT:inSet(ntProd)
      then return patt end
      
      return nil
    end
    return true, Util.map(G_Curr, eachProd)
  end
  
  return "simplifyGrammar", t, true
end
algConst.simplifyGrammar = simplifyGrammar

--- Simplify the grammar eliminating the unreachable symbols from startSymbol.
local function printGrammarInfo()
  local t = function(G_Orig, G_Curr)
    local _, rPId, rSym, sExcNT, sTerm = MI.extractStructures(G_Curr,"r", Util.getOrderIndex(G_Curr), true)
    print("#NTerminals: ", rPId:domain():card())
    print("#Terminals: ", sTerm:card())
    print("#Production Rules:", rPId:card())
    return true, G_Curr
  end
  return "printGrammarInfo", t, false
end
algConst.printGrammarInfo = printGrammarInfo

--- Persist the final grammar to provide a cache for the grammar.
local function cacheInitGrammar()
  local t = function(G_Orig, G_Curr, runConf, cntx)
    if not runConf.transGrammarCache then
      qPrint("cacheGrammar is off.")
      return true, G_Curr
    end
    
    local OrigCRC = Util.md5File(runConf.GrammarFile)
    print("G_Orig CRC:", OrigCRC)
      
    local GenNewCache = false
    local cacheFile = runConf.filePrefix .. ".cache"
    local f = io.open(cacheFile,"r")
    if f then
      local crcCache = f:read()
      crcCache = Util.splitString(crcCache,'=')[2]
      print("Cache file crc:", crcCache)
      f:close()
      if crcCache ~= OrigCRC then GenNewCache = true end
      print("File exists")
    else
      print("File doesn't exists")
      GenNewCache = true
    end
    
    if GenNewCache then
      f = io.open(cacheFile, "w")
      f:write(string.format("CRCGrammar=%s\n", OrigCRC))
      local funcWriteCache = function(s)
        f:write(s.."\n")
        if not runConf.quietExec then print(s) end
        return f
      end
      if not cntx.printOnly then cntx.printOnly = {} end
      cntx.printOnly.OutFunction = funcWriteCache
    end
    
    return true, G_Curr
  end
  return "cacheInitGrammar", t, false
end

algConst.cacheInitGrammar = cacheInitGrammar

--[[== End of Transformation Algorithms ==]]

local function getAlgFunctions(constTab)
  local t = {}
  local tf
  for _,v in ipairs(constTab) do
    tf = algConst[v]
    if tf then t[#t+1] = tf end
  end
  return t
end

--- Transformation function.
function grammarTransformations(G, runConfig, gtAlgs)
  if runConfig then
    quietExec = runConfig.quietExec or quietExec   
  end
  
  --[[
  local transMatrix = {simplifyGrammar, calcMinTreeHeight,
    printGrammarInfo, printOnly}
    ]]
  --local transMatrix = {simplifyGrammar, calcMinTreeHeight}
  --local transMatrix = {simplifyGrammar}
  --local transMatrix = {}
  local transMatrix
  if gtAlgs then
    transMatrix = getAlgFunctions(gtAlgs)
  else 
    transMatrix = {calcMinTreeHeight}
  end
  
  --[[
  if not quietExec then
   transMatrix[#transMatrix+1] = printOnly 
  end
  ]]
  
  if #transMatrix == 0 then
    return G
  end
  
  local retG = setmetatable(G, getmetatable(G))
  local fTransName, fTrans, fTransCritical = nil, nil, false
  local tRet, tmpG = nil, nil
  
  local tmr, r, t
  local transContext = {}
  for i=1,#transMatrix do
    tmr = BenchTimer.new()
    tmr:start()
    fTransName, fTrans, fTransCritical = transMatrix[i]()
    qPrint(string.format("Applying the transformation: %s (Critical? %s, with #G = %i) ...", fTransName, fTransCritical, Util.sizeTable(retG)))
    
    fRet, tmpG = fTrans(G, retG, runConfig, transContext)
    if fTransCritical and not fRet then
      tmr:stop()
      r,t = tmr:timeSum()
      qPrint(string.format("Fail on critical transformation %s. Time:%s", fTransName, t))
      break
    end
    
    if fRet then
      qPrint(string.format("Grammar was changed by transformation %s. #G now is %i", fTransName, Util.sizeTable(tmpG)))
      retG = tmpG
    end
    tmr:stop()
    r,t = tmr:timeSum()
    qPrint(string.format("End of transformation %s. Exe. time %s", fTransName, t))
  end
  
  return retG
end