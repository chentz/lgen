--[[--- Framework to implement a coverage criteria.
@author Cleverton Hentz
]]

require ("init")

local Set = require ("Set")
local Array = require ("Array")
local Stack = require ("Stack")

local Debug = require("dbg")

--config metas
local CoverageCriteria = {ccInstance=nil, GSS=nil, runConfig=nil}

--- Event dispach function.
local function doThings(fcc, f, fName, inInit)
  if fcc and fcc.ccInstance and fcc.ccInstance[fName] and inInit then
    return f(fcc.ccInstance)
  else
    return true
  end
end

--- Dispatched before the definition of the input grammar.
-- @event initGrammar
local function checkInitGrammar(fcc)
  if not fcc.isInitGrammar and not fcc.isInitGen then
    fcc.isInitGrammar = true
    fcc.stackSymbols = Stack.new()
    fcc.metaGrammar = {startSymbol=fcc.GSS,Productions={},Terminals=Set.new(), NTerminals=Set.new()}
    doThings(fcc, function(cc) return cc.initGrammar() end, "initGrammar", fcc.isInitGrammar)
  end
end

function getSymbols(node)
  if node.leaf then
    return Array.new(node.value)
  else
    local rsl = getSymbols(node.nodes[1])
    if node.value then rsl = rsl + Array.new(node.value) end
    for _i=2,#node.nodes do
      rsl = getSymbols(node.nodes[_i]) + rsl
    end
    return rsl
  end
end

function getProds(node)
  local coRsl = coroutine.wrap(function()
    if not node.alt then
      coroutine.yield(getSymbols(node))
    else
      --[[Assumo que nao ira existir encadeamento de "alts". Se nao e
      necessaria a recursao da funcao.]]
      for i,e in ipairs(node.nodes) do
        coroutine.yield(getSymbols(e))
      end
    end
  end)
  
  return coRsl
end

local function getProductions(tree, NT)
  --print("getProductions:Start",NT)
  local rsl = {}
  
  gCo = getProds(tree.root)
  for sym in gCo do
    --print("getProductions",sym)
    rsl[#rsl+1] = sym
  end
  --print("getProductions:End",NT)
  return rsl
end

--- Configure a coverage criteria to a framework instance.
-- @param fcc Framewrok instance
-- @param cc Coverage criteria instance
function CoverageCriteria.setCoverageCriteria(fcc, cc)
  --print("CoverageCriteria.setCoverageCriteria", fcc, cc)
  fcc.ccInstance = cc
end

--- Configure a runtime configuration to a framework instance.
-- @param fcc Framewrok instance
-- @param rc Runtime config table
function CoverageCriteria.setRunConfig(fcc, rc)
  --print("CoverageCriteria.setRunConfig", fcc, rc)
  fcc.runConfig = rc
end

--- Configure the start symbol to a framework instance.
-- @param fcc Framewrok instance
-- @param S The start symbol
function CoverageCriteria.setStartSymbol(fcc, S)
  --print("CoverageCriteria.setStartSymbol", fcc, S)
  fcc.GSS = S
end

--- Dispatched after the definition of the input grammar.
-- @param metaGrammar
-- @event finalizeGrammar

--- Dispatched before the generation execution for the input grammar.
-- @param maxCycles
-- @param maxTreeHeight
-- @param runConfig
-- @param runGrammar
-- @event initGen
function CoverageCriteria.initGen(fcc, maxCycles, maxTreeHeight, runGrammar)
  doThings(fcc,function(cc) return cc.finalizeGrammar(fcc.metaGrammar) end, "finalizeGrammar", fcc.isInitGrammar)
  fcc.isInitGrammar = false
  fcc.isInitGen = true
  fcc.firstInNT = false
  doThings(fcc, function(cc) return cc.initGen(maxCycles, maxTreeHeight, fcc.runConfig, runGrammar) end, "initGen", fcc.isInitGen)
end

--- Dispatched after the generation execution for the input grammar.
-- @param rslFunc Function to add a element on the result set.
-- @param rslDebug Function to add information on the debug file.
-- @event finalizeGen
function CoverageCriteria.finalizeGen(fcc, rslFunc, rslDebug)
  local ret = doThings(fcc,function(cc) return cc.finalizeGen(rslFunc, rslDebug) end, "finalizeGen", fcc.isInitGen)
  return ret
end

--- On the terminal definition
-- @param str Terminal symbol
-- @event onTerminal
function CoverageCriteria.doTerminal(fcc, str)
  checkInitGrammar(fcc)
  fcc.stackSymbols:push({term=true,value=str,leaf=true})
  fcc.metaGrammar.Terminals = fcc.metaGrammar.Terminals:include(str)
  --print("doTerminal",str)
  doThings(fcc, function(cc) cc.onTerminal(str) end, "onTerminal", not fcc.isInitGen)
end

--- On sequence definition.
-- @event onSeq
function CoverageCriteria.doSeq(fcc)
  checkInitGrammar(fcc)
  local p1, p2 = fcc.stackSymbols:pop(), fcc.stackSymbols:pop()
  fcc.stackSymbols:push({seq=true,nodes={p1,p2}})
  --print("doSeq")
  doThings(fcc,function(cc) cc.onSeq() end, "onSeq", not fcc.isInitGen)
end

--- On alternative definition.
-- @param n Number of alternatives
-- @event onAlt
function CoverageCriteria.doAlt(fcc, n)
  checkInitGrammar(fcc)
  
  local node = {alt=true, nodes={}}
  for i=n,1,-1 do
    node.nodes[i] = fcc.stackSymbols:pop()
  end
  fcc.stackSymbols:push(node)
  --print("doAlt", n)
  doThings(fcc,function(cc) cc.onAlt(n) end, "onAlt", not fcc.isInitGen)
end

--- On nonterminal definition.
-- @param str Nonterminal symbol.
-- @event onNonTerminal
function CoverageCriteria.doNonTerminal(fcc, str)
  --print("doNonTerminal", str)
  checkInitGrammar(fcc)
  fcc.stackSymbols:push({NTerm=true,value=str,leaf=true})
  doThings(fcc,function(cc) cc.onNonTerminal(str) end, "onNonTerminal", not fcc.isInitGen)
end

--- On production rule definition.
-- @param str Nonterminal symbol defined by the production.
-- @event onProductionRule
function CoverageCriteria.doProductionRule(fcc, str)
  checkInitGrammar(fcc)
  local tree = {root=fcc.stackSymbols:pop()}
  fcc.metaGrammar.NTerminals = fcc.metaGrammar.NTerminals:include(str)
  local t = getProductions(tree, str)
  
  fcc.metaGrammar.Productions[str] = t
  --print("doProductionRule", str)
  --print("doProductionRule", #t, dbg:tostring(t))
  assert(#t>0)
  doThings(fcc,function(cc) cc.onProductionRule(str) end, "onProductionRule", not fcc.isInitGen)
end

--- On optional definition.
-- @event onOptional
function CoverageCriteria.doOptional(fcc)
  checkInitGrammar(fcc)
  local p1 = fcc.stackSymbols:pop()
  fcc.stackSymbols:push({opt=true,nodes={p1}})
  --print("doOptional")
  doThings(fcc, function(cc) cc.onOptional() end, "onOptional", not fcc.isInitGen)
end

--- On zeroOrMore definition.
-- @event onZeroOrMore
function CoverageCriteria.doZeroOrMore(fcc)
  checkInitGrammar(fcc)
  local p1 = fcc.stackSymbols:pop()
  fcc.stackSymbols:push({zom=true,nodes={p1}})
  --print("doZeroOrMore")
  doThings(fcc, function(cc) cc.onZeroOrMore() end, "onZeroOrMore", not fcc.isInitGen)
end

--- On oneOrMore definition.
-- @event onOneOrMore
function CoverageCriteria.doOneOrMore(fcc)
  checkInitGrammar(fcc)
  local p1 = fcc.stackSymbols:pop()
  fcc.stackSymbols:push({orm=true,nodes={p1}})
  --print("doOneOrMore")
  doThings(fcc, function(cc) cc.onOneOrMore() end, "onOneOrMore", not fcc.isInitGen)
end

--- On oneOrMoreSep definition.
-- @param sep Separator
-- @event onOneOrMoreSep
function CoverageCriteria.doOneOrMoreSep(fcc, sep)
  checkInitGrammar(fcc)
  local p1 = fcc.stackSymbols:pop()
  fcc.stackSymbols:push({ooms=true,value=sep,nodes={p1}})
  fcc.metaGrammar.Terminals = fcc.metaGrammar.Terminals:include(sep)
  --print("doOneOrMoreSep", sep)
  doThings(fcc, function(cc) cc.onOneOrMoreSep(sep) end, "onOneOrMoreSep", not fcc.isInitGen)
end

--- On identify definition.
-- @param context Context table
-- @event onIdent
function CoverageCriteria.doIdent(fcc, context)
  checkInitGrammar(fcc)
  local cSymb = string.format("%%%s%%",context)
  fcc.stackSymbols:push({term=true,value=cSymb,leaf=true})
  fcc.metaGrammar.Terminals = fcc.metaGrammar.Terminals:include(cSymb)
  doThings(fcc,function(cc) cc.onIdent(context) end, "onIdent", not fcc.isInitGen)
end

--- Before add a sentence on the result set.
-- @param str The sentece produced
-- @return Should the sentence be included on the result set?
-- @event checkSentence
function CoverageCriteria.checkSentence(fcc, str)
  return doThings(fcc,function(cc) return cc.checkSentence(str) end, "checkSentence", fcc.isInitGen)
end

--- Before execute the terminal operator.
-- @param str The sentece produced
-- @return Should the terminal be executed?
-- @event checkTerminal
function CoverageCriteria.checkTerminal(fcc, terminal, prodNT)
  local f = fcc.firstInNT
  fcc.firstInNT = false
  return doThings(fcc,function(cc) return cc.checkTerminal(terminal, f, prodNT) end, "checkTerminal", fcc.isInitGen)
end

--- Before execute the nonterminal operator.
-- @param NonTerminal The nonterminal symbol
-- @param f Is it the first in the prodution rule?
-- @param prodNT The nonterminal associated to this nonterminal expansion.
-- @param maxCycles The current height of cycles by used the algorithm.
-- @param maxTreeHeight The current height of derivation tree used by the algorithm.
-- @param lookaheadSymbol The first symbol associated to this nonterminal expansion. The `MetaInfo.cOper` define the value set.
-- @return Should the nonterminal be executed?
-- @event checkNonTerminal
function CoverageCriteria.checkNonTerminal(fcc, nonTerminal, prodNT, maxCycles, maxTreeHeight, lookaheadSymbol)
  --print('checkNonTerminal', nonTerminal, prodNT)
  local f = fcc.firstInNT
  return doThings(fcc, function(cc)
    local r = cc.checkNonTerminal(nonTerminal, f, prodNT, maxCycles, maxTreeHeight, lookaheadSymbol)
    fcc.firstInNT = r --Only when nonterminal is expanded.
    return r
  end, "checkNonTerminal", fcc.isInitGen)
end

--- Before execute the sequence operator.
-- @param f Is it the first in the prodution rule?
-- @param prodNT The nonterminal associated to this nonterminal expansion.
-- @return Should the nonterminal be executed?
-- @event checkSeq
function CoverageCriteria.checkSeq(fcc, prodNT)
  local f = fcc.firstInNT
  fcc.firstInNT = false
  return doThings(fcc,function(cc) return cc.checkSeq(f,prodNT) end, "checkSeq", fcc.isInitGen)
end

--- Before execute the alternative operator.
-- @param f Is it the first in the prodution rule?
-- @param prodNT The nonterminal associated to this nonterminal expansion.
-- @treturn function A function that verify each alternative before execute.
-- @treturn bool Should the alternative be executed?
-- @event checkAlt
function CoverageCriteria.checkAlt(fcc, prodNT)
  local f = fcc.firstInNT
  fcc.firstInNT = false
  return doThings(fcc, function(cc) return cc.checkAlt(f, prodNT) end, "checkAlt", fcc.isInitGen)
end

--- Before execute the ZeroOrMore operator.
-- @param f Is it the first in the prodution rule?
-- @param recNum The limit of repetition define for this instance
-- @param prodNT The nonterminal associated to this nonterminal expansion.
-- @treturn function A function that verify each iteration before execute.
-- @treturn bool Should the operator be executed?
-- @event checkZeroOrMore
function CoverageCriteria.checkZeroOrMore(fcc, recNum, prodNT)
  local f = fcc.firstInNT
  fcc.firstInNT = false
  return doThings(fcc, function(cc) return cc.checkZeroOrMore(f, recNum, prodNT) end, "checkZeroOrMore", fcc.isInitGen)
end

--- Before execute the OneOrMore operator.
-- @param f Is it the first in the prodution rule?
-- @param recNum The limit of repetition define for this instance
-- @param prodNT The nonterminal associated to this nonterminal expansion.
-- @treturn function A function that verify each iteration before execute.
-- @treturn bool Should the operator be executed?
-- @event checkOneOrMore
function CoverageCriteria.checkOneOrMore(fcc, recNum, prodNT)
  local f = fcc.firstInNT
  fcc.firstInNT = false
  return doThings(fcc, function(cc) return cc.checkOneOrMore(f, recNum, prodNT) end, "checkOneOrMore", fcc.isInitGen)
end

--- Before execute the OneOrMoreSep operator.
-- @param f Is it the first in the prodution rule?
-- @param termnalSep The separator terminal
-- @param recNum The limit of repetition define for this instance
-- @param prodNT The nonterminal associated to this nonterminal expansion.
-- @treturn function A function that verify each iteration before execute.
-- @treturn bool Should the operator be executed?
-- @event checkOneOrMoreSep
function CoverageCriteria.checkOneOrMoreSep(fcc, terminalSep, recNumLim, prodNT)
  local f = fcc.firstInNT
  fcc.firstInNT = false
  local r = doThings(fcc, function(cc) return cc.checkOneOrMoreSep(f, terminalSep, recNumLim, prodNT) end, "checkOneOrMoreSep", fcc.isInitGen)
  --print("CoverageCriteria.checkOneOrMoreSep", fcc, f, terminalSep, recNum, recNumLim, r,prodNT)
  return r 
end

--- Before execute the OneOrMoreSep operator.
-- @param f Is it the first in the prodution rule?
-- @param context Context table 
-- @param prodNT The nonterminal associated to this nonterminal expansion.
-- @return Should the operator be executed?
-- @event checkIdent
function CoverageCriteria.checkIdent(fcc, context, prodNT)
  local f = fcc.firstInNT
  fcc.firstInNT = false
  return doThings(fcc, function(cc) return cc.checkIdent(f, context, prodNT) end, "checkIdent", fcc.isInitGen)
end

--- Create a new framework instance to be used
-- @return A framework instance
function createCC()
  local mt = {__index = function(t,k)
    local f = rawget(t, k)
    --print("__index:fcc", t, k, f)
    local ccIns = rawget(t,"ccInstance") 
    if f then
      return f
    elseif ccIns and ccIns[k] then
      checkInitGrammar(t)
      return ccIns[k]
    else
      return f
    end    
  end}
  
  local cc = setmetatable(CoverageCriteria, mt)
  --Init all module local vars
  --print("createCC: Init all module local vars.")
  cc.isInitGen = false
  cc.isInitGrammar = false

  cc.metaGrammar = {}
  cc.stackSymbols = nil
  cc.firstInNT = false
  
  return cc 
end 

return createCC()