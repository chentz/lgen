require("init")
local Set = require("Set")
local Debug = require("dbg")
local Stack = require("Stack")

dbg = Debug:new()
dbg.Enabled = true

require("CDRCoverage/Util")
--require("CDRCoverage/Debug")


-- Pilha usada para armazenar temporariamente os simbolos
-- da gramática durante a fase de construção
local grammarStack = nil

-- Armazena a estrutura da gramática
local grammar = nil

-- Armazena todas as ocorrências da gramática
local occs = nil

-- Armazena as ocorrências cobertas pelas sentenças geradas
local coveredOccs = nil

-- Indica se o próximo não-terminal é o símbolo inicial da gramática
local isStartSymbol = true

-- Armazena a árvore de derivação da sentença atual
local tree = nil

-- Ponteiro para o nó atualmente em análise da árvore
local currentNode = nil

-- Indica se a previsão das derivações está ativada
local previsionEnabled = true
local discardNonTerminal = 0
local discardAlt = 0

-- Tempo
local t0 = nil
local sa = 0
local sd = 0

local quietExec = false

local function qPrint(...)
  if not quietExec then
    print(...)
  end
end

--[[
 - Atualiza a referência para o nó atual, isto é, o nó onde
 - serão criados os próximos subnós da árvore.
--]]
function updateCurrentNode()
  while currentNode.parent and currentNode.vecSize == currentNode.cap do
    currentNode = currentNode.parent
  end
end

local CDRCoverage = {}

function CDRCoverage.initGrammar()
  t0 = os.clock()
  grammar = {}
  
  grammarStack = Stack.new()
end


function CDRCoverage.onProductionRule(nt)
  local tmp = grammarStack:pop()
  local rules = nil
  if #tmp == 1 and tmp[1].t == "alt" then
    rules = tmp[1].v
  else
    rules = {[1] = tmp}
  end
  grammar[nt] = rules

  --setNtOrder(nt) -- debug
end


function CDRCoverage.onNonTerminal(str)
  grammarStack:push({ {t="nonterm", v=str} })
end


function CDRCoverage.onTerminal(str)
  grammarStack:push({ {t="term", v=str} })
end


function CDRCoverage.onSeq()
  local arg1 = grammarStack:pop()
  local arg2 = grammarStack:pop()
  for _, elem in ipairs(arg1) do
    arg2[#arg2 + 1] = elem
  end
  grammarStack:push(arg2)
end


function CDRCoverage.onAlt(n)
  tmp = {}
  for i = n, 1, -1 do
    tmp[i] = grammarStack:pop()
  end
  grammarStack:push({ {t="alt", v=tmp} })
end


function CDRCoverage.onZeroOrMore()
  local arg1 = grammarStack:pop()
  local arg2 = grammarStack:pop()
  grammarStack:push({ {t="zm", v=arg2} })
end


function CDRCoverage.onOneOrMore()
  local arg1 = grammarStack:pop()
  local arg2 = grammarStack:pop()
  grammarStack:push({ {t="om", v=arg2} })
end


function CDRCoverage.onOneOrMoreSep()
  local arg1 = grammarStack:pop()
  local arg2 = grammarStack:pop()
  local arg3 = grammarStack:pop()
  grammarStack:push({ {t="omsep", v=arg3, sep=arg2[1]} })
end

function CDRCoverage.finalizeGrammar(metaGrammar)
  --printGrammar(grammar)
  occs, closure = getOccsFromGrammar(grammar, previsionEnabled)
  coveredOccs = Set.new{}
  --printGrammar(grammar)
  --print(occs)
end

function CDRCoverage.initGen(maxCycles, maxTreeHeight, runConfig)
  quietExec = runConfig.quietExec
  
  qPrint("[T_Fecho = " .. os.clock() - t0 .. "s, Requisitos = " .. occs:card() .. "]")
  qPrint(occs)
  --printClosure(closure)
end

function CDRCoverage.checkTerminal(terminal, firstInNT)
  updateCurrentNode(currentNode)

  currentNode.vecSize = currentNode.vecSize + 1
  currentNode.vec[currentNode.vecSize] = {}

  local node = currentNode.vec[currentNode.vecSize]
  node.symbol = terminal
  node.symbolType = "term"
  node.position = currentNode.vecSize
  node.parent = currentNode

  return true
end


function CDRCoverage.checkNonTerminal(nonTerminal, firstInNT)
  if isStartSymbol then
    isStartSymbol = false

    local root = {}
    root.symbol = nonTerminal
    root.symbolType = "nonterm"
    root.productionId = 1
    root.position = 1
    root.vec = {}
    root.vecSize = 0
    root.cap = 1

    tree = root
    currentNode = root
  else
    updateCurrentNode(currentNode)

    currentNode.vecSize = currentNode.vecSize + 1
    currentNode.vec[currentNode.vecSize] = {}

    local node = currentNode.vec[currentNode.vecSize]
    node.symbol = nonTerminal
    node.symbolType = "nonterm"
    node.productionId = 1
    node.position = currentNode.vecSize
    node.parent = currentNode
    node.vec = {}
    node.vecSize = 0
    node.cap = 1

    currentNode = node
  end

  -- Teste de poda dos não terminais que possuem uma única regra
  if previsionEnabled and #closure[nonTerminal] == 1 then
    local sentenceOccs = Set.intersection(occs, getOccsFromTree(tree))
    if sentenceNotContainsNewOccs(sentenceOccs, coveredOccs) then
      local ruleClosure = closure[nonTerminal][1]
      if isAllRuleOccsCovered(coveredOccs, ruleClosure) then
        discardNonTerminal = discardNonTerminal + 1
        return false
      end
    end
  end

  return true
end


function CDRCoverage.checkSeq(firstInNT)
  updateCurrentNode(currentNode)
  currentNode.cap = currentNode.cap + 1
end


function CDRCoverage.checkAlt(firstInNT)
  if not firstInNT then
      zeroOrMore = false
      updateCurrentNode()

      currentNode.vecSize = currentNode.vecSize + 1
      currentNode.vec[currentNode.vecSize] = {}

      local node = currentNode.vec[currentNode.vecSize]
      node.symbol = "alt"
      node.symbolType = "alt"
      node.productionId = 1
      node.position = currentNode.vecSize
      node.parent = currentNode
      node.vec = {}
      node.vecSize = 0
      node.cap = 1

      currentNode = node
  end

  local localTree = tree
  local localCurrentNode = currentNode

  return function (lastResult, index, count)
    tree = treeCopy(localTree)
    currentNode = getEquivNodeFromTree(tree, localCurrentNode)

    currentNode.productionId = index

    -- Teste de poda dos não-terminais que possuem mais de uma regra
    if previsionEnabled and currentNode.symbolType == "nonterm" then
      local sentenceOccs = Set.intersection(occs, getOccsFromTree(tree))
      if sentenceNotContainsNewOccs(sentenceOccs, coveredOccs) then
        local ruleClosure = closure[currentNode.symbol][index]
        if isAllRuleOccsCovered(coveredOccs, ruleClosure) then
          discardAlt = discardAlt + 1
          return false
        end
      end
    end

    return true
  end
end


function CDRCoverage.checkZeroOrMore(firstInNT, recNum)
  updateCurrentNode()
  currentNode.vecSize = currentNode.vecSize + 1
  currentNode.vec[currentNode.vecSize] = {}

  local node = currentNode.vec[currentNode.vecSize]
  node.symbol = "zm"
  node.symbolType = "zm"
  node.productionId = 1
  node.position = currentNode.vecSize
  node.parent = currentNode
  node.vec = {}
  node.vecSize = 0
  node.cap = 1

  currentNode = node

  local cnt = 0
  return function()
    cnt = cnt + 1
    return cnt < 2
  end
end


function CDRCoverage.checkOneOrMore(firstInNT, recNum)
  updateCurrentNode()
  currentNode.vecSize = currentNode.vecSize + 1
  currentNode.vec[currentNode.vecSize] = {}

  local node = currentNode.vec[currentNode.vecSize]
  node.symbol = "om"
  node.symbolType = "om"
  node.productionId = 1
  node.position = currentNode.vecSize
  node.parent = currentNode
  node.vec = {}
  node.vecSize = 0
  node.cap = 1

  currentNode = node

  local cnt = 0
  return function()
    cnt = cnt + 1
    return cnt < 2
  end
end


function CDRCoverage.checkOneOrMoreSep(firstInNT, recNum)
  updateCurrentNode()
  currentNode.vecSize = currentNode.vecSize + 1
  currentNode.vec[currentNode.vecSize] = {}

  local node = currentNode.vec[currentNode.vecSize]
  node.symbol = "omsep"
  node.symbolType = "omsep"
  node.productionId = 1
  node.position = currentNode.vecSize
  node.parent = currentNode
  node.vec = {}
  node.vecSize = 0
  node.cap = 1

  currentNode = node

  local cnt = 0
  return function()
    cnt = cnt + 1
    return cnt < 2
  end
end

function CDRCoverage.checkSentence()
  local treeOccs = getOccsFromTree(tree)
  local sentenceOccs = Set.intersection(occs, treeOccs)
  local newOccs = Set.diff(sentenceOccs, coveredOccs)
  if Set.card(newOccs) > 0 then
    --printTree(tree)
    --print(sentenceOccs)
    coveredOccs = Set.union(coveredOccs, newOccs)
    local q = Set.card(coveredOccs)
    local n = Set.card(occs)
    if n > 0 then
      local p = math.floor((q / n) * 100)
      --print(string.format("CDRCoverage: %d/%d (%d", q, n, p) .. "%)")
    else
      --print("CDRCoverage: empty")
    end
    sa = sa + 1
    return true
  else
    --printTree(tree)
    --print(treeOccs)
    sd = sd + 1
    return false
  end
end


function CDRCoverage.finalizeGen(rslFunc, rslDebug)
  local ret = {}
  local q = Set.card(coveredOccs)
  local n = Set.card(occs)
  ret.txCoverage = (q/n)*100
  rslDebug(string.format("CDRCoverage{%s}", ret.txCoverage))
  local dSet = occs - coveredOccs;
  ret.NotCover = dSet:table()
  rslDebug(string.format("CDNotCovered%s", tostring(dSet)))
  
  qPrint(dSet)
  qPrint()
  if n > 0 then
    local p = math.floor((q / n) * 100)
    qPrint(
      string.format(
        "\nCDRCoverage: %d/%d (%d", q, n, p
      )
      ..
      "%)\n"
    )
    qPrint("RT", q)
    qPrint("NC", p .. "%")
  else
    --print("\nCDRCoverage: empty\n")
    qPrint("RT", "-")
    qPrint("NC", "-")
  end
  --print("Discard NonTerminal: " .. discardNonTerminal)
  --print("Discard Alt: " .. discardAlt)
  qPrint("T", os.clock() - t0 .. "s")
  qPrint("SA", sa)
  qPrint("SD", sd)
  qPrint("DI", discardNonTerminal + discardAlt)
  --printClosure(closure)
  return ret
end


return CDRCoverage