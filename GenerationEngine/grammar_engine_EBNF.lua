require ("init")
local Collection = require ("Array")
local BenchTimer = require("BenchTimer")
local MI = require("MetaInfo")
local cOper = MI.cOper

gKleeneTmr = BenchTimer.new()

function range(beginR, endR)
  if type(beginR) ~= "string" or
     type(endR) ~= "string" or
     not beginR or
     not endR then error("range definition error!") end
     
  return metaInfo(nil,cOper.range,
    terminal(beginR,false), beginR, endR)
end

function empty(internal)
  return metaInfo(nil,cOper.emp,
  terminal("",internal), internal)
end

function optional(patt)
  cCriteria:doOptional()
  local e = empty(true) 
  return metaInfo(nil,cOper.opt,
  function (g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    --print("optional", prodNT, maxDerivLen)
    --e(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    
    local r = patt(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    
    e(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    return r
  end, patt)
end

--This function is not OK.
function negation(patt)
  return metaInfo(nil,cOper.neg,
    terminal("negation",false), patt)
end

local function kleeneClosure(beginPatt, basePatt, inducPatt, doFunc, chkFunc, dLabel, maxRec)
  doFunc()
  return function (g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    local tmr = BenchTimer.new(gKleeneTmr) 
    tmr:start()
    
    local r = false
    local fChkEach = chkFunc(numRecLimit, prodNT)
    
    if not fChkEach then fChkEach = true end
    --[[
    if not fChkEach then
      error(string.format("kleeneClosure:Check function is nil! (label '%s')",dLabel))
    end
    ]]
    
    if beginPatt then
      tmr:pause()
      r = beginPatt(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
      tmr:resume()
    end
    
    local nRept = maxRec or numRecLimit
    if not inducPatt and nRept > 1 then
      error(string.format("kleeneClosure:Invalid arguments! (label '%s')",dLabel))
    end
    
    if nRept < 1 then
      tmr:stop()
      return r
    end
    
    local fFold = basePatt
    
    for i = 1, nRept do
      if i > 1 then
        fFold = seq(inducPatt, fFold, true)
      end
      
      if (fChkEach==true) or fChkEach(i) then
        tmr:pause()
        r = fFold(g, prefix, maxCycles, maxDerivLen, prodNT, ...) or r
        tmr:resume()
      end
    end
    
    tmr:stop()
    return r
  end
end

function zeroOrMore(patt)
  local doF = function () return cCriteria:doZeroOrMore() end
  local chkF = function (recNum, prodNT) return cCriteria:checkZeroOrMore(recNum,prodNT) end
  return metaInfo(nil,cOper.zm,
  kleeneClosure(empty(true), patt, patt, doF, chkF,cOper.zm), patt) 
end

function oneOrMore(patt)
  local doF = function () return cCriteria:doOneOrMore() end
  local chkF = function (recNum, prodNT) return cCriteria:checkOneOrMore(recNum,prodNT) end
  return metaInfo(nil,cOper.om,
    kleeneClosure(nil, patt, patt, doF, chkF, cOper.om),
    patt)
end

function oneOrMoreSep(patt, str)
  local doF = function () return cCriteria:doOneOrMoreSep(str) end
  local chkF = function (recNum, prodNT) return cCriteria:checkOneOrMoreSep(str,recNum,numRecLimit,prodNT) end
  return metaInfo(nil,cOper.oms,
    kleeneClosure(nil, patt, seq(patt, terminal(str, true), true), doF, chkF, cOper.oms.." "..str),
    patt, str)
end

function oneOrMoreSepNew(patt, str)
  local opId = cOper.oms.."New"
  local doF = function () return cCriteria:doOneOrMoreSep(str) end
  local chkF = function (recNum, prodNT) return cCriteria:checkOneOrMoreSep(str,recNum,numRecLimit,prodNT) end
  
  
  --return patt
  local mf = kleeneClosure(seq(patt,seq(terminal(str, true),patt,true),true),patt, nil, doF, chkF, opId.." "..str,0)
  
  --local mf = kleeneClosure(nil, patt, seq(patt, terminal(str, true), true), doF, chkF, "oneOrMoreSep "..str)
  --print(opId,":",patt.opIdent, patt.args[1],patt.args[2])
  return metaInfo(nil,opId, mf, patt, str)
end

--oneOrMoreSep = oneOrMoreSepNew