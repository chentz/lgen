require("init")
local Set = require("Set")

local NT, Deriv = Set.new{}, Set.new{}
local TC, NDerivTerms = 0
local PC, NDerivProds = 0
local SentC, DerivC = 0,0 

function SentenceCount(t)
  SentC = t[1]
end

function Derivation(t)
  local ts = Set.new(t)
	Deriv = Deriv + ts
	DerivC = DerivC + 1
end

function NonTerm(t)
  NT = Set.new(t)
end

function TerminalCoverage(t)
  TC = t[1]
end

function TerminalNotCovered(s)
  NDerivTerms = Set.new(s)
end

function ProductionCoverage(t)
  PC = t[1]
end

function ProductionNotCovered(s)
  NDerivProds = Set.new(s)
end

dofile(arg[1])

local iNTCobs, iNTCount, iNTNCobs = Set.card(Deriv), Set.card(NT),  Set.card(NT-Deriv)
print("Uncovered nonterminals: ")
print(NT-Deriv)
print("Uncovered terminals: ")
print(NDerivTerms)
print("Uncovered productions: ")
print(NDerivProds)
print("===================================================================")
print("NT Count: " .. iNTCount)
print("NT Covered: " .. iNTCobs)
print("NTN Covered: " .. iNTNCobs)
print("===================================================================")
print("Coverage rate for NT. : " .. string.format("%.3f",(iNTCobs/iNTCount)*100))
print("Coverage rate for Terminals. : " .. string.format("%.3f",TC))
print("Coverage rate for Productions. : " .. string.format("%.3f",PC))
print(string.format("Number of Sentences and Derivations: %d - %d", SentC, DerivC))
