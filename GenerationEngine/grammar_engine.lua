require ("init")
local Debug = require("dbg")
local dbg = Debug:new()
dbg.Enabled = false

--- Unpack compatibility with Lua 5.3 and 5.X
local unpack = unpack or table.unpack

local Array = require ("Array")
local Set = require("Set")

local Util = require("Util")
local MI = require("MetaInfo")
metaInfo = MI.new
cOper = MI.cOper

Array.configToString("{","}",", ")
cCriteria = require("coverageCriteria")

local BenchTimer = require("BenchTimer")

local gNTTmr = BenchTimer.new()
local gDecisionTmr = BenchTimer.new()
local gSeqTmr = BenchTimer.new()
local gAltTmr = BenchTimer.new()
local gAltCheckPattTmr = BenchTimer.new()
local gTermTmr = BenchTimer.new()

local runConfig = {}

--- latest maxDerivLen used.
local lastMDL = nil

--F = {}
--[[
T = setmetatable({}, {
      __index = function (t, k) return terminal(k) end
    })
]]

--Auxiliary functions to memoize some unfeasible path over derivarion
local function checkMemoize(mStore, key, maxDerivLen)
  if not runConfig.useMStorage then return false end
  
  --print("checkMemoize", mStore, key, maxDerivLen)
  local firstStamp = mStore[key]
  if (not firstStamp) or (maxDerivLen > firstStamp) then return false end
  return true
end

--local function checkMemoize(mStore, key, maxDerivLen) return false end

local function saveMemoize(mStore, key, maxDerivLen)
  if not runConfig.useMStorage then return end
  
  --print("saveMemoize", mStore, key, maxDerivLen)
  local firstStamp = mStore[key]
  if firstStamp and maxDerivLen >= firstStamp then return end
  
  mStore[key] = maxDerivLen
end

--local function saveMemoize(mStore, key, maxDerivLen) return false end

local emptyTable = Util.emptyTable
local sizeTable = Util.sizeTable
local incGen = Util.incGen

local fIncNTDiscard = incGen()

function createGrammar(fcc)
  cCriteria = fcc
  return setmetatable({}, {
      __newindex = function (t, k, v)
        if k ~= "startSymbol" then
          cCriteria:doProductionRule(k)
        else
          --print("createGrammar", fcc, v)
          cCriteria:setStartSymbol(v)
        end
        return rawset(t,k,v)
      end
    })
end

function createNonterminalTable()
  return setmetatable({}, {
      __index = function (t, k) return non_terminal(k) end
    })
end

function createTerminalTable()
  return setmetatable({}, {
      __index = function (t, k) return terminal(k) end
    })
end

T = createTerminalTable()

V = createNonterminalTable()
    
G = createGrammar(cCriteria)


function setCoverageCriteria(cc, fcc)
  if fcc then
    cCriteria = fcc
  end
  cCriteria:setCoverageCriteria(cc)
  cCriteria:setRunConfig(runConfig)
end

function setStartSymbol(S)
  cCriteria:setStartSymbol(S)
end

local idTable = {}
--Example: idInc("ID","<%CONTEXT%_%VALUE%>")
function idInc(context, pattern)
  local strTempl = string.gsub(pattern, "%%CONTEXT%%", context)
  local hasValue = false
  if string.find(strTempl, "%%VALUE%%") then
    hasValue = true
  end
  
  cCriteria:doIdent(context)
  --cCriteria:doTerminal(context)
  return metaInfo(nil,cOper.idInc,  
  function(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    cCriteria:checkTerminal(string.format("%%%s%%",context), prodNT)
    local r = cCriteria:checkIdent(context, prodNT)
    if r then
      local sRsl = strTempl
      local s = Array.new({...})
      if strTempl ~= "" then
        if hasValue then
          if not idTable[context] then idTable[context] = 0 end
      
          sRsl = string.gsub(sRsl, "%%LAST_VAL%%", idTable[context])
          idTable[context] = idTable[context] + 1
          sRsl = string.gsub(sRsl, "%%VALUE%%", idTable[context]) 
        end
    
        sRsl = prefix..sRsl
      else sRsl = prefix end
    
      coroutine.yield(sRsl, s)
    end
    return r
  end, context, pattern)
end

--local cTerminal = 0
function terminal(str, internal)
  if not internal then cCriteria:doTerminal(str) end
  return metaInfo(nil,cOper.term, 
  function(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    local tmr = BenchTimer.new(gTermTmr)
    tmr:start()
    --Verifica permissao nos criterios de cobertura
    --dbg:print(string.rep("-", maxDerivLen).." terminal", prefix, maxDerivLen, str)
    local chk = true
    if not internal then chk = cCriteria:checkTerminal(str, prodNT) end
    if chk then
      --local c = os.clock()
      local s = Array.new({...})
      --cTerminal = cTerminal + os.clock()-c
      
      local sRsl
      if str == '' then sRsl = prefix
      else sRsl = prefix..str end
      
      lastMDL = math.min(maxDerivLen, lastMDL)
      
      --dbg:print(string.rep("-", maxDerivLen).." terminal", str, prodNT, maxCycles, maxDerivLen, dbg:tostring{...})
      tmr:stop()
      coroutine.yield(sRsl, s)
      --coroutine.yield(str, true)
      return true
    end
    tmr:stop()
    return false
  end, str, internal)
end

function empty(internal)
  return metaInfo(nil,cOper.emp,
  terminal("",internal), internal)
end

local function iterMinTSortAlts(t)
  local sTab = Util.copy(t)
  local sf = function(a,b)
    --print("sf", a, b)
    if not(a.MinTreeHeight and b.MinTreeHeight) then error("Invalid min. height info.") end
    
    return a.MinTreeHeight < b.MinTreeHeight
  end
  table.sort(sTab, sf)
  return next, sTab, nil
end

--- alt function.
--Memoize over alt is not ready yet. It's necessary make some changes on converage criteria implementations.
function alt(...)
  local fIterator = ipairs
  local patts = {...}
  local sortedPatts = false
  cCriteria:doAlt(#patts)
  local mi = metaInfo(nil,cOper.alt, nil, ...)
  
  local fTmp = function(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    local tmr = BenchTimer.new(gAltTmr)
    local tmrCk = BenchTimer.new(gAltCheckPattTmr)
    tmr:start()
    
    --print("alt:", lIdxAlt)
    --dbg:print(string.rep("-", maxDerivLen).." alt", maxDerivLen, prodNT, ...)
    local rsl = false
    
    --[[
    local hasMKey = checkMemoize(runConfig.mStorage.Alt, lIdxAlt, maxDerivLen)
    if hasMKey then
      tmr:stop()
      return false
    end
    ]]
    
    tmrCk:start()
    local checkPatti = cCriteria:checkAlt(prodNT)
    tmrCk:pause()
    
    if checkPatti then
      local lVArgs = {...}
      local rr = checkPatti
      
      --[[It's not safe until the fix of bug #39
      if runConfig.useMinTSortAlts and not sortedPatts then
        --print(prodNT)
        local n = #patts
        table.sort(patts, function(a,b)
          --print(a,a.index,a:tostring(), a.MinTreeHeight)
          --print(b,b.index,b:tostring(), b.MinTreeHeight)
          if not(a.MinTreeHeight and b.MinTreeHeight) then error("Invalid min. height info. Symbol:"..prodNT) end
        
          return a.MinTreeHeight < b.MinTreeHeight
        end)
        --assert(n==#patts)
        sortedPatts = true
      end
      ]]
      
      --Test config
      if runConfig.useRandomAlts then fIterator = Util.randomIterator
      elseif runConfig.useMinTSortAlts then fIterator = iterMinTSortAlts end
      
      --print(runConfig.useRandomAlts, sortedPatts, runConfig.useMinTSortAlts, fIterator)
            
      --for i, patt in ipairs(patts) do --Iteracao sobre os padroes de ...
      for i, patt in fIterator(patts) do
        --print("alt:",maxDerivLen, prodNT, i,#patts,patt.MinTreeHeight,patt.index,patt.opIdent,rawget(patts,i),prefix)
        --dbg:print(string.rep("-", maxDerivLen).." alt", prodNT, patt.index, patt.MinTreeHeight,"Ite:"..i, dbg:tostring(lVArgs))
        if checkPatti == true then
          rr = true
        else
          tmrCk:resume()
          rr = checkPatti(rsl, i, #patts)
          tmrCk:stop()
        end
        
        tmr:pause()
        if rr and
           patt(g, prefix, maxCycles, maxDerivLen, prodNT, unpack(lVArgs)) then
          rsl = true
        end
        tmr:resume()
      end
    else
      --Caso checkPatti == nil nao executa a funcao
      rsl = false
    end
    
    --if not rsl and not hasMKey then saveMemoize(runConfig.mStorage.Alt, lIdxAlt, maxDerivLen) end
    
    tmr:stop()
    return rsl
  end
  mi.opPatt = fTmp
  return mi
end

local function genCR(patt, g, maxCycles, maxDerivLen, prodNT, vargs)
  return coroutine.create(function(pre)
    local r = patt(g, pre, maxCycles, maxDerivLen, prodNT, unpack(vargs))
    --dbg:print(string.rep("-",maxDerivLen).." seq:patt1", prodNT, maxDerivLen, r)
    if not r then
      return "", nil
      --print("seq:patt1 cut", prodNT, maxDerivLen)
      --coroutine.yield("", nil)
    end
  end)
end
    
function seq(patt1, patt2, internal)
  if not internal then cCriteria:doSeq() end
  local mi = metaInfo(nil,cOper.seq,nil, patt1, patt2, internal)
  
  local f = function(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    local tmr = BenchTimer.new(gSeqTmr)
    tmr:start()
    --[[
    print(prodNT)
    print(patt1:tostring())
    print(mi.params[1]:tostring())
    print(patt2:tostring())
    print(mi.params[2]:tostring())
    ]]
    local varg = {...}
    --dbg:print(string.rep("-",maxDerivLen).." seq", maxDerivLen, prodNT, dbg:tostring{...})
    if not internal then cCriteria:checkSeq(prodNT) end
    
    local m1 = genCR(patt1, g, maxCycles, maxDerivLen, prodNT, varg)
    local cr2, rsl02, allRsl, padding = true, nil, false, nil
    local cr1, stri, rsl
    tmr:stop()
    
    cr1, stri, rsl = coroutine.resume(m1, prefix)
    while cr1 do
      if rsl then
        padding = stri .. " " --Espacamento entre terminais
        --Co-rotina que consome o segundo padrao
        local m2 = genCR(patt2, g, maxCycles, maxDerivLen, prodNT, varg)
        cr2, str2, rsl02 = coroutine.resume(m2,padding)
        while cr2 do
          if rsl02 then
            coroutine.yield(str2, rsl + rsl02)
            allRsl = true
          end
          cr2, str2, rsl02 = coroutine.resume(m2,padding)
        end
      end
      cr1, stri, rsl = coroutine.resume(m1,prefix)
    end
    return allRsl
  end
  mi.opPatt = f
  return mi
end

local iGCCount = 0
local function funcGC()
  iGCCount = iGCCount + 1
  if (iGCCount % 250000) == 0 then
    local c = os.clock()
    local mB,mA = 0, 0
    mB,mA = collectgarbage("count")
    collectgarbage("collect")
    mA = collectgarbage("count")
    --print("funcGC:Memory:", mB, mA, os.clock()-c, iGCCount, os.clock())
  end
end

function non_terminal(nt)
  cCriteria:doNonTerminal(nt)
  return metaInfo(nil,cOper.nt,
  function(g, prefix, maxCycles, maxDerivLen, prodNT, ...)
    local tmr = BenchTimer.new(gNTTmr)
    tmr:start()
    --dbg:print(string.rep("-", maxDerivLen).." non_terminal", prodNT, maxCycles, maxDerivLen, "Symbol: "..nt, dbg:tostring{...})
    if g[nt] == nil then
      error("Erro ao tentar resolver o simbolo: " .. nt)
    end
    
    local rsl, newCL, newDL = false, maxCycles, maxDerivLen
    --Procedimento de calculo das restricoes
    gDecisionTmr:start()
      
    --Check the coverage
    rsl = cCriteria:checkNonTerminal(nt, prodNT, maxCycles, maxDerivLen, g[nt].opIdent)
     
    local tmp
    if rsl then
      local ret = not (nt and maxDerivLen > 0)
      --print("nt:ret 1", ret)
      if (not ret) and maxCycles == 0 then ret = ( prodNT == nt ) end
      --print("nt:ret 2", ret)
      --[[It's not safe until the fix of bug #39]]
      if not ret and 
         runConfig.useMinTSortNT and runConfig.minSymbols[nt] > maxDerivLen then
        --print("NT",nt,runConfig.minSymbols[nt], maxDerivLen)
        ret = true
      end
      --print("nt:ret 3", ret)
      
      if runConfig.useMStorage and not ret then
        --dbg:print(string.rep("-", maxDerivLen).." non_terminal", "memoize checking")
        local hasMKey = checkMemoize(runConfig.mStorage.NT, nt, maxDerivLen)
        if hasMKey then
          --dbg:print(string.rep("-", maxDerivLen).." non_terminal", "memoize Acting")
          --print(dbg:tostring(runConfig.mStorage.NT))
          ret = true
        end
      end
      --print("nt:ret 4", ret)
      
      if ret then
        gDecisionTmr:stop()
        fIncNTDiscard()
        tmr:stop()
        return false
      end
      
      tmp = {...}
      if maxDerivLen > 0 then -- Derivation is ok
        rsl, newCL, newDL = decision(nt, maxCycles, maxDerivLen, tmp)
        --dbg:print(string.rep("-", maxDerivLen).." non_terminal", "decision result:",rsl)
      else
        rsl = false
      end
      --print("nt:ret 5", not rsl, ret)
      
      if runConfig.useMStorage and (not rsl and not hasMKey) then saveMemoize(runConfig.mStorage.NT, nt, maxDerivLen) end
    end
    gDecisionTmr:stop()
    
    if rsl then
      tmp[#tmp+1] = nt
      --Execucao do nao-terminal nt
      --dbg:print("NT:Exec:", nt, prodNT, maxCycles, maxDerivLen, dbg:tostring(tmp))
      tmr:pause()
      if prodNT then
        rsl = g[nt](g, prefix, newCL, newDL, nt, unpack(tmp))
      else
        rsl = g[nt](g, prefix, newCL, newDL, nt, nt)
      end
      tmr:resume()
    end
    
    if not rsl then
      funcGC()
      fIncNTDiscard()
    end --Controle de memoria
    tmr:stop()
    --local r,t = tmr:timeSum()
    --print(string.format("non_terminal: Timer for %s. prodNT: %s, NT: %s and Result: %s", t, prodNT, nt, rsl))
    return rsl
  end, nt)
end

function decision(v, maxCycles, maxDerivLen, derivationPath)
  local newMCy = maxCycles
  local cyNum = find(v, derivationPath)
  --Found cycles
  if cyNum > 0 then
    cyNum=cyNum-1
    newMCy = newMCy-1
  end
  --print("decision", maxCycles, maxDerivLen, v, cyNum, newMCy, dbg:tostring(derivationPath))
  return cyNum <= maxCycles, newMCy, maxDerivLen-1
end

function find(itm, tab)
  local r = 0
  for i, v in ipairs(tab) do
    if v == itm then
      r = r+1
    end
  end
  return r
end

local function qPrint(...)
  if not runConfig.quietExec then
    print(...)
  end
  return ...
end

function genCustom(g, rNum, maxCycles, maxDerivLen, rslFunc, rslDebug, runConf)
  --Configura n. rec para OOMS
  numRecLimit = rNum or 0
  
  maxDerivLen = maxDerivLen or 1
  maxCycles = maxCycles or 1
  rslDebug = rslDebug or print
  
  if not g[g.startSymbol] then
    error(string.format("Invalid start symbol! '%s' isn't a nonterminal.",g.startSymbol))
  end
  
  --Algorithm settings
  --Initialize memoize structures
  runConfig.mStorage = {}
  runConfig.mStorage.Alt = {}
  runConfig.mStorage.NT = {}
  --local DefStorage, DefMinTSortAlts, DefMinTSortNT = false, false, false
  local DefStorage, DefMinTSortAlts, DefMinTSortNT, DefRandomAlts = false, false, true, false
  local DefQuietExec = true
  if runConf then
    runConfig.useMStorage = Util.defVal(runConf.useMStorage, DefStorage)
    runConfig.useMinTSortAlts = Util.defVal(runConf.useMinTSortAlts, DefMinTSortAlts)
    runConfig.useRandomAlts = Util.defVal(runConf.useRandomAlts, DefRandomAlts)
    runConfig.useMinTSortNT = Util.defVal(runConf.useMinTSortNT, DefMinTSortNT)
    runConfig.quietExec = Util.defVal(runConf.quietExec, DefQuietExec)
    if not runConfig.quietExec then qPrint("genCustom:runConf", Util.tostring(runConf)) end
  else
    runConfig.useMStorage = DefStorage
    runConfig.useMinTSortAlts = DefMinTSortAlts
    runConfig.useRandomAlts = DefRandomAlts
    runConfig.useMinTSortNT = DefMinTSortNT
    runConfig.quietExec = DefQuietExec
  end
  
  if runConfig.useMinTSortNT then
    runConfig.minSymbols = MI.getMinSymbolTable(g)
    if not runConfig.minSymbols then
      runConfig.useMinTSortAlts = false
      runConfig.useMinTSortNT = false
      qPrint("genCustom: Min. Tree Height info is missed!")
    end 
  end
 
  cCriteria:initGen(maxCycles, maxDerivLen, g)
  
  local highestTree = 0
  lastMDL = 9999
  
  local co = coroutine.wrap(function()
    non_terminal(g.startSymbol)(g, "",maxCycles, maxDerivLen, nil)
  end)
  local iCount, dCount, c = 0, 0, os.clock()
  local initC = c
  local usedNTerms = Set.new()
  local sTmr = BenchTimer.new()
  local addSent, breakGen
  for str, dbg in co do
    sTmr:start()
    addSent, breakGen = cCriteria:checkSentence(str)
    --qPrint("genCustom", addSent, breakGen, iCount, str)
    if addSent then
      iCount = iCount + 1
      if (iCount % 10) == 0 then qPrint("Sent. Num:",iCount) end
      if not rslFunc(str,iCount) then 
        rslDebug(dbg,iCount)
        usedNTerms = usedNTerms + Set.new(dbg)
        
        highestTree = math.max(highestTree, (maxDerivLen-lastMDL) )
        lastMDL = 9999
      else
        qPrint("genCustom: rslFunc break the gen.!", iCount, str)
        breakGen = true
      end      
    else
      dCount = dCount + 1
    end
    sTmr:stop()
    if breakGen then break end
  end
  local genTime = os.clock()-initC
  rslDebug("--Total Time: " .. genTime)
  rslDebug("--Finished Memory: ".. collectgarbage("count"))
  rslDebug("--Discard NT: " .. iGCCount)
  rslDebug("--Deriv. Count: " .. iCount)
  rslDebug("--Num. Sentences: " .. iCount)
  rslDebug("SentenceCount{" .. iCount .."}")
  
  if not runConfig.quietExec then
    qPrint("--------Starting Debug information-------")
    qPrint("Debug useMStorage:", runConfig.useMStorage)
    qPrint("Debug useMinTSortAlts:", runConfig.useMinTSortAlts)
    qPrint("Debug useRandomAlts:", runConfig.useRandomAlts)
    qPrint("Debug useMinTSortNT:", runConfig.useMinTSortNT)
    
    local r,NTt, NTr, t, accTime
    r, NTt = gNTTmr:timeSum()
    accTime = NTt 
    NTr = gNTTmr:countSum()
    local useMemo = sizeTable(runConfig.mStorage.NT)
    qPrint(string.format("Debug non_terminal:   time: %.6f, global perc.: %.3f, count: %s and use memoize: %s", NTt, (NTt/genTime)*100,NTr,useMemo))
    
    r,t = gDecisionTmr:timeSum()
    r = gDecisionTmr:countSum()
    qPrint(string.format("Debug >>Decision      time: %.6f, local  perc.: %.3f and count: %s", t, (t/NTt)*100,r))
    
    r,t = gSeqTmr:timeSum()
    accTime = accTime + t
    r = gSeqTmr:countSum()
    qPrint(string.format("Debug Seq:            time: %.6f, global perc.: %.3f, count: %s and use memoize: %s", t, (t/genTime)*100, r, false))
    
    local Altt
    r,Altt = gAltTmr:timeSum()
    accTime = accTime + Altt
    r = gAltTmr:countSum()
    useMemo = sizeTable(runConfig.mStorage.Alt)
    --useMemo = false
    qPrint(string.format("Debug Alt:            time: %.6f, global perc.: %.3f, count: %s and use memoize: %s", Altt, (Altt/genTime)*100, r, useMemo))
    
    r,t = gAltCheckPattTmr:timeSum()
    r = gAltCheckPattTmr:countSum()
    qPrint(string.format("Debug >>Check Patt.   time: %.6f, local  perc.: %.3f and count: %s", t, (t/Altt)*100,r))
    
    r,t = gTermTmr:timeSum()
    accTime = accTime + t
    r = gTermTmr:countSum()
    qPrint(string.format("Debug Terminal:       time: %.6f, global perc.: %.3f, count: %s and use memoize: %s", t, (t/genTime)*100, r, false))
    
    if gKleeneTmr then
      r,t = gKleeneTmr:timeSum()
      accTime = accTime + t
      r = gKleeneTmr:countSum()
      qPrint(string.format("Debug Kleene:         time: %.6f, global perc.: %.3f, count: %s and use memoize: %s", t, (t/genTime)*100, r, false))
    end
    
    qPrint(string.format("Debug AllMeasurement: time: %.6f and global perc.: %.3f", accTime, (accTime/genTime)*100))
    
    r,t = sTmr:timeSum()
    qPrint(string.format("Debug Total S. Processing : %.6f and global perc.: %.3f", t, (t/genTime)*100))
    
    qPrint(string.format("Debug Total Gen.      time: %.5f", genTime))
    qPrint(string.format("Debug Gen.            rate: %.2f %s", iCount/genTime,"sent./time unit"))
    qPrint(string.format("Debug Gen. rate (Gen+Disc): %.2f %s", (iCount+dCount)/genTime,"sent./time unit"))
    r = fIncNTDiscard(true)
    qPrint(string.format("Debug NTExp Discard: %s, call perc.: %.3f", r, (r/NTr)*100))
    qPrint(string.format("Debug Memory usage:  %.2f Kbytes", collectgarbage("count")))
    qPrint("----------End Debug----------------------")
    qPrint("Num. Sentences: " .. iCount)
    qPrint("Highest Deriv. Tree: " .. highestTree)
  end
  
  rslDebug("--"..qPrint("Disc. Sentences: " .. dCount))
  --print("local s = Array.new({...}):", cTerminal)
  local ntStr = ""
  for itm, _t in pairs(g) do
    if itm ~= "startSymbol" then
      ntStr = string.format("%s'%s', ",ntStr, itm)
    end
  end
  rslDebug(string.format("NonTerm{%s}",string.sub(ntStr,1,string.len(ntStr)-2)))
  
  local ret = cCriteria:finalizeGen(rslFunc, rslDebug)
  return iCount, usedNTerms, ret, highestTree
end

function gen(g, rNum, maxCycles, maxDerivLen, filePrefix, rc)
  local fHnd = io.open(filePrefix .. ".out", "w+")
  local fHndDebug = io.open(filePrefix .. ".prf", "w+")
  local f, dbgF
  
  if fHndDebug ~= nil then
    dbgF = function(t)
            if type(t) == "table" then
              fHndDebug:write("Derivation" .. Array.tostring(t), "\n")
            else
              fHndDebug:write(t, "\n")
            end
          end
  else
    dbgF = print
  end
  
  if fHnd ~= nil then
    f = function (str,idx)
          fHnd:write(str, "\n")
          if idx % 5 == 0 then fHnd:flush() end
        end
  else
    f = print
  end
  
  local count = genCustom(g, rNum, maxCycles, maxDerivLen, f, dbgF, rc)
  
  if fHnd ~= nil then
    fHnd:flush()
    fHnd:close()
    if not rc.quietExec then
      local md5Out = Util.md5File(filePrefix .. ".out")
      qPrint(string.format("MD5 out file: %s, %s", Util.truncString(md5Out,4,4,'-'), md5Out)
             )
    end
  end
  
  if fHndDebug ~= nil then
    fHndDebug:flush()
    fHndDebug:close()
  end
end

local decTable = nil