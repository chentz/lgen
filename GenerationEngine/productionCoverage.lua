--[[-- Production coverage implementation.
The module implements the production coverage criterion. The main idea to the algorithm is
create a set of test requiremets, the `goalSymbols`, and for each requirement that will be
satisfied by the process it is removed from the intial set. At the end of the process we
check the number of inital requirements over the final ones and calculate the coverage rate.

The main process has the following steps:

1. The basic structures are computed on the event `initGen`. These structures are: 
`productions` and `PRules`. Currently, the `goalSymbols` is defined with all production 
rules, but it is also possible to define another intial set.
1. The events `checkNonTerminal` and `checkAlt` are used to control the `penSymbols` set.
These are also responsible to the optimization over the nonterminal and prodution rules 
expansion to restrict the execution of the algorithm over search spaces that don't contribute
to the coverage criterion coverage.
1. At the end of sentence generation, the `checkSentence` event is used to a final check over
the sentence to see if it will be used or discarted.
1. At the end of the process, the event `finalizeGen` is used to calculate the information about the
achievement of the coverage criterion.

Dependencies: `Util`, `TimeMachine`, `BenchTimer`, `Set`, `Relation`, `MetaInfo`

@author Cleverton Hentz
]]

require ("init")

local Util = require("Util")

local TM = require("TimeMachine")
local BenchTimer = require("BenchTimer")

local Set = require ("Set")
local Rel = require("Relation")
local MI = require("MetaInfo")

Set.configToString("{","}",", ")

local ProductionCoverage = {}

--funcoes especificas
--- The table with all productions rules from the input grammar.
local productions = nil

--- Table with the productions closure used by the algorithm.
local PRules = nil

--- A simplification of the input grammar used to calculate other structures.
local mGrammar = nil

--- The set thats represents the test requirement set.
local goalSymbols = nil
local goalSymbolsInitSize = 0

--- Fixed set of production to be satisfied by the coverage criterion. Use only for debug.
--[[local fixedGoalSymbol = Set.new{"Declaration#1","Declaration#2",
  "Declaration#3","Declaration#4", "Declaration#5", "Declaration#7"}]]
local fixedGoalSymbol = nil

--- The set of production rules used by the derivation processe for the current sentence.
local penSymbols = nil

--- The configuration of the production coverage criterion
local prodConfig = nil

--- Number of reachable production rules from the start symbol. 
local reachablePRCount = 0

local tm = TM:new{}

local currDerivNT, penSymbols, needContinue

local quietExec = true

local function qPrint(...)
  if not quietExec then
    print(...)
  end
end

--- ***Deprecated***, Construct the production information to the algorithm.
-- @tparam table prods Table of productions.
-- @treturn table Return a table with `<NonTerminal>#<Index>` syntax.
local function constructAllProductions(prods)
  local r = Set.new{}
  for _nt, s in pairs(prods) do
    for _i, si in ipairs(s) do
      r = r:include(string.format("%s#%i", _nt, _i))
    end
  end
  return r
end

local iID = 0
local function getIDIdent()
  iID = iID + 1
  return iID
end

local function getNTIndex(str)
  local nt, num = string.match(str, "(.+)#(%d+)")
  return nt, tonumber(num)
end

--- ***Deprecated***, Construct the transitive and reflexive closure from production rules.
-- @tparam table prods Table of productions.
-- @treturn table Return a table with `[<NonTerminal>] = {<NonTerminal>#<Index>}` syntax.
-- @see constructAllProductions
local function calcPRules(prods)
  local change
  local tNT, tmpS, rEqui
  local iCount, cCount = 0, 0
  
  --print("calcPRules:Inicio")
  repeat
  change = false
  for _nt, v in pairs(prods) do
    for _j=1,#v do
      local vj = v[_j]
      for _i, v2 in pairs(vj:internalSet()) do
        local iNT, iIndex = getNTIndex(_i)
        local sa = prods[_nt][_j] + prods[iNT][iIndex]
        if not (sa == prods[_nt][_j]) then
          prods[_nt][_j] = sa
          change = true
        end
      end
    end
  end
  until not change
end

local function ntSymbols(preds, gNT)
  local function getNTProductions(preds, nt)
    local rsl = {}
    --print(nt)
    for i, s in ipairs(preds[nt]) do
      rsl[#rsl+1] = nt .. "#" .. i
    end
    return Set.new(rsl)
  end
  
  local r1, r2 = {}
  for k, nt in pairs(preds) do
    r2 = {}
    for _i, s in ipairs(nt) do
      local sR = Set.new(string.format("%s#%i", k, _i))
      local eNT = s * gNT
      for iNT in pairs(eNT:internalSet()) do --cada NT
        sR = sR + getNTProductions(preds, iNT)
      end
      r2[#r2+1] = sR
    end
    r1[k] = r2
  end
  return r1
end

--- Check if the algorithm should continue.
-- Check is based on the `PRules` table and `goalSymbols` set.
-- @tparam string nonTerminal NT to verify.
-- @tparam number index Production index to verify
-- @treturn{nil,number,bool} It is OK or not ot go.
local function checkNeedContinue(nonTerminal, index)
  --print("checkNeedContinue", nonTerminal, index)
  local preds = PRules
  
  local cSymSize = goalSymbols:card()
  if (cSymSize == 0) or (not preds[nonTerminal]) then return nil end
  
  --print("checkNeedContinue", nonTerminal, index, #preds[nonTerminal], cSymSize)
  if index then
    local s = preds[nonTerminal][index]
    
    assert(s, "checkNeedContinue: Invalid 's' value! On NT "..nonTerminal)
    return (goalSymbols * s):card() > 0
  else
    local ntList = preds[nonTerminal]
    assert(ntList, "checkNeedContinue: Invalid 'ntList' value! On NT "..nonTerminal)
    for i, s in ipairs(ntList) do
      if (goalSymbols * s):card() > 0 then
        return i
      end
    end
  end
  return nil
end

--- Check if there are some grammar inconsistency
-- @param metaGrammar The meta-grammar
-- @return Is the grammar OK?
-- @return If non OK, present a error message
local function checkGrammar(metaGrammar)
  local sCheck = (metaGrammar.NTerminals * metaGrammar.Terminals)
  if sCheck:card() > 0 then
    return false, "The grammar is not context-free. (T must be disjoint of V): " .. sCheck:tostring()
  end
  
  local defSet, useSet = Set.new(), Set.new();
  for i, v in pairs(metaGrammar.NTerminals:internalSet()) do
    --print("CG1",i,v,metaGrammar.startSymbol)
    defSet = defSet:include(i)
  end
  
  for i,v in pairs(metaGrammar.Productions) do
    --print("CG2",i,v,#v)
    for i2,v2 in pairs(v) do
      --print("CG2.1",i2,v2)
      for i3,v3 in pairs(v2:internalSet()) do
        --print("CG2.2",i3,v3)
        if not metaGrammar.Terminals:inSet(i3) then useSet = useSet:include(i3) end
      end
    end
  end
  
  --print("checkGrammar: useSet", useSet)
  --print("checkGrammar: defSet", defSet)
  local rS1, rS2 = useSet - defSet, defSet - useSet 
  if rS1:card() > 0 then
    qPrint("checkGrammar:useSet - defSet", rS1)
    return false, "Some non-terminals aren't defined."
  end
  
  local tmp = rS2:card()
  if tmp > 0 and
     not(tmp == 1 and rS2:inSet(metaGrammar.startSymbol)) then
    qPrint("checkGrammar:defSet - useSet", rS2)
    return false, "Some non-terminals are defined but not used."
  end
  
  return true
end

--- Convert internal representation of production from Array to Set.
local function convertProdSet(prod)
  local rsl = {}
  for i,v in pairs(prod) do
    --print("convertProdSet", i,v)
    rsl[i] = prod[i]
    for i2,v2 in pairs(v) do
      --print("convertProdSet:2", i2,v2)
      rsl[i][i2] = Set.new(v2)
    end
  end
  return rsl
end

local f1 = function(a) return Util.truncString(Util.tostring(a),100,50) end

--- calculate the reachable production rules from the start symbol
local function calcReachablePR(start, prodClosure)
  local t = prodClosure[start]
  if not t then error("calcReachablePR: Invalid start symbol!") end
  local ret = Set.new()
  for _,s in ipairs(t) do
    ret = ret + Set.new(s)
  end
  return ret:card()
end

--- Generate information from the actual grammar to the algorithm.
-- @tparam table G The grammar received after the transformation process.
-- @tparam number maxTreeHeight The maximum derivation tree height from runtime configuration.
-- @treturn table Return a table with the fields: <br>
-- <ul>
-- <li>`Terminals`: (**Set**)</li>
-- <li>`NTerminals`: (**Set**)</li>
-- <li>`startSymbol`: (**String**)</li>
-- <li>`Productions`: (**table**)</li>
-- <li>`ClosureProductions`: (**table**)</li>
-- </ul>
local function genMetaGrammar(G, maxTreeHeight)
  local mG = {}
  local _,rPId, rSym, sExcNT, sTerm = MI.extractStructures(G,nil, Util.getOrderIndex(G), true)
  --rPId = {<NT,prodID>}
  --rSym = {<prodID,NT>}
  mG.Terminals = sTerm
  mG.NTerminals = rPId:domain()
  mG.startSymbol = G.startSymbol
  
  --[[
  print("rPId", rPId:card(), mG.NTerminals:card(), rPId)
  print("rSym", rSym:card(), f1(rSym))
  print("sTerm", sTerm:card(), f1(sTerm))
  ]]
  
  --Calculate the productions rules and collect info to the closure computation
  local retTab = {}
  local prods = Set.new()
  local s,a,b
  local rProdIdx = Rel.new()
  rPId:filterOverRelation(Rel.getSetIterationSort, function(k,v)
    kTab = {}
    
    inc = 1
    for e in v:getSortIterator() do
      --print(k,e, inc)
      s = string.format("%s#%s", k, inc)
      prods:include(s)
      b = Set.new{s}
      
      a = rSym%e
      if a then b = b + a end
      
      rProdIdx:include(k, b, true)
      
      kTab[inc] = Set.new(b)
      inc = inc+1
    end
    --print(k,e, s, b, rProdIdx%k)
    
    retTab[k] = kTab
    
    return true
  end)
  mG.Productions = prods
  
  --print("B1", Util.tostring(retTab.Lit_integer))
  --print("B2", rProdIdx%'Lit_integer')
  
  --rProdIdx, a = rProdIdx+0
  rProdIdx, a = rProdIdx+maxTreeHeight
  --[[
  rProdIdx = rProdIdx:filterOverRelation(Rel.getElemIteration, function(k,v)
    --print(k,v)
    return v:find('#')
  end, false)
  
  print("rProdIdx", f1(rProdIdx))
  print("rProdIdx", rProdIdx%'Lit_integer')
  ]]
  
  local tmpTab = {}
  local tmpSet
  for k,v in pairs(retTab) do 
    --print(k,Util.tostring(v))
    tmpTab[k] = {}
    for idx, v2 in ipairs(v) do
      --print(idx,v2)
      tmpSet = Set.new(v2 - mG.NTerminals)
      --print('>>> ', tmpSet)
      for e in pairs(v2 * mG.NTerminals) do
        tmpSet = tmpSet + ( (v2 + rProdIdx%e) - mG.NTerminals )
      end
      --print('==== ', tmpSet)
      tmpTab[k][idx] = tmpSet
    end
  end
  retTab = nil
  retTab = tmpTab
  --print("C1", Util.tostring(retTab))
  --print("C1", Util.tostring(retTab.Arithmetic_expression))
  mG.ClosureProductions = retTab
  
  --[[
  print(Util.tostring(mG.Productions))
  print()
  print(Util.tostring(mG.ClosureProductions.Lit_integer)) --:sub(1,160)
  print(Util.tostring(mG.ClosureProductions.Arithmetic_expression))
  ]]
  return mG
end

--- It happens after the reading of original grammar.
-- @param MG A meta-grammar
-- @event finalizeGrammar
-- @see GenerationEngine.coverageCriteria.finalizeGrammar
function ProductionCoverage.finalizeGrammar(MG)
  mGrammar = MG
  mGrammar.Productions = convertProdSet(MG.Productions)
  
  --check Grammar for errors.
  local err, errM = checkGrammar(mGrammar)
  if not err then
    qPrint("There are warnings about the grammar elements!")
    qPrint(errM)
  end
end

--[[-- It happens before the generation process.
On this function are created the basic structures to the algorithm:

* `productions` table is used to store the productions rules for the grammar;
* `PRules` table is used to store the production closure;
* `goalSymbols` set is used to define the productions' set used as goal to the algorithm.

@event initGen

@see GenerationEngine.coverageCriteria.initGen
]]
function ProductionCoverage.initGen(maxCycles, maxTreeHeight, runConfig, runGrammar)
  quietExec = runConfig.quietExec
  penSymbols = Set.new()
  needContinue=false
  prodConfig = runConfig.productionCoverage
  
  if prodConfig then
    fixedGoalSymbol = Util.defVal(prodConfig.goalSymbols, nil)
  end
  
  qPrint("ProductionCoverage.initGen", maxCycles, maxTreeHeight)
  
  local btCalcNT = BenchTimer.new()
  btCalcNT:start()
  
  if true then
    qPrint("ProductionCoverage.initGen: New Grammar structure Algorithm!")
    mGrammar = genMetaGrammar(runGrammar, maxTreeHeight)
    productions = mGrammar.Productions
    PRules = mGrammar.ClosureProductions
    
    --Collect info to the relative coverage.
    reachablePRCount = calcReachablePR(runGrammar.startSymbol, mGrammar.ClosureProductions)
  else
    qPrint("ProductionCoverage.initGen: Old Grammar structure Algorithm!")
    productions = constructAllProductions(mGrammar.Productions)
    PRules = ntSymbols(mGrammar.Productions, mGrammar.NTerminals)
    calcPRules(PRules, mGrammar.NTerminals)
  end
  
  if fixedGoalSymbol then --Debug only
    goalSymbols = Set.new(fixedGoalSymbol)
  else
    --Set Goal to the criterion
    goalSymbols = Set.new(productions)
    --goalSymbols = Set.new{'FunctionDeclaration#1'}
    --print(PRules.FunctionDeclaration[1])
  end
  
  goalSymbolsInitSize = goalSymbols:card()
  
  qPrint("ProductionCoverage.initGen", "goalSymbols Hash", Util.truncString(Util.hashCode(goalSymbols), 4,4,'-'))
  qPrint("ProductionCoverage.initGen", "goalSymbols", f1(goalSymbols))
  qPrint("ProductionCoverage.initGen", "productions Hash", Util.truncString(Util.hashCode(productions), 4,4,'-'))
  qPrint("ProductionCoverage.initGen", "productions", f1(productions))
  qPrint("ProductionCoverage.initGen", "PRules Hash", Util.truncString(Util.hashCode(PRules), 4,4,'-'))
  qPrint("ProductionCoverage.initGen", "PRules", f1(PRules))
  
  btCalcNT:stop()
  local r, v = btCalcNT:timeSum()
  qPrint("ProductionCoverage.initGen", "Time to calcNTerms:",v)
end

local function txCoverage(refVal, val)
  --print("txCoverage goalSymbols:", goalSymbols)
  if not refVal or refVal == 0 then return 0 end
  
  return math.min(100,
    ( val/refVal )*100 )
end

--- Collect the information about the results
-- @event finalizeGen
-- @see GenerationEngine.coverageCriteria.finalizeGen
function ProductionCoverage.finalizeGen(rslFunc, rslDebug)
  local ret = {}
  local usedProductions = goalSymbolsInitSize-goalSymbols:card()
  --print("ProductionCoverage.finalizeGen", goalSymbolsInitSize, reachablePRCount, goalSymbols:card(), usedProductions)
  ret.txCoverage = txCoverage(goalSymbolsInitSize, usedProductions)
  ret.txCoverageRel = txCoverage(reachablePRCount, usedProductions)
  ret.NotCover = goalSymbols:table()
  
  local s1 = string.format("ProductionCoverage{%s}", ret.txCoverage)
  qPrint("ProductionCoverage.finalizeGen", s1)
  rslDebug(s1)
  rslDebug(string.format("ProductionNotCovered%s", tostring(goalSymbols)))
  
  --[[
  print("N. Productions:", Set.card(productions))
  print("finalizeGen:productions:", tostring(productions))
  print("finalizeGen:goalSymbols:", tostring(goalSymbols))
  ]]
  return ret
end

--- Verify if the nonterminal contribute with a requirement.
-- @event checkNonTerminal
-- @see GenerationEngine.coverageCriteria.checkNonTerminal
function ProductionCoverage.checkNonTerminal(nonTerminal, firstInNT, prodNT, maxCycles, maxTreeHeight, lookaheadSymbol)
  currDerivNT = nonTerminal
  local r = needContinue or checkNeedContinue(nonTerminal) ~= nil
  --print("ProductionCoverage.checkNonTerminal", prodNT, nonTerminal, r, lookaheadSymbol)
  
  --[[
  The only situation that production id 1 is not always added
  is to the alt symbol. The others always use the first production.
  ]]
  if r and lookaheadSymbol ~= MI.cOper.alt then
    penSymbols = penSymbols:include(nonTerminal.."#1")
  end
  return r
end

--- Verify if the sentence contribute with a requirement.
-- If the sentence has new requirements, its is added 
-- to the result set and his requiriments are removed from 
-- the `goalSymbols` set.
-- @event checkSentence
-- @see GenerationEngine.coverageCriteria.checkSentence
local iCnt = 0
function ProductionCoverage.checkSentence()
  if goalSymbols:card() == 0 then
    return false, true
  end
 
  if (iCnt % 1500) == 0 then
    qPrint("Sent. Count:", iCnt)
    qPrint(string.format("ProductionCoverage{%s}", txCoverage()))
  end
  iCnt = iCnt + 1
  
  --s mantem as novas regras da derivacao corrente.
  local s = penSymbols * goalSymbols
  if s:card() > 0 then
    --print("checkSentence:",goalSymbols, penSymbols, s)
    goalSymbols = goalSymbols - s
    needContinue = false
    return true
  else
    return false
  end
end

--- Verify if the alternative contribute with a requirement.
-- If an alternative or production rule doesn't contribute to the 
-- coverage satisfaction, it is not executed.
-- @event checkAlt
-- @see GenerationEngine.coverageCriteria.checkAlt
function ProductionCoverage.checkAlt(firstInNT, prodNT)
  local cNT
  
  if prodNT then
    cNT = prodNT
  else
    cNT = currDerivNT
  end
  
  local pSym, nCont = penSymbols, needContinue
  local label = string.format("%s%s-%s", cNT, os.clock(), getIDIdent())
  local cnt = tm:current()
  --print("checkAlt:Bind penSymbols", penSymbols, label)
  cnt.penSymbols = penSymbols
  tm:savePoint(label)
  return function(lastResult, index, count)
    --print("Anom1I", label, index, count)
    if index > 1 then
      --Restaura ambiente
      --print("checkAlt:UnBind1 penSymbols", penSymbols, label)
      --print("checkAlt:UnBind1 cnt.penSymbols", cnt.penSymbols, label)
      tm:rollBack(label, index ~= count)
      penSymbols = cnt.penSymbols
      --print("checkAlt:UnBind2 penSymbols", penSymbols, label)
      --print("checkAlt:UnBind2 cnt.penSymbols", cnt.penSymbols, label)
      
      --Uma vez gerada uma sentenca que tem regras novas,
      --entao a variavel de controle needContinue deve
      --voltar a ser false.
      if lastResult then needContinue = false
      else needContinue = nCont end
    end
    
    local i = nil
    if firstInNT then i = index end
    if needContinue or checkNeedContinue(cNT, i) then
      needContinue = true
      
      if firstInNT then
        penSymbols = penSymbols:include(cNT.."#"..index)
      end
      --print("Anom1F1", label)
      return true
    else
      --print("Anom1F2", label)
      return false
    end
  end
end

--- Verify if the operator oneOrMoreSep contribute with a requirement.
-- @event checkOneOrMoreSep
-- @see GenerationEngine.coverageCriteria.checkOneOrMoreSep
function ProductionCoverage.checkOneOrMoreSep(firstInNT, terminalSep, recNum, prodNT)
  return function(idx)
    return idx <= 1 -- for this Coverage criterion this not matter.
  end
end

return ProductionCoverage