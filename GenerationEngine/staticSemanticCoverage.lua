require ("init")

local Set = require("Set")

local Debug = require("dbg")
dbg = Debug:new()
dbg.Enabled = true

--- Unpack compatibility with Lua 5.3 and 5.X
local unpack = unpack or table.unpack

local mt = {}
local StaticSemanticCoverage = {}
setmetatable(StaticSemanticCoverage, mt)

--funcoes especificas
local wCov = nil
local currID = nil
local rExp = nil
local identContext = nil

local function index(t, k)
  local f = rawget(t, k)
  if f then
    return f
  elseif wCov[k] then
    return wCov[k]
  end
  return nil
end

mt.__index = index

local idRandom = 0
local function getIdentFromContext()
  local n = Set.card(identContext)
  if n == 0 then print("zero") return rExp() end
  
  local i = (idRandom % n) + 1
  idRandom = idRandom + 1
  local ls = Set.table(identContext)
  if i <= #ls then
    return ls[i]
  else
    return "ID"
  end
end

local function wrappedCall(fName, ...)
  if wCov[fName] then
    return wCov[fName](unpack(arg))
  end
end

function StaticSemanticCoverage.wrappedCoverage(wrpCoverage)
  wCov = wrpCoverage
  if not wCov then
    error("You must configure wrappedCoverage")
  end
end

--Implementacao generica
function StaticSemanticCoverage.initGen()
  identContext = Set.new{}
  wrappedCall("initGen")
end

function StaticSemanticCoverage.onIdent(exp)
  rExp = exp
end

function StaticSemanticCoverage.checkSentence()
  currID = nil
  return wrappedCall("checkSentence")
end

function StaticSemanticCoverage.checkIdent(firstInNT)
  local r = currID
  if not r then
    r = getIdentFromContext()
  else
    currID = nil
  end
  return true, r
end

function StaticSemanticCoverage.checkAlt(firstInNT)
  local iSet = identContext
  local wf = wrappedCall("checkAlt", firstInNT)
  return function(lastResult, index, count) --checkPattI
    if index > 1 then
      --Restaura ambiente
      identContext = iSet
    end
    return wf(lastResult, index, count)
  end
end

function StaticSemanticCoverage.newContext(firstInNT)
  local id = rExp()
  currID = id
  identContext = identContext + Set.new{id}
end

return StaticSemanticCoverage
