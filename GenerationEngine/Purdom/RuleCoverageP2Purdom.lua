local Debug = require("dbg")

Debug.Enabled = false

local PhaseTwo = {}
local max_int = 65555

function PhaseTwo.init(G)
  local DLEN, PREV = {}, {}
	local nonTerminal = G:nonTerminals()
	
  for i, n in pairs(nonTerminal) do
	  DLEN[n] = max_int
    PREV[n] = -1
  end
	
	return DLEN, PREV
end

function PhaseTwo.process(G, SLEN, RLEN, DLEN, PREV)
  local change = true
	while change do
	  change = false
		DLEN[G:Start()] = SLEN[G:Start()]
		
		local continue
		local prods = G:productions()
		for i=0, #prods do
		  local p = prods[i]
		  Debug:print(i, p)
		  continue = false
			
		  if RLEN[i] == max_int then continue = true end
			if DLEN[p.LHS] == max_int then continue = true end
			if SLEN[p.LHS] == max_int then continue = true end
			
			if not continue then
				local sum = DLEN[p.LHS] + RLEN[i] - SLEN[p.LHS]
				for idx, n in pairs(G:nonTerminals(i)) do
				  Debug:print(idx, n)
					if sum < DLEN[n] then
						change = true
						DLEN[n] = sum
						PREV[n] = i
					end
				end --for
			end --if
		end --for
	end --while
end

return PhaseTwo
