local Debug = require("dbg")
local Stack = require("Stack")
local Trace = require("Trace")


local PhaseThree = {}
local max_int = 65555

local Null, Ready, Unsure, Finished = -1, -2, -3, -4

local PREV, SHORT = nil,nil
local STACK = Stack.new()

function PhaseThree.init(G, P, S)
  local t = Trace:new("init")
  t:beginTrace(P, S)
	PREV = P
	SHORT = S
	
	local nonTerminal = G:nonTerminals()
	local productions = G:productions()
	local ONCE,ONST,MARK = {},{},{}
	
  for i, n in pairs(nonTerminal) do
	  ONCE[n] = Ready
    ONST[n] = 0
  end
	
	for i, p in pairs(productions) do
	  MARK[i] = false
  end	
	t:endTrace(ONCE, ONST, MARK)
	return ONCE, ONST, MARK
end

local function short(ONCE, MARK, SHORT, nt)
  local t = Trace:new("short")
  t:beginTrace(ONCE, MARK, SHORT, nt)
  local prod_no = SHORT[nt]
	MARK[prod_no] = true
	if ONCE[nt] ~= Finished then
    ONCE[nt] = Ready
    t:body(1,"Ready",nt)
  end
	t:endTrace(ONCE, MARK, SHORT, nt, prod_no)
	return prod_no
end

local function load_ONCE(G, ONCE, MARK)
  local t = Trace:new("load_ONCE")
  t:beginTrace(ONCE, MARK)

	local set = Set.new{}
	local lhs
	local prods = G:productions()
  for i=0, #prods do
	  local p = prods[i]
	  t:body(1, "for", i, p)
		lhs = G.prod[i].LHS
	  if (MARK[i] == false) and (not Set.inSet(lhs, set)) then
		  t:body("1.1", "if (MARK[i] == false)", lhs)
		  set = Set.union(Set.new{lhs}, set)
		  ONCE[lhs] = i
		  MARK[i] = true
		end
	end

--[[
	local lhs
	local prods = G:productions()
  for i=0, #prods do
	  local p = prods[i]
	  t:body(1, "for", i, p)
		lhs = G.prod[i].LHS
	  if (MARK[i] == false) then
		  ONCE[lhs] = i
		  MARK[i] = true
		end
	end
]]
	t:endTrace(ONCE, MARK)
end

local function process_STACK(G, ONST, nt, prod_no, do_sentence, txtSentence)
  local t = Trace:new("process_STACK")
  t:beginTrace(ONST, nt, prod_no, do_sentence)
	
  ONST[nt] = ONST[nt] - 1
	local currRHS = G.prod[prod_no].RHS
	for i = 1, #currRHS do
	  local e = currRHS[i]
		t:body(1, "for", i, e)
		
	  Stack.push(STACK, e)
		if G:isNonTerminal(e) then
		  t:body("1.1", "if G:isNonTerminal(e)", e, ONST[e])
		  ONST[e] = ONST[e] + 1
		end
	end --for
	
	done = false
	while (not done) do
	  t:body("1.2", "while (not done)", STACK)
	  if Stack.empty(STACK) then
		  t:body("1.2.1","break")
		  do_sentence = false
			break
		else
		  t:body("1.2.2", "else")
		  nt = Stack.top(STACK)
			Stack.pop(STACK)
			t:body("1.2.3", "if", nt)
			if G:isTerminal(nt) then
			  t:body("1.2.3.1", "if G:isTerminal(nt)", nt)
				txtSentence = txtSentence .. nt
			else
			  t:body("1.2.3.2", "else")
			  done = true
			end --if
		end --if
	end -- while
	
	t:endTrace(ONST, nt, prod_no, do_sentence)
	return nt, prod_no, do_sentence, txtSentence
end

function PhaseThree.process(G, PREV, SHORT, ONCE, ONST, MARK)
	local t = Trace:new("process")
	
	t:beginTrace(PREV, SHORT, ONCE, ONST, MARK)
  local done = false
	local sentences = {}
	prod_no = 0 --Null
	local nt, do_sentence, once_nt
		
	while (not done) do
	  t:body(1, "while (not done)", ONCE, MARK, ONST)
	  if ONCE[G:Start()] == Finished then
		  t:body("1.1", "break")
		  break
		end
		ONST[G:Start()] = 1
		nt = G:Start()
		do_sentence = true
		local txtSentence = ""
		while do_sentence do
		  t:body("1.2", "while (do_sentence)", nt)
		  once_nt = ONCE[nt]
			if nt == G:Start() and once_nt == Finished then
			  Debug:print("do_sentence-1")
				t:body("1.2.1", "break")
			  done = true
				break
			elseif once_nt == Finished then
			  t:body("1.2.2", "short")
			  prod_no = short(ONCE, MARK, SHORT, nt)
			elseif once_nt >= 0 then
			  t:body("1.2.3", "once_nt >= 0", once_nt, nt, ONCE[nt])
			  prod_no = once_nt
				ONCE[nt] = Ready
			else
			  t:body("1.2.4", "else", ONCE, MARK)
			  load_ONCE(G, ONCE, MARK)
				Debug:print("ONCE:", ONCE)
				Debug:print("MARK:", MARK)
				for idx, I in pairs(G:nonTerminals()) do
				  if I ~= G:Start() and ONCE[I] >= 0 then
					  local J = I
						local K = PREV[J]
						t:body("1.2.4.1", "if ...", I, K, ONCE[I])
						while K >= 0 do
						  t:body("1.2.4.1.1", "while", K)
						  J = G.prod[K].LHS
							if ONCE[J] >= 0 then -- E uma ref para regra
							  t:body("1.2.4.1.2", "break")
							  break
							else -- Uma constante
							  if ONST[I] == 0 then
								  t:body("1.2.4.1.3", "if ONST[I] == 0", MARK[K])
								  ONCE[J] = K
									MARK[K] = true
								else
								  t:body("1.2.4.1.4", "else", ONCE[J])
								  ONCE[J] = Unsure
								end --if
							end --if
							K = PREV[J]
						end --while
					end --if
				end --for
				
				t:body("1.2.5", "do_sentence", ONST, nt)
				for i, n in pairs(G:nonTerminals()) do
				  if ONCE[n] == Ready then
					  t:body("1.2.5.1", "if ONCE[n] == Ready", n)
					  ONCE[n] = Finished
					end
				end --for
				
				t:body("1.2.6", "do_sentence", ONCE[nt])
				if nt == G:Start() and ONCE[nt] < 0 and ONST[G:Start()] == 0 then
				  t:body("1.2.6.1", "break")
				  break
				elseif ONCE[nt] < 0 then
				  t:body("1.2.6.2", "short",nt)
				  prod_no = short(ONCE, MARK, SHORT, nt)
				elseif ONCE[nt] >= 0 then
				  t:body("1.2.6.3", "elseif ONCE[nt] >= 0")
				  prod_no = ONCE[nt]
					ONCE[nt] = Ready
				end --if
				
			end --if..else
			t:body("1.2.7", "process_STACK")
			nt, prod_no, do_sentence, txtSentence = process_STACK(G, ONST, nt, prod_no, do_sentence, txtSentence)
			
			Debug:print("ONCE:", ONCE)
      Debug:print("ONST:", ONST)
      Debug:print("MARK:", MARK)
			Debug:print("txtSentence:", txtSentence)
		end--while
		t:body("1.3", "txtSentence", txtSentence)
		sentences[#sentences+1] = txtSentence
		--break
	end -- While
	t:endTrace(PREV, SHORT, ONCE, ONST, MARK)
	return sentences
end

return PhaseThree
