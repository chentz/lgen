require ("init")

Debug = require("dbg")
PhaseOne = require("RuleCoverageP1Purdom")
PhaseTwo = require("RuleCoverageP2Purdom")
PhaseThree = require("RuleCoverageP3Purdom")
Set = require("Set")

Debug.Enabled = true

local G

G = {prod = { [0]={LHS="S"; RHS={"E"}};
		          [1]={LHS="E"; RHS={"E","+","E"}};
              [2]={LHS="E"; RHS={"ID"}} };
		 term = {"+","ID"};
		 nonTerm = {"S","E"};
		 start = "S"
}

--[[
G = {prod = { [0]={LHS="S"; RHS={"E"}};
		          [1]={LHS="E"; RHS={"E","+","E"}};
							[2]={LHS="E"; RHS={"E","*","E"}};
              [3]={LHS="E"; RHS={"ID"}} };
		 term = {"+","*","ID"};
		 nonTerm = {"S","E"};
		 start = "S"
}
]]

--[[
G = {prod = { [0]={LHS="S"; RHS={"E"}};
		          [1]={LHS="E"; RHS={"EM"}};
              [2]={LHS="E"; RHS={"EV"}};
              [3]={LHS="EM"; RHS={"EM","+","EM"}};
              [4]={LHS="EM"; RHS={"ID1"}};
              [5]={LHS="EV"; RHS={"EV","*","EV"}};
              [6]={LHS="EV"; RHS={"ID2"}} };
		 term = {"+","ID1","ID2","*"};
		 nonTerm = {"S","E","EM","EV"};
		 start = "S"
    }
]]

function G:terminals()
  return self.term
end

function G:nonTerminals(...)
  if #arg == 0 then
    return self.nonTerm
	else
	  return self:nonTerminalsProd(arg[1])
	end
end

function G:productions()
  return self.prod
end

function G:nonTerminalsProd(p)
  if self.prod[p] then
		local sRp = Set.new(self.prod[p].RHS)
		local setNonTerm = Set.new(self.nonTerm)
		return Set.table(Set.intersection(sRp, setNonTerm))
	else
	  return {}
	end
end

function G:Start()
  return self.start
end

function G:isTerminal(t)
	return Set.inSet(t, Set.new(self.term))
end

function G:isNonTerminal(nt)
  return Set.inSet(nt, Set.new(self.nonTerm))
end

local SLEN, SHORT, RLEN = PhaseOne.init(G)
PhaseOne.process(G, SLEN, SHORT, RLEN)

print()
print("PhaseOne.process()")
Debug:print("SHORT:", SHORT)
Debug:print("RLEN:", RLEN)
Debug:print("SLEN:", SLEN)
print()

local DLEN, PREV = PhaseTwo.init(G)

PhaseTwo.process(G, SLEN, RLEN, DLEN, PREV)

print("PhaseTwo.process()")


--[[
--Corre��o para testar fase 3
DLEN = {S=3, E=3}
PREV = {S=-1, E=0} --> Null = -1
]]
Debug:print("SHORT:", SHORT)
Debug:print("RLEN:", RLEN)
Debug:print("SLEN:", SLEN)
Debug:print("DLEN:", DLEN)
Debug:print("PREV:", PREV)
print()

local ONCE, ONST, MARK = PhaseThree.init(G, PREV, SHORT)

--Debug.Enabled = true
Debug.Level = LEVEL.TRACE
--Debug.Level = LEVEL.ERROR

--Debug:print(ONCE, ONST, MARK)
local sentences = PhaseThree.process(G, PREV, SHORT, ONCE, ONST, MARK)

Debug:print("PREV:", PREV)
Debug:print("SHORT:", SHORT)
Debug:print("ONCE:", ONCE)
Debug:print("ONST:", ONST)
Debug:print("MARK:", MARK)

for i,p in ipairs(sentences) do
  print(i, p)
end
