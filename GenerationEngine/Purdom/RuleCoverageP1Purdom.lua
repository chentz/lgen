Debug = require("dbg")

local PhaseOne = {}

Debug.Enabled = false

function PhaseOne.init(G)
  Debug:print("init()")
	
	Debug:print("Terminais...")
  local terminais = G:terminals()
	Debug:print(terminais)
	Debug:print("NonTerminais...")
	local nonTerminal = G:nonTerminals()
	Debug:print(nonTerminal)
	Debug:print("Productions...")
	local productions = G:productions()
	Debug:print(productions)
	local max_int = 65555
	local SLEN = {}
	local SHORT = {} -- guarda a prod mais curta de cada nao terminal com mais de uma prod
	local RLEN = {}
	
  for i,t in pairs(terminais) do
	  Debug:print(i,t)
	  SLEN[t] = 1 --Valor constante para os terminais.
	end
	
	for i,n in pairs(nonTerminal) do
	  Debug:print(i,n)
	  SLEN[n] = max_int
		SHORT[n] = -1
	end
	
	for i,p in pairs(productions) do
	  Debug:print(i,p)
	  RLEN[i] = max_int
	end
	return SLEN, SHORT, RLEN
end

function PhaseOne.process(G, SLEN, SHORT, RLEN)
  Debug:print("PhaseOne()")
	local prods = G:productions()
	
  change = true
	while (change) do
	  change = false
		Debug:print("while(change)")
		for ip = 0,#prods do
		  sum = 1
			too_big = false
			
			Debug:print("Productions: ", ip)
			local currRHS = prods[ip].RHS
			
			for idx = 1, #currRHS do
			--for idx, e in pairs(prods[ip].RHS) do
			  local e = currRHS[idx]
			  if (SLEN[e] == max_int) then --Garante que o processo e iniciado pelos terminais.
				  too_big = true
					break
				else
				  sum = sum + SLEN[e]
				end
			end
			
			if (not too_big) and (sum < RLEN[ip]) then --[[Garante que o simbolo nao e grande (too_big)
			e que o valor de RLEN para a regra de producao diminua sempre que possivel.]]
			  RLEN[ip] = sum --Atualiza a RLEN para o tamanho da menor sentenca gerada a partir da regra de producao.
				if (sum < SLEN[prods[ip].LHS]) then -- Garante o menor valor para o nao-terminal
				  SHORT[prods[ip].LHS] = ip --[[ Atualiza a SHORT para a regra de producao que deve ser utilizada para 
					produzir a menor sentenca para o simbolo.]]
          
					SLEN[prods[ip].LHS] = sum --[[Atualiza a SLEN para o numero de passos para a menor derivacao da 
					sentenca a partir do nao-terminal.]]
					change = true
				end
			end
			Debug:print("SHORT:", SHORT)
      Debug:print("RLEN:", RLEN)
      Debug:print("SLEN:", SLEN)
		end --For p
	end --While
	Debug:print("PhaseOne() - Fim")
end

return PhaseOne

--[[
local G
G = {prod = { [0]={LHS="S"; RHS={"E"}};
		          [1]={LHS="E"; RHS={"E","+","E"}};
              [2]={LHS="E"; RHS={"ID"}} };
		 term = {"+","ID"};
		 nonTerm = {"S","E"}
    }
		
function G:terminals()
  return self.term
end

function G:nonTerminals()
  return self.nonTerm
end

function G:productions()
  return self.prod
end

local SLEN, SHORT, RLEN = init(G)
PhaseOne(G, SLEN, SHORT, RLEN)

Debug:print("SHORT:", SHORT)
Debug:print("RLEN:", RLEN)
Debug:print("SLEN:", SLEN)
]]--
