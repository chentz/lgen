require("Init")
local Set = require("Set")

local function loadTermSet(fSent)
  local t = {}
  local f = io.open(fSent,"r")
  
  local s = Set.new{}
  for l in f:lines() do
    --print("Line:", l)
    for v in l:gmatch("(%g+)") do
      --print(v)
      s:include(v)
    end
  end
  f:close()
  return s
end

function genTerm(SetG)
  return function(s)
    SetG:include(s)
  end
end

function retF(...)
  return nil
end

local GSet = Set.new{}

G = {}
V = {}

terminal = genTerm(GSet)

seq = retF
alt = retF

oneOrMore = retF
oneOrMoreSep = retF
zeroOrMore = retF
optional = retF

local f = arg[1]
local s = loadTermSet(f)
print("Info about test set:")
print("Terminals:", s:card(), s)

if not arg[2] then
  return
end

dofile(arg[2])
print("Info about grammar:")
print("Terminals:", GSet:card(), GSet)
print("Differences:")
print(GSet - s)