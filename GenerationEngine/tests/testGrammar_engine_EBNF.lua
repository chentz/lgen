require ("init")

local dbg = require("dbg"):new()

require("grammar_engine")
require("grammar_engine_EBNF")

--Temp location for aux. functions

local function genCustomToTable(g, rNum, maxCycles, maxDerivLen, debug)
  local tabRsl = {}
  local fRsl = function(sent) tabRsl[#tabRsl+1] = sent end
  local fDbg = function(msg)
    if debug then print(msg) end
  end
  local iCnt, lNT, CovRet = genCustom(g, rNum, maxCycles, maxDerivLen, fRsl, fDbg)
  return tabRsl, iCnt, CovRet
end

context("Basic tests for Generation Engine (EBNF constructions)", function()
  local GTst, VTst, FCCTst
  local DebugOn = false
  
  before(function()
    FCCTst = createCC()
    setCoverageCriteria(nil, FCCTst)
    GTst, VTst, TTst = createGrammar(FCCTst), createNonterminalTable(), createTerminalTable()
    GTst.startSymbol = 'S'
  end)
  
  after(function() GTst, FCCTst, VTst = nil, nil, nil end)
  
  context("Basic constructions of EBNF", function()
    test("ZeroOrMore 1", function()
      --Grammar
      GTst.S = zeroOrMore(TTst.a)
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      
      --print("ZM1:", dbg:tostring(sents))
      assert_equal(sents[1], '')
      assert_equal(sents[2], 'a')
    end)
    
    test("ZeroOrMore 2", function()
      --Grammar
      GTst.S = zeroOrMore(TTst.a)
      local sents,cnt = genCustomToTable(GTst,2,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,3)
      
      --print("ZM2:", dbg:tostring(sents))
      assert_equal(sents[1], '')
      assert_equal(sents[2], 'a')
      assert_equal(sents[3], 'a a')
    end)
    
    test("ZeroOrMore 3", function()
      --Grammar
      GTst.S = zeroOrMore(seq(TTst.a, TTst.b))
      local sents,cnt = genCustomToTable(GTst,2,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,3)
      assert_equal(sents[1], '')
      assert_equal(sents[2], 'a b')
      assert_equal(sents[3], 'a b a b')
    end)
    
    test("ZeroOrMore 4 (ZM and optional, bug issue #10)", function()
      --Grammar
      --[[Result set for this case
      ZM4:	{[1]="", [2]="a ;", [3]="a ", [4]=" ;", [5]=" ", [6]="a ; a ;", 
      [7]="a ; a ", [8]="a ;  ;", [9]="a ;  ", [10]="a  a ;", [11]="a  a ", 
      [12]="a   ;", [13]="a   ", [14]=" ; a ;", [15]=" ; a ", [16]=" ;  ;", 
      [17]=" ;  ", [18]="  a ;", [19]="  a ", [20]="   ;", [21]="   "}
      ]]
      GTst.S = zeroOrMore(seq(VTst.A, optional(terminal(";"))))
      GTst.A = alt(T.a, V.S) 
      local sents,cnt = genCustomToTable(GTst,2,1,3, DebugOn)
      
      --print("ZM4:", dbg:tostring(sents))
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,21)
      
      assert_equal(sents[1], '')
      assert_equal(sents[2], 'a ;')
      assert_equal(sents[5], ' ')
      assert_equal(sents[8], 'a ;  ;')
    end)
    
    test("OneOrMore 1", function()
      --Grammar
      GTst.S = oneOrMore(TTst.a)
      local sents,cnt = genCustomToTable(GTst,2,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      assert_equal(sents[1], 'a')
      assert_equal(sents[2], 'a a')
    end)
    
    test("OneOrMore 2", function()
      --Grammar
      GTst.S = oneOrMore(alt(TTst.a,TTst.b))
      local sents,cnt = genCustomToTable(GTst,2,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,6)
      --print("OM2:", dbg:tostring(sents))   
      assert_equal(sents[1], 'a')
      assert_equal(sents[2], 'b')
      assert_equal(sents[3], 'a a')
      assert_equal(sents[4], 'a b')
    end)
    
    test("OneOrMoreSep 1", function()
      --Grammar
      GTst.S = oneOrMoreSep(TTst.a,',')
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,1)
      assert_equal(sents[1], 'a')
    end)
    
    test("OneOrMoreSep 2", function()
      --Grammar
      GTst.S = oneOrMoreSep(TTst.a,',')
      local sents,cnt = genCustomToTable(GTst,2,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      --print("OMS2:", dbg:tostring(sents))
      assert_equal(sents[1], 'a')
      assert_equal(sents[2], 'a , a')
    end)
    
    test("OneOrMoreSep 3 - Terminal Coverage", function()
      --Grammar
      assert_not_nil(FCCTst)
      setCoverageCriteria(require('terminalCoverage'), FCCTst)
      
      GTst.S = oneOrMoreSep(TTst.a,':')
      
      local sents,cnt,cRet = genCustomToTable(GTst,2,1,1, DebugOn)
      --print("OMS3:", dbg:tostring(sents))
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      
      assert_equal(sents[1], 'a')
      assert_equal(sents[2], 'a : a')
      
      assert_not_nil(cRet)
      assert_equal(cRet.txCoverage, 100)
      assert_equal(#cRet.NotCover, 0)
    end)
    
    test("Optional 1", function()
      --Grammar
      GTst.S = seq(TTst.a, optional(TTst.b))
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      assert_equal(sents[1], 'a b')
      assert_equal(sents[2], 'a ')
    end)
    
    test("Range 1", function()
      --Grammar
      GTst.S = seq(TTst.a, range('b','c'))
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,1)
      assert_equal(sents[1], 'a b')
    end)
  end)
end)