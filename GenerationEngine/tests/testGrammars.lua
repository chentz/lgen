require ("init")

local dbg = require("dbg"):new{}
local Util = require("Util")
local SHA = require("sha2")

require("grammar_engine")
require("grammar_engine_EBNF")
require("grammar_transformation")

--Temp location for aux. functions

local function getDefConfigTable(rc)
  local ret = rc
  if not rc then
    ret = {}
    ret.useMStorage = true
    ret.useMinTSortAlts = false
    ret.useMinTSortNT = true
    ret.quietExec = true
  end
  return ret
end

local function transformGrammar(g, rc)
  return grammarTransformations(g, getDefConfigTable(rc))
end

local function genCustomToTable(g, rNum, maxCycles, maxDerivLen, debug, rConfig)
  local tabRsl = {}
  local fRsl = function(sent) tabRsl[#tabRsl+1] = sent end
  local fDbg = function(msg)
    if debug then print(msg) end
  end
  
  local rc = getDefConfigTable(rConfig) 
  local gT = g
  
  if rc.useMStorage or rc.useMinTSortAlts or rc.useMinTSortNT then
    gT = transformGrammar(g, rc)
  end
  
  local iCnt, lNT, CovRet = genCustom(gT, rNum, maxCycles, maxDerivLen, fRsl, fDbg, rc)
  
  return tabRsl, iCnt, CovRet
end

local function loadGrammar(lf, GTst, VTst, TTst)
  local safeEnv = {
          G = GTst,
          V = VTst,
          T = TTst,
          
          seq = seq,
          alt = alt,
          terminal = terminal,
          empty = empty,
          idInc = idInc,
          
          zeroOrMore = zeroOrMore,
          oneOrMore = oneOrMore,
          oneOrMoreSep = oneOrMoreSep,
          optional = optional,
          range = range
        }
  --print("loadGrammar", GTst, VTst, TTst)
  local gEx, err = loadfile(lf, 't', safeEnv)
  if (gEx) then
    gEx()
  else
    print(err)
    return false
  end
  return true
end

context("Tests for Generation Engine (Grammars Examples)", function()
  local GTst, FCCTst, VTst
  local DebugOn = false
  
  before(function()
    --print("Load",_LOADED, package.loaded, package.loaded["terminalCoverage"])
    collectgarbage("collect")
    local l = _LOADED or package.loaded
    if l then
      l["terminalCoverage"] = nil
      l["productionCoverage"] = nil
    end
    
    FCCTst = createCC()
    setCoverageCriteria(nil, FCCTst)
    GTst, VTst, TTst = createGrammar(FCCTst), createNonterminalTable(), createTerminalTable()
  end)
  
  after(function() GTst, FCCTst, VTst, TTst = nil, nil, nil, nil end)
  
  context("SPL Grammars", function()
    test("GPL Grammar", function()
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/GPLSPL.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt = genCustomToTable(GTst,1,1,4, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,48)
    end)
    
    test("HIS Grammar", function()
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/HISSPL.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt = genCustomToTable(GTst,1,1,5, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,6)
    end)
    
    test("ModelTransforSPL Grammar", function()
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/ModelTransforSPL.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt = genCustomToTable(GTst,1,1,4, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,0)
    end)
    
    test("ModelTransforSPL Grammar(TerminalCoverage)", function()
      --Grammar #1
      assert_not_nil(FCCTst)
      setCoverageCriteria(require('terminalCoverage'), FCCTst)
      local rGEx = loadGrammar('./Grammars/ModelTransforSPL.lua', GTst, VTst, TTst)
      
      assert_true(rGEx)
      
      local sents,cnt,cRet = genCustomToTable(GTst,1,1,10, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,40)
      
      assert_not_nil(cRet)
      --print(cRet.txCoverage)
      assert_true(cRet.txCoverage == 100)
      assert_true(#cRet.NotCover == 0)
    end)
    
    test("ModelTransforSPL Grammar(ProductionCoverage)", function()
      --Grammar #1
      assert_not_nil(FCCTst)
      setCoverageCriteria(require('productionCoverage'), FCCTst)
      local rGEx = loadGrammar('./Grammars/ModelTransforSPL.lua', GTst, VTst, TTst)
      
      assert_true(rGEx)
      
      local sents,cnt,cRet = genCustomToTable(GTst,1,1,12, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,59)
      
      assert_not_nil(cRet)
      --print(cRet.txCoverage)
      assert_true(cRet.txCoverage == 100)
      assert_true(#cRet.NotCover == 0)
    end)
  end)
  
  context("Lua Grammar", function()
    test("Lua without coverage criterion", function()
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/lua_grammar.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt = genCustomToTable(GTst,1,1,3, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,5)
    end)
  end)
  
  context("Other Grammars", function()
    test("RSS Grammar with TerminalCoverage", function()
      assert_not_nil(FCCTst)
      setCoverageCriteria(require('terminalCoverage'), FCCTst)
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/RSS.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt,cRet = genCustomToTable(GTst,1,1,5, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,8)
      
      --print("check",cRet)
      assert_not_nil(cRet)
      --print(cRet.txCoverage)
      assert_true(cRet.txCoverage == 100)
      assert_true(#cRet.NotCover == 0)
    end)
  end)
  
  context("Java 1.5 Grammar", function()
    test("Java without coverage criterion", function()
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/Java15_Mod.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt = genCustomToTable(GTst,1,1,5, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,982)
      local hash = Util.hashCode(sents)
      --print("223",hash)
      assert_equal(hash, "921955e9c8c43a4b7f2ec45b2f4be704b8231d0740f52a6dc79e7bec")
    end)
  end)
  
  context("Context-Dependent Coverage Criteria", function()
    test("Lammel (Victor) G1 without coverage criterion.", function()
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/Victor_G1.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt = genCustomToTable(GTst,1,2,6, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,9)
    end)
    
    test("Lammel (Victor) G1 with CDRC.", function()
      --Grammar #1
      assert_not_nil(FCCTst)
      setCoverageCriteria(require('CDRCoverage'), FCCTst)
      local rGEx = loadGrammar('./Grammars/Victor_G1.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt,cRet = genCustomToTable(GTst,1,1,10, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,3)
      
      --print("check",cRet)
      assert_not_nil(cRet)
      --print("check",cRet.txCoverage)
      assert_true(cRet.txCoverage == 87.5)
      assert_true(#cRet.NotCover == 1)
    end)
  end)
  
  context("B Grammar", function()
    test("B Small without coverage criterion", function()
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/b_grammar_small.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt = genCustomToTable(GTst,1,1,4, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,1)
      
      sents,cnt = genCustomToTable(GTst,1,1,11, DebugOn)
      
      --Check 2
      assert_equal(#sents,cnt)
      --print(cnt)
      assert_equal(cnt,86)
    end)
    
    test("B Small grammar with TerminalCoverage", function()
      --Grammar #1
      --print("B Small grammar with TerminalCoverage")
      assert_not_nil(FCCTst)
      setCoverageCriteria(require('terminalCoverage'), FCCTst)
      local rGEx = loadGrammar('./Grammars/b_grammar_small.lua', GTst, VTst, TTst)
      
      assert_true(rGEx)
      
      local sents,cnt,cRet = genCustomToTable(GTst,2,1,11, DebugOn)
      
      --[[
      print("Qtd.:",cnt)
      print("Table:",cRet)
      print("Tx. Cov.:",cRet.txCoverage)
      ]]
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,10)
      assert_not_nil(cRet)
      assert_true(cRet.txCoverage == 100)
      assert_true(#cRet.NotCover == 0)
    end)
    
    test("B Small grammar with ProductionCoverage", function()
      --Grammar #1
      assert_not_nil(FCCTst)
      setCoverageCriteria(require('productionCoverage'), FCCTst)
      local rGEx = loadGrammar('./Grammars/b_grammar_small.lua', GTst, VTst, TTst)
      
      assert_true(rGEx)
      
      local sents,cnt,cRet = genCustomToTable(GTst,1,1,11, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      --print(cnt)
      assert_true(cnt == 8)
      
      assert_not_nil(cRet)
      --print(cRet.txCoverage)
      assert_true(cRet.txCoverage == 100)
      assert_true(#cRet.NotCover == 0)
    end)
    
    test("B0 Small Grammar without coverage", function()
      --Grammar #1
      local rGEx = loadGrammar('./Grammars/b0_grammar.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt = genCustomToTable(GTst,1,1,4, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      --print(Util.tostring(sents))
      assert_equal(cnt,170)
      local hash = Util.hashCode(sents)
      --print("340",hash)
      assert_equal(hash, "73845a1a3030dd971d059d79b1f9ea9c205bd8e390db35c951ad260e")
      
      --Second round
      local sents,cnt = genCustomToTable(GTst,1,1,5, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      --print(Util.tostring(sents))
      assert_equal(cnt,19670)
      local hash = Util.hashCode(sents)
      --print("351",hash)
      assert_equal(hash, "732ecab3acfce544851dfc9a3e08ec7620b51629dc350f96eced6ced")
      
    end)
    
    test("B0 Full Grammar with production coverage", function()
      assert_not_nil(FCCTst)
      setCoverageCriteria(require('productionCoverage'), FCCTst)
      local rGEx = loadGrammar('./Grammars/b0_grammar_full.lua', GTst, VTst, TTst)
      assert_true(rGEx)
      
      local sents,cnt,cRet = genCustomToTable(GTst,1,1,11, DebugOn)
      assert_equal(#sents,cnt)
      
      --print(cnt)
      assert_equal(cnt,72)
      
      assert_not_nil(cRet)
      --print(cRet.txCoverage)
      assert_true(cRet.txCoverage == 100)
      assert_true(#cRet.NotCover == 0)
    end)
  end)
end)