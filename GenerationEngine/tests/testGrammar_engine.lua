require ("init")

local dbg = require("dbg"):new()

require("grammar_engine")

--Temp location for aux. functions

local function genCustomToTable(g, rNum, maxCycles, maxDerivLen, debug)
  local tabRsl = {}
  local fRsl = function(sent) tabRsl[#tabRsl+1] = sent end
  local fDbg = function(msg)
    if debug then print(msg) end
  end
  local iCnt, lNT = genCustom(g, rNum, maxCycles, maxDerivLen, fRsl, fDbg)
  return tabRsl, iCnt
end

context("Basic tests for Generation Engine (BNF constructions)", function()
  local GTst, VTst, FCCTst
  local DebugOn = false
  
  before(function()
    FCCTest = createCC()
    setCoverageCriteria(nil, FCCTst)
    GTst, VTst, TTst = createGrammar(FCCTest), createNonterminalTable(), createTerminalTable()
    GTst.startSymbol = 'S'
  end)
  
  after(function() GTst, FCCTst, VTst = nil, nil, nil end)
  
  context("Basic constructions of BNF", function()
    test("Terminal 1", function()
      --Grammar #1
      GTst.S = TTst.a
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,1)
    end)
    
    test("Empty 1", function()
      --Grammar #1
      GTst.S = empty()
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,1)
    end)
    
    test("Seq 1", function()
      --Grammar
      GTst.S = seq(TTst.a, TTst.b)
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,1)
      assert_equal(sents[1], 'a b')
    end)
    
    test("Seq 2", function()
      --Grammar
      GTst.S = seq(TTst.a, empty())
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,1)
      assert_equal(sents[1], 'a ')
    end)
    
    test("Alt 1", function()
      --Grammar
      GTst.S = alt(TTst.a, TTst.b)
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      assert_equal(sents[1], 'a')
      assert_equal(sents[2], 'b')
    end)
    
    test("Alt 2", function()
      --Grammar
      GTst.S = alt(TTst.a, TTst.b, TTst.c)
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,3)
      assert_equal(sents[1], 'a')
      assert_equal(sents[2], 'b')
      assert_equal(sents[3], 'c')
    end)
    
    test("Alt 3", function()
      --Grammar
      GTst.S = alt(TTst.a, seq(TTst.b, TTst.c))
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      assert_equal(sents[1], 'a')
      assert_equal(sents[2], 'b c')
    end)
    
    test("Alt 4 (Internal alts)", function()
      --Grammar
      GTst.S = seq(TTst.a, alt(TTst.b, TTst.c))
      local sents,cnt = genCustomToTable(GTst,1,1,1, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      assert_equal(sents[1], 'a b')
      assert_equal(sents[2], 'a c')
    end)
    
    test("NonTerminal 1", function()
      --Grammar
      GTst.S = seq(TTst.a, VTst.B)
      GTst.B = TTst.b
      local sents,cnt = genCustomToTable(GTst,1,1,2, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      assert_equal(cnt,1)
      assert_equal(sents[1], 'a b')
    end)
    
    test("Cycles 1", function()
      --Grammar
      GTst.S = seq(TTst.a, VTst.B)
      GTst.B = alt(TTst.b, seq(TTst.b, VTst.B))
      local sents,cnt = genCustomToTable(GTst,1,1,4, DebugOn)
      
      --Check
      --print("Cycles1:", dbg:tostring(sents))
      assert_equal(#sents,cnt)
      assert_equal(cnt,2)
      
      assert_equal(sents[1], 'a b')
      assert_equal(sents[2], 'a b b')
    end)
    
    test("Cycles 2", function()
      --Grammar
      GTst.S = seq(TTst.a, VTst.B)
      GTst.B = alt(TTst.b, seq(TTst.b, VTst.C))
      GTst.C = alt(empty(), VTst.B)
      local sents,cnt = genCustomToTable(GTst,1,1,4, DebugOn)
      
      --Check
      assert_equal(#sents,cnt)
      --print("Cycles 2", dbg:tostring(sents))
      assert_equal(cnt,3)
      assert_equal(sents[1], 'a b')
      assert_equal(sents[2], 'a b ')
      assert_equal(sents[3], 'a b b')
    end)
  end)
end)