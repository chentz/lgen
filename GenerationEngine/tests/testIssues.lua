require ("init")

--- load the GE symbols
require("grammar_engine")

local GR = require("GenerationRunner")

local Util = require("Util")

local defConf = {
  maxCyclesOp = 1,
  maxCycles = 1,
  grammarTrans=nil,
  altStartSymbol=nil,
  quietExec=true,
  useRandomAlts=false,
  useMinTSortAlts=false
}

context("Bug from the issues", function()
  local gr 
  local FCCTest, GTst, VTst, TTst
  local sents
  
  before(function()
    local fOut
    
    gr = GR.new(defConf)
    
    fOut, sents = GR.outTable()
    gr:setConfig({
      FuncResult=fOut
    })
    
    FCCTest, GTst, VTst, TTst = gr:createGrammarEnv()
    
    GTst.startSymbol = 'S'
  end)
  
  after(function() gr, GTst, FCCTst, VTst, TTst = nil, nil, nil, nil, nil end)
  
  test("Bug Cycle and production coverage - Issues #55, #47", function()
    gr:setConfig({
      coverageCrit="productionCoverage",
      maxDerivLen=2
    })
    
    local sCount, symSet, prodInfo, highestTree = gr:runGFile('./Grammars/BugCycles-01.lua')
    assert_equal(#sents, sCount)
    
    assert_equal(sCount, 3)
    assert_not_nil(prodInfo)
    assert_true(#prodInfo.NotCover == 1)
  end)
  
  test("Bug with MaxCycle grater than 1 - Issues #57", function()
    --Grammar
    GTst.S = seq(VTst.A, VTst.B)
    GTst.A = seq(TTst.a, VTst.C)
    GTst.B = seq(TTst.b, VTst.C)
    GTst.C = alt(TTst.c, seq(TTst.c, VTst.C))
    
    gr:setConfig({
      maxCycles = 3,
      maxDerivLen=6,
      quietExec=true
    })
    
    local sCount, symSet, prodInfo, highestTree = gr:runGrammar(GTst)
    assert_equal(#sents, sCount)
    assert_equal(highestTree, 6)
    
    --[[
    print(sCount, symSet, prodInfo, highestTree)
    print(Util.tostring(sents))
    ]]
  end)
  
end)