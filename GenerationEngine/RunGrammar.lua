--- the interface module to execute the generation process
--
-- @script RunGrammar
-- @usage
-- arg[1] -> Lua Grammar File
-- arg[2] -> "stats" for statistics or "gen" to the generation process.
-- if arg[2] == "stats"
--   arg[3] -> Initial derivation tree height
--   arg[4] -> Final derivation tree height
-- else
--   arg[3] -> maxCycle
--   arg[4] -> maxDerivLen
--   arg[5] -> CoverageCriteria
--   arg[6] -> File prefix for output file generation.

require('init')
local Util = require('Util')

--Args
local GFile = arg[1]
local GConf = GFile:gsub('lua','conf')

--- Default table for runtime configuration.
-- Can be used on the configuration files.
local DefOptionsTab = {
 quietExec=false, -- Quiet execution
 execMode='gen', -- Execution type gen or stats
 maxCyclesOp = 1,-- Max. cycles inside an operator. Like, `oneOrMore` operator.
 maxCycles = 1,  -- Global max. nonterminal cycles 
 maxDerivLen = 3,-- Max. derivation tree height
 maxDerivLenFinal = nil, -- Max. derivarion tree height final. Only for `stats`
 coverageCrit = 'none',-- Coverage criterion to use
 filePrefix = GFile,    -- Prefix for the output files
 transGrammarCache = false -- Use a cache file save the transformation grammar
}

local curRunConfig

local f = io.open(GConf,"r")
if f then
  f:close()
  dofile(GConf)
  
  --Load config
  if not runTimeConfig then error("Invalid runtime options!") end
  
  curRunConfig = Util.defTableValue(runTimeConfig, DefOptionsTab)
  if not curRunConfig.quietExec then print("Runtime options loaded from "..GConf) end
else
  GConf = nil
  curRunConfig = {}
  
  if arg[2] == 'stats' then
    curRunConfig.execMode = 'stats'
    curRunConfig.maxDerivLen = tonumber(arg[3])
    curRunConfig.maxDerivLenFinal = tonumber(arg[4])
  else 
    curRunConfig.execMode = 'gen'
    curRunConfig.maxCyclesOp = tonumber(arg[2])
    curRunConfig.maxCycles = tonumber(arg[3])
    curRunConfig.maxDerivLen = tonumber(arg[4])
    curRunConfig.coverageCrit = arg[5]
    curRunConfig.filePrefix = arg[6] or arg[1]
  end
  
  curRunConfig = Util.defTableValue(curRunConfig, DefOptionsTab)
end

-- Set the current grammar file
curRunConfig.GrammarFile = GFile

-- Printing the runtime options 
if not curRunConfig.quietExec then
  print("Runtime Options:")
  print(Util.tostring(curRunConfig))
end
     
--Requirement load
if curRunConfig.execMode == 'stats' then
  require("statsGrammar")
  --require("grammar_transformation")
else
  require("grammar_engine")
  require("grammar_engine_EBNF")
  require("grammar_transformation")
  
  if curRunConfig.coverageCrit and 
     curRunConfig.coverageCrit ~= "none" then --hasCoverage
    local icCrit = require(curRunConfig.coverageCrit)
    setCoverageCriteria(icCrit)
  end
end

dofile(GFile)

--Execution
if curRunConfig.execMode == 'stats' then
  local result, qTMem, qTTime
  print("Executing Statistics:");
  
  --[[
  print("Apply Grammar Transformations...")
  G = grammarTransformations(G)
  print("Stating the Generation Process...")
  ]]
  
  if not curRunConfig.maxDerivLenFinal then --Just one level at time
    result, qTMem, qTTime = calcQtdTrees(G, curRunConfig.maxDerivLen, true)
    print("calcQtdTrees (Generator Algorithm - Left Recursive (Leftmost Derivation) Deep Firts):");
    print("Total Time:", qTTime)
    print("Finished Memory:", qTMem)
    print("For maxDerivLen: ", curRunConfig.maxDerivLen, " - Qtd of Derivations Trees in G is ",result)
    fSentPSeg = (qTTime/result)
    estTime = (result*fSentPSeg)*60
    if estTime > 120 then estTime = (estTime/60) .. " Hours."
    else estTime = (estTime) .. " minutes." end
    print("Estimed generation time:", estTime)
  else --Calculate a range
    local sList = "{"
    local lastResult = 0
    for l=curRunConfig.maxDerivLen,curRunConfig.maxDerivLenFinal do
      result, qTMem, qTTime = calcQtdTrees(G, l, true)
      print("Level:", l, "Qtd. Tree:",result,"Delta:",result-lastResult)
      lastResult = result
      sList = sList .."{"..l..","..result.."}"
      if l ~= curRunConfig.maxDerivLenFinal then sList = sList .. "," end
    end
    sList = sList .."}"
    print("Mathematica List:",sList)
  end
else
  print("Executing Sentences Generation:")
      
  print("Apply Grammar Transformations...")
  G = grammarTransformations(G,curRunConfig)
  print("Stating the Generation Process...")
  gen(G, curRunConfig.maxCyclesOp, curRunConfig.maxCycles, curRunConfig.maxDerivLen, curRunConfig.filePrefix, curRunConfig)
end
print("RunGrammar have been finished!")