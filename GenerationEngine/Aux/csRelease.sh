#!/bin/sh
# $1 => release name
# $2 => release machine

rm -rf *.out *.log *.prf *.time
tar -cf ../$1.tar .
mgzip -1 ../$1.tar
if [ $2 ]; then
  rFile=$1.tar.gz
  echo "$2/$rFile"
  scp ../$rFile $2/$rFile
fi
