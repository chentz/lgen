V = setmetatable({}, {
      __index = function (t, k) return nonterm(k) end
    })
    
T = setmetatable({}, {
      __index = function (t, k) return terminal(k) end
    })
    
G = {}

local cfTerm, cfSeq, cfAlt, cfNonTerm
local cfOneOrMore, cfOneOrMoreSep, cfOptional, cfZeroOrMore

local maxOpRept

function idInc(c,v)
  return function (g, iDeriv, ...) return cfTerm(g, iDeriv, v, ...) end
end

function range(b,e)
  return function (g, iDeriv, ...) return cfTerm(g, iDeriv, b, ...) end
end

--Assuming that always it generate one element and always it is possible to generate it.
function negation(patt)
  return function (g, iDeriv, ...) return cfTerm(g, iDeriv, 'negation', ...) end
end

function terminal(s)
  return function (g, iDeriv, ...) return cfTerm(g, iDeriv, s, ...) end
end

empty = terminal

function zeroOrMore(patt)
  return function(g, iDeriv, ...)
    return cfZeroOrMore(g, iDeriv, patt, ...)
  end
end

function optional(patt)
  return function(g, iDeriv, ...)
    return cfOptional(g, iDeriv, patt, ...)
  end
end

function oneOrMore(patt)
  return function(g, iDeriv, ...)
    return cfOneOrMore(g, iDeriv, patt, ...)
  end
end

function oneOrMoreSep(patt, str)
  return function(g, iDeriv, ...)
    return cfOneOrMoreSep(g, iDeriv, patt, ...)
  end
end

function seq(patt1, patt2)
  return function(g, iDeriv, ...)
           --print("seq:Anon");
           return cfSeq(g, iDeriv, patt1, patt2, ...) 
         end
end

function alt(...)
  --print("alt")
  local patts = {...}
  --print("alt:2", #patts, cfAlt)
  return function(g, iDeriv, ...) 
           --print("alt:Anon");
           return cfAlt(g, iDeriv, patts, ...)
         end
end

function nonterm(NT)
  return function(g, iDeriv, ...)
           --print("nonterm:Anon");
           return cfNonTerm(g, iDeriv, NT, ...) 
         end
end

--calcQtdTrees
--[[
term = 1
A, B . Deriv(A) ^ Deriv(B) = #A * #B
A | B . Deriv(A) v Deriv(B) = #A + #B . Se for levar em consideração todas as derivações possível (a esquerda, direita ou outra ordem) de levar em consideração todas as permutações de 2.
Logo, #{A,B}! * #A + #B 
#N = Deriv(N) -> #N, 0
]]

local function cqtTerm(g, iDeriv, t, ...)
  --print(string.rep("-",iDeriv).." cqtTerm", iDeriv, t)
  return true, 1
end

local function cqtSeq(g, iDeriv, patt1, patt2, ...)
  --print(string.rep("-",iDeriv).." cqtSeq", iDeriv)
  local deriv1, lem1 = patt1(g, iDeriv, ...)
  --print("s:patt1", deriv1, lem1)
  if deriv1 then
    local deriv2, lem2 = patt2(g, iDeriv, ...)
    --print("s:patt2",deriv2, lem2)
    if deriv2 then
      return true, lem1 * lem2
    else
      return false, 0
    end
  else
    return false, 0
  end
end

local function cqtAlt(g, iDeriv, patts, ...)
  --print(string.rep("-",iDeriv).." cqtAlt", iDeriv)
  local rsl, deriv, lem, cLem
  rsl = false
  cLem = 0
  for i, patt in ipairs(patts) do
    deriv, lem = patt(g, iDeriv, ...)
    if deriv then
      cLem = cLem + lem
      rsl = true
    end
  end
  --print(string.rep("-",iDeriv).." cqtAlt:End", rsl, cLem)
  return rsl, cLem

  --[[
  --print("cqtAlt", patt1, patt2)
  local deriv1, lem1 = patt1(g,iDeriv, ...)
  --print("a:patt1",deriv1, lem1)
  
  local deriv2, lem2 = patt2(g, iDeriv, ...)
  --print("a:patt2", deriv2, lem2)
  if deriv1 or deriv2 then
    return true, lem1 + lem2
  else
    return false, 0
  end
  ]]
end
      
local function cqtNonTerm(g, iDeriv, NT, ...)
  --print(string.rep("-",iDeriv).." cqtNonTerm", iDeriv ,NT)
  if iDeriv > 0 then
    if not g[NT] then
      error(string.format("cqtNonTerm:The symbol '%s' isn't defined. Level: %s", NT, iDeriv))
    end
    --print(string.rep("-",iDeriv).." Call g["..NT.."]")
    return g[NT](g, iDeriv-1, ...)
  else
    return false, 0
  end
end

local function cqtKleeneClosure(beginPatt, basePatt, inducPatt, maxRec)
  return function(g, iDeriv, ...)
    local r = false
    local lem = 0
    
    if beginPatt then
      r,lem = beginPatt(g, iDeriv, ...)
    end
    
    if maxRec < 1 then return r,lem end
    
    local fFold = basePatt
    
    local cr,cl
    for i = 1, maxRec do
      if i > 1 then
        fFold = seq(inducPatt, fFold)
      end
      
      cr, cl = fFold(g, iDeriv, ...)
      r = cr or r
      --print(lem)
      lem = cl + lem
    end
    
    return r, lem
  end
end

local function cqtZeroOrMore(g, iDeriv, patt, ...)
  f = cqtKleeneClosure(cfTerm, patt, patt, maxOpRept)
  return f(g, iDeriv, ...)
end

local function cqtOneOrMore(g, iDeriv, patt, ...)
  f = cqtKleeneClosure(nil, patt, patt, maxOpRept)
  return f(g, iDeriv, ...)
end

local function cqtOneOrMoreSep(g, iDeriv, patt, ...)
  f = cqtKleeneClosure(nil, patt, seq(patt, terminal(str)), maxOpRept)
  return f(g, iDeriv, ...)
end

local function cqtOneOrMoreSepNew(g, iDeriv, patt, ...)
  --kleeneClosure(, patt, nil, doF, chkF, "oneOrMoreSep "..str,0)
  f = cqtKleeneClosure(seq(patt, seq(terminal(str), patt)), patt, seq(patt, terminal(str)), 0)
  return f(g, iDeriv, ...)
end

local function cqtOptional(g, iDeriv, patt, ...)
  f = alt(patt, empty())
  return f(g, iDeriv, ...)
end

function calcQtdTrees(g, maxDerivLen)
  --Config calc functions
  --print("calcQtdTrees", g.startSymbol)
  
  cfTerm = cqtTerm
  cfSeq  = cqtSeq
  cfAlt  = cqtAlt
  cfNonTerm = cqtNonTerm
  
  cfOneOrMore  = cqtOneOrMore
  cfOneOrMoreSep  = cqtOneOrMoreSep
  --cfOneOrMoreSep  = cqtOneOrMoreSepNew
  cfZeroOrMore  = cqtZeroOrMore
  cfOptional = cqtOptional
  
  maxOpRept = 1
  
  local c = os.clock()
  local calcLenG = nonterm(g.startSymbol)
  local deriv, lem = calcLenG(g, maxDerivLen, {})
  local time = os.clock()-c
  local mem = collectgarbage("count")
  return lem, mem, time
end

--calcMinDerivLen
--[[
N! -> Cycle(N) ? 0 ; N! + 1
A,B -> MAX(A!, B!)
A|B -> MIN(A!, B!)
T!  -> 0
]]
function calcMinDerivLen(g, rNum, maxCycles, maxDerivLen, filePrefix)
  --print("Start Qtd of Derivation Trees for maxDerivLen")
  numRecLimit = rNum
  
  local c = os.clock()
  local calcLemG = nonterm(g.startSymbol)
  local deriv, lem = calcLemG(g, maxDerivLen)
  print("Total Time:", os.clock()-c)
  print("Finished Memory:", collectgarbage("count"))
  print("For maxDerivLen: ", maxDerivLen, " - Qtd of Derivations Trees in G is ",lem)
  print("Finished")
end

--calcMaxTreesHeight