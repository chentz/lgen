G = {}

V = setmetatable({}, {
      __index = function (t, k) return non_terminal(k) end
})

function non_terminal(n)
  return function() return G[n]() end
end

function setCoverageCriteria(c)
  
end

function terminal(str)
  return function() return 0 end
end

function seq(patt1, patt2)
  return function()
  print(patt1, patt2)
  return math.max(patt1(), patt2()) end
end

function alt(...)
  local patts = {...}
  return function()
    local r = 999999999
    for i, patti in ipairs(patts) do
      print(i,patti)
      r = math.min(patti(), r)
      print("end")
    end
    return r
  end
end

function oneOrMoreSep(patt, sep)
  return function() return patt() end
end

function newContext(patt)
  return function() return patt() end
end

function Ident(patt)
  return function() return 0 end
end

function empty()
  return function() return 0 end
end

function optional(patt)
  return function() return patt() end
end

function foo(...)
  return function() return 0 end
end

constString, classGen, regGen = foo, foo, foo


function gen(t, ...)
  for i, v in pairs(t) do
    print(i)
    print(i,v())
  end
end
