#!/bin/sh
# $1 => file of sources
# $2 => ID
# $3 => onLog
# $3 => maxProc

maxrun=1
pRCount=0
pCount=1
cat $1 |
while read line
do
  if [ "$pRCount" -lt "$maxrun" ]; then
    $line > $2.$pCount.log &
    ((pRCount++))
  else
    $line > $2.$pCount.log
    pRCount=1
  fi
  ((pCount++))
done
