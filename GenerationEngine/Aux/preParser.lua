function init(dst, hnd, env)
  --Tratamento de arquivos
  local bodyOut, headOut = {lines={}}, {lines={}}
	
	bodyOut.write = function (self, l)
	  self.lines[#self.lines+1] = l
	end
	
	bodyOut.close = function (self, fileDst)
	  for _, l in ipairs(self.lines) do
		  fileDst:write(l)
			fileDst:write('\n')
		end
	  fileDst:flush()
	end
	
	headOut.write = bodyOut.write
	headOut.close = bodyOut.close
	
	env.bodyOut = bodyOut
	env.headOut = headOut
	--Fim de tratamento
	
  env.count = 0
	if hnd.init ~= nil then
	  hnd:init(env.headOut, env)
	end
end

function close(dst, hnd, env)
  if hnd.close ~= nil then
    hnd:close(env)
	end
	
	env.headOut:close(dst)
	env.bodyOut:close(dst)
  print("inst: " .. env.count)
end

function process(dst, line, env, lineNum)
  local i, j = string.find(line, "--@")
	
	if (i ~= nil) and (env.Type == "trc") then --� uma Tag
	  local i, f, id
		--ID
		i, f = string.find(line,"%b@(")
		id = string.sub(line, i+1, f-1)
		if env[id] == nil then
		  print("Handler nao localizado. Id: "..id)
			return
		end
		
		--Handler
		i, f = string.find(line,"%b()")
	  env[id]:process(env.bodyOut, string.sub(line, i+1,f-1), lineNum)
	elseif i == nil then
	  env.bodyOut:write(line)
	end
end

function preParse(Handler, sSrc, dSrc, sType)
	local fSrc, err = io.open(sSrc, "r")
	
	if fSrc == nil then
		print(string.format("Erro. O arquivo '%s' nao existe!",sSrc))
		return 1
	end
	
	local dst1 = dSrc or (sSrc .. ".trc")
	local fdst, err = io.open(dst1, "w")
	if fdst then
		local lineNum = 1
		local env = {Type = sType}
		env[Handler.Name] = Handler
		init(fdst, Handler, env)
		for line in fSrc:lines() do
		  --print(line)
			process(fdst, line, env, lineNum)
			lineNum = lineNum + 1
		end
		close(fdst, Handler, env)
		fdst:flush()
		fdst:close()
		return 0
	else
		print("erro:" .. err)
	end
end

--Handler
local TraceHandler = {Name = "T"}

function TraceHandler.process(self, fileOut, s, lineNum)
  local i, f, nm
	i, f = string.find(s, "=")
	
	if i ~= nil then
    self:pTags(fileOut, s, lineNum)
	else
	  self:pInstructions(fileOut, s, lineNum)
	end
end

function TraceHandler.pTags(self, fileOut, s, lineNum)
  print("pTags")
  for k, v in string.gmatch(s, "(%w+)=(%w+)") do
		if string.lower(k) == "name" then
			self.currTraceName = v
		else
			print(string.format("tag nao tratada. %s=%s"), k, v)
		end
	end
end

function TraceHandler.pInstructions(self, fileOut, s, lineNum)
  print("pInstructions:"..s)
	local k = string.sub(s, string.find(s, "%w+"))
	print(k)
	if string.lower(k) == "init" then
	  fileOut:write('local Debug = require ("dbg")')
		fileOut:write('local Trace = require ("Trace")')
		fileOut:write('')
		fileOut:write('Debug.Enabled = true')
		fileOut:write('Debug.Level = LEVEL.TRACE')
	elseif string.lower(k) == "begin" then
		fileOut:write(string.format('local t = Trace:new("%s")', self.currTraceName))
		fileOut:write("t:beginTrace()")
	elseif string.lower(k) == "body" then
	  local out = lineNum
	  for t in string.gmatch(s, '[^:][%a%d"%$%(%)]+') do
		  print(t)
			if t ~= "body" then
			  local i, f = string.find(t, '%b""')
				if i ~= nil then
		      local s = string.sub(t, i, f)
			    print(s)
			    out = out .. ', ' .. s
			  end
				
				i, f = string.find(t, '%b$$')
				if i ~= nil then
				  s = string.sub(t, i+1, f-1) --Tira os $$
				  print(s)
					out = out .. ', ' .. s
				end
			end
		end
	  fileOut:write(string.format("t:body(%s)", out))
	elseif string.lower(k) == "end" then
	  fileOut:write("t:endTrace()")
	else
		print("Instrucao nao tratada. %s".. k)
	end
end

preParse(TraceHandler, arg[1], arg[2], arg[3])
