require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local productionC = require ("productionCoverage")

G.startSymbol = "ST"
setCoverageCriteria(productionC)

G.ST = alt(V.X1, V.X2, V.X3)
G.X1 = alt(seq(terminal('1'), terminal('2')), seq(terminal('3'), terminal('4')))
G.X2 = alt(seq(terminal('2'), terminal('3')), terminal('1'))
G.X3 = terminal('5')

gen(G, tonumber(arg[1]), tonumber(arg[2]), arg[3] or arg[0])
