local function rule(id, ...)
  local patts = {...}
  local lstCmd = T[')']
  for k=#patts,1,-1 do
    if k == #patts then
      lstCmd = seq(patts[k], lstCmd)
    else
      lstCmd = seq(patts[k], seq(T[','], lstCmd))
    end
  end
  return seq(T[string.format("%s(",id)], lstCmd)
end

G.startSymbol = "S"
--[[
G.S = rule('r1', V.A, V.B)
G.A = rule('r2', V.C, T["'a'"])
G.B = rule('r3', T["'b'"], V.C)
G.C = alt(T["r4()"], 
          rule('r5', T["'c'"], V.C)
          )
      ]]
      
G.S = seq(V.A, V.B)
G.A = seq(V.C, T.a)
G.B = seq(T.b, V.C)
G.C = alt(empty(), seq(T.c, V.C))

--[[
Processo
1- Indexa as produções
r1 = S -> A B
r2 = A -> C a
r3 = B -> b C
r4 = C -> e()
r5 = C -> c C

<e_1,...,e_n> : Sequencia : S
{e_1,...,e_n} : Conjunto : C
s:S + e => <s, e>
e + s:S => <e, s>
s:S + {<ce_1,...>,...,<ce_n,...>} => {<s, ce_1,...>,...,<s, ce_n,...>}
{<ce_1,...>,...,<ce_n,...>} + s:S => {<ce_1,..., s>,...,<ce_n,...,s>}
{c1,...,{ci,...,cn},...} => {c1,...,ci,...,cn,...} 
s:S + c1:C + c2:C => s + (c1 + c2)
s1:S + (s2:S + s3:S) = (s1:S + s2:S) + s3:S
c1:C + c2:C + c3:C => ((c1 + c2) + c3)
c1:C + c2:C => c1 = {a1,a2,...} + c2 = {b1,b2,...} => {a1+b1,a1+b2,a1+...,a2+b1,a2+b2,a2+...}

2 - Adicionar o index a cada produção
S -> r1 + 'A B'
A -> r2 + 'C a'
B -> r3 + 'b C'
C -> r4 + 'e()'
C -> r5 + 'c C'

3 - Remover os terminais e os adiconar o prefixo 'V.' (q vai representar a computação do simbolo) a cada um dos NT 
S -> r1 + V.A + V.B
A -> r2 + V.C
B -> r3 + V.C
C -> r4  // 'e()' é considerado terminal
C -> r5 + V.C

4 - Simbolos NT que tem alternativa são unificados em um conjunto. Manter ordem das definições
S -> r1 + V.A + V.B
A -> r2 + V.C
B -> r3 + V.C
C -> {r4, r5 + V.C}

5 - Todos os simbolos r_index (Terminais?) são transformados em sequencia
S -> <r1> + V.A + V.B
A -> <r2> + V.C
B -> <r3> + V.C
C -> {<r4>, <r5> + V.C}

6 - Simplificar até reduzir apenas ao simbolo inicial S e simbolos NT que possuem alternativa (conjuntos)
S -> <r1> + {<r2> + V.C} + {<r3> + V.C}
C -> {<r4>, <r5> + {V.C}}

Caso C fosse expandido com limitação 0 (Sem ciclos)
S -> <r1> + {<r2> + V.C} + {<r3> + V.C}
C -> {<r4>}

==>


Caso C fosse expandido com limitação 1 (1 ciclos)
S -> <r1> + {<r2> + V.C} + {<r3> + V.C}
C -> {<r5,r4>}

==>


Perguntas que pode ser respondidas neste ponto:
Q1 - A Gramatica tem ciclos?
R: Grapho de dependencia, se existir ciclos a resposta é sim e não caso contrário.

Q2 - A Linguagem é infinita?
R: Mesma da pergunta Q1

Q3 - Qual a qtd. de arvores de derivação?
R: Se Q1 é sim então infinito, caso contrário contar o número de sequencias.
Também é possível colocar limitantes (altura e número de unfold de simbolos) e
contar a qtd. p gramáticas com ciclos.

Q4 - Qual a altura da menor arv. de derivação?
R:Se a linguagem não for vazia:
Tamanho da menor sequencia.

Q5 - Qual a altura da maior arv. de derivação?
R: Mesma restrição de Q4 e se for infinita pode aplicar mesmas limitações de Q3.
Caso finita, tamanho da maior sequencia.

Nota 1: O processo parando aqui é fácil de calcular qtd de arvores e detectar subarvores em comum na gramática.

8- Avaliar com calma: Simplificar simbols com alternativa quando não possuem NT internos

9- Avaliar com calma: Operação de conjuntos

Outra gramática como exemplo:
S -> A B C
A -> A A
A -> a
B -> b A
B -> b
C -> c

1-
r1 = S -> A B C
r2 = A -> A A
r3 = A -> a
r4 = B -> b A
r5 = B -> b
r6 = C -> c

2 and 3 -
S -> r1 + V.A + V.B + V.C
A -> r2 + V.A + V.A
A -> r3
B -> r4 + V.A
B -> r5
C -> r6

4 and 5-
S -> <r1> + V.A + V.B + V.C
A -> {<r2> + V.A + V.A, <r3>}
B -> {<r4> + V.A, <r5>}
C -> <r6>

6-
S -> <r1> + V.A + V.B + <r6>
A -> {<r2> + V.A + V.A, <r3>}
B -> {<r4> + V.A, <r5>}

Outro exemplo sem ciclos
S -> A B C
A -> a B
A -> a
B -> b C
B -> b
C -> c

1-
r1 = S -> A B C
r2 = A -> a B
r3 = A -> a
r4 = B -> b C
r5 = B -> b
r6 = C -> c

2,3,4,5,6,7-
S -> <r1> + V.A + V.B + <r6>
A -> {<r2> + V.B, <r3>}
B -> {<r4,r6>, <r5>}

8- Simplify(B)
S -> <r1> + V.A + {<r4,r6>, <r5>} + <r6>
A -> {<r2,r4,r6>, <r2,r5>, <r3>}

8- Simplify(A)
S -> <r1> + {<r2,r4,r6>, <r2,r5>, <r3>} + {<r4,r6>, <r5>} + <r6>

9- Operação de conjuntos
S -> <r1> + {<r2,r4,r6>, <r2,r5>, <r3>} + {<r4,r6>, <r5>} + <r6>

Passo 1 União conjuntos
S -> <r1> + {<r2,r4,r6,r4,r6>,<r2,r4,r6,r5>, <r2,r5,r4,r6>,
             <r2,r5,r5>, <r3,r4,r6>, <r3,r5>} + <r6>
             
Passo 2 Simplificação
S -> {<r1,r2,r4,r6,r4,r6,r6>, <r1,r2,r4,r6,r5,r6>, <r1,r2,r5,r4,r6,r6>,
      <r1,r2,r5,r5,r6>, <r1,r3,r4,r6,r6>, <r1,r3,r5,r6>}
]]