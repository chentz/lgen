require ("init")

local dbg = require("dbg"):new()

require("grammar_engine")

local function genCustomToTable(g, rNum, maxCycles, maxDerivLen, debug)
  local tabRsl = {}
  local fRsl = function(sent) tabRsl[#tabRsl+1] = sent end
  local fDbg = function(msg)
    if debug then print(msg) end
  end
  local iCnt, lNT = genCustom(g, rNum, maxCycles, maxDerivLen, fRsl, fDbg)
  return tabRsl, iCnt
end

local function rule(id, ...)
  local patts = {...}
  local lstCmd = T[')']
  for k=#patts,1,-1 do
    if k == #patts then
      lstCmd = seq(patts[k], lstCmd)
    else
      lstCmd = seq(patts[k], seq(T[','], lstCmd))
    end
  end
  return seq(T[string.format("%s(",id)], lstCmd)
end

local FCCTest = createCC()
setCoverageCriteria(nil, FCCTst)
local GTst, VTst, TTst = createGrammar(FCCTest), createNonterminalTable(), createTerminalTable()
GTst.startSymbol = 'S'
    
GTst.S = rule('r1', V.A, V.B)
GTst.A = rule('r2', V.C, T["'a'"])
GTst.B = rule('r3', T["'b'"], V.C)
GTst.C = alt(T["r4()"], 
          rule('r5', T["'c'"], V.C)
          )

local sents,cnt = genCustomToTable(GTst,1,2,tonumber(arg[1]), DebugOn)
--print(dbg:tostring(sents))
--print(cnt)
local tRsl = {}

local function f(fId)
  return string.gsub([[
  local function <ID>(...)
    local largs = {...}
    if #largs == 0 then return "<ID>" end
    
    local r = ""
    local val
    for idx,v in ipairs(largs) do
      if type(v) == "function" then
        val = v()
      else
        val = v
      end
      
      val =  "<ID>->" .. val
      if idx == 1 then
        r = r .. val 
      else
        r = r .. ", " .. val end 
    end
    print(r)
    return "<ID>"
  end
  ]], "<ID>", fId)
end

local env = {
  ["print"] = print,
  ["ipairs"] = ipairs,
  ["type"] = type,
  ["string"] = string
}

local tmplate = f("r1") .. "\n" ..
f("r2") .. "\n" ..
f("r3") .. "\n" ..
f("r4") .. "\n" ..
f("r5")

local t, err

for k,v in pairs(sents) do
  print(k,v)
  t = string.format("return %s", v)
  print(tmplate.. " " ..t)
  t, err = load(tmplate.. " " ..t, "ll:parser","t",env)
  if not t then
    print("Error: ", err)
  else
    print("load:", t)
    print(k, t())
    tRsl[#tRsl+1] = t
  end
end

for k,v in pairs(tRsl) do
  print(k,v, v())
end