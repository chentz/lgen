require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

G.startSymbol = "Component"
setCoverageCriteria(terminalC)

G.Component = V.Machine_abstract
G.Machine_abstract = seq(terminal("MACHINE"), seq(V.Ident, seq(V.Machine_abstractVRZ, terminal("END"))))
G.Machine_abstractVRZ = alt(terminal("xxx"), V.Clause_machine_abstract)
G.Clause_machine_abstract = alt(V.Clause_abstract_variables, V.Clause_operations)
G.Clause_abstract_variables = seq(terminal("ABSTRACT_VARIABLES"), V.Ident)
G.Clause_operations = seq(terminal("OPERATIONS"), V.Ident)
G.Ident = terminal("ID")

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
