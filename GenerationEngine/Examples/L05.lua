require('init')
--[[
local terminalC = require ("terminalCoverage")

setCoverageCriteria(terminalC)
]]
--require ("lenGrammar")

require ("grammar_engine")
require ("grammar_engine_EBNF")

--[[
A := B|C
B := 'b',C|'b'
C := 'c',A|'c'
]]
G.startSymbol = "A"
G.A = alt(V.B, V.C)
G.B = alt(seq(terminal("b"), V.C), terminal("b"))
G.C = alt(seq(terminal("c"), V.A), terminal("c"))

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])