--[[
ST = OMA | OMB
OMA = 'a' | 'a', OMA
OMB = 'b' | 'b', OMB
]]

G.startSymbol = "ST"

G.ST = alt(V.OMA, V.OMB) 
G.OMA = alt(terminal('a'), seq(terminal('a'), V.OMA))
G.OMB = alt(terminal('b'), seq(terminal('b'), V.OMB))