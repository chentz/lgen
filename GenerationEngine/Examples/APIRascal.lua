require("init")
local GR = require("GenerationRunner")
local Util = require("Util")

local RascalGF = './Grammars/Rascal.lua'

local fOut = GR.outLimitNumber(20, GR.outPrintConsole)
local conf = {
  maxDerivLen=12,
  grammarTrans=false,
  coverageCrit="productionCoverage",
  altStartSymbol=nil,
  quietExec=true,
  FuncResult=fOut
}

if arg[1] then conf.altStartSymbol=arg[1] end

local gr = GR.new()
gr:setConfig(conf)
local a,b,c = gr:runGFile(RascalGF)
print("NT List:", b)
if c then
  print("CCriterion Global Coverage:", c.txCoverage)
end
