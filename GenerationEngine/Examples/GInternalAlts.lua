--[[
ST = A,(B|C)
A = "a"
B = "b"
C = "c"
]]

G.startSymbol = "ST"

G.ST = seq(V.A, alt(V.B, V.C))
G.A = terminal('a')
G.B = terminal('b')
G.C = terminal('c')