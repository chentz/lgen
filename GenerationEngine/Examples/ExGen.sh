#!/bin/sh
# $1 => lua source
# $ => rNum
# $2 => mcycle
# $3 => mDeriv
# $4 => CoverageCrit
# $5 => prefix

file_name=$1
crit=""
case "$4" in
  t )
	  crit="terminalCoverage"
		;;
	prod )
	  crit="productionCoverage"
	  ;;
esac

lua ExampleRun.lua $file_name 1 $2 $3 $crit $5