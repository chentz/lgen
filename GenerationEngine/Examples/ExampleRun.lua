require('init')
--[[
arg[1] -> Grammar File
arg[2] -> "stats" for statistics or rNum limit.
if arg[2] == "stats"
  arg[3] -> maxDerivLen
else
  arg[3] -> maxCycle
  arg[4] -> maxDerivLen
  arg[5] -> CoverageCriteria
  arg[6] -> File prefix for output file generation.
]]

--Args
local GFile = arg[1]
local Gstats = arg[2] == 'stats'
local rNum, maxCycles,maxDerivLen,filePrefix

if Gstats then
  rNum = nil
  maxCycles = nil
  maxDerivLen = tonumber(arg[3])
  filePrefix = nil
  require("statsGrammar")
else
  rNum = tonumber(arg[2])
  maxCycles = tonumber(arg[3])
  maxDerivLen = tonumber(arg[4])
  coverageCrit = arg[5]
  filePrefix = arg[6] or arg[1]
  require("grammar_engine")
  require("grammar_engine_EBNF")
  
  if coverageCrit then --hasCoverage
    local icCrit = require(coverageCrit)
    setCoverageCriteria(icCrit)
  end
end

dofile(GFile)

if Gstats then
  local result, qTMem, qTTime
  print("Executing Statistics:");
  --[[
  result, qTMem, qTTime = calcMinDerivLen(G)
  print("calcMinDerivLen:");
  print("Total Time:", qTTime)
  print("Finished Memory:", qTMem)
  print("Minimum Derivation Length for Grammar is", result)
  ]]
  
  result, qTMem, qTTime = calcQtdTrees(G, maxDerivLen, true)
  print("calcQtdTrees (Generator Algorithm - Left Recursive (Leftmost Derivation) Deep Firts):");
  print("Total Time:", qTTime)
  print("Finished Memory:", qTMem)
  print("For maxDerivLen: ", maxDerivLen, " - Qtd of Derivations Trees in G is ",result)
else
  print("Executing Sentences Generation:");
  print("Config:", "rNum: "..rNum, "maxCycles: "..maxCycles,
        "maxDerivLen: "..maxDerivLen, "coverageCrit: ".. (coverageCrit or ""),
        "fileProfix: ".. filePrefix or "");
  gen(G, rNum, maxCycles, maxDerivLen, filePrefix)
end