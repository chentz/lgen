require("init")
local GR = require("GenerationRunner")

local r = GR.new()
r:setConfig({maxDerivLen=4, altStartSymbol="Algorithms", quietExec=false})
print(r:runGFile('./Grammars/GPLSPL.lua'))