--[[
S = (s1|s2),((b1|b2)|b3)
]]

G.startSymbol = "S"

G.S = seq(alt(terminal('s1'), terminal('s2')),
          alt(terminal('b1'), terminal('b2'), terminal('b3')) )