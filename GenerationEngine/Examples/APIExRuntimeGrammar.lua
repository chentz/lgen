require("init")
local GR = require("GenerationRunner")

local r = GR.new()
r:setConfig({maxDerivLen=4, altStartSymbol='B', quietExec=false})

local runCC, G, V, T = r:createGrammarEnv()

--Define the grammar
G.startSymbol = 'S'

G.S = seq(T.a, V.B)
G.B = T.b

print(r:runGrammar(G))