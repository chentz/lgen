require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local productionC = require ("productionCoverage")

G.startSymbol = "A"
setCoverageCriteria(productionC)

G.A = seq(terminal('a'), V.B)
G.B = alt(seq(terminal('c'), V.C), terminal('b'))
G.C = seq(terminal('d'), V.A)

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
