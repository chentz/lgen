--[[
PROG = 'prog', BLOCK
BLOCK = '{', ALG, '}'
ALG = '<Var>', '=', '<Exp>' |
      ALG |
      BLOCK
]]

G.startSymbol = "PROG"

G.PROG = seq(terminal('prog'), V.BLOCK) 
G.BLOCK = seq(seq(terminal('{'), V.ALG), terminal('}'))
G.ALG = alt(seq(seq(terminal('<Var>'), terminal('=')), terminal('<Exp>')),
                V.ALG,
                V.BLOCK)