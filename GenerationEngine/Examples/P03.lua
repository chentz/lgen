require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")

--local terminalC = require ("terminalCoverage")
--setCoverageCriteria(terminalC)
local productionC = require ("productionCoverage")
setCoverageCriteria(productionC)

G.startSymbol = "ST"

G.ST = alt(V.X, V.Y)
G.X = alt(optional(terminal("--")), terminal("a"))
G.Y = seq(terminal("b"), terminal("c"))

gen(G, tonumber(arg[1]), tonumber(arg[2]), arg[3] or arg[0])
