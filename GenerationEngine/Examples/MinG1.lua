G.startSymbol = "S"
                           --   P. ID - MinTree
G.S = seq(V.A, V.C)            -- r10 - 3
G.A = seq(V.B, T.a)            -- r1  - 2
G.B = alt(T.b,                 -- r2  - 1
      seq(V.A, T.b))           -- r3  - 3
G.C = alt(seq(T.c, V.C),       -- r4  - 3
      seq(T.c, V.A),           -- r5  - 3
      seq(T.c, seq(V.A, V.D)), -- r6  - 3
      seq(T.c, seq(V.D, V.D))  -- r7  - 2
      )
G.D = alt(T.d,                 -- r8  - 1
      T.dd)                    -- r9  - 1