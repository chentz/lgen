require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

setCoverageCriteria(terminalC)
--require ("lenGrammar")

--[[
A = A,a,A|c;
]]
G.startSymbol = "A"
G.A = alt(seq(seq(V.A, terminal('a')), V.A), terminal('c'))

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])