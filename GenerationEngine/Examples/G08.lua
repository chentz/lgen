require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

G.startSymbol = "ST"
setCoverageCriteria(terminalC)

G.ST = oneOrMore(V.NUM09)
G.NUM09 = alt(terminal('N0'), terminal('N1'), terminal('N3'))

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
