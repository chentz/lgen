require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

G.startSymbol = "ST"
setCoverageCriteria(terminalC)
--[[
ST := B | {'c', A};
A  := B, 'd' | A | ST;
B  := ST;
]]

G.ST = alt(V.B, V.STVRZ)
G.STVRZ = alt(empty(),
              seq(terminal('c'),V.A),
              V.STVRZ)
G.A = alt(seq(V.B, terminal('d')),
          V.A, 
          V.ST)
G.B = V.ST

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])

--[[

[A]={'A','B','d','ST'}
[STVRZ]={'','A','c','STVRZ'}
[ST]={'B','STVRZ'}
[B]={'ST'}

[A]={'','c','d'}
[STVRZ]={'','c','d'}
[ST]={'','c','d'}
[B]={'','c','d'}
]]