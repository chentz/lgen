G.startSymbol = "S"
                           --   P. ID - MinTree
G.S = seq(V.A, V.C)            -- r6  - 3
G.A = seq(V.B, T.a)            -- r1  - 2
G.B = alt(T.b,                 -- r2  - 1
      seq(V.A, T.b))           -- r3  - 3
G.C = alt(empty(),             -- r4  - 1
      seq(T.c, V.C))           -- r5  - 2