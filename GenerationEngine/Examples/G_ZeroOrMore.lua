require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
--local terminalC = require ("terminalCoverage")

G.startSymbol = "ST"
--setCoverageCriteria(terminalC)
--[[
ST := 'a' B1 'c' B1 ;
B1  := {B2}
B2  := 'b';
]]
G.ST = seq(terminal('a'), seq(V.B1, seq(terminal('c'), V.B1) ))
G.B1 = zeroOrMore(V.B2) 
G.B2 = terminal('b')

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])