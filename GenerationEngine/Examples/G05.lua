require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

G.startSymbol = "Component"
setCoverageCriteria(terminalC)

G.Component = seq(terminal("MACHINE"), seq(terminal("ID"), seq(V.Machine_abstractVRZ, terminal("END"))))
G.Machine_abstractVRZ = alt(terminal("xxx"), seq(V.Clause_machine_abstract, V.Machine_abstractVRZ))
G.Clause_machine_abstract = alt(V.Clause_abstract_variables, terminal("OP"))
G.Clause_abstract_variables = alt(terminal("ABSTRACT_VARIABLES"), terminal("VARIABLES"))
G.Ident = terminal("ID")

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
