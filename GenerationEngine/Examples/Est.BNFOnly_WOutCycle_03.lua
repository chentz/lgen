--[[
S = a,S,b | c
]]

G.startSymbol = "S"

--G.S = alt(seq(terminal('a'),seq(V.S, terminal("b"))), terminal('c'))
G.S = alt(seq(seq(terminal('a'),V.S), terminal("b")), terminal('c'))

--[[Forma 1 - seq(x,seq(y,z))
1- 03.10 (466.43)
2- 03.04 (681.67)
3- 03.03 (608.78)
Media 3.06 (585.63)

Forma 2 - seq(x, seq(y,z))
1- 03.04 (565.06)
2- 03.09 (599.58)
3- 03.09 (436.10)
Media 3.07 (533.58)
]]