require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

G.startSymbol = "ST"
setCoverageCriteria(terminalC)
--[[
ST = X1 | X2;
X1 = ('1'|'2'),('3'|'4')
X2 = '2' | ('5','1')
]]

G.ST = alt(V.X1, V.X2)
G.X1 = seq(alt(terminal('1'), terminal('2')), alt(terminal('3'), terminal('4')))
G.X2 = alt(terminal('2'), seq(terminal('5'), terminal('1')))

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
