--require ("grammar_engine")
--require ("grammar_engine_EBNF")
--local terminalC = require ("terminalCoverage")

--setCoverageCriteria(terminalC)
require ("lenGrammar")

--[[
(1,)
ST>B>
     A>B>A>c
       c   
         C>c>A>c
     C>c
       A>B>
ST = A|B;
A = a,B|c;
B = A,b,C;
C = c|A;
]]
G.startSymbol = "ST"
G.ST = alt(V.A, V.B)
G.A = alt(seq(terminal('a'), V.B), terminal('c'))
G.B = seq(seq(V.A, terminal('b')), V.C)
G.C = alt(terminal('c'),V.A)

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])

--[[

[A]={'A','B','d','ST'}
[STVRZ]={'','A','c','STVRZ'}
[ST]={'B','STVRZ'}
[B]={'ST'}

[A]={'','c','d'}
[STVRZ]={'','c','d'}
[ST]={'','c','d'}
[B]={'','c','d'}
]]