require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")
local productionC = require ("productionCoverage")
setCoverageCriteria(productionC)

--[[ bank ::= action*
	action ::= dep | deb
	dep ::= "deposit" account amount
	deb ::= "debit" account amount
	account ::= digit3
	amount ::= "$" digit+ "." digit2
	digit ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
--]]	 

G.startSymbol = "bank"
G.bank = V.actionVRZ
G.actionVRZ = alt(empty(), seq(V.action, V.actionVRZ))
G.action = alt(V.dep, V.deb)
G.dep = seq(terminal("deposit"), seq(V.account, V.amount))
G.deb = seq(terminal("debit"), seq(V.account, V.amount))
G.account = seq(V.digit, seq(V.digit, V.digit))
G.amount = seq(terminal("$"), seq(oneOrMore(V.digit), seq(terminal("."), seq(V.digit, V.digit))))
G.digit = alt(terminal("0"), terminal("1"), terminal("2"), terminal("3"), terminal("4"), terminal("5"), terminal("6"), terminal("7"), terminal("8"), terminal("9"))

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
