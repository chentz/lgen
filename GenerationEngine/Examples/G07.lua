require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

--local g = {startSymbol="Component"}
G.startSymbol = "Component"
setCoverageCriteria(terminalC)

G.Component = V.Machine_abstract
G.Machine_abstract = seq(terminal("MACHINE"), seq(V.Header, seq(V.Machine_abstractVRZ, terminal("END"))))
G.Machine_abstractVRZ = alt(empty(), seq(V.Clause_machine_abstract, V.Machine_abstractVRZ))
G.Clause_machine_abstract = alt(V.Clause_abstract_variables, V.Clause_operations)
G.Clause_abstract_variables = alt(seq(terminal("ABSTRACT_VARIABLES"), oneOrMoreSep(V.Ident, ",")), seq(terminal("VARIABLES"), oneOrMoreSep(V.Ident, ",")))
G.Clause_operations = seq(terminal("OPERATIONS"), oneOrMoreSep(V.Operation, ";"))
G.Operation = seq(V.Header_operation, seq(terminal("="), V.Level1_substitution))
--[[
G.Header_operation = seq(optional(seq(oneOrMoreSep(V.Ident, ","), terminal("<--"))), 
                         seq(oneOrMoreSep(V.Ident, "."), 
												     optional(seq(terminal("("), 
														              seq(oneOrMoreSep(V.Ident, ","), terminal(")"))
																					)
																			)
														)
													)

G.Header_operation = seq(alt(terminal("<--"), empty()),
                         seq(V.Ident, 
												     alt(seq(terminal("("), terminal(")")	),
																 empty()
																			)
														)
													)
]]

G.Header_operation = seq(alt(terminal("<--"), empty()),
                         seq(V.Ident, 
												     alt(terminal("("), empty())
														)
													)
G.Level1_substitution = terminal("ls")
G.Header = seq(V.Ident, optional(seq(terminal("("), seq(oneOrMoreSep(V.Ident, ","), terminal(")")))))
G.Ident = terminal("ID")

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
