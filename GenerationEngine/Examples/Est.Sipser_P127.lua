--[[
ST = A,B
A = 'a'
B = 'b'
]]

G.startSymbol = "ST"

G.ST = seq(V.A, V.B) 
G.A = terminal('a')
G.B = terminal('b')