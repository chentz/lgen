require('init')

require("grammar_engine")
require ("grammar_engine_EBNF")

local productionC = require ("productionCoverage")
setCoverageCriteria(productionC)


G.startSymbol = 'Call'
G.Call = seq(V.CallerOS, seq(V.ServerOS, V.CalleeOS))
G.CallerOS= alt( terminal('Mac'), terminal('Win'))
G.ServerOS= alt( terminal('Lin'), terminal('Sun'), terminal('Win'))

G.CalleeOS= alt( terminal('Mac'), terminal('Win'))

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])