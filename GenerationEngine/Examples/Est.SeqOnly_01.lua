require('init')

require ("grammar_engine")

G.startSymbol = "ST"

--[[
ST = 'a', 'b', AB
AB = 'a2b2'
]]

G.ST = seq(seq(terminal('a'), terminal('b')), V.AB)
G.AB = terminal('a2b2')

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
