--[[
X = aX | bX | a | b

L(G) = {e^ie^j | e in {a,b} and i,j > 0}

w in L(G)

lg(w) = 1
QtdTrees(1) = 2
MaxTreeHeight = 1

a = {X => a}
b = {X => b}

lg(w) = 2
QtdTrees(2) = 4, h <= 2 : 6
MaxTreeHeight = 2

aa = {X => aX => aa}
ab = {X => aX => ab}
ba = {X => bX => ba}
bb = {X => bX => bb}

lg(w) = 3
QtdTrees(3) = 8, h <= 3 : 14
MaxTreeHeight = 3

aaa = {X => aX => aaX => aaa}
aab = {X => aX => aaX => aab}
aba = {X => aX => abX => aba}
abb = {X => aX => abX => abb}
baa = {X => bX => baX => baa}
bab = {X => bX => baX => bab}
bba = {X => bX => bbX => bba}
bbb = {X => bX => bbX => bbb}

lg(w) = 4
QtdTrees(4) = 2^4 = 16, h <= 4 : 30
MaxTreeHeight = 4
]]

G.startSymbol = "X"

G.X = alt(seq(terminal('a'), V.X),
          seq(terminal('b'), V.X), 
          terminal('a'),
          terminal('b'))