require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

G.startSymbol = "ST"
setCoverageCriteria(terminalC)

G.ST = seq(terminal("a"), seq(V.B, V.C))
G.B = terminal("1")
G.C = alt(terminal("d"), seq(terminal("c"), V.B))

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])
