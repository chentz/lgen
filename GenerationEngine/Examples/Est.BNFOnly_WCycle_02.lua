--[[
X = 'a' | 'b' | XX

if G is not ambiguous
f[x_] = f[h-1] + 2^h
f[1] = 2

w in L(G)

lg(w) = 1
QtdTrees(1) = 2

a = {X => a}
b = {X => b}

lg(w) = 2
QtdTrees(2) = 2 + (4 * 1) = 6

aa = {X => XX => aX => aa}
ab = {X => XX => aX => ab}
ba = {X => XX => bX => ba}
bb = {X => XX => bX => bb}

lg(w) = 3
QtdTrees(3) = 6 + (8 * 2) = 22
aaa ={X => XX => XXX => aXX => aaX => aaa
                 aX  => aXX => aaX => aaa }
                 
aab ={X => XX => XXX => aXX => aaX => aab
                 aX  => aXX => aaX => aab }

aba ={X => XX => XXX => aXX => abX => aba
                 aX  => aXX => abX => aba }
                 
abb ={X => XX => XXX => aXX => abX => abb
                 aX  => aXX => abX => abb }
                 
baa ={X => XX => XXX => bXX => baX => baa
                 bX  => bXX => baX => baa }

bba ={X => XX => XXX => bXX => bbX => bba
                 bX  => bXX => bbX => bba }

bba ={X => XX => XXX => bXX => bbX => bba
                 bX  => bXX => bbX => bba }
                 
bbb ={X => XX => XXX => bXX => bbX => bbb
                 bX  => bXX => bbX => bbb }
                 
lg(w) = 4
QtdTrees(4) = 22 + (16 * 5) = 102

aaaa ={X => XX => XXX => XXXX => aXXX => aaXX => aaaX => aaaa
                      => aXX  => aXXX => aaXX => aaaX => aaaa 
               => aX  => aXX
               
                      => aaX  => aaXX => aaaX => aaaa
                      => aXX  => aXXX => aaXX => aaaX => aaaa
                         aaX  => aaXX => aaaX => aaaa
                         }
                         ...
bbbb

lg(w) = 5
QtdTrees(5) = 102 + (32 * ) = 

aaaaa ={X => XX => XXX => XXXX => XXXXX => aXXXX => aaXXX => aaaXX => aaaaX => aaaa
                               => aXXX  => aXXXX => aaXXX => aaaXX => aaaaX => aaaa
                                        => aaXX  => aaXXX => aaaXX => aaaaX => aaaa
                                                 => aaaX  => aaaXX => aaaaX => aaaa                               
                       => aXX  => aXXX  => aaXX => aaaX => aaaa 
                               => aaX   => aaXX => aaaX => aaaa
                       => aaX  =>
                       
                   aXX => aXXX => aaXX  => aaaX => aaaa
                          aaX  => aaXX  => aaaX => aaaa
                          }
                         ...
bbbbb
]]

G.startSymbol = "X"

G.X = alt(terminal('a'), terminal('b'), seq(V.X, V.X))