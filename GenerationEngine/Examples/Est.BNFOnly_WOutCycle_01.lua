--[[
ST = 'a', 'b' | AB
AB = 'a2b2'
]]

G.startSymbol = "ST"

G.ST = alt(seq(terminal('a'), terminal('b')) , V.AB)
G.AB = terminal('a2b2')