require('init')
--[[
require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

setCoverageCriteria(terminalC)
]]
require ("lenGrammar")

G.startSymbol = "Component"
G.Component = V.Predicate
G.Predicate = alt(V.Conjunction_predicate, V.Equals_predicate)
G.Conjunction_predicate = seq(seq(V.Predicate, terminal("&")), V.Predicate)
G.Equals_predicate = seq(seq(V.Expression, terminal("=")), V.Expression)
G.Expression = V.Expressions_primary
G.Expressions_primary = V.Data
G.Data = alt(oneOrMoreSep(V.Ident, "."), seq(oneOrMoreSep(V.Ident, "."), terminal("$0")))
G.Ident = terminal("ID")



gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])