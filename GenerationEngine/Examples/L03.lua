require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")
local terminalC = require ("terminalCoverage")

setCoverageCriteria(terminalC)
--require ("lenGrammar")

--[[
A = A,B,A|c;
B = a
]]

G.startSymbol = "A"
G.A = alt(seq(seq(V.A, V.B), V.A), terminal('c'))
G.B = terminal('a')

gen(G, tonumber(arg[1]), tonumber(arg[2]), tonumber(arg[3]), arg[4] or arg[0])