require('init')

require ("grammar_engine")
require ("grammar_engine_EBNF")

--local terminalC = require ("terminalCoverage")
--setCoverageCriteria(terminalC)
local productionC = require ("productionCoverage")
setCoverageCriteria(productionC)

G.startSymbol = "ST"

G.ST = alt(V.D, V.EB)
G.D = alt(V.I, seq(V.I, terminal("$")))
G.EB = terminal("eb")
G.I = terminal("ID")

gen(G, tonumber(arg[1]), tonumber(arg[2]), arg[3] or arg[0])
