local Set = require("Set")


--[[
 - Calcula e retorna todas as ocorrências da gramática.
 -
 - As ocorrências são os requisitos que o critério de
 - cobertura deve cumprir. Elas são armazenadas como
 - string seguindo o formato:
 -
 -     <regra1>_<posição>_<regra2>,
 -
 - onde:
 -
 - <regra1>  é a produção de origem
 - <posição> é a posição da produção de origem em que
 -           se encontra o não-terminal a ser expandido
 - <regra2>  é a produção a ser aplicada no não-terminal
 -
 - Exemplo:
 -
 -     Gramática:
 -
 -         S = A alt('x' | 'y' 'z' A) zm('q' A)
 -         A = 'a'
 -
 -     Ocorrências:
 -
 -         * S#1_1_A#1
 -         * S#1_2#2.3_A#1
 -         * S#1_3.2_A#1
 -
--]]
function getOccsFromGrammar(grammar, previsionEnabled)
  local occs = {}
  local closure = {}
  for nt, ntRules in pairs(grammar) do
    closure[nt] = {}
    for id, rule in ipairs(ntRules) do
      closure[nt][id] = Set.new{}
      for pos, item in ipairs(rule) do
        prefix = string.format("%s#%d_%d", nt, id, pos)
        local itemOccs = getOccsFromItem(prefix, item, grammar)
        for k, v in ipairs(itemOccs) do
          occs[#occs + 1] = v
          closure[nt][id]:include(v)
        end
      end
    end
  end

  if previsionEnabled then
    -- Continua o cálculo do fecho até concluir
    local closureNotEnd = true
    --print("Gerando fecho de ocorrencias...")
    while closureNotEnd do
      closureNotEnd = false
      for nt, ntClosure in pairs(closure) do
        for id, ruleClosure in ipairs(ntClosure) do
          for occ in pairs(ruleClosure:internalSet()) do
            local _, _, _, nt2, id2 =
                string.match(occ, "(.+)#(%d+)_([\\.#%d]+)_(.+)#(%d+)")
            id2 = tonumber(id2)
            for _occ in pairs(closure[nt2][id2]:internalSet()) do
              if not closure[nt][id]:inSet(_occ) then
                closure[nt][id]:include(_occ)
                closureNotEnd = true
              end
            end
          end
        end
      end
    end
    --print("\nGeracao do fecho concluida!\n")
  end

  return Set.new(occs), closure
end


function getOccsFromItem(prefix, item, grammar)
  local occs = {}
  if item.t == "nonterm" then
    local nt2 = item.v
    for id2, rule in ipairs(grammar[nt2]) do
      occs[#occs + 1] = prefix .. "_" .. nt2 .. "#" .. id2
    end
  elseif item.t == "alt" then
    for argIdx, arg in ipairs(item.v) do
      for elemIdx, elem in ipairs(arg) do
        local elemPrefix = prefix .. "#" .. argIdx .. "." .. elemIdx
        local elemOccs = getOccsFromItem(elemPrefix, elem, grammar)
        for k, v in ipairs(elemOccs) do
          occs[#occs + 1] = v
        end
      end
    end
  elseif item.t == "zm" or item.t == "om" or item.t == "omsep" then
    if item.t == "zm" then
      prefix = prefix .. ".1#2"
    end
    for elemIdx, elem in ipairs(item.v) do
      local elemPrefix = prefix .. "." .. elemIdx
      local elemOccs = getOccsFromItem(elemPrefix, elem, grammar)
      for k, v in ipairs(elemOccs) do
        occs[#occs + 1] = v
      end
    end
  end
  return occs
end


--[[
 - Retorna as ocorrências usadas na sentença atual.
 -
 - As ocorrências são obtidas a partir da árvore
 - de derivação da sentença.
--]]
function getOccsFromTree(tree)
  local occs = {}
  getOccsFromTreeRec(tree, occs)
  return Set.new(occs)
end


--[[
 - Função recursiva que adiciona em 'occs' as ocorrências
 - existentes no nó de entrada e em seus respectivos nós-filhos.
--]]
function getOccsFromTreeRec(node, occs, prefix)
  local pos = node.position
  local id = node.productionId

  if node.symbolType == "nonterm" then
    local nt = node.symbol
    if prefix then
      occs[#occs + 1] = prefix .. pos .. "_" .. nt .. "#" .. id
    end
    prefix = nt .. "#" .. id .. "_"
  elseif node.symbolType == "alt" then
    prefix =  prefix .. pos .. "#" .. id .. "."
  elseif node.symbolType == "zm"
      or node.symbolType == "om"
      or node.symbolType == "omsep" then
    prefix =  prefix .. pos .. "."
  end

  if node.vec then
    for _, subNode in ipairs(node.vec) do
      getOccsFromTreeRec(subNode, occs, prefix)
    end
  end
end


--[[
 - Função recursiva que retorna uma cópia em memória
 - da árvore de derivação.
 -
 - A chamada a esta função deve ser 'treeCopy(tree)'.
--]]
function treeCopy(node, parent)
  local t = {}
  t.symbol = node.symbol
  t.symbolType = node.symbolType
  t.productionId = node.productionId
  t.position = node.position
  t.parent = parent
  if node.vec then
    t.vec = {}
    t.vecSize = node.vecSize
    t.cap = node.cap
    for i, v in ipairs(node.vec) do
      t.vec[i] = treeCopy(v, t)
    end
  end
  return t
end


--[[
 - Retorna o nó pertencente à árvore 'tree' que seja
 - equivalente ao nó 'node'.
 -
 - O nó é obtido da seguinte forma:
 -
 -     * primeiro, é registrado o caminho de nós a partir
 -       de 'node' até a raiz;
 -     * em seguida, esse caminho é reproduzido em 'tree'.
 -
 - Essa função é utilizada para obter a variável 'currentNode'
 - de uma cópia da árvore.
--]]
function getEquivNodeFromTree(tree, node)
  local reversePath = {}
  local n = node

  -- Registra o caminho de nós a partir de
  -- 'node' até a raiz
  while n.parent do
    for i, v in ipairs(n.parent.vec) do
      if v == n then
        reversePath[#reversePath + 1] = i
        n = n.parent
        break
      end
    end
  end

  -- Reproduz o caminho construído para obter
  -- o nó correspondente na nova árvore
  n = tree
  for i = #reversePath, 1, -1 do
    n = n.vec[ reversePath[i] ]
  end

  return n
end

function sentenceNotContainsNewOccs(sentenceOccs, coveredOccs)
  local newOccs = Set.diff(sentenceOccs, coveredOccs)
  return Set.card(newOccs) == 0
end

function isAllRuleOccsCovered(coveredOccs, ruleClosure)
  if Set.card(ruleClosure) > 0 then
    local uncoveredOccs = Set.diff(ruleClosure, coveredOccs)
    if Set.card(uncoveredOccs) == 0 then
      return true
    end
  else
    return true
  end
  return false
end

function getOccFromNtNode(node)
  local occ = node.position .. "_" .. node.symbol .. "#" .. node.productionId
  local n = node.parent
  while n and n.symbolType ~= "nonterm" do
    if n.symbolType == "alt" then
        occ = n.position .. "#" .. n.productionId .. "." .. occ
    else -- zeroOrMore, oneOrMore, oneOrMoreSep
      occ = n.position .. "." .. occ
    end
    n = n.parent
  end

  if not n then
    return nil
  else
    occ = n.symbol .. "#" .. n.productionId .. "_" .. occ
  end

  return occ
end

