local Set = require("Set")


local grammarOrder = {}


function setNtOrder(nt)
  grammarOrder[#grammarOrder + 1] = nt
end


function itemStr(item)
  local concatElems = function(args, str)
                        if not str then
                          str = ""
                        end
                        for id, elem in ipairs(args) do
                          if (id > 1) then
                            str = str .. ", "
                          end
                          str = str .. itemStr(elem)
                        end
                        return str
                      end

  if item.t == "nonterm" then
    return item.v
  elseif item.t == "term" then
    if item.v == '' then
      return ""
    else
      return "\"" .. item.v .. "\""
    end
  elseif item.t == "alt" then
    local res = "("
    for argIdx, arg in ipairs(item.v) do
      res = concatElems(arg, res)
      if argIdx < #item.v then
        res = res .. " | "
      end
    end
    return res .. ")"
  elseif item.t == "zm" then
    return "{" .. concatElems(item.v) .. "}"
  elseif item.t == "om" then
    return "(" .. concatElems(item.v) .. ")+"
  elseif item.t == "omsep" then
    return "(" .. concatElems(item.v) .. ")+" .. itemStr(item.sep)
  elseif item.t == "opt" then
    return "[" .. concatElems(item.v) .. "]"
  end
  return "unknown()"
end


function printGrammar(grammar)
  local s = ""
  for _, nt in ipairs(grammarOrder) do
    s = s .. nt .. " = "
    local ntRules = grammar[nt]
    for ruleId, rule in ipairs(ntRules) do
      --print("printGrammar", ruleId, rule)
      for k, v in ipairs(rule) do
        s = s .. itemStr(v)
        if (k < #rule) then s = s .. ", " end
      end
      if (ruleId < #ntRules) then
        s = s .. "\n    | "
      end
    end
    s = s .. ";\n\n"
  end
  print(s)
end


--[[
 - Imprime a árvore de derivação da sentença.
--]]
function printTree(tree)
  print("\nTree:")
  printTreeRec(tree, "")
end


--[[
 - Função recursiva que imprime as informações do nó de entrada
 - e de seus respectivos subnós.
--]]
function printTreeRec(node, str)
  if node then
    if node.symbolType == "term" then
      print(
        str .. string.format(
          "\"%s\" (%d)",
          node.symbol,
          node.position
        )
      )
    else
      print(
        str .. string.format(
          "%s#%d (%d)",
          node.symbol,
          node.productionId,
          node.position
        )
      )
    end
    if node.vec then
      for _, subnode in ipairs(node.vec) do
        printTreeRec(subnode, str .. "----------")
      end
    end
  end
end

function printClosure(closure)
  for _, nt in ipairs(grammarOrder) do
    local ntClosure = closure[nt]
    for id, ruleClosure in ipairs(ntClosure) do
      print(nt, id, ruleClosure)
    end
  end
end

