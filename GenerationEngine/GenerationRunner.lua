--- Module to run the generation engine as an API.
-- @author Cleverton Hentz
--
-- Dependencies: `Util`, `grammar_engine`, `grammar_engine_EBNF`, `grammar_transformation`

require ("init")

local Util = require("Util")

require("grammar_engine")
require("grammar_engine_EBNF")
require("grammar_transformation")

local GenRunner = {}

local mt = {__index=GenRunner}

--- send the output to the console.
function GenRunner.outPrintConsole(sent, index)
  print(index,sent)
end

--- discard the output.
function GenRunner.outSendNull(info, index)
  
end

--- output limited function.
function GenRunner.outLimitNumber(lNumber, fOut)
  if not fOut then error("GenRunner.outLimitNumber:Invalid fOut!") end
  
  return function(sent, index)
    local rF = fOut(sent, index)
    
    if index >= lNumber then return true end
    
    return rF
  end  
end

--- output in a table.
function GenRunner.outTable()
  local buf = {}
  return function(sent, index)
    buf[#buf+1] = sent
    
    return false
  end, buf
end

--- Default table for runtime configuration.
-- Can be used on the configuration files.
local DefOptionsTab = {
 quietExec=true, -- Quiet execution
 maxCyclesOp = 1,-- Max. cycles inside an operator. Like, `oneOrMore` operator.
 maxCycles = 1,  -- Global max. nonterminal cycles 
 maxDerivLen = 3,-- Max. derivation tree height
 maxDerivLenFinal = 4, -- Max. derivarion tree height final. Only for `stats`
 coverageCrit = nil,-- Coverage criterion to use
 filePrefix = nil, -- Prefix for the output files
 FuncResult = GenRunner.outPrintConsole, -- Default function to handle the result sentences
 FuncMeta = GenRunner.outSendNull, -- Default function to handle the meta-information about the sentences
 transGrammarCache = false, -- Use a cache file save the transformation grammar
 grammarTrans = nil, -- Use the grammar transformation chain
 useRandomAlts = false, -- Use the pseudo-random algorithm to sort the alts instructions
 altStartSymbol = nil -- Use an alternative start symbol to the generation process
}

local function modelInternal() return {id='GenRunner'} end

local function isAGenRunner(obj)
  return (type(obj) == 'table' and obj.id and obj.id == 'GenRunner')
end

--- create a new GenerationRunner object from def. values
function GenRunner.new(initConfig)
  if initConfig and type(initConfig) ~= 'table' then error("Invalid initial config!") end
  
  local gr = setmetatable(modelInternal(), mt)
  gr.cConfig = Util.defTableValue(initConfig, DefOptionsTab)
  return gr
end

--- create a new GenerationRunner object from a config file.
-- It create a new GenerationRunner object based on a config file.
-- @param f Path to the `.conf` file.
-- @return a new GenerationRunner
function GenRunner.newConfFile(f)
  if not Util.fileExists(f) then error("Invalid file!") end
  
  local gr = setmetatable(modelInternal(), mt)
end

--- return a copy of the current configuration.
function GenRunner.getConfig(gr)
  return Util.copy(gr.cConfig)
end

--- change the current configuration.
-- @param gr The GenRunner object
-- @param newConfig A table with the new config. See `DefOptionsTab` to all options.
function GenRunner.setConfig(gr, newConfig)
  if newConfig then gr.cConfig = Util.defTableValue(newConfig, gr.cConfig) end
end

local function loadGrammarF(lf, GTst, VTst, TTst)
  local safeEnv = {
          G = GTst,
          V = VTst,
          T = TTst,
          
          seq = seq,
          alt = alt,
          terminal = terminal,
          empty = empty,
          idInc = idInc,
          
          zeroOrMore = zeroOrMore,
          oneOrMore = oneOrMore,
          oneOrMoreSep = oneOrMoreSep,
          optional = optional,
          range = range,
          negation = negation
        }
  --print("loadGrammar", GTst, VTst, TTst)
  local gEx, err = loadfile(lf, 't', safeEnv)
  if (gEx) then
    gEx()
  else
    print(err)
    return false
  end
  return true
end

local function runGrammar(gr, rG)
  local rc = gr.cConfig
  if not rc.quietExec then print("GenRunner.runGFile:Config", Util.tostring(rc)) end
    
  local oldSS = rG.startSymbol
  if rc.altStartSymbol then
    if not rG[rc.altStartSymbol] then error("Invalid altStartSymbol!") end
    
    rG.startSymbol = rc.altStartSymbol
  end
  
  if rc.grammarTrans then
    print(rc.grammarTrans)
    rG = grammarTransformations(rG, rc, rc.grammarTrans)
  end
  
  local sentCount, usedNTerms, rStatus, highestT = genCustom(rG, rc.maxCyclesOp, rc.maxCycles,
    rc.maxDerivLen, rc.FuncResult, rc.FuncMeta, rc)
   
  if rc.altStartSymbol then rG.startSymbol = oldSS end
  
  return sentCount, usedNTerms, rStatus, highestT
end

function GenRunner.createGrammarEnv(gr)
  local rc = gr.cConfig
  local newCC = createCC()
  
  if rc.coverageCrit then
    local cc = require(rc.coverageCrit)
    if not cc then error("Error loading the CC: " .. rc.coverageCrit) end
    
    setCoverageCriteria(cc, newCC)
  else setCoverageCriteria(nil, newCC) end
  
  local newG, newV, newT = createGrammar(newCC), createNonterminalTable(), createTerminalTable()
  return newCC, newG, newV, newT
end

--- run the generation engine based on a grammar file.
function GenRunner.runGFile(gr, gFile)
  --print("GenRunner.runGFile")
  local rc = gr.cConfig
  
  local FCCTst, GTst, VTst, TTst = GenRunner.createGrammarEnv(gr)
  
  local rGEx = loadGrammarF(gFile, GTst, VTst, TTst)
  if not rGEx then error("Error loading the grammar!") end
  
  return runGrammar(gr, GTst)
end

--- execute the GenerationRunner object.
-- @param gr GenerationRunner object.
function GenRunner.runGrammar(gr, rG)
  return runGrammar(gr, rG)
end

return GenRunner