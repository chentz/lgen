--- Build a graph production based graph representing the grammar.
-- @script buildProdGraph
-- @usage
-- arg[1] -> Lua Grammar File
-- arg[2] -> Action.
-- @author Cleverton Hentz
--
-- Dependencies: `dbg`, `Set`, `Relation`, `MetaInfo`, `Util`

require("init")

local dbg = require("dbg"):new{}
local Set = require("Set")
local Rel = require("Relation")
local MI = require("MetaInfo")
local Util = require("Util")


local cOper = MI.cOper

G = setmetatable({}, {
      __newindex = function (t, k, v)
        if k ~= "startSymbol" then
          local r = prodDef(t, k, v)
          if r then v = r end
        end
        
        return rawset(t,k,v)
      end
    })

V = setmetatable({}, {
      __index = function (t, k) return non_terminal(k) end
    })

T = setmetatable({}, {
      __index = function (t, k) return terminal(k) end
    })

function prodDef(table, key, value)
  return nil
end

function idInc(c,p)
  return MI.new(nil,cOper.idInc,nil,c,p)
end

function terminal(s)
  return MI.new(nil,cOper.term,nil,s)
end

function range(b,e)
  return MI.new(nil,cOper.range,nil,b,e)
end

function empty()
  return MI.new(nil,cOper.emp)
end

function zeroOrMore(patt)
  return MI.new(nil,cOper.zm,nil,patt)
end

function optional(patt)
  return MI.new(nil,cOper.opt,nil,patt)
end

function oneOrMore(patt)
  return MI.new(nil,cOper.om,nil,patt)
end

function oneOrMoreSep(patt, str)
  return MI.new(nil,cOper.oms,nil,patt,str)
end

function seq(patt1, patt2)
  return MI.new(nil,cOper.seq,nil,patt1,patt2)
end

function alt(...)
  return MI.new(nil,cOper.alt,nil,...)
end

function negation(patt)
  return MI.new(nil,cOper.neg,nil,patt)
end

function non_terminal(NT)
  return MI.new(nil,cOper.nt,nil,NT)
end

local function getOrderIndex(g)
  local r = {}
  for k,_ in pairs(g) do
    r[#r+1] = k
  end
  table.sort(r)
  return r
end

local function printPDG(g, bName)
  local t = ""
  local newG, rPId, rSym = MI.extractStructures(g, "r", getOrderIndex(g))
  if not rPId then error("Invalid grammar!") end
  
  local r = rSym..rPId
  --print("printPDG.r:", r)
  --print(rPId)
  local vCount = rPId:card()
  local eCount = r:card()
  local dotTamp = ' P%s [label="%s(%s)"%s];\n'
  local ss
  for a,b in pairs(rPId) do
    if a == g.startSymbol then ss = ",fontcolor=red"
    elseif not (r%b) then ss = ",shape=box,fontcolor=blue"
    else ss = "" end
    t = t .. string.format(dotTamp, b, b, a, ss)
  end
  
  dotTamp = '\n P%s -> P%s;'
  for a,b in pairs(r) do
    t = t .. string.format(dotTamp, a, b)
  end
  
  return string.format([[digraph G {
 //Title
 labelloc="t";
 label="Prodution Dependency Graph to %s(%d+%d = %d)";
%s
}]], bName, vCount, eCount, vCount+eCount, t)
end

local function printGDG(g, bName)
  local t = ""
  local newG, rPId, rSym = MI.extractStructures(g,"r", getOrderIndex(g))
  if not rPId then error("Invalid grammar!") end
  
  local r = rPId..rSym
  local vCount = 0
  local eCount = r:card()
  
  --print("Result:", r)
  local dotTamp = ' P%s [label="%s"%s];\n'
  local ss
  local mapProd = {}
  for a,b in pairs(rPId) do
    if a == g.startSymbol then ss = ",fontcolor=red"
    elseif not (r%a) then ss = ",shape=box,fontcolor=blue"
    else ss = "" end
    if not mapProd[a] then
      t = t .. string.format(dotTamp, b, a, ss)
      mapProd[a] = b
      vCount = vCount+1
    end
  end
  
  dotTamp = '\n P%s -> P%s;'
  local va,vb
  for a,b in pairs(r) do
    va = mapProd[a]
    vb = mapProd[b]
    --print(va,vb)
    t = t .. string.format(dotTamp, va, vb)
  end
  
  return string.format([[digraph G {
 //Title
 labelloc="t";
 label="Grammar Dependency Graph to %s (%d+%d=%d)";
%s
}]], bName, vCount, eCount, vCount+eCount, t)
end

local function getMostComplexSymbol(Symbols, RulesDef, MinTreeRel)
--Most complex symbol
  --local r1 = rSym:invert()
  local tCardOut = {}
  local sElem, t2, c
  
  if not limit then limit = Symbols:card() end
  
  for k,s in Symbols:getSetIteration() do
    --print(k)
    if MinTreeRel then c = (MinTreeRel%s):min()
    else c = 0 end
    --print(k,s,c)  
    tCardOut[k] = s:card()*0.8 + c*0.2
  end
  --dbg:echo(tCardOut)
  --dbg:echo(tCardOut)
  
  t2 = {}
  for k,v in pairs(tCardOut) do
    t2[#t2+1] = {key=k,index=v}
  end
  
  table.sort(t2, function (a,b) return a.index > b.index end)
  return t2
end

local function printStats(g, bName)
  local t = ""
  local t2, a, b
  local igOps = {cOper.zm,cOper.opt}
  
  local nG, rPId, rSym, sExcNT, sTerm
  local fPId, fSym
  
  --Information about GDG
  nG, fPId, fSym, sExcNT, sTerm = MI.extractStructures(g,"r", getOrderIndex(g), false)
  local numNT = fPId:domain():card()
  local gdgGraph = fPId..fSym
  local ShortPaths = gdgGraph:shortestPath(g.startSymbol)
  local maxPathGraph = ShortPaths:range():max()
  --print("Max. path lenght table:",ShortPaths)
  print("Max. path lenght(GDG Vertex Eccentricity):",maxPathGraph)
    
  --print("Grammar hashCode:", Util.hashCode(g))
  nG, rPId, rSym, sExcNT = MI.extractStructures(g,"r", getOrderIndex(g), true, igOps)
  if not rPId then error("Invalid grammar!") end
  
  local tCard = rPId:card()
  local cardLimit = 50
  local r = rSym..rPId
  print("startSymbol: ", g.startSymbol)
  if tCard < cardLimit then print("Productions:", tCard, rPId)
  else print("Productions card.:", tCard) end
  --print("rPId hashCode:", rPId:hashCode())
  print("Num. Nonterminals:", numNT)
  print("Num. Terminals:", sTerm:card())
  
  --[[
  tCard = rSym:card()
  if tCard < cardLimit then print("Prod. X Symbols:", tCard, rSym)
  else print("Prod. X Symbols card.:", tCard) end
  --print("rSym hashCode:", rSym:hashCode())
  ]]
  
  tCard = r:card()
  print("Grammar complexity index:", rPId:card()+r:card())
  if tCard < cardLimit then print("Rel:", tCard, r)
  else print("Rel. 1 Card. :",tCard) end
  
  local rTC, rTCSteps = r:transitiveClosure(-1)
  tCard = rTC:card()
  if tCard < cardLimit then print("Rel. Closure:", rTC) end
  print("Trans. Rel. card.:",tCard)
  
  --Cycles  
  local rCycles = rTC:filterOverRelation(pairs, function(k,v) return k == v end)
  if rCycles then
    print("There are cycles on grammar:")
    local rDirectCycles = r:filterOverRelation(pairs, function(k,v) return k == v end)
    print("Direct cycles:")
    local r1
    t2 = rDirectCycles:domain()
    for k,v in rPId:getSetIteration() do
      r1 = v * t2
      if r1:card() > 0 then print(k, r1) end
    end
    
    print("Indirect cycles:")
    t2 = (rCycles:domain()-rDirectCycles:domain())
    for k,v in rPId:getSetIteration() do
      r1 = v * t2
      if r1:card() > 0 then print(k, r1) end
    end
  end
  
  --Connected components
  b = r:range()
  local rNConnected = rPId:filterOverRelation(pairs, function(k,v)
    return not b:inSet(v) and not sExcNT:inSet(k) and g.startSymbol ~= k 
  end)
  print("Disconnected symbols:")
  print(rNConnected:domain())
  
  --Minimun High for each simbol
  --print("SHAs:", Util.hashCode(rPId),Util.hashCode(rSym))
  a,b = MI.calcMinTreeHeightRules(rPId, rSym)
  if a then
    print("The algorithm run Ok. The min. table is:")
    print(a)
    
    --Debug
    --[[
    local rel1 = Rel.new()
    rel1('r1',2); rel1('r10',3); rel1('r11',3); rel1('r12',3); rel1('r13',3); rel1('r14',3); rel1('r15',2); rel1('r16',4); rel1('r17',4); rel1('r18',3); rel1('r19',4); rel1('r2',3); rel1('r20',3); rel1('r21',4); rel1('r22',4); rel1('r23',3); rel1('r24',3); rel1('r25',1); rel1('r26',3); rel1('r27',3); rel1('r28',3); rel1('r29',3); rel1('r3',4); rel1('r30',3); rel1('r31',3); rel1('r32',4); rel1('r33',4); rel1('r34',4); rel1('r35',4); rel1('r36',4); rel1('r37',1); rel1('r38',1); rel1('r39',4); rel1('r4',3); rel1('r40',4); rel1('r41',2); rel1('r42',4); rel1('r43',4); rel1('r44',5); rel1('r45',4); rel1('r46',4); rel1('r47',4); rel1('r48',5); rel1('r49',1); rel1('r5',3); rel1('r50',1); rel1('r51',2); rel1('r52',1); rel1('r53',1); rel1('r54',1); rel1('r55',2); rel1('r56',3); rel1('r57',3); rel1('r58',4); rel1('r59',2); rel1('r6',3); rel1('r60',3); rel1('r61',3); rel1('r62',2); rel1('r63',2); rel1('r64',3); rel1('r65',3); rel1('r66',3); rel1('r67',5); rel1('r68',5); rel1('r69',1); rel1('r7',3); rel1('r70',1); rel1('r71',4); rel1('r8',3); rel1('r9',3);
    if rel1 ~= a then
      print("Differences:")
      for k,v in pairs(a) do
        b = (rel1%k):first()
        if b ~= v then
          print(k,v,b)
        end
      end
    end
    ]]
    
    --Get most complex symbols
    print("Most complex symbols on grammar:")
    local rankSymbols = getMostComplexSymbol(rPId, rSym, a)
    for i=1,5 do
      print("Order:", i ,rankSymbols[i].key, rankSymbols[i].index)
    end
  
    print("startSymbol:", g.startSymbol)
    t2 = rPId%g.startSymbol
    print("startSymbol prods. ids:", t2:card(), t2)
    t2 = a:filterOverRelation(pairs, function(k,v) return t2:inSet(k) end)
    print("startSymbol Min:", t2:card(), t2)
    
    print(string.format("The smallest tree has the height of: %s (%s)", (t2:invert()):min()))
    print(string.format("The max tree has the height of: %s (%s)", (t2:invert()):max()))
    
    local minMaxSymbols = MI.getMaxOfMinSymbol(rPId, a)
    --[[
    print("Symbols Min Max:")
    for k,b in pairs(minMaxSymbols) do
      print(k, b)
    end
    ]]
    
    --print("r2",r2)
    
    t2 = Set.new()
    for k,v in pairs(ShortPaths) do
      --print(k, v+minMaxSymbols[k])
      t2:include(v+minMaxSymbols[k])
    end
    print("Min. Tree height to Production coverage is", t2:max())
  else
    print("Thera is a problem with the algorithm. The residual rel. is:")
    print(b)
  end
  return nil
end

local function readSetStr()
  local sRet = Set.new{}
  return function(s)
    local ret = false
    for v in s:gmatch("(%g+)") do
      sRet:include(v)
      ret = true
    end
    return ret
  end, sRet
end

local function printCheck(g, sFile)
  local tf, sentTerms = readSetStr()
  local ln = Util.readContent(sFile, tf)-1
  print("File number of lines:", ln)
  print("Num term. on sentence file:", sentTerms:card())
  
  local nG, rPId, rSym, sExcNT, sTerm
  local fPId, fSym
  local gSort = Util.getOrderIndex(g)
  
  --Information about GDG
  
  nG, fPId, fSym, sExcNT, sTerm = MI.extractStructures(g,"r", gSort, false)
  print("Num NT on grammar:",fPId:domain():card())
  print("Num Terminals on grammar:",sTerm:card())
  local notInSentences = (sTerm-sentTerms)
  print("Num terminals not in sentences", notInSentences:card())
  print(notInSentences)
  print("Prod. size:", fPId:card())
  
  local NTxTerm = MI.extractNTxTerms(g, gSort)
  local r2 = NTxTerm:filterOverRelation(pairs, function(k,v)
    return notInSentences:inSet(v)
  end)
  print("Number of terminals with Mutiple use on diff.:",r2:card()-r2:invert():card())
  local nc = r2:domain():card()
  print("Number of NT not in sentences*",nc,1-(nc/fPId:domain():card()))
  print(r2:domain())
  
  --[[ Testing about grammar slice and hypergraphs ]]
  
  local gdgGraph = fPId..fSym
  nc, nG = gdgGraph:shortestPath(g.startSymbol,'Natural')
  print(nc, Util.tostring(nG))
  
  print("Has direct cycles:", gdgGraph:hasDirectCycles())
  print(fSym:domain():card())
  nc = fPId:invert()
  r2 = fSym:filterOverRelation(Rel.getSetIterationSort, function(k,v)
    --print(k,v, (nc%k):first())
    return not v:inSet((nc%k):first())
  end, true)
  print("Result prod. set size:", r2:domain():card())
  print("Set prod. excluded:",(fSym:domain() - r2:domain()):card())
  --[[
  nG = Set.new(nG)
  r2 = Set.new{}
  nc = fSym:filterOverRelation(pairs, function(k,v)
    if nG:inSet(v) and not r2:inSet(v) then
      r2:include(v)
      return true
    else
      return false
    end
  end)
  print(nc)
  print(nc:domain())
  print(nc:domain():card())
  nc = (fPId%nG) + nc:domain()
  print(nc, nc:card())
  ]]
end

-- $1 => Lua Grammar file
-- $2 => Action
local file = tostring(arg[1])
if #arg > 1 then typeAct = arg[2]
else typeAct = "pdg" end

dofile(file)

local bName = string.match(file, "(.+)%.[^%.]+$")
print("Base file name:", bName)
local outFName = bName..string.format('_%s.dot', typeAct)
print("Output file name:", outFName)
if typeAct == "pdg" then
  print("Generating the production dependency graph...")
  local f = io.open(outFName,'w')
  f:write(printPDG(G, bName))
  f:close()
elseif typeAct == "gdg" then
  print("Generating the grammar dependency graph...")
  local f = io.open(outFName,'w')
  f:write(printGDG(G, bName))
  f:close()
elseif typeAct == "stats" then
  print("Generating the stats about grammar...")
  local r = printStats(G, bName)
  if r then
    local f = io.open(outFName,'w')
    f:write(r)
    f:close()
  end
elseif typeAct == "check" then
  local sentFile = arg[3]
  print("Checking grammar and sentence file...")
  if not Util.fileExists(sentFile) then
    print("Invalid sentences file!")
    return 1
  end  
  printCheck(G, sentFile)
else
  print("Invalid option!")
  return 1
end
print("Finished!")
