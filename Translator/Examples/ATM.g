/*
[Gramática ATM SPL];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar ATMSPL;

ATMsoftware
    : UserInterface Bankingfunctionality 
    | UserInterface Bankingfunctionality Additionalfunctionality
    ;

UserInterface 
            : r_1_5
            ;

r_1_5 
    : 't_Basic2Dview'
    | 't_Deluxe3Dview'
    ;

Bankingfunctionality
                   : 't_Checkingbalance' 't_Withdrawnmoney' 't_Viewtransaction' 't_Changeaccountpassword' 't_Printingbalanceafterwithdrawal' 
                   | 't_Checkingbalance' 't_Withdrawnmoney' 't_Viewtransaction' 't_Changeaccountpassword' 't_Printingbalanceafterwithdrawal'    
                     't_Depositmoney' 
                   |'t_Printingstatement'
                   |'t_Changemaximumlimitforwithdrawal'
                   |'t_Localtransfertothesamebank'
                   | Localtransfertootherbanks
                   |'t_Internationaltransfer'
                   |'t_Moneyexchange'
                   |'t_Checkingmoneyexchangerate'
                   ;

Localtransfertootherbanks 
                        : r_2_16_17
                        ;  

r_2_16_17_sec 
            : LAMBDA 
            | r_2_16_17
            ;

r_2_16_17 
        : 't_Detuschbank' r_2_16_17_sec 
        | 't_Stadtparkasse' r_2_16_17_sec 
        | 't_Volksbank' r_2_16_17_sec
        ;

Additionalfunctionality 
                      : 't_Mobiletop_up' 
                      | 't_Paythebills'
                      | Languageselection
                      ;

Languageselection 
                : r_4_30_31
                ;

r_4_30_31 
        : 't_German'
        | 't_English'
        | 't_French'
        | 't_Italian'
        ;


