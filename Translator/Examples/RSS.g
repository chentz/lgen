/*
[Gramática Bicycle SPL];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar RSS;

probes
    : titleprobe rightsprobe linkprobe linkprobe
    ;

titleprobe
    : type html html
    ;

rightsprobe 
    : type html html
    ;

linkprobe    : type html html
             | type html html
             ;

type: 'List(">","type="html">")'
    ;

lt : '&lt'
   ;

gt : '&gt'
   ;

html : lt 'script' attr gt lt '/script' gt
     | lt 'applet' attr gt lt '/applet' gt
     | lt 'embed' attr gt lt '/embed' gt
     | lt 'object' attr gt lt '/object' gt 
     | lt 'meta' attr gt lt '/meta' gt 
     | lt 'a' attr gt lt '/a' gt
     ;

attr : '""'
     |'style' valueempty
     |'abbr' valueempty
     ;

valueempty : '=' '""'
            ;
