/*
[Gramática Call];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar Call;

Call
    : CallerOS ServerOS CalleeOS
    ;

CallerOS
    : 'Mac'|'Win'
    ;

ServerOS
    : 'Lin'|'Sun'|'Win'
    ;

CalleeOS
    : 'Mac'|'Win'
    ;


