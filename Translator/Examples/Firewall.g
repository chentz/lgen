/*
[Gramática Firewall];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar Firewall;

S
    : NSEPARATOR NBadFlags0 NSYN NBadFlags1 NSYNACK NBadFlags2 NACK1 NBadFlags3 NFINACK1 NBadFlags4 NFINACK2 NBadFlags5 NACK2 NBadFlags6
    ;

NSEPARATOR
        : '! ! ! ! ! ! ! ! !'
        ;

NSYN 
  : '\\n' 'SYN' 'SYN' 'ack' 'fin' 'psh' 'rst' 'urg' 'OUT' 'ACCEPT'
  ;

NSYNACK 
     : '\\n' 'SYNACK' 'SYN' 'ACK' 'fin' 'psh' 'rst' 'urg' 'IN' 'ACCEPT'
     ;

NACK1 
   : '\\n' 'ACK1' 'syn' 'ACK' 'fin' 'psh' 'rst' 'urg' 'OUT' 'ACCEPT'
   ;

NFINACK1 
      : '\\n' 'FINACK1' 'syn' 'ACK' 'FIN' 'psh' 'rst' 'urg' 'IN' 'ACCEPT'
      ;

NFINACK2 
      : '\\n' 'FINACK2' 'syn' 'ACK' 'FIN' 'psh' 'rst' 'urg' 'OUT' 'ACCEPT'
      ;

NACK2 
   : '\\n' 'ACK2' 'syn' 'ACK' 'fin' 'psh' 'rst' 'urg' 'IN' 'ACCEPT'
   ;

NBadFlags0 
        : NBadFlag0
        ;

NBadFlag0
       : '\\n' 'badFlag0' 'StaticBadFlags()' 'IN' 'DROP'
       ;

NBadFlags1 
        : '\\n' 'badFlag1' NFlag1 'IN' 'DROP'
        ;

NFlag1 
    : 'StaticBadFlags()'
    ;

NBadFlags2 
        : '\\n' 'badFlag2' NFlag2 'IN' 'DROP'
        ;

NFlag2 
    : 'StaticBadFlags()'
    ;

NBadFlags3
        : '\\n' 'badFlag3' NFlag3 'IN' 'DROP'
        ;

NFlag3
    : 'StaticBadFlags()'
    ;

NBadFlags4 
        : '\\n' 'badFlag4' NFlag4 'IN' 'DROP'
        ;

NFlag4 
    : 'StaticBadFlags()'
    ;

NBadFlags5 
        : '\\n' 'badFlag5' NFlag5 'IN' 'DROP'
        ;

NFlag5 
    : 'StaticBadFlags()'
    ;

NBadFlags6 
        : '\\n' 'badFlag6' NFlag6 'IN' 'DROP'
        ;
NFlag6 
    : 'StaticBadFlags()';
