/*
[Gramática Model Transformation SPL];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/
grammar ModelTransSPL;

Model_Transformation
    : Transformation_rules Source_Target_relationship Rule_application_strategy Rule_scheduling Rule_organization Tracing Directionality
    | Transformation_rules Source_Target_relationship Rule_application_strategy Rule_scheduling Rule_organization Tracing Directionality Rule_application_scoping
    ;

Transformation_rules
    : LHS_RHS
    | LHS_RHS 't_LHS_RHS_Syntactic_Separation'
    | 't_Bidirectionality'
    | 't_Parameterization'
    | 't_Intermediate_structures'
    ;

LHS_RHS
    : Variables
    | id_1
    ;

Variables
    : id_0
    ;

id_0
    : 't_Untyped'
    | 't_Syntactically_typed'
    | 't_Semantically_typed'
    ;

id_1_sec
    : LAMBDA
    | id_1
    ;

id_1
    : Patterns id_1_sec
    | Logic id_1_sec
    ;

Patterns
    : Form_Patt Syntax Typing
    ;

Form_Patt
    : id_2
    ;

id_2
    : 't_Strings'
    | 't_Terms'
    | 't_Graphs'
    ;

Syntax
    : id_3
    ;

id_3_sec
    : LAMBDA
    | id_3
    ;

id_3
    : 't_Abstract' id_3_sec
    | Concrete id_3_sec
    ;

Concrete
    : id_4
    ;

id_4_sec
    : LAMBDA
    | id_4
    ;

id_4
    : 't_Textual' id_4_sec
    | 't_Graphical' id_4_sec
    ;

Typing
    : id_5
    ;

id_5
    : 't_Untyped'
    | 't_Syntactically_typed'
    | 't_Semantically_typed'
    ;

Logic
    : id_6
    ;

id_6_sec
    : LAMBDA
    | id_6
    ;

id_6
    : 't_Non_executable' id_6_sec
    | Executable id_6_sec
    ;

Executable
    : id_7
    ;

id_7_sec
    : LAMBDA
    | id_7
    ;

id_7
    : 't_Imperative' id_7_sec
    | 't_Declarative' id_7_sec
    ;

Rule_application_scoping
    : id_8
    ;

id_8_sec
    : LAMBDA
    | id_8
    ;

id_8
    : 't_Source' id_8_sec
    | 't_Target' id_8_sec
    ;

Source_Target_relationship
    : id_9
    ;

id_9_sec
    : LAMBDA
    | id_9
    ;

id_9
    : 't_New_target' id_9_sec
    | Existing_target id_9_sec
    ;

Existing_target
    : Update
    | Update 't_In_place'
    ;

Update
    : id_10
    ;

id_10
    : 't_Destructive'
    | 't_Extension_only'
    ;

Rule_application_strategy
    : id_11
    ;

id_11_sec
    : LAMBDA
    | id_11
    ;

id_11
    : 't_Deterministic' id_11_sec
    | Non_deterministic id_11_sec
    | 't_Interactive' id_11_sec
    ;

Non_deterministic
    : id_12
    ;

id_12_sec
    : LAMBDA
    | id_12
    ;

id_12
    : 't_Concurrent' id_12_sec
    | 't_One_point' id_12_sec
    ;

Rule_scheduling
    : Form_RS Rule_selection
    | Form_RS Rule_selection Rule_Iteration
    | 't_Phasing'
    ;

Form_RS
    : id_13
    ;

id_13_sec
    : LAMBDA
    | id_13
    ;

id_13
    : 't_Implicit' id_13_sec
    | Explicit id_13_sec
    ;

Explicit
    : id_14
    ;

id_14
    : 't_Internal'
    | 't_External'
    ;

Rule_selection
    : id_15
    ;

id_15_sec
    : LAMBDA
    | id_15
    ;

id_15
    : 't_Explicit_condition' id_15_sec
    | 't_Non_determinism' id_15_sec
    | 't_Conflict_resolution' id_15_sec
    | 't_Interactive' id_15_sec
    ;

Rule_Iteration
    : id_16
    ;

id_16
    : 't_Recursion'
    | 't_Looping'
    | 't_Fixpoint_Iteration'
    ;

Rule_organization
    : Organizational_Structure
    | Organizational_Structure 't_Modulatiry_mechanisms'
    | Reuse_mechanisms
    ;

Reuse_mechanisms
    : id_17
    ;

id_17_sec
    : LAMBDA
    | id_17
    ;

id_17
    : 't_Inheritance' id_17_sec
    | 't_Logical_composition' id_17_sec
    ;

Organizational_Structure
    : id_18
    ;

id_18
    : 't_Source_oriented'
    | 't_Target_oriented'
    | 't_Independent'
    ;

Tracing
    : Dedicated_support
    ;

Dedicated_support
    : Storage_location Control
    ;

Storage_location
    : id_19
    ;

id_19
    : Model
    | 't_Separate'
    ;

Model
    : id_20
    ;

id_20_sec
    : LAMBDA
    | id_20
    ;

id_20
    : 't_Source' id_20_sec
    | 't_Target' id_20_sec
    ;

Control
    : id_21
    ;

id_21
    : 't_Manual'
    | Automatic
    ;

Automatic
    : id_22
    ;

id_22
    : 't_All_rules'
    | 't_Selected_rules'
    ;

Directionality
    : id_23
    ;

id_23_sec
    : LAMBDA
    | id_23
    ;

id_23
    : 't_Unidirectional' id_23_sec
    | Bidirectional id_23_sec
    ;

Bidirectional
    : id_24
    ;

id_24
    : 't_Bidirectional_rules'
    | 't_Complementary_pairs'
    ;