/*
[Gramática HIS SPL];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar HISSPL;

LHS_communication
    : communication message
    ;

his
    : services administration detection_devices action_devices monitor_control quality_attributes
    | services administration detection_devices action_devices monitor_control quality_attributes LHS_communication
    ;

LHS_flood
    : flood 't_moisture_sensor'
    ;

services
    : 't_security' intrusion fire
    | 't_security' intrusion fire LHS_flood
    ;

intrusion
    : detection_intrusion action_intrusion
    ;

detection_intrusion
    : motion
    ;

motion
    : monitoring_detecting_motion
    ;

monitoring_detecting_motion
    : id_0
    ;

id_0
    : 't_discrete_value'
    | 't_continuous_value'
    ;

LHS_message
    : message communication
    ;

action_intrusion
    : 't_door_operation' 't_alarm'
    | 't_door_operation' 't_alarm' LHS_message
    ;

message
    : 't_voice'
    | 't_voice' 't_data'
    ;

fire
    : detection_fire action_fire
    ;

detection_fire
    : smoke
    ;

smoke
    : monitoring_detecting_smoke
    ;

monitoring_detecting_smoke
    : id_1
    ;

id_1
    : 't_discrete_value'
    | 't_continuous_value'
    ;

LHS_water
    : 't_water' 't_sprinkler'
    ;

action_fire
    : LHS_water
    | LHS_water 't_gas'
    ;

flood
    : detection_flood action_flood
    ;

detection_flood
    : moisture
    ;

moisture
    : monitoring_detecting_moisture
    ;

monitoring_detecting_moisture
    : id_2
    ;

id_2
    : 't_discrete_value'
    | 't_continuous_value'
    ;

LHS_pumping
    : 't_pumping' 't_sump_pump'
    ;

action_flood
    : 't_water_main'
    | 't_water_main' LHS_pumping
    ;

administration
    : hmi
    ;

hmi
    : id_3
    ;

id_3
    : 't_standard'
    | 't_advanced'
    ;

communication
    : 't_telephone'
    | 't_telephone' internet
    ;

internet
    : connection
    ;

connection
    : id_4
    ;

id_4
    : 't_tcp'
    | 't_udp'
    ;

LHS_moisture_sensor
    : 't_moisture_sensor' 't_flood'
    ;

detection_devices
    : 't_motion_sensor' 't_skoke_sensor'
    | 't_motion_sensor' 't_skoke_sensor' LHS_moisture_sensor
    ;

LHS_sprinkler
    : 't_sprinkler' 't_water'
    ;

LHS_sump_pump
    : 't_sump_pump' 't_pumping'
    ;

action_devices
    : LHS_sprinkler
    | LHS_sprinkler LHS_sump_pump
    ;

monitor_control
    : 't_direct' scheduled responding_strategy 't_event_based'
    ;

scheduled
    : 't_periodic' 't_one_time'
    ;

responding_strategy
    : 't_priority'
    | 't_priority' 't_sequential'
    ;

quality_attributes
    : 't_usability' 't_scalability' reliability 't_safety'
    ;

reliability
    : redundancy_control
    ;

redundancy_control
    : id_5
    ;

id_5
    : 't_active'
    | 't_standby'
    ;