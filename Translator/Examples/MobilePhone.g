/*
[Gramática Mobile Phone SPL];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar MobilePhoneSPL;

MobilePhone
          : UtilityFunctions Settings
          | UtilityFunctions Settings Media
          | Connectivity
          ;

LHS_Games
        : 't_Games' 't_JavaSupport'
        ;

UtilityFunctions
               : 't_Calls' Messaging 't_Alarm' 't_RingingTones'
               | 't_Calls' Messaging 't_Alarm' 't_RingingTones' LHS_Games
               ;

Messaging
        :r_1_3_5
        ;

r_1_3_5_sec 
          : LAMBDA 
          | r_1_3_5
          ;

r_1_3_5 
      : 't_SMS' r_1_3_5_sec 
      | 't_MMS' r_1_3_5-sec
      ;

LHS_JavaSupport 
              : 't_JavaSupport' 't_Games'
              ;

Settings 
       : OS 
       | OS LHS_JavaSupport
       ;


OS 
  :r_11_12_14
  ;

r_11_12_14 
         : 't_Symbian'
         | 't_WinCE'
         ;


Media 
    : 't_Camera'
    | 't_Camera' 't_MP3'
    ;

Connectivity 
           :'t_USB'
           |'t_USB' 't_Bluetooth'
           ;
