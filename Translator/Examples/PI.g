/*
[Gramática Processamento de Imagens];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar PI;

S
    : z A
    | z S
    ;
A
    : x A
    | x B
    ;

B
    : z B
    | z C
    ;

C
    : 'ε'
    | S
    ;

z
    : '0'
    ;

x
    : '1'
    | '2'
    | '3'
    | '4'
    | '5'
    | '6'
    | '7'
    | '8'
    | '9'
    ;



