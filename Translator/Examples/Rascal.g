/*
Translation from Rascal language definition in SDF to ANTLR v3.
By Cleverton Hentz

Rule equivalences:

1(Zero or More Sep): "{" {Expression ","}* elements0 "}" ==> "{" (Expression ("," Expression)*)? "}"

Version 0.1 - 05/09/2016
Version 0.2 - 02/16/2017
*/

grammar Rascal;

BooleanLiteral : 'true' 
	             | 'false';
	
Literal	: IntegerLiteral 
	| RegExpLiteral 
	| RealLiteral 
	| BooleanLiteral 
	| StringLiteral 
	| DateTimeLiteral 
	| LocationLiteral
	| RationalLiteral
	;
	
Expression : Concrete;
Pattern    : Concrete;

Concrete : '(' LAYOUTLIST Sym LAYOUTLIST ')' LAYOUTLIST '`' ConcretePart* '`';

//Verify the precedence order between ~ and +
ConcretePart : (~('`'|'<'|'>'|'\\'|'\n'))+
  | '\n' ('\ '|'\t'|'\u00A0'|'\u1680'|'\u2000'..'\u200A'|'\u202F'|'\u205F'|'\u3000')* '\''
  | ConcreteHole
  | '\<'
  | '\>'
  | '\\`'
  | '\\\\'
;
  
ConcreteHole : '<' Sym Name '>';

//Start Symbol
Module : Header Body
;

ModuleParameters : '[' TypeVar (',' TypeVar)* ']'
;
                 
DateAndTime : '$' DatePart 'T' TimePartNoTZ '$'
	| '$' DatePart 'T' TimePartNoTZ TimeZonePart '$'
	;
	
Strategy : 'top-down-break' 
	| 'top-down'
	| 'bottom-up' 
	| 'bottom-up-break'
	| 'outermost' 
	| 'innermost'
	;
	
/* Disable
UnicodeEscape	: '\\' 'u' ('0'..'9'|'A'..'F'|'a'..'f') ('0'..'9'|'A'..'F'|'a'..'f') ('0'..'9'|'A'..'F'|'a'..'f') ('0'..'9'|'A'..'F'|'a'..'f')
    | '\\' 'U' (('0' ('0'..'9'|'A'..'F'|'a'..'f') ) | '10') ('0'..'9'|'A'..'F'|'a'..'f') ('0'..'9'|'A'..'F'|'a'..'f') ('0'..'9'|'A'..'F'|'a'..'f') ('0'..'9'|'A'..'F'|'a'..'f') 
    | '\\' 'a' '0'..'7' ('0'..'9'|'A'..'F'|'a'..'f')
;
*/

UnicodeEscape	: '<UnicodeEscape>';
	
Variable : Name '=' Expression
	       | Name
	;
	       
OctalIntegerLiteral : '0' ('0'..'7')+
;
         
TypeArg	: Type 
        | Type Name
;
        
Renaming : Name '=>' Name
;
	
Catch :'catch' ':' Statement
	    | 'catch' Pattern ':' Statement
	;
         
PathChars	: URLChars '|'
;
	
Signature : FunctionModifiers Type Name Parameters 'throws' Type (',' Type)*
	        | FunctionModifiers Type Name Parameters
	;
          
//Check
Sym  : Nonterminal
	   | '&' Nonterminal
//   | Nonterminal >> "[" "[" {Sym ","}+ parameters "]"
	   | 'start' '[' Nonterminal  ']'
	   | Sym NonterminalLabel
     | Class
     | StringConstant
     | CaseInsensitiveStringConstant
     | Sym '+' 
     | Sym '*' 
     | '{' Sym Sym '}' '+' 
     | '{' Sym Sym '}' '*' 
     | Sym '?' 
     | '(' Sym '|' Sym ('|' Sym)* ')'
	   | '(' Sym Sym+ ')'
     | '(' ')'
     | Sym '@' IntegerLiteral
     | Sym '$' 
     | '^' Sym
     | Sym '!' NonterminalLabel
     | Sym  '>>' Sym
     | Sym '!>>' Sym
     | Sym '<<' Sym 
     | Sym '!<<' Sym
     | Sym '\\' Sym
;
	   
TimePartNoTZ : '0'..'2' '0'..'9' '0'..'5' '0'..'9' '0'..'5' '0'..'9' ( (','|'.') '0'..'9' ( '0'..'9' '0'..'9'? )? )? 
	           | '0'..'2' '0'..'9' ':' '0'..'5' '0'..'9' ':' '0'..'5' '0'..'9' ( (','|'.') '0'..'9' ( '0'..'9' '0'..'9'? )? )? 
;
     
Header : Tags 'module' QualifiedName ModuleParameters Import*
       | Tags 'module' QualifiedName Import*;

//Name : ('A'..'Z' | 'a'..'z' | '_') ('0'..'9'|'A'..'Z'|'_'|'a'..'z')*
//     | '\\' ('A'..'Z' | '_' | 'a'..'z') ('-'|'0'..'9'|'A'..'Z'|'_'|'a'..'z')*
//;

Name : '<NAME>';
     
SyntaxDefinition :  Visibility 'layout' Sym '=' Prod ';' 
                 | 'lexical' Sym '=' Prod ';' 
	               | 'keyword' Sym '=' Prod ';'
                 | Start 'syntax' Sym '=' Prod ';'
;

Kind : 'function' 
	   | 'variable' 
	   | 'all' 
	   | 'anno' 
	   | 'data' 
	   | 'view' 
	   | 'alias' 
	   | 'module' 
	   | 'tag'
;
	
ImportedModule : QualifiedName
               | QualifiedName ModuleActuals Renamings 
	             | QualifiedName Renamings 
	             | QualifiedName ModuleActuals 
;
	   
Target : 
       | Name
;

IntegerLiteral : DecimalIntegerLiteral 
               | HexIntegerLiteral 
               | OctalIntegerLiteral
;
	     
FunctionBody : '{' Statement* '}'
;
  
Expression : 
  Concrete //Added because the union of two definitions from the original.
  | '{'  Statement+ '}' 
	| '(' Expression ')' 
	| Type Parameters '{' Statement+  '}' 
	| '[' Expression ',' Expression '..' Expression ']'
	| Parameters '{' Statement*  '}' 
	| Label Visit
	| '(' Expression '|' Expression  '|' Expression (',' Expression)* ')' 
	| 'type' '(' Expression ',' Expression ')'  
	| Expression '(' (Expression (',' Expression)*)? KeywordArguments  ')'
	| Literal 
	| 'any' '(' Expression (',' Expression)* ')' 
	| 'all' '(' Expression (',' Expression)* ')' 
	| Comprehension
	| '{' (Expression (',' Expression)*)? '}' 
	| '[' (Expression (',' Expression)*)? ']'
	| '#' Type
  | '[' Expression '..' Expression ']'
  | '<' Expression (',' Expression)* '>' 
	| '(' (Mapping (',' Mapping)*)? ')' 
	| '<it>'//('A'..'Z'|'a'..'z'|'_') it
	| QualifiedName 
  | Expression '[' Expression (',' Expression)* ']'
  | Expression '[' OptionalExpression '..' OptionalExpression ']' 
  | Expression '[' OptionalExpression ',' Expression '..' OptionalExpression ']' 
	| Expression '.' Name 
	| Expression '[' Name '=' Expression ']' 
	| Expression  '<' Field (Field ',')* '>' 
	| Expression '[' '@' Name '=' Expression ']' 
  | Expression '@' Name 
  | Expression 'is' Name
	| Expression 'has' Name
	| Expression '+'
  | Expression '*' 
	| Expression '?' 
	| '!' Expression 
	| '-' Expression 
	| '*' Expression
  | '[' Type ']' Expression
	| Expression 'o' Expression 
  | Expression '*' Expression  
	| Expression 'join' Expression 
  | Expression '%' Expression
  | Expression '/' Expression 
	| Expression '&' Expression 
	| Expression '+' Expression  
	| Expression '-' Expression
  | Expression '<<' Expression
	| Expression '>>' Expression 
	| Expression 'mod' Expression
	| Expression 'notin' Expression  
	| Expression 'in' Expression 
	| Expression '>=' Expression  
	| Expression '<=' Expression 
	| Expression '<' Expression 
	| Expression '>' Expression 
	| Expression '==' Expression
	| Expression '!=' Expression 
	| Expression '?' Expression
	| Pattern '!:=' Expression  
  | Pattern ':=' Expression 
  | Pattern '<-' Expression 
	| Expression '==>' Expression  
  | Expression '<==>' Expression 
	| Expression '&&' Expression 
	| Expression '||' Expression 
	| Expression '?' Expression ':' Expression
	;

OptionalExpression : Expression
                   | ()
;
                   
UserType : QualifiedName 
	       | QualifiedName '[' Type (',' Type)* ']'
	;
	       
Import : 'extend' ImportedModule ';' 
	| 'import' ImportedModule ';'
	| 'import' QualifiedName '=' LocationLiteral ';'
  | SyntaxDefinition
;
  
Body : Toplevel*
;
     
URLChars : ~('\t'..'\n'| '\r' | '\ ' | '<' | '|')*
;

TimeZonePart : ('+'|'\-') ('0'..'1') ('0'..'9') ':' ('0'..'5') ('0'..'9') 
	           | 'Z' 
	           | ('+'|'\-') ('0'..'1') ('0'..'9')
             | ('+'|'\-') ('0'..'1') ('0'..'9') ':' ('0'..'5') ('0'..'9')
	;

ProtocolPart : ProtocolChars 
             | PreProtocolChars Expression ProtocolTail
;
             
StringTemplate : 'if'  '(' Expression (',' Expression)* ')' '{' Statement* StringMiddle Statement* '}' 
  | 'if' '(' Expression (',' Expression)* ')' '{' Statement* StringMiddle Statement* '}' 'else' '{' Statement* StringMiddle Statement* '}' 
	| 'for' '(' Expression (',' Expression)* ')' '{' Statement* StringMiddle Statement* '}' 
	| 'do' '{' Statement* StringMiddle Statement* '}' 'while' '(' Expression ')' 
	| 'while' '(' Expression ')' '{' Statement* StringMiddle Statement* '}'
	;

PreStringChars : '"' StringCharacter* '<'
;
               
CaseInsensitiveStringConstant : '\'' StringCharacter* '\''
;

Backslash : '\\'
;

Label : Name ':' 
	    |
;

MidProtocolChars : '>' URLChars '<'
;

NamedBackslash : '\\'
;
	
Field	: IntegerLiteral 
	    | Name
;
	
JustDate : '$' DatePart '$'
;

PostPathChars : '>' URLChars '|'
;

PathPart : PathChars 
	       | PrePathChars Expression PathTail
;

DatePart : '0'..'9' '0'..'9' '0'..'9' '0'..'9' '-' '0'..'1' '0'..'9' '-' '0'..'3' '0'..'9'
	       | '0'..'9' '0'..'9' '0'..'9' '0'..'9' '0'..'1' '0'..'9' '0'..'3' '0'..'9'
;

FunctionModifier : 'java' 
	               | 'test' 
	               | 'default'
;

Assignment : '?=' 
	| '/=' 
	| '*=' 
	| '&=' 
  | '-=' 
  | '=' 
	| '+=' 
	| '<<='
	;
	
Assignable : '(' Assignable ')'
	         | QualifiedName
           | Assignable '[' Expression ']' 
           | Assignable '[' OptionalExpression '..' OptionalExpression ']' 
           | Assignable '[' OptionalExpression ',' Expression '..' OptionalExpression ']'     
           | Assignable '.' Name 
           | Assignable '?' Expression 
           | Name '(' Assignable (',' Assignable)* ')'  
           | '<' Assignable (',' Assignable)* '>' 
	         | Assignable '@' Name
;

StringConstant :  '\"' StringCharacter* '\"'
;

Assoc	: 'assoc' 
	    | 'left' 
	    | 'non-assoc' 
	    | 'right'
;

Replacement : Expression 
	          | Expression 'when' Expression (',' Expression)*
;

DataTarget : 
 	         | Name ':'
;

StringCharacter	: '\\' ('"'|'\''|'<'|'>'|'\\'|'b'|'f'|'n'|'r'|'t') 
	| UnicodeEscape 
	| ~( '"'|'\''|'<'|'>'|'\\')
	| '\n' ('\ '|'\t'|'\u00A0'|'\u1680'|'\u2000'..'\u200A'|'\u202F'|'\u205F'|'\u3000')* '\'' 
;
	
JustTime : '$T' TimePartNoTZ '$'
	| '$T' TimePartNoTZ TimeZonePart '$'
;

MidStringChars : '>' StringCharacter* '<'
;

ProtocolChars	: '|' URLChars '://'
;

RegExpModifier : ('d'|'i'|'m'|'s')*
;

CommonKeywordParameters : ()
  | '(' KeywordFormal (',' KeywordFormal)* ')'
;
    
Parameters : '(' Formals KeywordFormals ')' 
	         | '(' Formals '...' KeywordFormals ')'
;

OptionalComma : ','?
;

KeywordFormals : OptionalComma (','|'\ '|'('|'\t'|'\n')
    | ()
;

KeywordFormal : Type Name '=' Expression
;

KeywordArguments : OptionalComma (','|'\ '|'('|'\t'|'\n') 
    | ()
;

KeywordArgument : Name '=' '<&T>';

RegExp : ~('/ '|'<'|'>'|'\\') 
	     | '<' Name '>' 
       | '\\' ('/ '|'<'|'>'|'\\')
	     | '<' Name ':' NamedRegExp* '>' 
       | Backslash
;
    
LAYOUTLIST : LAYOUT*;

LocalVariableDeclaration : Declarator 
	                       | 'dynamic' Declarator
;

RealLiteral : '0'..'9'+ ('D'| 'F'|'d'|'f')
	          | '0'..'9'+ ('E'|'e') ('+'|'-')? '0'..'9'+ ('D'| 'F'|'d'|'f')?
          	| '0'..'9'+ '.' '0'..'9'* ('D'| 'F'|'d'|'f')?  
          	| '0'..'9'+ '.' '0'..'9'* ('E'|'e') ('+'|'-')? '0'..'9'+ ('D'| 'F'|'d'|'f')? 
          	| '.' '0'..'9'+ ('D'| 'F'|'d'|'f')? 
          	| '.' '0'..'9'+ ('E'|'e') ('+'|'-')? '0'..'9'+ ('D'| 'F'|'d'|'f')? 
;
	
Range : Char '-' Char 
	    | Char
;

LocationLiteral : ProtocolPart PathPart
;

ShellCommand : 'set' QualifiedName Expression 
	| 'undeclare' QualifiedName 
	| 'help' 
	| 'edit' QualifiedName 
	| 'unimport' QualifiedName 
	| 'declarations' 
	| 'quit' 
	| 'history' 
  | 'test' 
	| 'modules' 
	| 'clear'
;
	
StringMiddle : MidStringChars 
             | MidStringChars StringTemplate StringMiddle 
	           | MidStringChars Expression StringMiddle
;

QualifiedName : Name ('::' Name)*
;

RationalLiteral : '0'..'9' '0'..'9'* 'r'
   | '1'..'9' '0'..'9'* 'r' '0'..'9' '0'..'9'*
;

DecimalIntegerLiteral	: '0' 
	                    | '1'..'9' '0'..'9'* 
;

DataTypeSelector : QualifiedName '.' Name
;

StringTail : MidStringChars Expression StringTail 
	         | PostStringChars 
	         | MidStringChars StringTemplate StringTail
;
	
PatternWithAction : Pattern '=>' Replacement 
	                | Pattern ':' Statement
;

/* Disable
LAYOUT : Comment 
	     | ('\u0009'..'\u000D'|'\u0020'|'\u0085'|'\u00A0'|'\u1680'|'\u180E'|'\u2000'..'\u200A'|'\u2028'|'\u2029'|'\u202F'|'\u205F'|'\u3000') 
;
*/
LAYOUT : '<LAYOUT>';

Visit	: Strategy 'visit' '(' Expression ')' '{' Case+ '}' 
	    | 'visit' '(' Expression ')' '{' Case+ '}'
;

Commands : EvalCommand+
;

//Check
EvalCommand : Declaration  
  | Statement 
  | Import
  | Output
;

//Check
Output :  '<Output>'
//  '⇨' ~('\n'|'\r')* '\n'
//  | ^ '≫' ~('\n'|'\r')* '\n'
//  | ^ '⚠' ~('\n'|'\r')* '\n'
;

Command	: Expression 
	      | Declaration 
	      | ':' ShellCommand 
	      | Statement 
	      | Import
;

/* Disable
TagString : '\\' ( ~('{'|'}') |  ('\\' ('{'|'}')) | TagString)* '\\'
;
*/

TagString : '<TagString>';

ProtocolTail : MidProtocolChars Expression ProtocolTail 
	           | PostProtocolChars
;

/* Disable
Nonterminal : ( 'A'..'Z' ('0'..'9'|'A'..'Z'|'_'|'a'..'z')* ) 
;
*/

Nonterminal : '<Nonterminal>'
;

PathTail : MidPathChars Expression PathTail 
	       | PostPathChars
;

Visibility : 'private'
	         | 
	         | 'public'
;

StringLiteral	: PreStringChars StringTemplate StringTail 
	            | PreStringChars Expression StringTail 
	            | StringConstant
;
	
//Check
Comment : '<Comment>'
;

Renamings : 'renaming' Renaming (',' Renaming)*
;

Tags : Tag*
;

Formals	: (Pattern (',' Pattern)*)?
;

PostProtocolChars : '>' URLChars '://'
;

Start : 
	    | 'start'
;

Statement : 'assert' Expression ';' 
	        | 'assert' Expression ':' Expression ';' 
	        | Expression ';' 
	        | Label Visit 
	        | Label 'while' '(' Expression (',' Expression)* ')' Statement 
	        | Label 'do' Statement 'while' '(' Expression ')' ';' 
	        | Label 'for' '(' Expression (',' Expression)* ')' Statement 
	        | Label 'if' '(' Expression (',' Expression)* ')' Statement () 
        	| Label 'if' '(' Expression (',' Expression)* ')' Statement 'else' Statement 
	        | Label 'switch' '(' Expression ')' '{' Case+ '}' 
	        | 'fail' Target ';' 
	        | 'break' Target ';' 
	        | 'continue' Target ';' 
          | 'filter' ';'
	        | 'solve' '(' QualifiedName (',' QualifiedName)* Bound ')' Statement
	        | 'try' Statement Catch+ 
	        | 'try' Statement Catch+ 'finally' Statement 
	        | Label '{' Statement+ '}' 
	        | ';' 
	        | 'global' Type QualifiedName (',' QualifiedName)* ';' 
	        | Assignable Assignment Statement
	        
	        | 'return' Statement  
		      | 'throw' Statement
		      | 'insert' DataTarget Statement
		      | 'append' DataTarget Statement
          | FunctionDeclaration 
	        | LocalVariableDeclaration ';'
;

StructuredType : BasicType '[' TypeArg (',' TypeArg)* ']'
;

NonterminalLabel : 'a'..'z' ('0'..'9'|'A'..'Z'|'_'|'a'..'z')*
;
	
FunctionType : Type '(' TypeArg (',' TypeArg)* ')'
;

Case : 'case' PatternWithAction 
	   | 'default' ':' Statement
;

Declarator : Type Variable (',' Variable)*
;

Bound	: ';' Expression 
	    |
;

RascalKeywords : 'o'
               | 'syntax'
               | 'keyword'
               | 'lexical'
               | 'int'
               | 'break'
               | 'continue'
               | 'rat' 
               | 'true' 
               | 'bag' 
               | 'num' 
               | 'node' 
               | 'finally' 
               | 'private' 
               | 'real' 
               | 'list' 
               | 'fail' 
               | 'filter' 
               | 'if' 
               | 'tag' 
               | BasicType
               | 'extend' 
               | 'append' 
               | 'rel' 
               | 'lrel'
               | 'void' 
               | 'non-assoc' 
               | 'assoc' 
               | 'test' 
               | 'anno' 
               | 'layout' 
               | 'data' 
               | 'join' 
               | 'it' 
               | 'bracket' 
               | 'in' 
               | 'import' 
               | 'false' 
               | 'all' 
               | 'dynamic' 
               | 'solve' 
               | 'type' 
               | 'try' 
               | 'catch' 
               | 'notin' 
               | 'else' 
               | 'insert' 
               | 'switch' 
               | 'return' 
               | 'case' 
               | 'while' 
               | 'str' 
               | 'throws' 
               | 'visit' 
               | 'tuple' 
               | 'for' 
               | 'assert' 
               | 'loc' 
               | 'default' 
               | 'map' 
               | 'alias' 
               | 'any' 
               | 'module' 
               | 'mod'
               | 'bool' 
               | 'public' 
               | 'one' 
               | 'throw' 
               | 'set' 
               | 'start'
               | 'datetime' 
               | 'value'
;

Type : '(' Type ')' 
	| UserType
	| FunctionType
	| StructuredType
	| BasicType
	| DataTypeSelector
	| TypeVar 
	| Sym
;
	
Declaration : Tags Visibility Type Variable (',' Variable)* ';' 
	| Tags Visibility 'anno' Type Type '@' Name ';' 
	| Tags Visibility 'alias' UserType '=' Type ';' 
	| Tags Visibility 'tag' Kind Name 'on' Type (',' Type)* ';' 
	| Tags Visibility 'data' UserType CommonKeywordParameters ';' 
	| Tags Visibility 'data' UserType CommonKeywordParameters '=' Variant ('|' Variant)* ';'
	| FunctionDeclaration
;

Class : '[' Range* ']' 
	    | '!' Class 
	    | Class '-' Class 
	    | Class '&&' Class 
	    | Class '||' Class 
	    | '(' Class ')'
;

RegExpLiteral : '/' RegExp* '/' RegExpModifier
;

FunctionModifiers	: FunctionModifier*
;

Comprehension : '{' Expression (',' Expression)* '|' Expression (',' Expression)* '}'
	            | '(' Expression ':' Expression '|' Expression (',' Expression)* ')' 
	            | '[' Expression (',' Expression)* '|' Expression (',' Expression)* ']';

Variant : Name '(' TypeArg (',' TypeArg)*  KeywordFormals ')'
;

FunctionDeclaration : Tags Visibility Signature ';' 
	                  | Tags Visibility Signature '=' Expression ';'
	                  | Tags Visibility Signature '=' Expression 'when' Expression (',' Expression)* ';'
	                  | Tags Visibility Signature FunctionBody
;

PreProtocolChars : '|' URLChars '<'
;

NamedRegExp : '<' Name '>' 
	          | '\\' ('/'|'<'|'>'|'\\') 
          	| NamedBackslash 
	          | ~('/'|'<'|'>'|'\\');

ProdModifier : Assoc 
	           | 'bracket' 
	           | Tag
;

Toplevel : Declaration
;

PostStringChars : '>' StringCharacter* '\"'
;

HexIntegerLiteral : '0' ('X'|'x') ('0'..'9'|'A'..'F'|'a'..'f')+
;
	
TypeVar	: '&' Name 
	      | '&' Name '<:' Type
;
                 
BasicType : 'value'
	| 'loc' 
	| 'node'
	| 'num'
	| 'type'
	| 'bag'
	| 'int'
	| 'rat'
	| 'rel'
	| 'lrel'
	| 'real'
	| 'tuple'
	| 'str'
	| 'bool' 
	| 'void' 
	| 'datetime'
	| 'set'
	| 'map'
	| 'list'
;
    
Char : '\\' ('\ '|'\"'|'\''|'\-'|'<'|'>'|'\['|'\\'|'\]'|'b'|'f'|'n'|'r'|'t') 
     | ~('\ '|'"'|'\''|'\-'|'<'|'>'|'\['|'\\'|'\]')
     | UnicodeEscape
;
    
Prod : ':' Name
	   | ProdModifier* Name ':' Sym*
     | '...'
	   | ProdModifier* Sym*
	   | Assoc '(' Prod ')' 
	   | Prod '|' Prod
	   | Prod '>' Prod
;
	
DateTimeLiteral : JustDate 
	              | JustTime 
	              | DateAndTime
;

PrePathChars : URLChars '<'
;

//Check
Mapping : '<Mapping>'
;

MidPathChars : '>' URLChars '<'
;

Pattern : 
  Concrete
  | '{' (Pattern (',' Pattern)*)? '}'
	| '[' (Pattern (',' Pattern)*)? ']' 
	| QualifiedName 
	| QualifiedName '*'
	| '*' Pattern
  | '+' Pattern 
	| '-' Pattern
  | Literal 
	| '<' Pattern (',' Pattern)* '>' 
	| Type Name 
	| '(' (Mapping (',' Mapping)*)? ')' 
	| 'type' '(' Pattern ',' Pattern ')' 
	| Pattern '(' (Pattern (',' Pattern)*)? KeywordArguments ')' 
	| Name ':' Pattern
  | '[' Type ']' Pattern 
	| '/' Pattern 
	| '!' Pattern 
	| Type Name ':' Pattern
;

Tag : '@' Name TagString
    | '@' Name
    | '@' Name '=' Expression
;

ModuleActuals : '[' Type (',' Type)* ']'
;