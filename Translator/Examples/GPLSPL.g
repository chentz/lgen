/*
[Gramática GPL SPL];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar GPLSPL;

GPL
    : Graph_Type Algorithms
    | Graph_Type Algorithms search
    ;

Graph_Type
    : r_1_2
    | r_1_5
    ;

LHS_directed
    : 't_directed' 't_Strongly_Connected'
    ;

r_1_2
    : LHS_directed
    | 't_undirected'
    ;

r_1_5
    : 't_weighted'
    | 't_unweighted'
    ;

search
    : r_8_9
    ;

LHS_DFS
    : 't_DFS' 't_Cycle_Detection'
    ;

r_8_9
    : LHS_DFS
    | 't_BFS'
    ;

Algorithms
    : r_12_13
    ;

r_12_13_sec
    : LAMBDA
    | r_12_13
    ;

LHS_Cycle_Detection
    : 't_Cycle_Detection' 't_DFS'
    ;

LHS_Strongly_Connected
    : 't_Strongly_Connected' 't_directed'
    ;

r_12_13
    : 't_Shortest_Path' r_12_13_sec
    | Coloring r_12_13_sec
    | LHS_Cycle_Detection r_12_13_sec
    | 't_MST' r_12_13_sec
    | LHS_Strongly_Connected r_12_13_sec
    ;

Coloring
    : r_12_13_16_20
    ;

r_12_13_16_20_sec
    : LAMBDA
    | r_12_13_16_20
    ;

r_12_13_16_20
    : 't_Approximation' r_12_13_16_20_sec
    | 't_Brute_Force' r_12_13_16_20_sec
    ;