/*
[Gramática Bicycle SPL];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar BicycleSPL;

Bicycle
    : Frame 't_Wheels' 't_Saddle' Color 't_Handle_bars'
    | Frame 't_Wheels' 't_Saddle' Color 't_Handle_bars' Gears
    | 't_Pedals'
    | Warranty
    | Maintenance_plan
    ;

Frame
    : r_1_2
    ;

LHS_Carbon
    : Carbon 't_Red'
    ;

r_1_2
    : 't_Iron'
    | 't_Aluminum'
    | LHS_Carbon
    ;

Carbon
    : r_1_2_5_6
    ;

r_1_2_5_6
    : 't_White'
    | 't_Red'
    | 't_Black'
    ;

LHS_Front
    : 't_Front' 't_Rear'
    ;

LHS_Rear
    : 't_Rear' 't_Front'
    ;

Gears
    : LHS_Front
    | LHS_Rear
    ;

Color
    : r_15_16
    ;

LHS_Red
    : 't_Red' 't_Carbon'
    ;

r_15_16
    : LHS_Red
    | 't_Green'
    | 't_Blue'
    ;

Warranty
    : r_22_23
    ;

r_22_23
    : 't_1_year'
    | 't_2_years'
    | 't_3_years'
    ;

Maintenance_plan
    : r_29_30
    ;

r_29_30
    : 't_2_years'
    | 't_3_years'
    | 't_lifetime'
    ;