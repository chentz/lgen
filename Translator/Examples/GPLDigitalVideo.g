/*
[Gramática DigitalVideo SPL];

Símbolos não permitidos nos identificadores:
-,_(no início)

*/

grammar dvs;

dvs
   : control server_pc play 
   | control server_pc play LHS_network_hw 
   | client_pc 
   | LHS_handheld
   ;

LHS_network_hw
             : network_hw net
             ;

LHS_handheld 
           : 't_handheld' 't_irda_port'
           ;

LHS_net 
      : net network_hw
      ;

control  
      : 't_remote' play1
      | 't_remote' play1 telephone 
      | LHS_net 
      | edit
      ;

telephone
        :'t_sms'
        ;

net 
  : 't_email' 
  | 't_email' 't_web' 
  | 't_wap'
  ;

LHS_add_music 
            : 't_add_music' 't_audio'
            ;

edit 
   : LHS_add_music
   ;

LHS_audio
        : 't_audio' 't_add_music'
        ;

play1 
   : 't_video'
   | 't_video' 't_slides'
   | LHS_audio
   ;

network_hw 
         : 't_modem'
         | 't_ethernet'
         ;

LHS_irda_port
            : 't_irda_port' 't_handheld'
            ;

server_pc 
        : 't_network'
        | LHS_irda_port
        ;

play
   : 't_on_demand'
   ;

client_pc
        : 't_network'
        ;
