-- This grammar definition is based upon original SDF EBNF difined on Hentz dissertation 2010.

-- lexical elements
local space = S("\r\n\f\t ")
local comment = P("--") * notPatt(S("\r\n\f"))^0

local blockComment = P("(*") * (P("'*)'") + 
                                notPatt(P("*)"))
                               )^0 * P("*)")
local skip = (space + comment + blockComment)^0

local letters = R("az", "AZ")
local alpha = letters + R("09")

local Name = taggedCap("Name",
                         C(letters * (alpha + P("_"))^0 + 
                         P(".") * -P(".") ) * skip )
--local MetaIdentifier = Name
local stringLex = (P('"') * C((P('\\') * P(1) + (1 - P('"')))^0) * P('"') +
                   P("'") * C((P("\\")* P(1) + (1 - P("'")))^0) * P("'")) * skip
                
-- Aux. ANTLR functions
local token = getFuncToken(skip)

local function symb(str,cap)
  if not cap then
    return token(P(str))
  else
    return token(P(str) / cap)
  end
end

local function OMSep(pat, sep) return (pat * (symb(sep) * pat)^0) end

-- Grammar definition

--  nonterminals declarations
local Syntax = V("Syntax")
local SyntaxRule = V("SyntaxRule")
local DefinitionsList = V("DefinitionsList")
local SingleDefinition = V("SingleDefinition")
local Primary = V("Primary")
--Term is removed because simplification of grammar and AST. 
--local Term = V("Term")
local Empty = V("Empty")
local OptionalSequence = V("OptionalSequence")
local RepeatedSequence = V("RepeatedSequence")
local RepeatedSequenceP = V("RepeatedSequenceP")
local ZeroOrMore = V("ZeroOrMore")
local OneOrMore = V("OneOrMore")
local OneOrMoreSeparation = V("OneOrMoreSeparation")
local GroupedSequence = V("GroupedSequence")
local TerminalString = V("TerminalString")
local MetaIdentifier = V("MetaIdentifier")
--local Name = V("Name")

--  Grammar Declaration
local tabG = { Syntax,
  Syntax = taggedCap("Syntax", SyntaxRule^1);
  --MetaIdentifier = taggedCap("MetaIdentifier", symb("=") * DefinitionsList * symb(";"));
  SyntaxRule = taggedCap("SyntaxRule", Name * symb("=") * DefinitionsList * symb(";"));
  DefinitionsList = taggedCap("DefinitionsList", OMSep(SingleDefinition, "|"));
  SingleDefinition = taggedCap("SingleDefinition", OMSep(Primary, ","));
  Primary = taggedCap("Primary", -- The order is changed for adapt PEG semantics
        OptionalSequence +
        RepeatedSequence +
        GroupedSequence  +
        TerminalString   + 
        MetaIdentifier   +
        Empty);
  TerminalString = taggedCap("TerminalString", stringLex);
  Empty = taggedCap("Empty", skip);
  OptionalSequence = taggedCap("OptionalSequence", symb("[") * DefinitionsList *  symb("]"));
  RepeatedSequence = 
        taggedCap("RSZeroOrMore", symb("{") * DefinitionsList * symb("}") * RepeatedSequenceP) + --ZeroOrMore
        taggedCap("RSOneOrMoreSep",
          MetaIdentifier * symb("+") * TerminalString * RepeatedSequenceP + 
          TerminalString * symb("+") * TerminalString * RepeatedSequenceP +
          symb("[") * DefinitionsList * symb("]") * symb("+") * TerminalString * RepeatedSequenceP +
          GroupedSequence * symb("+") * TerminalString * RepeatedSequenceP +
          Empty * symb("+") * TerminalString * RepeatedSequenceP) + -- OneOrMoreSep
        taggedCap("RSOneOrMore",
          MetaIdentifier * symb("+") * RepeatedSequenceP +
          TerminalString * symb("+") * RepeatedSequenceP +  
          symb("[") * DefinitionsList * symb("]") * symb("+") * RepeatedSequenceP +    
          GroupedSequence * symb("+") * RepeatedSequenceP +
          Empty * symb("+") * RepeatedSequenceP); --OneOrMore
        
  RepeatedSequenceP = taggedCap("RepeatedSequenceP", 
        Empty +
        symb("+") * RepeatedSequenceP + 
        symb("+") * TerminalString * RepeatedSequenceP);
        
  GroupedSequence = taggedCap("GroupedSequence", symb("(") * DefinitionsList * symb(")") );
  
  MetaIdentifier = taggedCap("MetaIdentifier", Name);
  }

G = skip * P(tabG) * -1

-- VVV Start transformation implementation VVVV -----

--- Translator interface
local Trans = {}

--- Local visitor to translate EBNF->LGenSpec Notation
local TransVisitor = {}

--- Import standard library for LGen Specification Transformation
local lgSpecT = require("LGenSpecTrans");

--- Plug the standard actions
TransVisitor.Syntax = lgSpecT.defVisitor(nil,nil,'Syntax')
TransVisitor.DefinitionsList = lgSpecT.defVisitor(nil, {Accu=lgSpecT.concat; resultFunc=lgSpecT.resultAccu('alt',',',false)}, 'DefinitionsList')
TransVisitor.SingleDefinition = lgSpecT.defVisitor(nil, {Accu=lgSpecT.concat; resultFunc=lgSpecT.resultAccu('seq',',',true)}, 'SingleDefinition')
TransVisitor.OptionalSequence = lgSpecT.defVisitor(nil, {Accu=function(rs,context,t,skip) return string.format('optional(%s)',rs) end}, 'OptionalSequence')
TransVisitor.GroupedSequence = lgSpecT.defVisitor(nil,nil,'GroupedSequence')

TransVisitor.MetaIdentifier = lgSpecT.defVisitor(nil,{Accu=function(rs,context,t,skip) return string.format('V.%s',rs) end},'MetaIdentifier')
TransVisitor.Name = lgSpecT.defValue('Name')
TransVisitor.TerminalString = lgSpecT.defValue('TerminalString','terminal("%s")')
TransVisitor.Empty = lgSpecT.defVisitorConst('empty()','Empty')

--Special handle for RepeatedSequence symbol, it because modification of grammar to eliminate left recursion.
TransVisitor.RSZeroOrMore = lgSpecT.defVisitor({'RepeatedSequenceP'}, {Accu=function(rs,context,t,skip) return string.format('zeroOrMore(%s)',rs) end}, 'RSZeroOrMore')
TransVisitor.RSOneOrMore = lgSpecT.defVisitor({'RepeatedSequenceP'}, {Accu=function(rs,context,t,skip) return string.format('oneOrMore(%s)',rs) end}, 'RSOneOrMore')

function TransVisitor.RSOneOrMoreSep(t)
  --print('TransVisitor.RSOneOrMoreSep')
  local skip = nil
  local patt, sep
  
  for k,v in ipairs(t) do
    --print(k,v.tag)
    if k == 1 then --First value is pattern
      --print('TransVisitor.RSOneOrMoreSep:ExecPattern', v.tag)
      patt = TransVisitor[v.tag](v)
      --print('TransVisitor.RSOneOrMoreSep:ExecPatternRet', v.tag, patt)
    elseif k == 2 then --Second is separator
      sep = v[1]
    --else This is the RepeatedSequenceP tail, for now I throw away this part.
    end
  end
  --print('TransVisitor.RSOneOrMoreSep:', patt, sep)
  return string.format('oneOrMoreSep(%s, "%s")', patt, sep)
end

--Special handle for others symbols.
function TransVisitor.SyntaxRule(t)
  --print('TransVisitor.SyntaxRule')
  local skip = nil
  
  local name
  local dList
  local r
  
  for k,v in ipairs(t) do
    if v.tag == 'Name' then
      name = lgSpecT.callFunc(v.tag, v, skip)
      --print('TransVisitor.SyntaxRule:',name)
    else
      --print('TransVisitor.SyntaxRule', v.tag)
      dList = TransVisitor[v.tag](v)
    end
  end
  
  --print('TransVisitor.SyntaxRule:2:1',name,dList)
  coroutine.yield(string.format('G.%s = %s', name, dList))
end

function TransVisitor.Primary(t)
  --print('TransVisitor.Primary')
  local skip = nil
  local r
  
  for k,v in ipairs(t) do
    --[[
    if v.tag == 'MetaIdentifier' then
      r = 'V.' .. lgSpecT.callFunc(v.tag, v, skip)
    else
      --print('TransVisitor.Primary',v.tag)
    ]]
      r = TransVisitor[v.tag](v)
    --end
  end
  --print('TransVisitor.Primary:Ret',r)
  return r
end

--- Callback ref to local AST Visitor
lgSpecT.setASTVisitor(TransVisitor)

local function visitTransLGenSpec(t, f, startSymbol)
  return lgSpecT.visit(t, f, startSymbol, 'Syntax')
end

function Trans.getVisitor(tp)
  if tp == "trans" then return visitTransLGenSpec end
  return nil
end

--- Debug function
function debugInfo()
  return tabG, skip
end

--- Return interface
return {G, Trans, debugInfo}