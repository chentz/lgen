local Set = require ("Set")
local out
local ASTVisitor

local LGenSpecTrans = {}

function LGenSpecTrans.setASTVisitor(vis)
  ASTVisitor = vis
end

function LGenSpecTrans.outL(s,...)
  if s then
    out(s..'\n',...)
  else
    out('\n')
  end
end

--Alias
local outL = LGenSpecTrans.outL

-- Standart function visit
function LGenSpecTrans.visit(t,f, startSymbolInput, sSymbolAST)
  out = function(s, ...)
    if #{...} == 0 then f:write(s)
    else f:write(string.format(s, ...)) end
  end
  
  --File tamplete
  if startSymbolInput then
    if type(startSymbolInput) == "string" then
      outL([[G.startSymbol = "%s"]], startSymbolInput)
    elseif type(startSymbolInput) == "table" then
      outL(startSymbolInput[1])
    end
  end

  --Start Translate
  if not ASTVisitor[sSymbolAST] then
    print("Invalid sSymbolAST:", sSymbolAST)
    return false
  end
  
  local rulesEnum = coroutine.wrap(function()
        ASTVisitor[sSymbolAST](t)
      end)
      
  for rule in rulesEnum do
    --print('visit:',rule)
    outL(rule)
  end
  outL()
  
  if startSymbolInput and type(startSymbolInput) == "table" then
    outL(startSymbolInput[2])
  end
  return true
end

function LGenSpecTrans.callFunc(f, t, sUseless)
  if not f then
    print('callFunc error. f == nil')
    return nil
  end
  
  local s = Set.new(sUseless)
  if s:inSet(f) then
    return nil
  end
  
  if not ASTVisitor[f] then
    print(string.format("Symbol don't reconize: %s", f))
    return nil
  end
  
  return ASTVisitor[f](t)  
end

local callFunc = LGenSpecTrans.callFunc

function LGenSpecTrans.defStopCall(table, name)
  --if name then print(string.format('defStopCall:Def:%s',name)) end
  return function(t)
    --if name then print(string.format('defStopCall:Exec:%s',name)) end
    return nil
  end
end

function LGenSpecTrans.defValue(name, pattFormat)
  --if name then print(string.format('defValue:Def:%s',name)) end
  return function(t)
    --if name then print(string.format('defValue:Exec:%s(%s)',name,t[1])) end
    if pattFormat then
      return string.format(pattFormat, t[1])
    else
      return t[1]
    end
  end
end

function LGenSpecTrans.defVisitorConst(c, name)
  --if name then print(string.format('defVisitorConst:Def:%s',name)) end
  return function()
    --if name then print(string.format('defVisitorConst:Exec:%s',name)) end
    return c
  end
end

function LGenSpecTrans.defVisitor(skip, fTab, name)
  --if name then print(string.format('defVisitor:Def:%s',name)) end
  return function(t)
    --if name then print(string.format('defVisitor:Exec:%s',name)) end
    local rs
    local context = {}
    local rCF
    
    for k,v in ipairs(t) do
      --print("defVisitor:Exec2:",k,v)
      if type(v) == 'table' then
        rCF = callFunc(v.tag, v, skip)
        if rCF and fTab and fTab.Accu then
          --if name then print(string.format('defVisitor:Exec_Accu:%s',name),v.tag,r,rs,fTab, context) end
          local rTmp = fTab.Accu(rCF, context, v, skip)
          rs = rTmp or rCF
        elseif rCF then
          rs = rCF
        end
      else
        print(string.format('defVisitor:Error parsing %s (%s)',v.tag, v))
      end
    end
    
    --if name then print(string.format('defVisitor:Exec_Result:%s',name),t.tag,rs) end
    if fTab and fTab.resultFunc then
      return fTab.resultFunc(context, t, skip)
    else return rs end
  end
end

function LGenSpecTrans.concat(rs,context, t,skip)
  if rs and not context.retRs then
    context.retRs = {rs}
  elseif rs then
    context.retRs[#context.retRs+1] = rs
  end
end

function LGenSpecTrans.resultAccu(accu, sep, leftGroup, oBracket, cBracket)
  if not oBracket then
    oBracket = '('
    cBracket = ')'
  end
  return function(context,t,skip)
    --print('resultAccu:Exec', context.retRs, t.tag, accu)
    local r
    if context.retRs and #context.retRs == 1 then
      r = context.retRs[1]
    elseif context.retRs and #context.retRs > 2 and leftGroup then      
      local i = 1
      r = ''
      while i < #context.retRs do
        r = r ..accu..oBracket..context.retRs[i]..sep
        i = i+1
      end
      r = string.format('%s%s%s',r,context.retRs[i],string.rep(cBracket,i-1))
    elseif context.retRs and #context.retRs > 1 then
      r = accu..oBracket..context.retRs[1]
      for i=2,#context.retRs do
        r = r .. sep .. context.retRs[i]
      end
      r = r .. cBracket
    else
      return nil
    end
    --print('resultAccu:Exec:Ret', r, t.tag, accu)
    return r
  end
end

function LGenSpecTrans.accuRange(rs,context,t,skip)
  if rs then
    rs = string.format("'%s'",rs)
    if not context.retRs then
      context.retRs = {rs}
    else
      context.retRs[#context.retRs+1] = rs
    end
  end
end

return LGenSpecTrans