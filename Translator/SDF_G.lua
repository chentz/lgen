--[[
This grammar definition is base on Rascal SDF.
-- https://github.com/usethesource/rascal/blob/master/src/org/rascalmpl/library/lang/rascal/syntax/Rascal.rsc
-- https://github.com/usethesource/rascal/blob/master/src/org/rascalmpl/library/lang/sdf2/syntax/Sdf2.rsc
]] 

-- lexical elements
local space = S("\r\n\f\t ")
local comment = P("//") * notPatt(S("\r\n\f"))^0

--local blockComment = P("/*") * notPatt(P("*/"))^0 * P("*/");
local blockComment = P("/*") * (P("'*/'") + 
                                notPatt(P("*/"))
                               )^0 * P("*/")
local skip = (space + comment + blockComment)^0

local letters = R("az", "AZ")
local alpha = letters + R("09")

local Name = taggedCap("Name", C(letters * (alpha + P("_"))^0 + 
                                 P(".") * -P(".") ) * skip )

-- Aux. ANTLR functions
local token = getFuncToken(skip)

local function symb(str,cap)
  if not cap then
    return token(P(str))
  else
    return token(P(str) / cap)
  end
end

local function OMSep(pat, sep) return (pat * (symb(sep) * pat)^0) end

-- Grammar definition

--  nonterminals declarations
local SyntaxDefinition = V("SyntaxDefinition")

--  Grammar Declaration

local G = P { SyntaxDefinition,
  SyntaxDefinition = taggedCap("SyntaxDefinition",
    symb("grammar") * Name * symb(";") * Header * Rule^1
  );
  
  -- Header
  Header = taggedCap("Header", HeaderSection^0);  --@category="Comment"
  HeaderSection = taggedCap("HeaderSection", UselessHeaderSection + TokensHeaderSection);  --@category="Comment"
  UselessHeaderSection = taggedCap("UselessHeaderSection", SectionName * CurlyBlock); --@category="Comment"
  TokensHeaderSection = taggedCap("TokensHeaderSection", symb("tokens") * symb("{") * TokenDefinition^1 * symb("}"));  --@category="Comment"
  TokenDefinition = taggedCap("TokenDefinition", 
                    RealTokenDefinition +
                    FakeTokenDefinition +
                    UselessTokenDefinition); --@category="Comment"
  UselessTokenDefinition = taggedCap("UselessTokenDefinition", Name * symb(";")); --@category="Comment"                        
  RealTokenDefinition = taggedCap("RealTokenDefinition", Name * symb("=") * QuotedBlock * symb(";")); --@category="Comment"
  FakeTokenDefinition = taggedCap("FakeTokenDefinition", Name * symb(":") * QuotedBlock * symb(";") * Finally^-1); --@category="Comment"
  Finally = taggedCap("Finally", symb("finally") * CurlyBlock); --@category="Comment"
  SectionName = taggedCap("SectionName",
                symb("options","%0") +
                symb("@header","%0") +
                symb("@members","%0") +
                symb("@rulecatch","%0") +
                symb("@lexer::header","%0") +
                symb("@lexer::members","%0") +
                symb("@parser::header","%0") + 
                symb("@parser::members","%0")); --@category="Comment"
                
                

  -- Rules
  Rule = taggedCap("Rule", symb("fragment")^-1 * Name * Arguments^-1 * RuleHeader^0 * symb(":") * OMSep(Alternative, "|") * symb(";"));
  
  Arguments = taggedCap("Arguments", symb("returns")^-1 * SquareBlock); -- @category="Comment"
  RuleHeader = taggedCap("RuleHeader", 
               Options +
               symb("scope") * CurlyBlock +
               symb("@after") * CurlyBlock +
               symb("@init") * CurlyBlock); -- @category="Comment"
  Options = taggedCap("Options", symb("options") * CurlyBlock); -- @category="Comment"
  
  Alternative = taggedCap("Alternative", AltStart^-1 * Symbol^0 * AltTail^-1);
  AltStart = taggedCap("AltStart", ParenthesisBlock * symb("=>") +
             CurlyBlock);  -- @category="Comment"
      
  -- Change for elimination of left recursion           
  Symbol = taggedCap("Symbol",
           (
           SymbolGroup +
           SymbolPrefix + --This pattern would appears before the next.Treatment of umbiguity
           SymbolName +
           SymbolRange + --@category="Variable" --This pattern would appears before the next.Treatment of umbiguity
           SymbolQuoted --[[ + --Disable
           SymbolEmpty]]
           ) * Modifier^-1 -- @category="Variable"
           );
           
  SymbolGroup = taggedCap("SymbolGroup", symb("~","%0")^-1 * symb("(") *  OMSep(Alternative, "|") * symb(")"));
  SymbolPrefix = taggedCap("SymbolPrefix", Prefix * Symbol);
  SymbolName = taggedCap("SymbolName", Name);
  SymbolRange = taggedCap("SymbolRange", QuotedBlock * symb("..") * QuotedBlock);
  SymbolQuoted = taggedCap("SymbolQuoted", QuotedBlock);
  SymbolEmpty = taggedCap("SymbolEmpty", skip);
           
  Modifier = taggedCap("Modifier",
             symb("*","%0") +
             symb("?","%0") +
             symb("+","%0") + 
             UselessModifier); -- @category="Comment"
             
  UselessModifier = taggedCap("UselessModifier",
                    symb("!") +
                    symb("^") + 
                    --CurlyBlock +
                    Arguments); --@category="Comment"
  Prefix = taggedCap("Prefix", 
           UselessPrefix +
           Name * SelectorSymbol); -- @category="Comment"
  SelectorSymbol = taggedCap("SelectorSymbol", symb("=","%0") + symb("+=","%0")); --@category="Comment"
  UselessPrefix = taggedCap("UselessPrefix", OptionsSemicolon); --@category="Comment"  
  OptionsSemicolon = taggedCap("OptionsSemicolon", Options * symb(":")); -- @category="Comment"  
  Options = taggedCap("Options", symb("options") * CurlyBlock); -- @category="Comment"
           
  AltTail = taggedCap("AltTail",
            Rewrite * OptionalCurlyBlock^-1 +
            OptionalCurlyBlock * Rewrite^-1 +
            Rewrite * Rewrite); --@category="Comment"
            
  Rewrite = taggedCap("Rewrite", symb("->") * OptionalCurlyBlock^-1 * RewrittenTree); -- @category="Comment"
  RewrittenTree = taggedCap("RewrittenTree", symb("^") * ParenthesisBlock * Modifier^-1 * Symbol^0 + 
                  Symbol^1); --@category="Comment"
  OptionalCurlyBlock = taggedCap("OptionalCurlyBlock", CurlyBlock * symb("?")^-1); --@category="Comment"
            
  -- Aux
  SquareBlock = taggedCap("SquareBlock", symb("[") * SquareBlockElement^0 * symb("]")); --@category="Comment"
  SquareBlockElement = notPatt(S("[]")) + 
                       SquareBlock; --@category="Comment"
  
  CurlyBlock  = taggedCap("CurlyBlock", symb("{") * Cg(CurlyBlockElement^0) * symb("}")); --@category="Comment"
  CurlyBlockElement  = notPatt(S("{}")) + 
                       CurlyBlock; --@category="Comment"
  
  QuotedBlock  = taggedCap("QuotedBlock", P("'") * Cg(QuotedBlockSymbol^0) * symb("'"));
  QuotedBlockSymbol = notPatt(S("'\\")) + 
                      P("\\") * S([[rtnfu?'"\\]]) + 
                      S("\\.");
  
  ParenthesisBlock = taggedCap("ParenthesisBlock", symb("(") * Cg(ParenthesisBlockElement^0) * symb(")")); --@category="Comment"
  ParenthesisBlockElement = notPatt(S("()")) +
                            ParenthesisBlock; --@category="Comment"
}

G = skip * G * -1

-- VVV Start transformation implementation VVVV -----

--- Translator interface
local Trans = {}

--- Local visitor to translate ANTLR->LGenSpec Notation
local TransVisitor = {}

--- Import standard library for LGen Specification Transformation
local lgSpecT = require("LGenSpecTrans");

--- Config TransVisitor table for default action
local function defIndex(defFunc)
  return function(table, key)
    local h
    local v = rawget(table, key)
    
    if v ~= nil then return v end
    return defFunc(table, key)
  end
end

TransVisitor = setmetatable(TransVisitor, {
      __index = defIndex(lgSpecT.defStopCall)
    })

--- Plug the standard actions
TransVisitor.Grammar = lgSpecT.defVisitor({'Name','Header'},nil,'Grammar')
TransVisitor.Alternative = lgSpecT.defVisitor({'AltStart','AltTail'},{Accu=lgSpecT.concat; resultFunc=lgSpecT.resultAccu('seq',',',true)},'Alternative')
TransVisitor.SymbolPrefix =  lgSpecT.defVisitor({'Prefix'},nil,'SymbolPrefix')
TransVisitor.SymbolName =  lgSpecT.defVisitor(nil,{Accu=function(rs,context, t,skip) return 'V.'..rs end},'SymbolName')
TransVisitor.SymbolRange = lgSpecT.defVisitor(nil,{Accu=lgSpecT.accuRange; resultFunc=lgSpecT.resultAccu('range',',',true)},'SymbolRange')
TransVisitor.SymbolQuoted = lgSpecT.defVisitor(nil,{Accu=function(rs,context, t,skip) return string.format("terminal('%s')",rs) end},'SymbolQuoted')
TransVisitor.SymbolEmpty = lgSpecT.defVisitorConst('empty()','SymbolEmpty')

--- Define special actions to grammar's symbols
function TransVisitor.Rule(t)
  --print('TransVisitor.Rule')
  local skip = {'Arguments','RuleHeader'}
  
  local name
  local alt, alts
  local manyAlts = false
  local r
  
  for k,v in ipairs(t) do
    if v.tag == 'Name' then
      name = lgSpecT.callFunc(v.tag, v, skip)
      --print('TransVisitor.Rule:',name)
    elseif v.tag == 'Alternative' then
      alt = lgSpecT.callFunc(v.tag, v, skip)
      if alts then
        alts = alts..','..alt
        manyAlts = true
      else alts = alt end
    else
      lgSpecT.callFunc(v.tag, v, skip)
    end
  end
  
  if manyAlts then 
    alts = 'alt('..alts..')'
  end
  --print('TransVisitor.Rule:2:',name,alts)
  coroutine.yield(string.format('G.%s = %s', name, alts))
end

function TransVisitor.Symbol(t)
  local skip = nil
  local r
  --print('TransVisitor.Symbol')
  
  local modLua
  for k,v in ipairs(t) do
    if v.tag == 'Modifier' then
      if v[1] == '*' then
        modLua = 'zeroOrMore'
      elseif v[1] == '?' then
        modLua = 'optional'
      elseif v[1] == '+' then
        modLua = 'oneOrMore'
      end      
    else
      r = lgSpecT.callFunc(v.tag, v, skip)
    end
  end
  
  if modLua then r = string.format('%s(%s)',modLua, r) end 
  return r
end

function TransVisitor.SymbolGroup(t)
  --print('TransVisitor.SymbolGroup')
  local rs
  local context = {}
  local modLua
  
  for k,v in ipairs(t) do
    if type(v) == 'table' then
      rs = lgSpecT.callFunc(v.tag, v, skip)
      --print(v.tag,r,rs, fTab, context)
      
      if rs and not context.retRs then
        context.retRs = {rs}
      elseif rs then
        context.retRs[#context.retRs+1] = rs
      end
    elseif v == '~' then
      modLua = 'not'
    else
      print(string.format('defVisitor:Error parsing %s',v))
    end
  end
    
  local r
  local accu,sep = 'alt',','
  if context.retRs and #context.retRs == 1 then
    r = context.retRs[1]
  elseif context.retRs and #context.retRs > 1 then
    r = accu..'('..context.retRs[1]
    for i=2,#context.retRs do
      r = r .. sep .. context.retRs[i]
    end
    r = r .. ')'
  end
  
  if modLua then r = string.format('%s(%s)', modLua, r) end
  
  return r
end

function TransVisitor.Name(t)
  return t[1]
end

function TransVisitor.QuotedBlock(t)
  return t[1]
end

--- Callback ref to local AST Visitor
lgSpecT.setASTVisitor(TransVisitor)

local function visitTransLGenSpec(t, f, startSymbol)
  return lgSpecT.visit(t, f, startSymbol, 'Grammar')
end

function Trans.getVisitor(tp)
  if tp == "trans" then return visitTransLGenSpec end
  return nil
end

--- Return interface
return {G, Trans}