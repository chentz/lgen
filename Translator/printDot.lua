local out
local dotPrefix = "N"
local lastNode = 0
local muteTags = {"CurlyBlock"}

local function outL(s,...)
  if s then
    out(s..'\n',...)
  else
    out('\n',...)
  end
end

local function getNextNode() 
  lastNode = lastNode + 1
  return lastNode
end

local function newNName(n)
  local n = n or getNextNode()
  return dotPrefix..'_'..n
end

-- AST Parse Functions
local ASTVisitor = {}

local function defIndex(defFunc)
  return function(table, key)
    local h
    local v = rawget(table, key)
    
    if v ~= nil then return v end
    return defFunc(table, key)
  end
end

local function defNullFunc(t)
  return newNName()
end

local function defEmpty(t)
  return nil
end

local function defStopNode(t)
  local _name = newNName()
  outL(' %s [label="%s",fontcolor=red];',_name, t.tag)
  return _name
end

local function defCall(t)
  if t.tag then
    local _name = newNName()
    print(string.format('defCall:%s',t.tag))
    outL(' %s [label="%s"];',_name, t.tag)
  
    for k,v in ipairs(t) do    
      if ASTVisitor[v.tag] then
        local rs = ASTVisitor[v.tag](v)
        if rs then outL(' %s -> %s;', _name, rs) end
      elseif type(v) == 'string' then
        local rs = ASTVisitor['Value'](v)
        if rs then outL(' %s -> %s;', _name, rs) end
      elseif type(v) == 'table' then
        print(string.format("Symbol don't reconize:%s(%s) in %s", tostring(v), v.tag, t.tag))
      else
        print(string.format("Symbol don't reconize: %s,%s in %s", k,v, t.tag))
      end
    end
    
    return _name
  end
end

local function defFuncName(t)
  local _name = newNName()
  outL(' %s [label="%s"];',_name, t.tag)
  outL(' %s -> %s;', _name, ASTVisitor.Value(t[1]))
  return _name
end

ASTVisitor = setmetatable(ASTVisitor, {
      __index = defIndex(function(table, key) return defCall end)
    })

function ASTVisitor.Value(t)
  local nn = getNextNode()
  local _name = newNName(nn)
  if #t > 20 then t = string.sub(t,1,17)..'...' end
  outL(' %s [label="%s",shape=box,fontcolor=blue];', _name, t)
  return _name
end

--[[
ASTVisitor.Grammar = defCall
ASTVisitor.Header = defCall
ASTVisitor.HeaderSection = defCall
ASTVisitor.UselessHeaderSection = defCall
ASTVisitor.TokensHeaderSection = defCall
ASTVisitor.TokenDefinition = defCall
ASTVisitor.RealTokenDefinition = defCall
ASTVisitor.FakeTokenDefinition = defCall
ASTVisitor.UselessTokenDefinition = defCall
ASTVisitor.Finally = defCall

ASTVisitor.Rule = defCall
ASTVisitor.Arguments = defCall
ASTVisitor.RuleHeader = defCall
ASTVisitor.Options = defCall
ASTVisitor.Alternative = defCall
ASTVisitor.AltStart = defCall
ASTVisitor.AltTail = defCall

ASTVisitor.Symbol = defCall
ASTVisitor.SymbolGroup = defCall
ASTVisitor.SymbolPrefix = defCall
ASTVisitor.SymbolName = defCall
ASTVisitor.SymbolRange = defCall
ASTVisitor.SymbolQuoted = defCall
ASTVisitor.Prefix = defCall
]]

--Name likes
ASTVisitor.Name = defFuncName
ASTVisitor.SectionName = defFuncName
ASTVisitor.Modifier = defFuncName
ASTVisitor.ParenthesisBlock = defFuncName
ASTVisitor.CurlyBlock = defFuncName
ASTVisitor.SquareBlock = defFuncName
ASTVisitor.QuotedBlock = defFuncName
ASTVisitor.SelectorSymbol = defFuncName

local function printNode(t)
  return ASTVisitor.Grammar(t);
end
  
local function genDotAST(t, o)
  out = function(s, ...) o:write(string.format(s, ...)) end
  --File tamplete
  outL('digraph G {')
  --outL(' node [shape=record];')
  local r = printNode(t) ~= nil
  out('}')
  return r
end

local dp = {
  print_ast = genDotAST
}
return dp