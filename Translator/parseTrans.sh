#!/bin/bash
# $1 => .ebnf file name
# $2 => StartSymbol 
# $3 => output file name (OPT)
# $4 => Debug flag (OPT)

g_file_name=$1
g_base_name="${g_file_name%.*}"
g_ext="${g_file_name##*.}"
tmp_file=$g_file_name.lua

case $g_ext in
ebnf)
	pMod="EBNF_G"
	;;
g)
  pMod="ANTLR_G"
	;;
esac

echo "Extension $g_ext dectected. Using $pMod module."

if [ -z "$pMod" ]; then
  echo "Error loading module."
  exit 1
fi

if [ -z "$2" ]; then
  startSymbol=$1
else
  startSymbol=$2
fi

if [ -z "$3" ]; then
  output_file=$g_base_name.lua
else
  output_file=$3
fi

debugFlag=$4

#Verify charactes
echo "--- Check $g_file_name file... ---"
grep --color='auto' -P -n '[\x80-\xFF]' $g_file_name #Special characteres
grep --color='auto' -P -n '[^\x5C]\x5C[^\x5C]' $g_file_name #Backslash
echo "--- $g_file_name file checked. ---"
 
parser_cmd="lua parser.lua $pMod $g_file_name trans $startSymbol $debugFlag"
print_cmd="tail -n20 $output_file"

echo "--- Start Parser $g_file_name file... ---"
#echo "$parser_cmd"
$parser_cmd
#echo $?

if [[ $? == 0 ]]; then
  echo "--- $g_file_name file Parsed! ---"
  mv $tmp_file $output_file
  echo "..."
  $print_cmd
  echo "--- $output_file file generated! ---"
else
  echo "--- $g_file_name file with erros! ---"
  exit 1
fi