require("init")
local lpeg = require("lpeg")
local pd = require("pegdebug")
local util = require("Util")

--print("Modules load ok.")

--LPeg basic functions
P, S, V, R = lpeg.P, lpeg.S, lpeg.V, lpeg.R
C, Cg, Ct, Cc, Cmt = lpeg.C, lpeg.Cg, lpeg.Ct, lpeg.Cc, lpeg.Cmt

-- Aux. functions
function taggedCap(tag, pat)
  return Ct(Cg(Cc(tag), "tag") * pat)
end

function getFuncToken(sk)
  return function(pat)
    return pat * sk
  end
end

function notPatt(pat) return (1-pat) end

-- Parser functions
function printAST(t,level)
  local l = level or 1
  local lSep = string.rep("-",l)
  
  print(string.format("%s Tag: %s",lSep,t.tag))
  for k,v in pairs(t) do
    if (k ~= "tag") and (type(v) ~= "table") then
      print(string.format("%s Token: %s",lSep,v))
    elseif (k ~= "tag") then
      printAST(v, l+1)
    end
  end
end

function testLPEG(Grm, str, tName, expectedR, verbose) 
  local verbose = verbose~=nil and verbose or false
  local expectedR = expectedR==nil and true or expectedR
  
  print("=========","Start test", tName,"=========")
  print('Print AST for sentence:')
  print("======Start======")
  print(str)
  print("======End========")
  print()
  
  local t = lpeg.match(Grm, str)
  local success = t ~= nil
  
  if not success then
    print("Parser error.")
  elseif verbose then
    printAST(t)
  end
  
  local s
  if success == expectedR then
    s = "Status: Successes"
  else
    s = "Status: Fail"
  end
  
  print("=========","End test", tName,s ,"=========")
  print()
  return success
end

local function getcontents(filename)
  file = io.open(filename, "r")
  contents = file:read("*a")
  file:close()
  return contents
end

--[[
These are valid args:
arg[1] -> Grammar File used to parse.
arg[2] -> Lang. input file.
arg[3] -> Config what to do with AST. 
Valid values are: 
text -> only print AST.
trans(default) -> Translate AST to LGen Lua notation.
dot -> Translate AST to dot notation for graph vizualization.
arg[4] -> Only in trans type. StartSymbol of input file.
]]
if #arg < 2 then
  print ("Usage: lua parser.lua <grammar def file .lua> <Input file of the Language> <ASTAction:text(def), dot, trans> (in trans: StartSymbol)")
  return 1
end

local GFile = arg[1]
local inputFName = arg[2]
local typeAct = "text"

if #arg > 2 then typeAct = arg[3] end

local contents = getcontents(inputFName)

local tmpRet = require(GFile) --Fix for return two values in require
local G, TA, debFunc = tmpRet[1], tmpRet[2], tmpRet[3]

if not G then 
  print("Input notation didn't define.")
  os.exit (1)
end

if not TA then 
  print("Input actions didn't define.")
  os.exit (1)
end

local t
if #arg > 4 and debFunc then 
  local tabG, skip = debFunc()
  local dOut = {}
  local debugG = pd.trace(tabG, {out=dOut,
    ['+']=true, --Match
    ['/']=false  --Captures
  })
  t = lpeg.match(skip * lpeg.P(debugG) * -1, contents)
  if t then
    print("Parsed OK!")
    os.exit(1)
  else
    util.tableFilter(dOut, function(k,v,idx)
      if idx > (#dOut-10) then
        print(util.truncString(v, 60, 80))
      end
    end, false)
    print("Parse errors!")
    os.exit(1)
  end
else
  t = lpeg.match(G, contents)
end

if not t then
  print ()
  print ("Parse error: syntax error")
  os.exit (1)
end

if (typeAct == "text") then
  printAST(t)
elseif (typeAct == "trans") then
  local f = io.open(inputFName..'.lua','w')
  local TTransV = TA.getVisitor("trans")
  TTransV(t, f, arg[4])
  f:close()
elseif (typeAct == "dot") then
  local f = io.open(inputFName..'.dot','w')
  local pd = require('printDot')
  pd.print_ast(t, f)
  f:close()
elseif (typeAct == "gdg") then
  local f = io.open(inputFName..'.dot','w')
  local TTransV = require('printDotGDGraph')
  TTransV(t, f, arg[4])
  f:close()
else
  print ("Invalid action to AST.")
  os.exit (1)
end
os.exit (0)