#!/bin/sh
# $1 => .ebnf file name
# $2 => Start symbol
# $3 => output file name
lspec_file_name=$1
base_name="${lspec_file_name%.*}"

parser_cmd="lua buildProdGraph.lua $lspec_file_name stats"

echo "--- Start Parser $lspec_file_name file... ---"
$parser_cmd
echo "--- $lspec_file_name file Parsed! ---"