#!/bin/sh
# $1 => .ebnf file name
# $2 => Start symbol
# $3 => output file name
lspec_file_name=$1
base_name="${lspec_file_name%.*}"

parser_cmd="lua buildProdGraph.lua $lspec_file_name gdg"

base_name+=_gdg
tmp_file=$base_name.dot
output_file=$base_name.png
dot_cmd="dot -Tpng -o $output_file $tmp_file"

echo "--- Start Parser $lspec_file_name file... ---"
$parser_cmd
echo "--- $lspec_file_name file Parsed! ---"
$dot_cmd
echo "--- $output_file file generated! ---"