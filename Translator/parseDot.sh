#!/bin/sh
# $1 => .ebnf file name
# $2 => output file name
g_file_name=$1
g_base_name="${g_file_name%.*}"
tmp_file=$g_file_name.dot

if [ -z "$2" ]; then
  output_file=$g_base_name.png
else
  output_file=$2
fi

parser_cmd="lua parser.lua EBNF_G $g_file_name dot"
print_cmd="cat $tmp_file"
dot_cmd="dot -Tpng -o $output_file $tmp_file"

echo "--- Start Parser $g_file_name file... ---"
$parser_cmd
echo "--- $g_file_name file Parsed! ---"
$print_cmd
$dot_cmd
echo "--- $output_file file generated! ---"