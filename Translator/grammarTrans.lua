--- module contains the transformations over the grammar.
-- @script grammarTrans
-- @usage
-- arg[1] -> Lua Grammar File
-- arg[2] -> Action.
-- arg[3] -> Action param.
-- @author Cleverton Hentz
--
-- Dependencies: `Set`, `Relation`, `MetaInfo`, `Util`

require("init")

local Set = require("Set")
local Rel = require("Relation")
local MI = require("MetaInfo")
local Util = require("Util")

local MD = require('openssl').digest
local md5 = function (m) return MD.digest("md5",m) end

local cOper = MI.cOper

G = setmetatable({}, {
      __newindex = function (t, k, v)
        if k ~= "startSymbol" then
          local r = prodDef(t, k, v)
          if r then v = r end
        end
        
        return rawset(t,k,v)
      end
    })

V = setmetatable({}, {
      __index = function (t, k) return non_terminal(k) end
    })

T = setmetatable({}, {
      __index = function (t, k) return terminal(k) end
    })
   
--Binds to each construction
function prodDef(table, key, value)
  return nil
end

function idInc(c,p)
  return MI.new(nil,cOper.idInc,nil,c,p)
end

function terminal(s)
  return MI.new(nil,cOper.term,nil,s)
end

function range(b,e)
  return MI.new(nil,cOper.range,nil,b,e)
end

function empty()
  return MI.new(nil,cOper.emp)
end

function zeroOrMore(patt)
  return MI.new(nil,cOper.zm,nil,patt)
end

function optional(patt)
  return MI.new(nil,cOper.opt,nil,patt)
end

function oneOrMore(patt)
  return MI.new(nil,cOper.om,nil,patt)
end

function oneOrMoreSep(patt, str)
  return MI.new(nil,cOper.oms,nil,patt,str)
end

function seq(patt1, patt2)
  return MI.new(nil,cOper.seq,nil,patt1,patt2)
end

function alt(...)
  return MI.new(nil,cOper.alt,nil,...)
end

function negation(patt)
  return MI.new(nil,cOper.neg,nil,patt)
end

function non_terminal(NT)
  return MI.new(nil,cOper.nt,nil,NT)
end

--- simplify the grammar based on a start symbol.
function startSSimplification(g, startSym)
  local gKSort = Util.getOrderIndex(g);
  local newG, rPId, rSym = MI.extractStructures(g,"r", gKSort)
  local SymGraph = rPId..rSym
  local sSym = Set.new()
  SymGraph:BFS(startSym, function(elem, index)
    --print(index, elem)
    sSym(elem)
    return true
  end)
  
  local t=""
  local eachRuleFun = function(sym, patt)
    -- print(sym, patt)
    if sSym:inSet(sym) then
      t = t .. string.format("G.%s = %s\n", sym, patt:tostring()) 
    end
    return patt
  end
  
  newG = MI.mapEachRule(Util.copy(g), eachRuleFun, gKSort)
  
  return string.format([[G.startSymbol = "%s"

%s
]], startSym, t)
end

function prodHash(g)
  local gKSort = Util.getOrderIndex(g);
  local newG, rPId, rSym = MI.extractStructures(g,"r", gKSort)
  local SymGraph = rPId..rSym
  local sProdName = rPId:domain()
  local pVal, temps
  for v in sProdName:getSortIterator() do
    temps = SymGraph%v
    if temps then
      pVal = '["'
      for v2 in (SymGraph%v):getSortIterator() do
        pVal = pVal .. v2 .. '","'
      end
      pVal = pVal:sub(1,pVal:len()-2)
      pVal = pVal .. ']'
    else pVal = "[]" end
    
    print(string.format("%s : %s : %s",v, md5(pVal):sub(1,7), pVal))
  end
end


local file = tostring(arg[1])
if #arg > 1 then typeAct = arg[2]
else typeAct = "sss" end
local actParam = arg[3]

dofile(file)

local bName = string.match(file, "(.+)%.[^%.]+$")
print("Base file name:", bName)
local outFName = bName..string.format('_%s.dot', typeAct)
print("Output file name:", outFName)

if typeAct == "sss" then
  local f = io.open(outFName,'w')
  f:write(startSSimplification(G, actParam))
  f:close()
elseif typeAct == "prodHash" then
  prodHash(G)
end  