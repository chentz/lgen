--[[
Generate Grammar Dependency Graph for a grammar.
By Cleverton Hentz
]]
local function out(s, ...)
  if #{...} == 0 then coroutine.yield(s)
  else coroutine.yield(string.format(s, ...)) end
end

local function outL(s, ...)
  if s then
    out(s..'\n',...)
  else
    out('\n')
  end
end

local dotPrefix = "N"
local lastNode = 0
local startSymbol

--Aux. functions
local function getNextNode() 
  lastNode = lastNode + 1
  return lastNode
end

local function newNName(n)
  local n = n or getNextNode()
  return dotPrefix..n
end

local GNodes = {}

local function node(nName)
  if not nName then return nil end
  
  local iName = GNodes[nName];
  if not iName then
    iName = newNName()
    GNodes[nName] = iName
    if startSymbol == nName then
      out(' %s [label="%s",fontcolor=red];',iName, nName)
    else
      out(' %s [label="%s"];',iName, nName)
    end
  end
  
  return iName
end

-- AST Parse Functions
local lgSpecT = require("LGenSpecTrans");

local ASTVisitor = {}

--Default behavior
local function defIndex(defFunc)
  return function(table, key)
    local h
    local v = rawget(table, key)
    
    if v ~= nil then return v end
    return defFunc(table, key)
  end
end

ASTVisitor = setmetatable(ASTVisitor, {
  __index = defIndex(function(table, key) return lgSpecT.defVisitor(nil,nil,key) end)
})

--END Default behavior

ASTVisitor.Syntax = lgSpecT.defVisitor(nil,nil,'Syntax')
ASTVisitor.DefinitionsList = lgSpecT.defVisitor(nil, {Accu=lgSpecT.concat; resultFunc=lgSpecT.resultAccu('',';',false,'','')}, 'DefinitionsList')
ASTVisitor.SingleDefinition = lgSpecT.defVisitor(nil, {Accu=lgSpecT.concat; resultFunc=lgSpecT.resultAccu('',';',false, '','')}, 'SingleDefinition')

--Special handle for others symbols.
function ASTVisitor.SyntaxRule(t)
  print('ASTVisitor.SyntaxRule')
  local skip = nil
  
  local name
  local dList
  local r
  
  for k,v in ipairs(t) do
    if v.tag == 'Name' then
      name = lgSpecT.callFunc(v.tag, v, skip)
      --print('ASTVisitor.SyntaxRule:',name)
    else
      --print('ASTVisitor.SyntaxRule', v.tag)
      dList = ASTVisitor[v.tag](v)
    end
  end
  
  --print('ASTVisitor.SyntaxRule:2:1',name,dList)
  if dList then out(' %s -> {%s};', name, dList) end
end

function ASTVisitor.TerminalString(t)
  return nil
end

function ASTVisitor.Name(t)
  print("ASTVisitor.Name", t)
  local value = t[1]
  if #value > 23 then value = string.sub(value,1,20)..'_' end
  
  return node(value)
end

--coroutine.yield(string.format('G.%s = %s', name, dList))

--- Callback ref to local AST Visitor
lgSpecT.setASTVisitor(ASTVisitor)

local function visitTransLGenSpec(t, f, S)
  --File tamplete
  startSymbol = S
  return lgSpecT.visit(t, f, {'digraph G {','}'}, 'Syntax')
end

return visitTransLGenSpec