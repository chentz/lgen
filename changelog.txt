Changelog 1.3.4 - 27/09/2019
--------------------------------------
- The release structure was changed. Now each step is inside the following folders:
  Translation inside "Trans" and Generation Engine inside "GenEngine". Each of these
  folders have its own "bin" folder with the script to run each step.
Scripts
- Change from "sh" to "bash" to run the translator script parseTrans.sh

Changelog 1.3.3 - 11/09/2019
--------------------------------------
GenEngine
- Fixed bug related to the use of MetaInfo module
Scripts
- Improve the Lua executable detection on profileLua.sh
- Add an option to define the Lua executable on profileLua.sh

Changelog 1.3.2 - 24/02/2017
--------------------------------------
GenEngine
- Add an option to use alternatives into alt instruction randomly during the generation
- Add an API to use the generation engine from another Lua program
- Add the information about the highest derivation tree in the generated sentence set
- Fixed a bug related to the maxCycles when is it grater than 2
- Bugs fixed: #47, #50 and #55
Translator
- Added a grammar definition to Rascal (adapted from Rascal github repository.)
- Added a tool to calculate hash code by production from a grammar

Changelog 1.3.1 - 26/08/2016
--------------------------------------
GenEngine
- Add quite option to execute LGen. Resolves #32
- Add support to new optimisations algorithms to test set of LGen. Resolves #36
- Add support to transformation to test set of LGen. Resolves #42
- Minor fixes

Changelog 1.3.0 - 26/01/2015
--------------------------------------
GenEngine
- Optimizations to cut the search over invalid paths. Memoize and use of minimun height for derivation tree.
- Support to transformation over input grammar.
- Add support to CDRC criterion. issue #31
- A new algorithm to calculate the minimum tree height per rule. issue #34
- Use the minimun tree height to optimize the generation process. issue #35
- Flush to file the sentences to test set. For each 5 sentences generated these are flushed. issue #30
- Add detailed debug information about execution times per operators.  
Translator
- Generate graph of symbol dependence and rule dependence. issue #33
- Generate a report about the input grammar. With info about: disconnected symbols,
  min. derivation tree height, symbol and grammar complexity, minimun derivation tree height to cover production coverage,
  and detection of cycles.  

Changelog 1.2.2 - 08/12/2014
--------------------------------------
GenEngine
- Improvement over generation process by use of memoization over non_terminal function. 
- Add Context-Dependent coverage criterion on LGen (Implemented by Victor Pereira Ferreira). issue #31
- Add initial support to a grammar transformation process before the generation process.
- Partial flush of output files during the generation process. issue #30
Translator
- Added a grammar definition to Java 1.5 (from Terence Parr's ANTLR definition)

Changelog 1.2.1 - 06/06/2014
--------------------------------------
GenEngine
- Added the function idInc to generate lexical symbols. Examples of usage of this new function in grammars "b0_grammar" and "b_grammar_small" lua files.
- Treatment to special characters when print set of not covered terminals in terminalCoverage
- Fix the text message "filePrefix" in RunGrammar.
Translator
- Added a new grammar for B0 specification language.
Changelog 1.2.0 - 25/11/2013
--------------------------------------
GenEngine
- Fix the problem with derivation count. issue #16
- Rewrite the code for EBNF operators (oneOrMore, oneOrMoreSep, zeroOrMore, optional). issues #10, #12
- Reduce verbose of LGen and revise the messages. issue #5
- Update the internal test set. issue #25
Translator
- Fix the problem with translation of EBNF operators. issue #26

Changelog 1.1.3 - 05/11/2013
--------------------------------------
GenEngine
- Implement test structure for Generation Engine. (issues #14 and #15)
- Fixed a bug in coverage criteria framework that inform wrong value to lhs nonterminal of a production. (issue #24)
Translator
- Reduce verbose in Translator.

Changelog 1.1.2 - 04/10/2013
--------------------------------------
GenEngine
- Fixed a bug when use production coverage criteria with internal Alts in input grammar. (issue #11)

Changelog 1.1.1 - 28/09/2013
--------------------------------------
GenEngine
- Fixed a problem with grammar integrity check for coverage criteria. (issue #9)
- Fixed a bug when the grammar use internal alt, and for terminal coverage with this kind of grammar. (issue #11)
- Fixed a bug when coverage criteria framework handle with alt, and add a param for number of an alt. (issue #13)
Translator
- Included grammars for B language and Lua language.

Changelog 1.1.0 - 03/09/2013
--------------------------------------
GenEngine
- Changed internal structure of coverage criteria for easy new implementations. (issue #2)
- Fixed the problem in implementation for verify input grammars. (issue #1)
Translator
- Implement EBNF notation for Lua translator.

Changelog 1.0.0 - 25/07/2013
--------------------------------------
- The first version for use of LGen.