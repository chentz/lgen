# README #

### LGen Overview
The Lua Language Generator (LGen) is a sentence (test data) generator based on syntax description and which uses coverage criteria to restrict the set of generated sentences. This generator takes as input a grammar described in a notation based on Extended BNF (EBNF) and returns a set of sentences of the language corresponding to this grammar.

More information on http://lgen.wikidot.com/

### Project dependencies
* Project uses many modules from LuaTools https://github.com/chentz78/LuaTools
* Project documentation using LDoc https://github.com/stevedonovan/LDoc